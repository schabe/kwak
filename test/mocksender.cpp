#include "mocksender.h"

MockSender::~MockSender()
{
    free(this->lastSubApproved);
    free(this->lastSubRequest);
    free(this->lastSubRejected);
}
