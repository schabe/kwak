#ifndef KWAK_TEST_ACCOUNT_H
#define KWAK_TEST_ACCOUNT_H

#include <QObject>

#include "account.h"


class KwakAccountTest: public QObject
{
    Q_OBJECT
public:
    KwakAccountTest(void);
    ~KwakAccountTest(void);
public slots:
    void statusChanged(void);
private slots:
    void testSetAndGet();
    void testUpdateJid();
    void testSaveAndLoad();
    void testSaveAndLoadWithResourcePart();
    void testSetProperties();
    void testSetStatus();
    void testSetStatusEmitsSignal();
private:
    QString lastStatus;
    KwakAccount acc;
};

#endif
