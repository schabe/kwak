#include <QtTest/QtTest>

#include "account.h"
#include "xmpp/definitions.h"

#include "test_account.h"


KwakAccountTest::KwakAccountTest()
    : lastStatus(STATUS_UNKNOWN)
{
}

KwakAccountTest::~KwakAccountTest()
{
}

void KwakAccountTest::statusChanged() {
    lastStatus = acc.getStatus();
}

void KwakAccountTest::testSetAndGet() {
    acc.setJid("me@example.org");
    acc.setPassword("s3kr1t");
    QCOMPARE(acc.getJid(), QString("me@example.org"));
    QCOMPARE(acc.getPassword(), QString("s3kr1t"));
}

void KwakAccountTest::testUpdateJid() {
    acc.setJid("me@example.org");
    acc.setPassword("s3kr1t");

    QCOMPARE(acc.getResourcePart(), QString(""));

    Jid jid("me@example.org/bar");
    acc.update(jid);
    QCOMPARE(acc.getResourcePart(), QString("bar"));

    jid = Jid("me@example.org/kwak.asd");
    acc.update(jid);
    QCOMPARE(acc.getResourcePart(), QString("kwak.asd"));
}

void KwakAccountTest::testSetProperties() {
    acc.setProperty("jid", "somebody@example.org");
    acc.setProperty("password", "123456");

    QCOMPARE(acc.getJid(), QString("somebody@example.org"));
    QCOMPARE(acc.getPassword(), QString("123456"));
}

void KwakAccountTest::testSaveAndLoad() {
    acc.setJid("you@example.org");
    acc.setPassword("geh31m");
    acc.save();

    KwakAccount acc2;
    acc2.load();
    QCOMPARE(acc2.getJid(), QString("you@example.org"));
    QCOMPARE(acc2.getPassword(), QString("geh31m"));
}

void KwakAccountTest::testSaveAndLoadWithResourcePart() {
    acc.setJid("you@example.org");
    acc.setPassword("geh31m");

    Jid jid("you@example.org/foo");
    acc.update(jid);
    acc.save();

    KwakAccount acc2;
    acc2.load();
    QCOMPARE(acc2.getJid(), QString("you@example.org"));
    QCOMPARE(acc2.getPassword(), QString("geh31m"));
    QCOMPARE(acc2.getResourcePart(), QString("foo"));
}

void KwakAccountTest::testSetStatus() {
    acc.setStatus("online");
    QCOMPARE(acc.getStatus(), QString("online"));

    acc.setStatus("away");
    QCOMPARE(acc.getStatus(), QString("away"));
}

void KwakAccountTest::testSetStatusEmitsSignal() {
    QCOMPARE(lastStatus, QString(STATUS_UNKNOWN));
    QObject::connect(&acc, SIGNAL(statusChanged()), this, SLOT(statusChanged()));

    acc.setStatus("online");
    QCOMPARE(lastStatus, QString(STATUS_ONLINE));

    acc.setStatus("away");
    QCOMPARE(lastStatus, QString(STATUS_AWAY));
}

#ifdef BUILD_FOR_QT5
QTEST_GUILESS_MAIN(KwakAccountTest);
#else
QTEST_MAIN(KwakAccountTest);
#endif
