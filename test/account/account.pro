HEADERS += \
    test_account.h \
    ../../client/account.h \
    ../../client/xmpp/jid/barejid.h \
    ../../client/xmpp/jid/jid.h

SOURCES += \
    test_account.cpp \
    ../../client/account.cpp \
    ../../client/xmpp/jid/barejid.cpp \
    ../../client/xmpp/jid/jid.cpp

LIBS += -lz

lessThan(QT_MAJOR_VERSION, 5) {
    QT += script
    CONFIG += qtestlib
} else {
    QT += network widgets testlib
    DEFINES += BUILD_FOR_QT5
}

INCLUDEPATH += $$PWD/../../include
INCLUDEPATH += $$PWD/../../client

TARGET = account_test
QMAKE_CXXFLAGS += -g
