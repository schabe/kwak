#ifndef MOCKSENDER_H
#define MOCKSENDER_H

#include <QObject>

#include "stanza/bookmarks/outbound/addbookmarkrequeststanza.h"
#include "stanza/muc/mucchatmessagestanza.h"
#include "stanza/muc/privatemessagestanza.h"
#include "stanza/sender.h"
#include "stanza/subscription/outbound/subscriptionapprovalstanza.h"
#include "stanza/subscription/outbound/subscriptiondeniedstanza.h"
#include "stanza/subscription/outbound/subscriptionrequeststanza.h"
#include "xmpp/jid/jid.h"


class MockSender : public QObject, public StanzaSender
{
    Q_OBJECT
public:
    MockSender(QObject *parent = 0) : QObject(parent) {
        this->lastBookmarkAdded = NULL;
        this->lastContact = NULL;
        this->lastPrivateChat = NULL;
        this->lastSubRequest = NULL;
        this->lastSubApproved = NULL;
        this->lastSubRejected = NULL;
    }
    virtual ~MockSender();
    void send(OutboundAddBookmarkRequestStanza &req) {
        this->lastBookmarkAdded = (OutboundAddBookmarkRequestStanza*) malloc(sizeof(req));
        memcpy(this->lastBookmarkAdded, &req, sizeof(req));
    }
    void send(Contact *contact, QString msg) {
        this->lastContact = contact;
        this->lastMessage = QString(msg);
    }
    void send(MUCChatMessageStanza &msg) {
        this->lastMUC = msg.getTo();
        this->lastMessage = msg.getBody();
    }
    void send(PrivateChat &chat, PrivateMessageStanza &msg) {
        this->lastMessage = msg.getBody();
        this->lastPrivateChat = &chat;
    }
    void send(SubscriptionApprovalStanza &sub) {
        this->lastSubApproved = (SubscriptionApprovalStanza*) malloc(sizeof(sub));
        memcpy(this->lastSubApproved, &sub, sizeof(sub));
    }
    void send(SubscriptionDeniedStanza &rej) {
        this->lastSubRejected = (SubscriptionDeniedStanza*) malloc(sizeof(rej));
        memcpy(this->lastSubRejected, &rej, sizeof(rej));
    }
    void send(SubscriptionRequestStanza &req) {
        this->lastSubRequest = (SubscriptionRequestStanza*) malloc(sizeof(req));
        memcpy(this->lastSubRequest, &req, sizeof(req));
    }
    Contact *lastContact;
    Jid lastMUC;
    PrivateChat *lastPrivateChat;
    QString lastMessage;
    OutboundAddBookmarkRequestStanza *lastBookmarkAdded;
    SubscriptionApprovalStanza *lastSubApproved;
    SubscriptionRequestStanza *lastSubRequest;
    SubscriptionDeniedStanza *lastSubRejected;
};

#endif // MOCKSENDER_H
