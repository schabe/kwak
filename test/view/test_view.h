#ifndef KWAK_TEST_VIEW_H
#define KWAK_TEST_VIEW_H

#include <QObject>
#include "../mocksender.h"
#include "stanza/sender.h"
#include "roster/privatechat.h"


class MockRosterItem;
class MockGroupChat;


class ViewTest: public QObject
{
    Q_OBJECT
public slots:
    void receiveSignal();
private slots:
    /* Roster view */
    void testRosterViewSignal();
    void testRosterGetContacts();
    void testRosterUpdateContacts();
    void testRosterViewAddMUC();
    void testRosterViewAddPrivateChat();

    /* Contact view */
    void testContactViewDelegate();
    void testNameChangedSignal();
    void testStatusChangedSignal();
    void testContactViewMessagesChangedSignal();
    void testContactViewGetName();
    void testContactViewGetJid();
    void testContactViewGetMessages();
    void testContactViewHasMessages();
    void testContactViewLastMessage();
    void testSendMessageToContact();

    /* MUC view */
    void testMUCViewDelegate();
    void testMUCViewGetMessages();
    void testMUCViewMessagesChangedSignal();
    void testMUCRoomNameWithNoSubject();
    void testMUCRoomNameWithSubject();
    void testMUCRoomNameChangedSignal();
    void testMUCViewHasMessages();
    void testMUCViewLastMessage();
    void testSendMessageToMUC();

    /* Private chat view */
    void testPrivateChatViewDelegate();
    void testPrivateChatViewGetMessages();
    void testPrivateChatViewMessagesChangedSignal();
    void testPrivateChatViewMUCProperty();
    void testPrivateChatViewNameProperty();
    void testPrivateChatViewNameHasMessagesProperty();
    void testPrivateChatViewNameLastMessageProperty();
    void testSendMessageToPrivateChat();

    /* Chat invite */
    void testChatInviteViewDelegate();
    void testRosterViewShowsChatInvite();
    void testChatInviteJidProperty();
    void testAcceptChatInvite();
    void testAcceptChatInviteIgnoreInvalidParameter();
    void testRejectChatInvite();
    void testRejectChatInviteIgnoreInvalidParameter();

    /* MUC invite */
    void testMUCInviteViewDelegate();
    void testRosterViewShowsMUCInvite();
    void testMUCInviteJidProperty();
    void testMUCInviteFromProperty();
    void testAcceptMUCInvite();
    void testAcceptMUCInviteIgnoreInvalidParameter();
    void testRejectMUCInvite();
private:
    bool signalReceived;
    bool messagesChangeReceived;
    void loadMessage(MockRosterItem *, const QString &);
    void loadMessage(MockGroupChat *, const QString &);
    void loadMessage(PrivateChat &, const QString &);
};

#endif
