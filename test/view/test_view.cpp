#include <QtTest/QtTest>

#include "roster.h"
#include "roster/chatinvite.h"
#include "roster/contact.h"
#include "roster/mucinvite.h"
#include "stanza/message/chatmessagestanza.h"
#include "view/chatinviteview.h"
#include "view/contactview.h"
#include "view/mucinviteview.h"
#include "view/mucview.h"
#include "view/privatechatview.h"
#include "view/rosterview.h"
#include "test_view.h"


class MockRosterItem : public Contact
{
public:
    MockRosterItem(const BareJid &jid, const QString &name)
        : Contact(jid, name) {}
    void emitNameChangedSignal() {
        emit nameChanged();
    }
    void emitStatusChangedSignal() {
        emit statusChanged();
    }
    void emitMessagesChanged() {
        emit messagesChanged();
    }
    void addMessage(KwakMessage* m) {
        this->messages.append(m);
    }
};

class MockGroupChat : public GroupChat
{
public:
    MockGroupChat(const BareJid &jid, const QString &nick)
        : GroupChat(jid, nick) {}
    void emitMessagesChanged() {
        emit messagesChanged();
    }
    void addMessage(KwakMessage* m) {
        this->messages.append(m);
    }
};

class MockRoster : public KwakRoster
{
public:
    void emitRosterChangedSignal() {
        emit rosterChanged();
    }
    void addContact(const BareJid &jid, const QString &name) {
        this->addMember(jid, name);
        emit rosterChanged();
    }
    void addGroupChat(const BareJid &jid, const QString &nick) {
        GroupChat *muc = new GroupChat(jid, nick);
        this->add(muc);
    }
    void removeContact(const BareJid &jid) {
        RosterItem *item = this->findContact(jid);
        this->members.removeAll(item);
        delete(item);
        emit rosterChanged();
    }
};

class MockChatInvite : public ChatInvite
{
public:
    MockChatInvite(const BareJid &jid) : ChatInvite(jid) {
        this->accepted = false;
        this->rejected = false;
    }
    void accept(StanzaSender *) {
        this->accepted = true;
    }
    void reject(StanzaSender *) {
        this->rejected = true;
    }
    bool accepted;
    bool rejected;
};

class MockMUCInvite : public MUCInvite
{
public:
    MockMUCInvite(const BareJid &muc, const Jid &from, const QString &pw)
        : MUCInvite(muc, from, pw) {
        this->accepted = false;
        this->rejected = false;
    }
    void accept(StanzaSender *, QString nick) {
        this->accepted = true;
        this->nick = nick;
    }
    void reject() {
        this->rejected = true;
    }
    bool accepted;
    bool rejected;
    QString nick;
};

void ViewTest::receiveSignal()
{
    this->signalReceived = true;
}

void ViewTest::testRosterViewSignal()
{
    MockRoster roster;
    RosterView view(roster);

    QObject::connect(&view, SIGNAL(contactsChanged()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;
    roster.emitRosterChangedSignal();
    QVERIFY(this->signalReceived);
}

void ViewTest::testRosterGetContacts()
{
    MockRoster roster;
    RosterView view(roster);

    QCOMPARE(view.getContacts().size(), 0);
    roster.addContact(Jid("me@example.org"), QString("Just me"));
    QCOMPARE(view.getContacts().size(), 1);

    QList<QObject *> contacts = view.getContacts();
    QObject *obj = contacts.at(0);
    QVERIFY(obj != NULL);

    ContactView *contact = qobject_cast<ContactView *>(obj);
    QVERIFY(contact != NULL);
    QCOMPARE(contact->getName(), QString("Just me"));
}

void ViewTest::testRosterUpdateContacts()
{
    MockRoster roster;
    RosterView view(roster);

    QCOMPARE(view.getContacts().size(), 0);
    roster.addContact(Jid("me@example.org"), QString("Just me"));
    QCOMPARE(view.getContacts().size(), 1);

    roster.addContact(Jid("you@example.org"), QString("Just you"));
    roster.addContact(Jid("them@example.org"), QString("Just them"));
    QCOMPARE(view.getContacts().size(), 3);

    roster.removeContact(Jid("you@example.org"));
    QCOMPARE(view.getContacts().size(), 2);

    QList<QObject *> contacts = view.getContacts();
    QObject *obj = contacts.at(0);
    QVERIFY(obj != NULL);

    ContactView *contact = qobject_cast<ContactView *>(obj);
    QVERIFY(contact != NULL);
    QCOMPARE(contact->getName(), QString("Just me"));

    contacts = view.getContacts();
    obj = contacts.at(1);
    QVERIFY(obj != NULL);

    contact = qobject_cast<ContactView *>(obj);
    QVERIFY(contact != NULL);
    QCOMPARE(contact->getName(), QString("Just them"));
}

void ViewTest::testRosterViewAddMUC()
{
    MockRoster roster;
    RosterView view(roster);

    QCOMPARE(view.getContacts().size(), 0);
    roster.addGroupChat(Jid("me@example.org"), QString("Just me"));
    QCOMPARE(view.getContacts().size(), 1);

    QList<QObject *> contacts = view.getContacts();
    QObject *obj = contacts.at(0);
    QVERIFY(obj != NULL);

    MUCView *muc = qobject_cast<MUCView *>(obj);
    QVERIFY(muc != NULL);
}

void ViewTest::testRosterViewAddPrivateChat()
{
    MockRoster roster;
    RosterView view(roster);

    QCOMPARE(view.getContacts().size(), 0);
    PrivateChat *chat = new PrivateChat(Jid("topic@muc.example.org/user"));
    roster.add(chat);
    QCOMPARE(view.getContacts().size(), 1);

    QList<QObject *> contacts = view.getContacts();
    QObject *obj = contacts.at(0);
    QVERIFY(obj != NULL);

    PrivateChatView *cv = qobject_cast<PrivateChatView *>(obj);
    QVERIFY(cv != NULL);
}

void ViewTest::testContactViewDelegate()
{
    MockRosterItem c(Jid("foo@example.org"), QString("Test"));
    ContactView view(&c);
    QCOMPARE(view.getDelegate(), QString("Contact.qml"));
}

void ViewTest::testNameChangedSignal()
{
    MockRosterItem c(Jid("foo@example.org"), QString("Test"));
    ContactView view(&c);

    this->signalReceived = false;
    QObject::connect(&view, SIGNAL(nameChanged()),
            this, SLOT(receiveSignal()));

    c.emitNameChangedSignal();
    QVERIFY(this->signalReceived);
}

void ViewTest::testStatusChangedSignal()
{
    MockRosterItem c(Jid("foo@example.org"), QString("Test"));
    ContactView view(&c);

    this->signalReceived = false;
    QObject::connect(&view, SIGNAL(statusChanged()),
            this, SLOT(receiveSignal()));

    c.emitStatusChangedSignal();
    QVERIFY(this->signalReceived);
}

void ViewTest::testContactViewMessagesChangedSignal()
{
    MockRosterItem c(Jid("foo@example.org"), QString("Test"));
    ContactView view(&c);

    this->signalReceived = false;
    QObject::connect(&view, SIGNAL(messagesChanged()),
            this, SLOT(receiveSignal()));

    c.emitMessagesChanged();
    QVERIFY(this->signalReceived);
}

void ViewTest::testContactViewGetName()
{
    MockRosterItem c(Jid("foo@example.org"), QString("Test"));
    ContactView view(&c);
    QCOMPARE(view.getName(), QString("Test"));
}

void ViewTest::testContactViewGetJid()
{
    MockRosterItem c(Jid("foo@example.org"), QString("Test"));
    ContactView view(&c);
    QCOMPARE(view.getJid(), QString("foo@example.org"));
}

void ViewTest::loadMessage(MockRosterItem *roster, const QString &body)
{
    ChatMessageStanza m1(Jid("me@example.com"), Jid("you@example.com"),
            QString(body));

    KwakMessage *msg = new KwakMessage(m1);
    roster->addMessage(msg);
}

void ViewTest::loadMessage(MockGroupChat *chat, const QString &body)
{
    MUCChatMessageStanza m1(Jid("me@example.com"), QString(body));

    KwakMessage *msg = new KwakMessage(m1);
    chat->addMessage(msg);
}

void ViewTest::loadMessage(PrivateChat &chat, const QString &body)
{
    PrivateMessageStanza pm(Jid("me@example.com"), body);
    KwakMessage *msg = new KwakMessage(pm);
    chat.addMessage(msg);
}

void ViewTest::testContactViewGetMessages()
{
    MockRosterItem c(Jid("foo@example.org"), QString("Test"));
    ContactView view(&c);

    this->loadMessage(&c, "Hello");
    this->loadMessage(&c, "World");

    QCOMPARE(view.getMessages().size(), 2);
    QObject *ob1 = view.getMessages().at(0);
    QObject *ob2 = view.getMessages().at(1);

    QVERIFY(ob1 != NULL);
    QVERIFY(ob2 != NULL);

    KwakMessage *in1 = qobject_cast<KwakMessage *>(ob1);
    KwakMessage *in2 = qobject_cast<KwakMessage *>(ob2);

    QVERIFY(in1 != NULL);
    QVERIFY(in2 != NULL);

    QCOMPARE(in1->getText(), QString("Hello"));
    QCOMPARE(in2->getText(), QString("World"));
}

void ViewTest::testContactViewHasMessages()
{
    MockRosterItem c(Jid("foo@example.org"), QString("Test"));
    ContactView view(&c);

    QCOMPARE(view.property("hasMessages").toBool(), false);
    this->loadMessage(&c, "Hello");
    QCOMPARE(view.property("hasMessages").toBool(), true);
}

void ViewTest::testContactViewLastMessage()
{
    MockRosterItem c(Jid("foo@example.org"), QString("Test"));
    ContactView view(&c);

    QVariant var = view.property("lastMessage");
    QVERIFY(var.isValid());
    QObject *obj = qvariant_cast<QObject *>(var);
    QVERIFY(obj == NULL);

    this->loadMessage(&c, "Hello");
    var = view.property("lastMessage");
    QVERIFY(var.isValid());
    obj = qvariant_cast<QObject *>(var);
    QVERIFY(obj != NULL);

    KwakMessage *last = qobject_cast<KwakMessage *>(obj);
    QVERIFY(last != NULL);

    QCOMPARE(last->getText(), QString("Hello"));

    this->loadMessage(&c, "cruel");
    this->loadMessage(&c, "World");

    var = view.property("lastMessage");
    QVERIFY(var.isValid());
    obj = qvariant_cast<QObject *>(var);
    QVERIFY(obj != NULL);

    last = qobject_cast<KwakMessage *>(obj);
    QVERIFY(last != NULL);

    QCOMPARE(last->getText(), QString("World"));
}

void ViewTest::testSendMessageToContact()
{
    MockRosterItem c(Jid("foo@example.org"), QString("Test"));
    ContactView view(&c);
    MockSender sender;

    view.sendMessage(&sender, QString("Hello"));

    QVERIFY(sender.lastContact != NULL);
    QVERIFY(sender.lastMUC == Jid(""));
    QCOMPARE(sender.lastContact->getJid(), Jid("foo@example.org"));
    QCOMPARE(sender.lastMessage, QString("Hello"));
}

void ViewTest::testMUCViewDelegate()
{
    GroupChat c(Jid("foo@example.org"), QString("Test"));
    MUCView view(&c);
    QCOMPARE(view.getDelegate(), QString("MUC.qml"));
}

void ViewTest::testMUCViewGetMessages()
{
    MockGroupChat c(Jid("foo@example.org"), QString("Test"));

    this->loadMessage(&c, "Hello");
    this->loadMessage(&c, "World");

    MUCView view(&c);
    QCOMPARE(view.getMessages().size(), 2);
    QObject *ob1 = view.getMessages().at(0);
    QObject *ob2 = view.getMessages().at(1);

    QVERIFY(ob1 != NULL);
    QVERIFY(ob2 != NULL);

    KwakMessage *in1 = qobject_cast<KwakMessage *>(ob1);
    KwakMessage *in2 = qobject_cast<KwakMessage *>(ob2);

    QVERIFY(in1 != NULL);
    QVERIFY(in2 != NULL);

    QCOMPARE(in1->getText(), QString("Hello"));
    QCOMPARE(in2->getText(), QString("World"));
}

void ViewTest::testMUCViewMessagesChangedSignal()
{
    MockGroupChat c(Jid("foo@example.org"), QString("Test"));
    MUCView view(&c);

    this->signalReceived = false;
    QObject::connect(&view, SIGNAL(messagesChanged()),
            this, SLOT(receiveSignal()));

    c.emitMessagesChanged();
    QVERIFY(this->signalReceived);
}

void ViewTest::testMUCRoomNameWithNoSubject()
{
    GroupChat c(Jid("foo@example.org"), QString("Test"));
    MUCView view(&c);

    QCOMPARE(view.getRoomName(), QString("foo@example.org"));
}

void ViewTest::testMUCRoomNameWithSubject()
{
    GroupChat c(Jid("foo@example.org"), QString("Test"));
    MUCView view(&c);
    c.setSubject("Things");

    QCOMPARE(view.getRoomName(), QString("Things"));
}

void ViewTest::testMUCRoomNameChangedSignal()
{
    GroupChat c(Jid("foo@example.org"), QString("Test"));
    MUCView view(&c);

    this->signalReceived = false;
    QObject::connect(&view, SIGNAL(nameChanged()),
            this, SLOT(receiveSignal()));

    c.setSubject("Stuff");
    QVERIFY(this->signalReceived);
}

void ViewTest::testMUCViewHasMessages()
{
    MockGroupChat c(Jid("foo@example.org"), QString("Test"));
    MUCView view(&c);

    QCOMPARE(view.property("hasMessages").toBool(), false);
    KwakMessage *msg = new KwakMessage(QString("mucmsg02"), Jid("me"),
            Jid("you"), QString("hi"), QDateTime::currentDateTime());
    c.addMessage(msg);
    QCOMPARE(view.property("hasMessages").toBool(), true);
}

void ViewTest::testMUCViewLastMessage()
{
    MockGroupChat c(Jid("foo@example.org"), QString("Test"));
    MUCView view(&c);

    QVariant var = view.property("lastMessage");
    QVERIFY(var.isValid());
    QObject *obj = qvariant_cast<QObject *>(var);
    QVERIFY(obj == NULL);

    KwakMessage *msg = new KwakMessage(QString("mucmsg01"), Jid("me"),
                Jid("you"), QString("hi"), QDateTime::currentDateTime());
    c.addMessage(msg);

    var = view.property("lastMessage");
    QVERIFY(var.isValid());
    obj = qvariant_cast<QObject *>(var);
    QVERIFY(obj != NULL);

    KwakMessage *last = qobject_cast<KwakMessage *>(obj);
    QVERIFY(last != NULL);

    QCOMPARE(last->getFrom(), Jid("me"));
    QCOMPARE(last->getText(), QString("hi"));
}

void ViewTest::testSendMessageToMUC()
{
    GroupChat c(Jid("foo@example.org"), QString("Test"));
    MUCView view(&c);
    MockSender sender;

    view.sendMessage(&sender, QString("Hello"));

    QVERIFY(sender.lastContact == NULL);
    QVERIFY(sender.lastMUC != Jid(""));
    QCOMPARE(sender.lastMUC, Jid("foo@example.org"));
    QCOMPARE(sender.lastMessage, QString("Hello"));
}

void ViewTest::testPrivateChatViewDelegate()
{
    PrivateChat c(Jid("chat@muc.example.org/user"));
    PrivateChatView view(&c);
    QCOMPARE(view.getDelegate(), QString("PrivateChat.qml"));
}

void ViewTest::testPrivateChatViewGetMessages()
{
    PrivateChat c(Jid("chat@muc.example.org/user"));

    this->loadMessage(c, "Hello");
    this->loadMessage(c, "World");

    PrivateChatView view(&c);
    QCOMPARE(view.getMessages().size(), 2);
    QObject *ob1 = view.getMessages().at(0);
    QObject *ob2 = view.getMessages().at(1);

    QVERIFY(ob1 != NULL);
    QVERIFY(ob2 != NULL);

    KwakMessage *in1 = qobject_cast<KwakMessage *>(ob1);
    KwakMessage *in2 = qobject_cast<KwakMessage *>(ob2);

    QVERIFY(in1 != NULL);
    QVERIFY(in2 != NULL);

    QCOMPARE(in1->getText(), QString("Hello"));
    QCOMPARE(in2->getText(), QString("World"));
}

void ViewTest::testPrivateChatViewMessagesChangedSignal()
{
    PrivateChat c(Jid("chat@muc.example.org/user"));
    PrivateChatView view(&c);

    this->signalReceived = false;
    QObject::connect(&view, SIGNAL(messagesChanged()),
            this, SLOT(receiveSignal()));

    this->loadMessage(c, "World");
    QVERIFY(this->signalReceived);
}

void ViewTest::testPrivateChatViewMUCProperty()
{
    PrivateChat c(Jid("chat@muc.example.org/user"));
    PrivateChatView view(&c);

    QVariant muc = view.property("muc");
    QVERIFY(muc.isValid());
    QCOMPARE(muc.toString(), QString("chat@muc.example.org"));
}

void ViewTest::testPrivateChatViewNameProperty()
{
    PrivateChat c(Jid("chat@muc.example.org/user"));
    PrivateChatView view(&c);

    QVariant name = view.property("name");
    QVERIFY(name.isValid());
    QCOMPARE(name.toString(), QString("user"));
}

void ViewTest::testPrivateChatViewNameHasMessagesProperty()
{
    PrivateChat c(Jid("chat@muc.example.org/user"));
    PrivateChatView view(&c);

    QVariant var = view.property("hasMessages");
    QVERIFY(var.isValid());
    QCOMPARE(var.toBool(), false);

    this->loadMessage(c, "World");
    var = view.property("hasMessages");
    QVERIFY(var.isValid());
    QCOMPARE(var.toBool(), true);
}

void ViewTest::testPrivateChatViewNameLastMessageProperty()
{
    PrivateChat c(Jid("chat@muc.example.org/user"));
    PrivateChatView view(&c);

    QVariant var = view.property("lastMessage");
    QVERIFY(var.isValid());
    QObject *obj = qvariant_cast<QObject *>(var);
    QVERIFY(obj == NULL);

    this->loadMessage(c, "Hello");

    var = view.property("lastMessage");
    QVERIFY(var.isValid());
    obj = qvariant_cast<QObject *>(var);
    QVERIFY(obj != NULL);

    KwakMessage *last = qobject_cast<KwakMessage *>(obj);
    QVERIFY(last != NULL);

    QCOMPARE(last->getText(), QString("Hello"));

    this->loadMessage(c, "Cruel");
    this->loadMessage(c, "World");

    var = view.property("lastMessage");
    QVERIFY(var.isValid());
    obj = qvariant_cast<QObject *>(var);
    QVERIFY(obj != NULL);

    last = qobject_cast<KwakMessage *>(obj);
    QVERIFY(last != NULL);

    QCOMPARE(last->getText(), QString("World"));
}

void ViewTest::testSendMessageToPrivateChat()
{
    PrivateChat c(Jid("chat@muc.example.org/user"));
    PrivateChatView view(&c);
    MockSender sender;

    view.sendMessage(&sender, QString("Hello"));

    QVERIFY(sender.lastContact == NULL);
    QVERIFY(sender.lastMUC == Jid(""));
    QVERIFY(sender.lastPrivateChat != NULL);
    QCOMPARE(sender.lastPrivateChat->getJid(), Jid("chat@muc.example.org/user"));
    QCOMPARE(sender.lastMessage, QString("Hello"));
}

void ViewTest::testChatInviteViewDelegate()
{
    ChatInvite inv(Jid("you@example.com"));
    ChatInviteView view(&inv);
    QCOMPARE(view.getDelegate(), QString("ChatInvite.qml"));
}

void ViewTest::testRosterViewShowsChatInvite()
{
    MockRoster roster;
    RosterView view(roster);

    Jid jid("me@example.com");
    ChatInvite *inv = new ChatInvite(jid);
    roster.add(inv);

    view.updateContacts();
    QList<QObject *> contacts = view.getContacts();
    QCOMPARE(contacts.size(), 1);

    QObject *obj = contacts.at(0);
    ChatInviteView *v = qobject_cast<ChatInviteView *>(obj);
    QVERIFY(v != NULL);
}

void ViewTest::testChatInviteJidProperty()
{
    ChatInvite inv(Jid("you@example.com"));
    ChatInviteView view(&inv);

    QVariant jid = view.property("from");
    QVERIFY(jid.isValid());
    QCOMPARE(jid.toString(), QString("you@example.com"));
}

void ViewTest::testAcceptChatInvite()
{
    MockChatInvite inv(Jid("you@example.com"));
    ChatInviteView view(&inv);

    MockSender sender;

    QVERIFY(!inv.accepted);
    view.accept(&sender);
    QVERIFY(inv.accepted);
}

void ViewTest::testAcceptChatInviteIgnoreInvalidParameter()
{
    MockChatInvite inv(Jid("you@example.com"));
    ChatInviteView view(&inv);

    QObject sender;

    QVERIFY(!inv.accepted);
    view.accept(&sender);
    QVERIFY(!inv.accepted);
}

void ViewTest::testRejectChatInvite()
{
    MockChatInvite inv(Jid("you@example.com"));
    ChatInviteView view(&inv);

    MockSender sender;

    QVERIFY(!inv.rejected);
    view.reject(&sender);
    QVERIFY(inv.rejected);
}

void ViewTest::testRejectChatInviteIgnoreInvalidParameter()
{
    MockChatInvite inv(Jid("you@example.com"));
    ChatInviteView view(&inv);

    QObject sender;

    QVERIFY(!inv.rejected);
    view.reject(&sender);
    QVERIFY(!inv.rejected);
}

void ViewTest::testMUCInviteViewDelegate()
{
    Jid from("crone1@shakespeare.lit/desktop");
    BareJid muc(Jid("darkcave@macbeth.shakespeare.lit"));
    MUCInvite inv(muc, from, "");
    MUCInviteView view(&inv);
    QCOMPARE(view.getDelegate(), QString("MUCInvite.qml"));
}

void ViewTest::testRosterViewShowsMUCInvite()
{
    MockRoster roster;
    RosterView view(roster);

    Jid from("crone1@shakespeare.lit/desktop");
    BareJid muc(Jid("darkcave@macbeth.shakespeare.lit"));
    MUCInvite *inv = new MUCInvite(muc, from, "");
    roster.add(inv);

    view.updateContacts();
    QList<QObject *> contacts = view.getContacts();
    QCOMPARE(contacts.size(), 1);

    QObject *obj = contacts.at(0);
    MUCInviteView *v = qobject_cast<MUCInviteView *>(obj);
    QVERIFY(v != NULL);
}

void ViewTest::testMUCInviteJidProperty()
{
    BareJid muc(Jid("test@muc.example.net"));
    Jid jid("crone1@shakespeare.lit/desktop");
    MUCInvite inv(muc, jid, "");
    MUCInviteView view(&inv);

    QVariant prop = view.property("jid");
    QVERIFY(prop.isValid());
    QCOMPARE(prop.toString(), QString("test@muc.example.net"));
}

void ViewTest::testMUCInviteFromProperty()
{
    BareJid muc(Jid("darkcave@macbeth.shakespeare.lit"));
    Jid jid("crone1@shakespeare.lit/desktop");
    MUCInvite inv(muc, jid, "");
    MUCInviteView view(&inv);

    QVariant from = view.property("from");
    QVERIFY(from.isValid());
    QCOMPARE(from.toString(), QString("crone1@shakespeare.lit"));
}

void ViewTest::testAcceptMUCInvite()
{
    BareJid muc(Jid("darkcave@macbeth.shakespeare.lit"));
    Jid jid("crone1@shakespeare.lit/desktop");
    MockMUCInvite inv(muc, jid, "");
    MUCInviteView view(&inv);

    MockSender sender;

    QVERIFY(!inv.accepted);
    view.accept(&sender, "Hecate");
    QVERIFY(inv.accepted);
    QCOMPARE(inv.nick, QString("Hecate"));
}

void ViewTest::testAcceptMUCInviteIgnoreInvalidParameter()
{
    BareJid muc(Jid("darkcave@macbeth.shakespeare.lit"));
    Jid jid("crone1@shakespeare.lit/desktop");
    MockMUCInvite inv(muc, jid, "");
    MUCInviteView view(&inv);

    QObject sender;

    QVERIFY(!inv.accepted);
    view.accept(&sender, "Hecate");
    QVERIFY(!inv.accepted);
}

void ViewTest::testRejectMUCInvite()
{
    BareJid muc(Jid("darkcave@macbeth.shakespeare.lit"));
    Jid jid("crone1@shakespeare.lit/desktop");
    MockMUCInvite inv(muc, jid, "");
    MUCInviteView view(&inv);

    QVERIFY(!inv.rejected);
    view.reject();
    QVERIFY(inv.rejected);
}

#ifdef BUILD_FOR_QT5
QTEST_GUILESS_MAIN(ViewTest);
#else
QTEST_MAIN(ViewTest);
#endif
