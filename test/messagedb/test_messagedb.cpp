#include "test_messagedb.h"

#include <unistd.h>

#include <QtGlobal>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QtTest/QtTest>

#include <strophe.h>

#include "db/messagedb.h"
#include "stanza/muc/mucchatmessagestanza.h"
#include "stanza/muc/privatemessagestanza.h"
#include "stanza/message/chatmessagestanza.h"
#include "stanza/message/inboundmessagestanza.h"

class MockInboundMessage : public MessageStanza
{
public:
    MockInboundMessage(const Jid &from, const Jid &to,
            const QString &body, const QString &id)
        : MessageStanza()
    {
        this->from = Jid(from);
        this->to = Jid(to);
        this->body = body;
        this->id = id;
    }

    MockInboundMessage(const Jid &from, const Jid &to,
            const QString &body, const QDateTime &delay, const QString &id)
        : MessageStanza()
    {
        this->from = Jid(from);
        this->to = Jid(to);
        this->body = body;
        this->sent = delay;
        this->id = id;
    }
};

class MessageDBTester : public MessageDB
{
public:
    int version()
    {
        return this->getVersion();
    }

    void setVersion(int version)
    {
        QSqlQuery query;

        query.prepare("INSERT INTO meta (key, value) VALUES ('version', :v)");
        query.bindValue(":v", QVariant::fromValue(version));

        if (!query.exec()) {
            qWarning() << "Update failed:" << query.lastError();
        }
    }
};

void KwakMessageDBTest::testInitializeTables()
{
    MessageDB db;
    db.open(":memory:");
}

void KwakMessageDBTest::testSaveLoadInboundMessage()
{
    MessageDB db;
    db.open(":memory:");

    Jid sender("somebody");
    Jid to("romeo@example.net");

    MockInboundMessage m(sender, to, "Hey there", "msg0001");
    db.addMessage(m);
    QList<KwakMessage*> messages = db.getMessagesFrom(sender);

    QCOMPARE(messages.size(), 1);
    KwakMessage *msg = messages.at(0);
    QCOMPARE(msg->getText(), QString("Hey there"));
    QCOMPARE(msg->getFrom(), Jid("somebody"));
    QCOMPARE(msg->getTo(), Jid("romeo@example.net"));
    QCOMPARE(msg->getId(), QString("msg0001"));
    delete msg;
}

void KwakMessageDBTest::testSaveLoadDelayedInboundMessage()
{
    MessageDB db;
    db.open(":memory:");

    Jid me("me@example.net");
    Jid you("you@example.com");
    QDateTime then(QDate(2012, 1, 1), QTime(10, 0, 0), Qt::UTC);

    MockInboundMessage m(me, you, "Hello", then, "msg0002");
    db.addMessage(m);
    QList<KwakMessage *> messages = db.getMessagesFrom(me);

    QCOMPARE(messages.size(), 1);
    KwakMessage *msg = messages.at(0);
    QCOMPARE(msg->getFrom(), Jid("me@example.net"));
    QCOMPARE(msg->getTo(), Jid("you@example.com"));
    QCOMPARE(msg->getText(), QString("Hello"));
    QCOMPARE(msg->getId(), QString("msg0002"));
    QCOMPARE(msg->getSent(), then);
    delete msg;
}

void KwakMessageDBTest::testSaveLoadOutboundMessage()
{
    MessageDB db;
    db.open(":memory:");

    Jid from("me@example.com");
    Jid to("someone@example.org");

    ChatMessageStanza m(from, to, "hello, world");
    db.addMessage(m);
    QList<KwakMessage*> messages = db.getMessagesTo(Jid("someone@example.org"));

    QCOMPARE(messages.size(), 1);
    KwakMessage *msg = messages.at(0);
    QCOMPARE(msg->getText(), QString("hello, world"));
    QCOMPARE(msg->getFrom(), Jid("me@example.com"));
    QCOMPARE(msg->getTo(), Jid("someone@example.org"));
    qDebug() << "ID" << msg->getId();
    QVERIFY(msg->getId() != QString(""));
    QCOMPARE(msg->getId(), m.getId());
    delete msg;
}

void KwakMessageDBTest::testSaveLoadInboundMUCMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_message_new(ctx, "groupchat",
            "me@example.com", "1234");
    xmpp_stanza_set_from(stanza, "mychat@muc.example.com/fred");

    xmpp_stanza_t *body = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(body, "body");

    xmpp_stanza_t *text = xmpp_stanza_new(ctx);
    xmpp_stanza_set_text(text, "Hi there");
    xmpp_stanza_add_child(body, text);
    xmpp_stanza_release(text);

    xmpp_stanza_add_child(stanza, body);
    xmpp_stanza_release(body);

    MessageDB db;
    db.open(":memory:");

    MUCChatMessageStanza m(stanza);
    db.addMessage(m);

    QList<KwakMessage*> messages =
        db.getMUCMessages(BareJid(Jid("mychat@muc.example.com")));

    QCOMPARE(messages.size(), 1);
    KwakMessage *msg = messages.at(0);
    QCOMPARE(msg->getText(), QString("Hi there"));
    QCOMPARE(msg->getTo(), Jid("me@example.com"));
    delete msg;

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void KwakMessageDBTest::testSaveLoadOutboundMUCMessage()
{
    MessageDB db;
    db.open(":memory:");

    MUCChatMessageStanza m(BareJid(Jid("muc@chat.example.com")), "hello, world");
    db.addMessage(m);
    QList<KwakMessage*> messages = db.getMessagesTo(Jid("muc@chat.example.com"));

    QCOMPARE(messages.size(), 1);
    KwakMessage *msg = messages.at(0);
    QCOMPARE(msg->getText(), QString("hello, world"));
    QCOMPARE(msg->getTo(), Jid("muc@chat.example.com"));
    delete msg;
}

void KwakMessageDBTest::testSaveLoadInboundPrivateMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_message_new(ctx, "chat",
            "crone1@shakespeare.lit/desktop", "hgn27af1");
    xmpp_stanza_set_id(stanza, "hgn27af1");
    xmpp_stanza_set_from(stanza, "coven@chat.shakespeare.lit/secondwitch");

    xmpp_stanza_t *body = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(body, "body");

    xmpp_stanza_t *text = xmpp_stanza_new(ctx);
    xmpp_stanza_set_text(text, "I'll give thee a wind.");
    xmpp_stanza_add_child(body, text);
    xmpp_stanza_release(text);

    xmpp_stanza_add_child(stanza, body);
    xmpp_stanza_release(body);

    MessageDB db;
    db.open(":memory:");

    PrivateMessageStanza m(stanza);
    db.addMessage(m);

    QList<KwakMessage*> messages =
        db.getMessagesFrom(Jid("coven@chat.shakespeare.lit/secondwitch"));

    QCOMPARE(messages.size(), 1);
    KwakMessage *msg = messages.at(0);
    QCOMPARE(msg->getText(), QString("I'll give thee a wind."));
    QCOMPARE(msg->getTo(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(msg->getId(), QString("hgn27af1"));
    delete msg;

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void KwakMessageDBTest::testSaveLoadOutboundPrivateMessage()
{
    MessageDB db;
    db.open(":memory:");

    PrivateMessageStanza m(Jid("muc@chat.example.com/foo"), "hello, world");
    db.addMessage(m);
    QList<KwakMessage*> messages =
        db.getMessagesTo(Jid("muc@chat.example.com/foo"));

    QCOMPARE(messages.size(), 1);
    KwakMessage *msg = messages.at(0);
    QCOMPARE(msg->getText(), QString("hello, world"));
    QCOMPARE(msg->getTo(), Jid("muc@chat.example.com/foo"));
    delete msg;
}

void KwakMessageDBTest::testMultipleSenders()
{
    MessageDB db;
    db.open(":memory:");

    Jid alice("alice");
    Jid bob("bob");

    MockInboundMessage msg1(alice, bob, "hey there", "msg0003");
    MockInboundMessage msg2(bob, alice, "hello", "msg0004");
    MockInboundMessage msg3(bob, alice, "bye", "msg0005");

    db.addMessage(msg1);
    db.addMessage(msg2);
    db.addMessage(msg3);

    QList<KwakMessage*> messages = db.getMessagesFrom(Jid("bob"));
    QCOMPARE(messages.size(), 2);
    delete(messages.at(1));
    delete(messages.at(0));

    messages = db.getMessagesFrom(Jid("alice"));
    QCOMPARE(messages.size(), 1);
    delete(messages.at(0));

    messages = db.getMessagesFrom(Jid("dave"));
    QCOMPARE(messages.size(), 0);
}

void KwakMessageDBTest::createEmptyInvalidDB(const QString &fn)
{
    QString name;

    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(fn);
        if (!db.open()) {
            qWarning() << db.lastError();
            QFAIL("Failed to open database");
        }
        name = db.connectionName();

        /* create weird 'messages' table */
        QSqlQuery query("CREATE TABLE messages (something TEXT)");
        db.close();
    }

    QSqlDatabase::removeDatabase(name);
}

void KwakMessageDBTest::testUpgradeUnknownToVersion1()
{
    char fn[PATH_MAX];
    int fd, rows;
    QString name;

    /* setup database in temp file */
    snprintf(fn, sizeof(fn), "/tmp/kwak-db-XXXXXX");
    fd = mkstemp(fn);
    QVERIFY(fd != -1);
    this->createEmptyInvalidDB(fn);

    /* open file as kwak db.
     *
     * this should upgrade it to the latest schema. since there is no version
     * number set, we the messages table should be replaced and a new meta
     * table should be added with the correct version number.
     * */
    MessageDB *kwakdb = new MessageDB();
    kwakdb->open(fn);
    delete kwakdb; /* this should fully close the database */

    /* now verify the result */
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(fn);
        if (!db.open()) {
            qWarning() << db.lastError();
            QFAIL("Failed to re-open database");
        }
        name = db.connectionName();

        /* insert message into table and load it again */
        QSqlQuery insert("INSERT INTO messages (from_jid,to_jid,message,sent) "
                "VALUES ('from_test', 'to_test', 'msg_test', 131)");
        QSqlQuery select("SELECT from_jid,to_jid,message,sent FROM messages");

        rows = 0;

        while (select.next())
        {
            rows++;
            QCOMPARE(select.value(0).toString(), QString("from_test"));
            QCOMPARE(select.value(1).toString(), QString("to_test"));
            QCOMPARE(select.value(2).toString(), QString("msg_test"));
            QCOMPARE(select.value(3).toInt(), 131);
        }
        QCOMPARE(rows, 1);

        rows = 0;
        select.exec("SELECT value FROM meta WHERE key='version'");

        while (select.next())
        {
            rows++;
            QCOMPARE(select.value(0).toString(), QString("0"));
        }
        QCOMPARE(rows, 1);

        db.close();
    }
    QSqlDatabase::removeDatabase(name);

    QCOMPARE(unlink(fn), 0);
}

void KwakMessageDBTest::testGetVersion()
{
    MessageDBTester db;
    db.open(":memory:");

    QCOMPARE(db.version(), 0);

    db.setVersion(25);
    QCOMPARE(db.version(), 25);
}

void KwakMessageDBTest::testConstructMessage()
{
    Jid from("from@example.org");
    Jid to("to@example.org");
    QString body("Hello");
    QDateTime then(QDate(2023, 11, 2), QTime(13, 8, 25), Qt::UTC);

    KwakMessage msg("msg0008", from, to, body, then);
    QCOMPARE(msg.getFrom(), Jid("from@example.org"));
    QCOMPARE(msg.getTo(), Jid("to@example.org"));
    QCOMPARE(msg.getText(), QString("Hello"));
    QCOMPARE(msg.getSent(), then);
    QCOMPARE(msg.getId(), QString("msg0008"));
}

void KwakMessageDBTest::testCopyMessage()
{
    Jid from("from@example.org");
    Jid to("to@example.org");
    QString body("Hello");
    QDateTime then(QDate(2023, 11, 2), QTime(13, 8, 25), Qt::UTC);

    KwakMessage orig("msg0009", from, to, body, then);
    KwakMessage msg(orig);

    QCOMPARE(msg.getFrom(), Jid("from@example.org"));
    QCOMPARE(msg.getTo(), Jid("to@example.org"));
    QCOMPARE(msg.getText(), QString("Hello"));
    QCOMPARE(msg.getSent(), then);
    QCOMPARE(msg.getId(), QString("msg0009"));
}

void KwakMessageDBTest::testConstructMessageFromInboundStanza()
{
    Jid from("from@example.org");
    Jid to("to@example.org");
    QString body("Hello");
    time_t now = time(NULL);
    QDateTime then;

    MockInboundMessage stanza(from, to, body, then, "msg0006");
    KwakMessage msg(stanza);
    QCOMPARE(msg.getFrom(), Jid("from@example.org"));
    QCOMPARE(msg.getTo(), Jid("to@example.org"));
    QCOMPARE(msg.getText(), QString("Hello"));
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY(now - (msg.getSent().toTime_t()) <= 1); /* max 1 second */
#else
    QVERIFY(now - (msg.getSent().toSecsSinceEpoch()) <= 1); /* max 1 second */
#endif
    QCOMPARE(msg.getId(), QString("msg0006"));
}

void KwakMessageDBTest::testConstructMessageFromDelayedInboundStanza()
{
    Jid from("from@example.org");
    Jid to("to@example.org");
    QString body("Hello");
    QDateTime then(QDate(2023, 11, 2), QTime(13, 8, 25), Qt::UTC);

    MockInboundMessage stanza(from, to, body, then, "msg0007");
    KwakMessage msg(stanza);
    QCOMPARE(msg.getFrom(), Jid("from@example.org"));
    QCOMPARE(msg.getTo(), Jid("to@example.org"));
    QCOMPARE(msg.getText(), QString("Hello"));
    QCOMPARE(msg.getSent(), then);
    QCOMPARE(msg.getId(), QString("msg0007"));
}

void KwakMessageDBTest::testConstructMessageFromOutboundStanza()
{
    Jid from("from@example.org");
    Jid to("to@example.org");
    QString body("Hello");
    time_t now = time(NULL);

    ChatMessageStanza stanza(from, to, body);
    KwakMessage msg(stanza);
    QCOMPARE(msg.getFrom(), Jid("from@example.org"));
    QCOMPARE(msg.getTo(), Jid("to@example.org"));
    QCOMPARE(msg.getText(), QString("Hello"));
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY(now - (msg.getSent().toTime_t()) <= 1); /* max 1 second */
#else
    QVERIFY(now - (msg.getSent().toSecsSinceEpoch()) <= 1); /* max 1 second */
#endif
    QVERIFY(msg.getId().length() > 20);
    QCOMPARE(msg.getId(), stanza.getId());
}

void KwakMessageDBTest::testConstructMessageFromOutboundMUCMessage()
{
    Jid to("test@muc.example.org");
    QString body("Hello");
    time_t now = time(NULL);

    MUCChatMessageStanza stanza(BareJid(to), body);
    KwakMessage msg(stanza);
    QCOMPARE(msg.getFrom(), Jid(""));
    QCOMPARE(msg.getTo(), Jid("test@muc.example.org"));
    QCOMPARE(msg.getText(), QString("Hello"));
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY(now - (msg.getSent().toTime_t()) <= 1); /* max 1 second */
#else
    QVERIFY(now - (msg.getSent().toSecsSinceEpoch()) <= 1); /* max 1 second */
#endif
    QVERIFY(msg.getId().length() > 20);
    QCOMPARE(msg.getId(), stanza.getId());
}

void KwakMessageDBTest::testConstructMessageFromInboundMUCMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_message_new(ctx, "groupchat",
            "me@example.com", "1234");
    xmpp_stanza_set_from(stanza, "mychat@muc.example.com/fred");
    xmpp_stanza_set_id(stanza, "hgn27af2");

    xmpp_stanza_t *body = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(body, "body");

    xmpp_stanza_t *text = xmpp_stanza_new(ctx);
    xmpp_stanza_set_text(text, "Hi there");
    xmpp_stanza_add_child(body, text);
    xmpp_stanza_release(text);

    xmpp_stanza_add_child(stanza, body);
    xmpp_stanza_release(body);

    xmpp_stanza_t *delay = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(delay, "delay");
    xmpp_stanza_set_attribute(delay, "xmlns", "urn:xmpp:delay");
    xmpp_stanza_set_attribute(delay, "stamp", "2002-10-13T23:58:37Z");
    xmpp_stanza_add_child(stanza, delay);
    xmpp_stanza_release(delay);

    MUCChatMessageStanza mymsg(stanza);
    KwakMessage msg(mymsg);
    QCOMPARE(msg.getFrom(), Jid("fred"));
    QCOMPARE(msg.getTo(), Jid("me@example.com"));
    QCOMPARE(msg.getText(), QString("Hi there"));
    QCOMPARE(msg.getSent(), QDateTime(QDate(2002, 10, 13),
                QTime(23, 58, 37), Qt::UTC));
    QCOMPARE(msg.getId(), QString("hgn27af2"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void KwakMessageDBTest::testConstructMessageFromOutboundMUCPrivateMessage()
{
    Jid to("test@muc.example.org");
    QString body("Hello");
    time_t now = time(NULL);

    PrivateMessageStanza stanza(to, body);
    KwakMessage msg(stanza);
    QCOMPARE(msg.getFrom(), Jid(""));
    QCOMPARE(msg.getTo(), Jid("test@muc.example.org"));
    QCOMPARE(msg.getText(), QString("Hello"));
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY(now - (msg.getSent().toTime_t()) <= 2);
#else
    QVERIFY(now - (msg.getSent().toSecsSinceEpoch()) <= 2);
#endif
    QVERIFY(msg.getId().length() > 20);
    QCOMPARE(msg.getId(), stanza.getId());
}

/* XEP-0045 Example 47 */
void KwakMessageDBTest::testConstructMessageFromInboundMUCPrivateMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_message_new(ctx, "chat",
            "crone1@shakespeare.lit/desktop", "hgn27af1");
    xmpp_stanza_set_from(stanza, "coven@chat.shakespeare.lit/secondwitch");
    xmpp_stanza_set_id(stanza, "hgn27af1");

    xmpp_message_set_body(stanza, "I'll give thee a wind.");

    xmpp_stanza_t *x = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(x, "x");
    xmpp_stanza_set_attribute(x, "xmlns", "http://jabber.org/protocol/muc#user");
    xmpp_stanza_release(x);

    PrivateMessageStanza mymsg(stanza);
    KwakMessage msg(mymsg);
    QCOMPARE(msg.getFrom(), Jid("coven@chat.shakespeare.lit/secondwitch"));
    QCOMPARE(msg.getTo(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(msg.getText(), QString("I'll give thee a wind."));
    QCOMPARE(msg.getId(), QString("hgn27af1"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

#ifdef BUILD_FOR_QT5
QTEST_GUILESS_MAIN(KwakMessageDBTest);
#else
QTEST_MAIN(KwakMessageDBTest);
#endif
