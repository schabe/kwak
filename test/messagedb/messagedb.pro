HEADERS += \
    test_messagedb.h \
    ../../client/db/message.h \
    ../../client/db/messagedb.h \
    ../../client/roster/muc.h \
    ../../client/roster/rosteritem.h \
    ../../client/xmpp/strophehelpers.h \
    ../../client/xmpp/jid/barejid.h \
    ../../client/xmpp/jid/jid.h \
    ../../client/stanza/base/inbound/inboundstanza.h \
    ../../client/stanza/message/chatmessagestanza.h \
    ../../client/stanza/message/messagestanza.h \
    ../../client/stanza/muc/mucchatmessagestanza.h \
    ../../client/stanza/muc/mucmessagestanza.h \
    ../../client/stanza/muc/privatemessagestanza.h \
    ../../client/stanza/muc/subjectchangestanza.h

SOURCES += \
    test_messagedb.cpp \
    ../../client/db/message.cpp \
    ../../client/db/messagedb.cpp \
    ../../client/roster/muc.cpp \
    ../../client/roster/rosteritem.cpp \
    ../../client/xmpp/strophehelpers.cpp \
    ../../client/xmpp/jid/barejid.cpp \
    ../../client/xmpp/jid/jid.cpp \
    ../../client/stanza/base/inbound/inboundstanza.cpp \
    ../../client/stanza/message/chatmessagestanza.cpp \
    ../../client/stanza/message/messagestanza.cpp \
    ../../client/stanza/muc/mucchatmessagestanza.cpp \
    ../../client/stanza/muc/mucmessagestanza.cpp \
    ../../client/stanza/muc/privatemessagestanza.cpp \
    ../../client/stanza/muc/subjectchangestanza.cpp

LIBS += -lz

QT += sql

lessThan(QT_MAJOR_VERSION, 5) {
    QT += script
    CONFIG += qtestlib
} else {
    QT += network testlib
    DEFINES += BUILD_FOR_QT5
}

contains(MEEGO_EDITION,harmattan) {
    LIBS += -lexpat -lresolv \
        $$PWD/../../lib/harmattan/libstrophe.a \
        $$PWD/../../lib/harmattan/libssl.a \
        $$PWD/../../lib/harmattan/libcrypto.a
} else:simulator {
    LIBS += -lexpat -lresolv \
        $$PWD/../../lib/simulator/libstrophe.a \
        $$PWD/../../lib/simulator/libssl.a \
        $$PWD/../../lib/simulator/libcrypto.a
} else {
    LIBS += -lstrophe -lssl -lcrypto
}

INCLUDEPATH += $$PWD/../../include
INCLUDEPATH += $$PWD/../../client

TARGET = messagedb_test
QMAKE_CXXFLAGS += -g
