#ifndef KWAK_TEST_MESSAGEDB_H
#define KWAK_TEST_MESSAGEDB_H

#include <QObject>


class KwakMessageDBTest: public QObject
{
    Q_OBJECT
private slots:
    void testInitializeTables();
    void testSaveLoadInboundMessage();
    void testSaveLoadDelayedInboundMessage();
    void testSaveLoadOutboundMessage();
    void testSaveLoadInboundMUCMessage();
    void testSaveLoadOutboundMUCMessage();
    void testSaveLoadInboundPrivateMessage();
    void testSaveLoadOutboundPrivateMessage();
    void testMultipleSenders();
    void testUpgradeUnknownToVersion1();
    void testGetVersion();
    void testConstructMessage();
    void testCopyMessage();
    void testConstructMessageFromInboundStanza();
    void testConstructMessageFromDelayedInboundStanza();
    void testConstructMessageFromOutboundStanza();
    void testConstructMessageFromOutboundMUCMessage();
    void testConstructMessageFromInboundMUCMessage();
    void testConstructMessageFromOutboundMUCPrivateMessage();
    void testConstructMessageFromInboundMUCPrivateMessage();
private:
    void createEmptyInvalidDB(const QString &);
};

#endif
