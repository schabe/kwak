#include "test_roster.h"

#include <QtTest/QtTest>
#include <strophe.h>

#include "../mocksender.h"
#include "db/messagedb.h"
#include "roster.h"
#include "roster/chatinvite.h"
#include "roster/contact.h"
#include "roster/muc.h"
#include "roster/mucinvite.h"
#include "roster/privatechat.h"
#include "stanza/roster/inbound/rosterresultstanza.h"
#include "stanza/roster/inbound/rostersetstanza.h"
#include "xmpp/jid/barejid.h"

class MockGroupChat : public GroupChat
{
public:
    MockGroupChat(const BareJid &jid, const QString &nick)
        : GroupChat(jid, nick) {}
    void addMessage(KwakMessage *msg) {
        this->messages.append(msg);
    }
};

xmpp_stanza_t *KwakRosterTest::createRosterIQ(xmpp_ctx_t *ctx, const char *type)
{
    xmpp_stanza_t *iq = xmpp_iq_new(ctx, type, "roster");

    xmpp_stanza_t *query = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(query, "query");
    xmpp_stanza_set_ns(query, XMPP_NS_ROSTER);
    xmpp_stanza_add_child(iq, query);
    xmpp_stanza_release(query);

    return iq;
}

void KwakRosterTest::receiveSignal()
{
    this->signalReceived = true;
}

void KwakRosterTest::receiveSignalWithString(QString val)
{
    this->signalReceived = true;
    this->stringReceived = val;
}

void KwakRosterTest::rosterAdd(xmpp_ctx_t *ctx, xmpp_stanza_t *roster,
        const char *jid, const char *name)
{
    xmpp_stanza_t *query = xmpp_stanza_get_child_by_name(roster, "query");

    xmpp_stanza_t *item = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(item, "item");
    xmpp_stanza_set_attribute(item, "jid", jid);

    if (name) {
        xmpp_stanza_set_attribute(item, "name", name);
    }
    xmpp_stanza_add_child(query, item);

    xmpp_stanza_release(item);
}

void KwakRosterTest::rosterAddRemoved(xmpp_ctx_t *ctx, xmpp_stanza_t *roster,
        const char *jid)
{
    xmpp_stanza_t *query = xmpp_stanza_get_child_by_name(roster, "query");

    xmpp_stanza_t *item = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(item, "item");
    xmpp_stanza_set_attribute(item, "jid", jid);
    xmpp_stanza_set_attribute(item, "subscription", "remove");
    xmpp_stanza_add_child(query, item);

    xmpp_stanza_release(item);
}

void KwakRosterTest::testLoadFromQueryResultNoItems()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    MessageDB db;
    db.open(":memory:");

    InboundRosterResultStanza stanza(iq);
    KwakRoster roster;
    roster.update(&stanza, db);
    QCOMPARE(roster.getMembers().size(), 0);

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testLoadFromQueryResultSingleItem()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test68@example.com", NULL);
    MessageDB db;
    db.open(":memory:");

    InboundRosterResultStanza stanza(iq);
    KwakRoster roster;
    roster.update(&stanza, db);
    QCOMPARE(roster.getMembers().size(), 1);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test68@example.com"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testLoadFromQueryResultMultipleItems()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test98@example.com", NULL);
    rosterAdd(ctx, iq, "test104@example.com", NULL);
    rosterAdd(ctx, iq, "test110@example.com", NULL);
    MessageDB db;
    db.open(":memory:");

    InboundRosterResultStanza stanza(iq);
    KwakRoster roster;
    roster.update(&stanza, db);
    QCOMPARE(roster.getMembers().size(), 3);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(((Contact *)members[0])->getJid(), Jid("test98@example.com"));
    QCOMPARE(((Contact *)members[1])->getJid(), Jid("test104@example.com"));
    QCOMPARE(((Contact *)members[2])->getJid(), Jid("test110@example.com"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testRepeatedLoadFromQueryResultMultipleItems()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test98@example.com", NULL);
    rosterAdd(ctx, iq, "test104@example.com", NULL);
    rosterAdd(ctx, iq, "test110@example.com", NULL);
    MessageDB db;
    db.open(":memory:");

    InboundRosterResultStanza stanza(iq);
    KwakRoster roster;
    roster.update(&stanza, db);
    QCOMPARE(roster.getMembers().size(), 3);
    roster.update(&stanza, db);
    QCOMPARE(roster.getMembers().size(), 3);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(((Contact *)members[0])->getJid(), Jid("test98@example.com"));
    QCOMPARE(((Contact *)members[1])->getJid(), Jid("test104@example.com"));
    QCOMPARE(((Contact *)members[2])->getJid(), Jid("test110@example.com"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testLoadFromQueryResultSingleItemWithoutName()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test68@example.com", NULL);
    MessageDB db;
    db.open(":memory:");

    InboundRosterResultStanza stanza(iq);
    KwakRoster roster;
    roster.update(&stanza, db);
    QCOMPARE(roster.getMembers().size(), 1);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test68@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("test68@example.com"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testLoadFromQueryResultSingleItemWithName()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test68@example.com", "My Test User 68");
    MessageDB db;
    db.open(":memory:");

    InboundRosterResultStanza stanza(iq);
    KwakRoster roster;
    roster.update(&stanza, db);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test68@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("My Test User 68"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testUpdateAddName()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test159@example.com", NULL);

    InboundRosterSetStanza stanza(iq);
    roster.update(&stanza, db);
    QCOMPARE(roster.getMembers().size(), 1);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test159@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("test159@example.com"));

    xmpp_stanza_release(iq);

    iq = createRosterIQ(ctx, "get");
    rosterAdd(ctx, iq, "test159@example.com", "My Test User");

    InboundRosterSetStanza update(iq);
    roster.update(&update, db);

    members = roster.getMembers();
    QCOMPARE(roster.getMembers().size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test159@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("My Test User"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

/* RFC 2621 Section 2.4.1: Including an empty 'name' attribute is equivalent
 * to including no 'name' attribute */
void KwakRosterTest::testUpdateSetEmptyName()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test192@example.com", "My Test User");

    InboundRosterSetStanza stanza(iq);
    roster.update(&stanza, db);
    QCOMPARE(roster.getMembers().size(), 1);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test192@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("My Test User"));

    xmpp_stanza_release(iq);

    iq = createRosterIQ(ctx, "get");
    rosterAdd(ctx, iq, "test192@example.com", "");

    InboundRosterSetStanza update(iq);
    roster.update(&update, db);

    members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test192@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("test192@example.com"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testUpdateRemoveName()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test192@example.com", "My Test User");

    InboundRosterSetStanza stanza(iq);
    roster.update(&stanza, db);
    QCOMPARE(roster.getMembers().size(), 1);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test192@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("My Test User"));

    xmpp_stanza_release(iq);

    iq = createRosterIQ(ctx, "get");
    rosterAdd(ctx, iq, "test192@example.com", NULL);

    InboundRosterSetStanza update(iq);
    roster.update(&update, db);

    members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test192@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("test192@example.com"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testUpdateChangeName()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test223@example.com", "My Test User");

    InboundRosterSetStanza stanza(iq);
    roster.update(&stanza, db);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test223@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("My Test User"));

    xmpp_stanza_release(iq);

    iq = createRosterIQ(ctx, "get");
    rosterAdd(ctx, iq, "test223@example.com", "My Other User");

    InboundRosterSetStanza update(iq);
    roster.update(&update, db);

    members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test223@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("My Other User"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testUpdateKeepName()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test254@example.com", "My Test User");

    InboundRosterSetStanza stanza(iq);
    roster.update(&stanza, db);
    QList<RosterItem*> members = roster.getMembers();

    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test254@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("My Test User"));

    xmpp_stanza_release(iq);

    iq = createRosterIQ(ctx, "get");
    rosterAdd(ctx, iq, "test254@example.com", "My Test User");

    InboundRosterSetStanza update(iq);

    roster.update(&update, db);
    members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test254@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("My Test User"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testUpdateKeepNoName()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test285@example.com", NULL);

    InboundRosterSetStanza stanza(iq);
    roster.update(&stanza, db);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test285@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("test285@example.com"));

    xmpp_stanza_release(iq);

    iq = createRosterIQ(ctx, "get");
    rosterAdd(ctx, iq, "test285@example.com", NULL);

    InboundRosterSetStanza update(iq);
    roster.update(&update, db);

    members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test285@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("test285@example.com"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testUpdateAddMember()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test297@example.com", NULL);

    InboundRosterSetStanza stanza(iq);
    roster.update(&stanza, db);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test297@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("test297@example.com"));

    xmpp_stanza_release(iq);

    iq = createRosterIQ(ctx, "get");
    rosterAdd(ctx, iq, "test310@example.com", NULL);

    InboundRosterSetStanza update(iq);
    roster.update(&update, db);

    members = roster.getMembers();
    QCOMPARE(members.size(), 2);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test297@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("test297@example.com"));
    QCOMPARE(((Contact*)members[1])->getJid(), Jid("test310@example.com"));
    QCOMPARE(((Contact*)members[1])->getName(), QString("test310@example.com"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testUpdateAddMemberWithName()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test333@example.com", NULL);

    InboundRosterSetStanza stanza(iq);
    roster.update(&stanza, db);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test333@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("test333@example.com"));

    xmpp_stanza_release(iq);

    iq = createRosterIQ(ctx, "get");
    rosterAdd(ctx, iq, "test346@example.com", "John");

    InboundRosterSetStanza update(iq);
    roster.update(&update, db);

    members = roster.getMembers();
    QCOMPARE(members.size(), 2);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test333@example.com"));
    QCOMPARE(((Contact*)members[0])->getName(), QString("test333@example.com"));
    QCOMPARE(((Contact*)members[1])->getJid(), Jid("test346@example.com"));
    QCOMPARE(((Contact*)members[1])->getName(), QString("John"));

    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

/* Replace roster with item missing */
void KwakRosterTest::testUpdateRemoveMissingMember()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test369@example.com", NULL);
    rosterAdd(ctx, iq, "test370@example.com", NULL);
    rosterAdd(ctx, iq, "test371@example.com", NULL);
    rosterAdd(ctx, iq, "test372@example.com", NULL);

    InboundRosterResultStanza stanza(iq);
    roster.update(&stanza, db);
    xmpp_stanza_release(iq);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 4);

    iq = createRosterIQ(ctx, "resul");
    rosterAdd(ctx, iq, "test369@example.com", NULL);
    rosterAdd(ctx, iq, "test370@example.com", NULL);
    rosterAdd(ctx, iq, "test372@example.com", NULL);

    InboundRosterResultStanza shorterStanza(iq);
    roster.update(&shorterStanza, db);
    xmpp_stanza_release(iq);

    members = roster.getMembers();
    QCOMPARE(members.size(), 3);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test369@example.com"));
    QCOMPARE(((Contact*)members[1])->getJid(), Jid("test370@example.com"));
    QCOMPARE(((Contact*)members[2])->getJid(), Jid("test372@example.com"));


    xmpp_ctx_free(ctx);
}

/* Do not touch MUCs when updating roster contacts */
void KwakRosterTest::testUpdateLeaveMUCs()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test369@example.com", NULL);
    rosterAdd(ctx, iq, "test370@example.com", NULL);
    rosterAdd(ctx, iq, "test371@example.com", NULL);
    rosterAdd(ctx, iq, "test372@example.com", NULL);
    InboundRosterResultStanza stanza(iq);
    roster.update(&stanza, db);

    GroupChat *muc = new GroupChat(Jid("muc@example.org"), QString("myself"));
    roster.add(muc);

    xmpp_stanza_release(iq);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 5);

    iq = createRosterIQ(ctx, "result");
    InboundRosterResultStanza shorterStanza(iq);

    roster.update(&shorterStanza, db);
    xmpp_stanza_release(iq);

    members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((GroupChat*)members[0])->getJid(), Jid("muc@example.org"));


    xmpp_ctx_free(ctx);
}

/* RFC 2621 Section 2.5: Deleting a Roster Item */
void KwakRosterTest::testUpdateDeleteRosterItem()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "test369@example.com", NULL);
    rosterAdd(ctx, iq, "test370@example.com", NULL);
    rosterAdd(ctx, iq, "test371@example.com", NULL);
    rosterAdd(ctx, iq, "test372@example.com", NULL);

    InboundRosterResultStanza stanza(iq);
    roster.update(&stanza, db);
    xmpp_stanza_release(iq);

    QCOMPARE(roster.getMembers().size(), 4);

    iq = createRosterIQ(ctx, "set");
    xmpp_stanza_t *query = xmpp_stanza_get_child_by_name(iq, "query");
    xmpp_stanza_t *item = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(item, "item");
    xmpp_stanza_set_attribute(item, "jid", "test371@example.com");
    xmpp_stanza_set_attribute(item, "subscription", "remove");
    xmpp_stanza_add_child(query, item);
    xmpp_stanza_release(item);

    InboundRosterSetStanza removeStanza(iq);
    roster.update(&removeStanza, db);
    xmpp_stanza_release(iq);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 3);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test369@example.com"));
    QCOMPARE(((Contact*)members[1])->getJid(), Jid("test370@example.com"));
    QCOMPARE(((Contact*)members[2])->getJid(), Jid("test372@example.com"));

    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testUpdateExistingItemFromPresence()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "alice@example.com", NULL);
    rosterAdd(ctx, iq, "bob@example.com", NULL);
    rosterAdd(ctx, iq, "carlo@example.com", NULL);

    InboundRosterResultStanza resultStanza(iq);
    roster.update(&resultStanza, db);
    QCOMPARE(roster.getMembers().size(), 3);

    xmpp_stanza_release(iq);

    xmpp_stanza_t *stanza = xmpp_presence_new(ctx);
    xmpp_stanza_set_from(stanza, "bob@example.com/foo");
    xmpp_stanza_t *status = xmpp_stanza_new_from_string(ctx,
        "<show>dnd</show>");
    xmpp_stanza_add_child(stanza, status);

    InboundPresenceStanza p(stanza);
    roster.update(&p);

    xmpp_stanza_release(status);
    xmpp_stanza_release(stanza);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 3);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("alice@example.com"));
    QCOMPARE(((Contact*)members[0])->getStatus(), QString("unknown"));
    QCOMPARE(((Contact*)members[1])->getJid(), Jid("bob@example.com"));
    QCOMPARE(((Contact*)members[1])->getStatus(), QString("dnd"));
    QCOMPARE(((Contact*)members[2])->getJid(), Jid("carlo@example.com"));
    QCOMPARE(((Contact*)members[2])->getStatus(), QString("unknown"));

    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testReceivePresenceBeforeRoster()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    xmpp_stanza_t *stanza = xmpp_presence_new(ctx);
    xmpp_stanza_set_from(stanza, "bob@example.com/foo");
    xmpp_stanza_t *status = xmpp_stanza_new_from_string(ctx,
        "<show>dnd</show>");
    xmpp_stanza_add_child(stanza, status);

    InboundPresenceStanza p(stanza);
    roster.update(&p);

    xmpp_stanza_release(status);
    xmpp_stanza_release(stanza);

    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "alice@example.com", NULL);
    rosterAdd(ctx, iq, "bob@example.com", NULL);
    rosterAdd(ctx, iq, "carlo@example.com", NULL);

    InboundRosterResultStanza resultStanza(iq);
    roster.update(&resultStanza, db);
    QCOMPARE(roster.getMembers().size(), 3);

    xmpp_stanza_release(iq);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 3);
    QCOMPARE(((Contact*)members[0])->getJid(),
            Jid("alice@example.com"));
    QCOMPARE(((Contact*)members[0])->getStatus(),
            QString("unknown"));
    QCOMPARE(((Contact*)members[1])->getJid(),
            Jid("bob@example.com"));
    QCOMPARE(((Contact*)members[1])->getStatus(),
            QString("dnd"));
    QCOMPARE(((Contact*)members[2])->getJid(),
            Jid("carlo@example.com"));
    QCOMPARE(((Contact*)members[2])->getStatus(),
            QString("unknown"));

    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testIgnoreUpdateFromUnknownPresence()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "alice@example.com", NULL);

    InboundRosterResultStanza resultStanza(iq);
    roster.update(&resultStanza, db);
    QCOMPARE(roster.getMembers().size(), 1);

    xmpp_stanza_release(iq);

    xmpp_stanza_t *stanza = xmpp_presence_new(ctx);
    xmpp_stanza_set_from(stanza, "bob@example.com/foo");
    xmpp_stanza_t *status = xmpp_stanza_new_from_string(ctx,
        "<show>away</show>");
    xmpp_stanza_add_child(stanza, status);

    InboundPresenceStanza p(stanza);
    roster.update(&p);

    xmpp_stanza_release(status);
    xmpp_stanza_release(stanza);

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("alice@example.com"));
    QCOMPARE(((Contact*)members[0])->getStatus(), QString("unknown"));

    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testSubscribe()
{
    KwakRoster roster;
    QCOMPARE(roster.getMembers().size(), 0);

    roster.subscribe(BareJid(Jid("test@example.com")));

    QList<RosterItem*> members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test@example.com"));

    roster.subscribe(BareJid(Jid("test@example.org")));
    members = roster.getMembers();
    QCOMPARE(members.size(), 2);

    QCOMPARE(((Contact*)members[0])->getJid(), Jid("test@example.com"));
    QCOMPARE(((Contact*)members[1])->getJid(), Jid("test@example.org"));
}

void KwakRosterTest::testFindContact()
{
    KwakRoster roster;
    RosterItem *item;

    item = roster.findContact(BareJid(Jid("001@example.org")));
    QVERIFY(item == NULL);

    roster.subscribe(BareJid(Jid("001@example.org")));
    item = roster.findContact(BareJid(Jid("001@example.org")));
    QVERIFY(item != NULL);

    Contact *contact = qobject_cast<Contact *>(item);
    QVERIFY(contact != NULL);
    QCOMPARE(contact->getJid(), Jid("001@example.org"));
}

class TestMessage : public MessageStanza
{
public:
    TestMessage(const Jid &from, const Jid &to, const QString &body,
                const QDateTime &dt)
    {
        this->from = Jid(from);
        this->to = Jid(to);
        this->body = QString(body);
        this->sent = dt;
    }
};

class TestMUCMessage : public MUCChatMessageStanza
{
public:
    TestMUCMessage(const QString &id, const Jid &from, const Jid &to, const
            QString &body, const QDateTime &dt)
        : MUCChatMessageStanza(to, body)
    {
        this->id = id;
        this->from = Jid(from);
        this->sent = dt;
    }
};

void KwakRosterTest::testContactLoadMessages()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    Jid me("me@example.net");
    Jid you("you@example.com");

    TestMessage m1(me, you, "Hi there",
            QDateTime(QDate(2010, 1, 1), QTime(10, 0, 0), Qt::UTC));
    TestMessage m2(me, you, "You around?",
            QDateTime(QDate(2010, 1, 1), QTime(10, 1, 0), Qt::UTC));
    TestMessage m3(you, me, "Oh, hi",
            QDateTime(QDate(2010, 1, 1), QTime(10, 2, 0), Qt::UTC));
    TestMessage m4(me, you, "Sup?",
            QDateTime(QDate(2010, 1, 1), QTime(10, 3, 0), Qt::UTC));

    db.addMessage(m1);
    db.addMessage(m4);
    db.addMessage(m3);
    db.addMessage(m2);

    Contact rosterMe(me, "");
    QCOMPARE(rosterMe.getMessages().size(), 0);

    rosterMe.loadMessages(db);

    QList<KwakMessage *> messages = rosterMe.getMessages();
    QCOMPARE(messages.size(), 4);

    KwakMessage *msg = messages.at(0);
    QCOMPARE(msg->getText(), QString("Hi there"));

    msg = messages.at(1);
    QCOMPARE(msg->getText(), QString("You around?"));

    msg = messages.at(2);
    QCOMPARE(msg->getText(), QString("Oh, hi"));

    msg = messages.at(3);
    QCOMPARE(msg->getText(), QString("Sup?"));
}

void KwakRosterTest::testContactLoadMessagesEmitsSignal()
{
    BareJid jid(Jid("romeo@example.com"));
    Contact c(jid, QString(""));
    MessageDB db;

    QObject::connect(&c, SIGNAL(messagesChanged()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;
    c.loadMessages(db);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testContactConstructor()
{
    BareJid jid(Jid("romeo@example.com"));
    Contact c(jid, QString("Romeo"));

    QCOMPARE(QString(c.getJid()), QString("romeo@example.com"));
    QCOMPARE(QString(c.getName()), QString("Romeo"));
}

void KwakRosterTest::testContactConstructorWithEmptyName()
{
    BareJid jid(Jid("romeo@example.com"));
    Contact c(jid, QString(""));

    QCOMPARE(QString(c.getJid()), QString("romeo@example.com"));
    QCOMPARE(QString(c.getName()), QString("romeo@example.com"));
}

void KwakRosterTest::testContactSetGetName()
{
    BareJid jid(Jid("romeo@example.com"));
    Contact c(jid, QString(""));
    QCOMPARE(QString(c.getName()), QString("romeo@example.com"));

    c.setName("Romeo");
    QCOMPARE(QString(c.getName()), QString("Romeo"));

    c.setName("Oh Romeo");
    QCOMPARE(QString(c.getName()), QString("Oh Romeo"));

    c.setName("");
    QCOMPARE(QString(c.getName()), QString("romeo@example.com"));
}

void KwakRosterTest::testContactSetNameEmitsSignal()
{
    BareJid jid(Jid("romeo@example.com"));
    Contact c(jid, QString(""));

    QObject::connect(&c, SIGNAL(nameChanged()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;
    c.setName("Bernie");
    QVERIFY(this->signalReceived);

    this->signalReceived = false;
    c.setName("");
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testContactSetGetStatus()
{
    BareJid jid(Jid("romeo@example.com"));
    Contact c(jid, QString(""));
    QCOMPARE(QString(c.getStatus()), QString("unknown"));

    c.setStatus("online", Jid(jid));
    QCOMPARE(QString(c.getStatus()), QString("online"));

    c.setStatus("away", Jid(jid));
    QCOMPARE(QString(c.getStatus()), QString("away"));
}

void KwakRosterTest::testContactSetStatusEmitsSignal()
{
    BareJid jid(Jid("romeo@example.com"));
    Contact c(jid, QString(""));

    QObject::connect(&c, SIGNAL(statusChanged()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;
    c.setStatus("online", Jid(jid));
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testContactReceiveMessage()
{
    Contact juliet(BareJid(Jid("juliet@example.com")), "Juliet");
    QCOMPARE(juliet.getMessages().size(), 0);

    KwakMessage *msg = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));
    juliet.receiveMessage(msg);
    QCOMPARE(juliet.getMessages().size(), 1);

    QObject *obj = juliet.getMessages().at(0);
    QVERIFY(obj != NULL);

    KwakMessage *msgIn = static_cast<KwakMessage *>(obj);
    QVERIFY(msgIn != NULL);

    QCOMPARE(msgIn->getText(), msg->getText());
}

void KwakRosterTest::testContactReceiveMessageEmitsSignal()
{
    Contact juliet(BareJid(Jid("juliet@example.com")), "Juliet");
    QCOMPARE(juliet.getMessages().size(), 0);

    KwakMessage *msg = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));

    QObject::connect(&juliet, SIGNAL(messagesChanged()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;
    juliet.receiveMessage(msg);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testContactAddMessage()
{
    Contact romeo(BareJid(Jid("romeo@example.net")), "Romeo");
    QCOMPARE(romeo.getMessages().size(), 0);

    KwakMessage *msg = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));

    romeo.addMessage(msg);
    QCOMPARE(romeo.getMessages().size(), 1);
}

void KwakRosterTest::testContactAddMessageEmitsSignal()
{
    Contact romeo(BareJid(Jid("romeo@example.net")), "Romeo");
    KwakMessage *msg = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));

    QObject::connect(&romeo, SIGNAL(messagesChanged()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;
    romeo.addMessage(msg);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testContactInitialToAddress()
{
    Contact romeo(BareJid(Jid("romeo@example.net")), "Romeo");
    QCOMPARE(QString(romeo.getToAddress()), QString("romeo@example.net"));

    Contact juliet(BareJid(Jid("juliet@example.net/orchard")), "Juliet");
    QCOMPARE(QString(juliet.getToAddress()), QString("juliet@example.net"));
}

void KwakRosterTest::testLockContactOnInboundMessage()
{
    Contact juliet(BareJid(Jid("juliet@example.com")), "Juliet");
    QCOMPARE(QString(juliet.getToAddress()), QString("juliet@example.com"));

    KwakMessage *msg = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));
    juliet.receiveMessage(msg);
    QCOMPARE(QString(juliet.getToAddress()),
             QString("juliet@example.com/balcony"));
}

void KwakRosterTest::testUnlockContactAfterMessageFromOtherResource()
{
    /* initial state */
    Contact juliet(BareJid(Jid("juliet@example.com/orchard")), "Juliet");
    QCOMPARE(QString(juliet.getToAddress()), QString("juliet@example.com"));

    /* receive first message -> lock */
    KwakMessage *msg1 = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));
    juliet.receiveMessage(msg1);
    QCOMPARE(QString(juliet.getToAddress()),
             QString("juliet@example.com/balcony"));

    /* receive second message -> keep locked */
    KwakMessage *msg2 = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));
    juliet.receiveMessage(msg2);
    QCOMPARE(QString(juliet.getToAddress()),
             QString("juliet@example.com/balcony"));

    /* receive message from other resource -> unlock */
    KwakMessage *msg3 = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/orchard"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));
    juliet.receiveMessage(msg3);
    QCOMPARE(QString(juliet.getToAddress()),
             QString("juliet@example.com"));

    /* receive another message -> lock */
    KwakMessage *msg4 = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));
    juliet.receiveMessage(msg4);
    QCOMPARE(QString(juliet.getToAddress()),
             QString("juliet@example.com/balcony"));
}

void KwakRosterTest::testUnlockContactAfterPresenceFromOtherResource()
{
    /* initial state */
    Contact juliet(BareJid(Jid("juliet@example.com/orchard")), "Juliet");
    QCOMPARE(QString(juliet.getToAddress()), QString("juliet@example.com"));

    /* receive message -> lock */
    KwakMessage *msg1 = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));
    juliet.receiveMessage(msg1);
    QCOMPARE(QString(juliet.getToAddress()),
             QString("juliet@example.com/balcony"));

    /* receive presence from same resource -> keep locked */
    juliet.setStatus("online", Jid("juliet@example.com/balcony"));
    QCOMPARE(QString(juliet.getToAddress()),
             QString("juliet@example.com/balcony"));

    /* receive presence from other resource -> unlock */
    juliet.setStatus("online", Jid("juliet@example.com/orchard"));
    QCOMPARE(QString(juliet.getToAddress()), QString("juliet@example.com"));

    /* receive another message -> lock */
    KwakMessage *msg4 = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));
    juliet.receiveMessage(msg4);
    QCOMPARE(QString(juliet.getToAddress()),
             QString("juliet@example.com/balcony"));
}

void KwakRosterTest::testGroupChatConstructor()
{
    BareJid jid(Jid("chat@muc.example.com"));
    GroupChat c(jid, QString("MyNick"));

    QCOMPARE(QString(c.getJid()), QString("chat@muc.example.com"));
    QCOMPARE(QString(c.getNick()), QString("MyNick"));
}

void KwakRosterTest::testGroupChatUpdateMessagesFromOtherChat()
{
    BareJid jid(Jid("mymuc@chat.example.org"));

    MockGroupChat muc1(jid, QString("myself"));
    MockGroupChat muc2(jid, QString("myself"));

    KwakMessage *msg1 = new KwakMessage(QString("ktx72v49"),
            Jid("001@example.com"),
            Jid("me@example.org"), QString("hello1"),
            QDateTime::currentDateTime());
    KwakMessage *msg2 = new KwakMessage(QString("ktx72v49"),
            Jid("002@example.com"),
            Jid("me@example.org"), QString("hello2"),
            QDateTime::currentDateTime());
    KwakMessage *msg3 = new KwakMessage(QString("ktx72v49"),
            Jid("003@example.com"),
            Jid("me@example.org"), QString("hello2"),
            QDateTime::currentDateTime());

    muc1.addMessage(msg1);
    muc2.addMessage(msg2);
    muc2.addMessage(msg3);

    muc1.updateMessages(&muc2);

    QList<KwakMessage *> messages = muc1.getMessages();
    QCOMPARE(messages.size(), 2);
    QCOMPARE(messages.at(0)->getText(), QString("hello2"));
    QCOMPARE(messages.at(1)->getText(), QString("hello2"));
}

void KwakRosterTest::testRosterAddFindMUC()
{
    KwakRoster roster;
    RosterItem *item;
    BareJid jid(Jid("mymuc@chat.example.org"));
    GroupChat *muc = new GroupChat(jid, QString("myself"));

    item = roster.findMUC(BareJid(Jid("mymuc@chat.example.org")));
    QVERIFY(item == NULL);

    roster.add(muc);
    item = roster.findMUC(BareJid(Jid("mymuc@chat.example.org")));
    QVERIFY(item != NULL);

    Contact *contact = qobject_cast<Contact *>(item);
    QVERIFY(contact == NULL);
    GroupChat *mymuc = qobject_cast<GroupChat *>(item);
    QVERIFY(mymuc != NULL);
    QCOMPARE(mymuc->getJid(), Jid("mymuc@chat.example.org"));
    QCOMPARE(mymuc->getNick(), QString("myself"));
}

void KwakRosterTest::testRosterDoNotAddDuplicateMUCs()
{
    KwakRoster roster;
    BareJid jid(Jid("mymuc@chat.example.org"));

    GroupChat *muc1 = new GroupChat(jid, QString("myself"));
    GroupChat *muc2 = new GroupChat(jid, QString("myself"));

    roster.add(muc1);
    roster.add(muc2);

    QList<RosterItem *> members = roster.getMembers();
    QCOMPARE(members.size(), 1);
    delete(muc2);
}

void KwakRosterTest::testRosterReplaceMessagesWhenReAddingMUC()
{
    KwakRoster roster;
    BareJid jid(Jid("mymuc@chat.example.org"));

    MockGroupChat *muc1 = new MockGroupChat(jid, QString("myself"));
    MockGroupChat *muc2 = new MockGroupChat(jid, QString("myself"));

    KwakMessage *msg1 = new KwakMessage(QString("ktx72v49"),
            Jid("001@example.com"),
            Jid("me@example.org"), QString("hello1"),
            QDateTime::currentDateTime());
    KwakMessage *msg2 = new KwakMessage(QString("ktx72v49"),
            Jid("002@example.com"),
            Jid("me@example.org"), QString("hello2"),
            QDateTime::currentDateTime());
    KwakMessage *msg3 = new KwakMessage(QString("ktx72v49"),
            Jid("003@example.com"),
            Jid("me@example.org"), QString("hello2"),
            QDateTime::currentDateTime());

    muc1->addMessage(msg1);
    muc2->addMessage(msg2);
    muc2->addMessage(msg3);

    roster.add(muc1);
    roster.add(muc2);

    GroupChat *muc = roster.findMUC(jid);
    QList<KwakMessage *> messages = muc->getMessages();
    QCOMPARE(messages.size(), 2);
    QCOMPARE(messages.at(0)->getText(), QString("hello2"));
    QCOMPARE(messages.at(1)->getText(), QString("hello2"));
    delete(muc2);
}

/* this should never happen, but let's make sure that we don't crash if it
 * does */
void KwakRosterTest::testUpdateMUCWithRosterResult()
{
    KwakRoster roster;

    /* MUC */
    BareJid jid(Jid("mymuc@chat.example.org"));
    GroupChat *muc = new GroupChat(jid, QString("myself"));

    /* database */
    MessageDB db;
    db.open(":memory:");

    /* InboundRosterResultStanza */
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "mymuc@chat.example.org", "My chat");

    /* update roster */
    InboundRosterResultStanza stanza(iq);
    roster.add(muc);
    roster.update(&stanza, db);

    /* make sure that nothing has been modified and that the MUC has not been
     * removed from the roster */
    RosterItem *item = roster.findMUC(BareJid(Jid("mymuc@chat.example.org")));
    QVERIFY(item != NULL);

    GroupChat *mymuc = qobject_cast<GroupChat *>(item);
    QVERIFY(mymuc != NULL);
    QCOMPARE(mymuc->getJid(), Jid("mymuc@chat.example.org"));
    QCOMPARE(mymuc->getNick(), QString("myself"));

    /* cleanup */
    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

/* this should never happen, but let's make sure that we don't crash if it
 * does */
void KwakRosterTest::testUpdateMUCWithRosterPush()
{
    KwakRoster roster;

    /* MUC */
    BareJid jid(Jid("mymuc@chat.example.org"));
    GroupChat *muc = new GroupChat(jid, QString("myself"));

    /* database */
    MessageDB db;
    db.open(":memory:");

    /* InboundRosterSetStanza */
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *iq = createRosterIQ(ctx, "result");
    rosterAdd(ctx, iq, "mymuc@chat.example.org", "My chat");

    /* update roster */
    InboundRosterSetStanza stanza(iq);
    roster.add(muc);
    roster.update(&stanza, db);

    /* make sure that nothing has been modified */
    Contact *item = roster.findContact(BareJid(Jid("mymuc@chat.example.org")));
    QVERIFY(item == NULL);

    GroupChat *mymuc = roster.findMUC(BareJid(Jid("mymuc@chat.example.org")));
    QVERIFY(mymuc != NULL);
    QCOMPARE(mymuc->getJid(), Jid("mymuc@chat.example.org"));
    QCOMPARE(mymuc->getNick(), QString("myself"));

    /* cleanup */
    xmpp_stanza_release(iq);
    xmpp_ctx_free(ctx);
}

/* for now we do nothing with presence broadcasts for MUCs */
void KwakRosterTest::testUpdateMUCWithPresenceBroadcast()
{
    KwakRoster roster;

    /* MUC */
    BareJid jid(Jid("mymuc@chat.example.org"));
    GroupChat *muc = new GroupChat(jid, QString("myself"));

    /* InboundPresenceStanza */
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_presence_new(ctx);
    xmpp_stanza_set_from(stanza, "mymuc@chat.example.org");
    xmpp_stanza_t *status = xmpp_stanza_new_from_string(ctx,
        "<show>dnd</show>");
    xmpp_stanza_add_child(stanza, status);
    xmpp_stanza_release(status);

    InboundPresenceStanza p(stanza);

    /* update roster */
    roster.add(muc);
    roster.update(&p);

    /* make sure that nothing has been modified */
    RosterItem *item = roster.findMUC(BareJid(Jid("mymuc@chat.example.org")));
    QVERIFY(item != NULL);

    GroupChat *mymuc = qobject_cast<GroupChat *>(item);
    QVERIFY(mymuc != NULL);
    QCOMPARE(mymuc->getJid(), Jid("mymuc@chat.example.org"));
    QCOMPARE(mymuc->getNick(), QString("myself"));

    /* cleanup */
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void KwakRosterTest::testMUCLoadMessages()
{
    KwakRoster roster;
    MessageDB db;
    db.open(":memory:");

    Jid me("me@example.net");
    Jid muc("muc@chat.example.com");
    Jid you("muc@chat.example.com/you");

    TestMUCMessage m1("m1", me, muc, "Hi there",
            QDateTime(QDate(2010, 1, 1), QTime(10, 0, 0), Qt::UTC));
    TestMUCMessage m2("m2", me, muc, "You around?",
            QDateTime(QDate(2010, 1, 1), QTime(10, 1, 0), Qt::UTC));
    TestMUCMessage m3("m3", you, me, "Oh, hi",
            QDateTime(QDate(2010, 1, 1), QTime(10, 2, 0), Qt::UTC));
    TestMUCMessage m4("m4", me, muc, "Sup?",
            QDateTime(QDate(2010, 1, 1), QTime(10, 3, 0), Qt::UTC));

    db.addMessage(m1);
    db.addMessage(m4);
    db.addMessage(m3);
    db.addMessage(m2);

    GroupChat myMUC(muc, "");
    QCOMPARE(myMUC.getMessages().size(), 0);

    myMUC.loadMessages(db);

    QList<KwakMessage *> messages = myMUC.getMessages();
    QCOMPARE(messages.size(), 4);

    QObject *obj = messages.at(0);
    KwakMessage *msg = qobject_cast<KwakMessage *>(obj);
    QCOMPARE(msg->getText(), QString("Hi there"));
    QCOMPARE(msg->getId(), QString("m1"));

    obj = messages.at(1);
    msg = qobject_cast<KwakMessage *>(obj);
    QCOMPARE(msg->getText(), QString("You around?"));
    QCOMPARE(msg->getId(), QString("m2"));

    obj = messages.at(2);
    msg = qobject_cast<KwakMessage *>(obj);
    QCOMPARE(msg->getText(), QString("Oh, hi"));
    QCOMPARE(msg->getId(), QString("m3"));

    obj = messages.at(3);
    msg = qobject_cast<KwakMessage *>(obj);
    QCOMPARE(msg->getText(), QString("Sup?"));
    QCOMPARE(msg->getId(), QString("m4"));
}

void KwakRosterTest::testMUCLoadMessagesEmitsSignal()
{
    BareJid jid(Jid("romeo@example.com"));
    GroupChat c(jid, QString(""));
    MessageDB db;

    QObject::connect(&c, SIGNAL(messagesChanged()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;
    c.loadMessages(db);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testAddMUCEmitsSignal()
{
    KwakRoster roster;

    QObject::connect(&roster, SIGNAL(rosterChanged()),
            this, SLOT(receiveSignal()));

    GroupChat *muc = new GroupChat(Jid("romeo@example.com"), "");

    this->signalReceived = false;
    roster.add(muc);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testGetMUCs()
{
    KwakRoster roster;
    GroupChat *item;
    BareJid jid(Jid("mymuc@chat.example.org"));
    GroupChat *muc = new GroupChat(jid, QString("myself"));

    roster.add(muc);
    QList<GroupChat *> mucs = roster.getMUCs();
    QCOMPARE(mucs.size(), 1);

    item = mucs.at(0);
    QVERIFY(item != NULL);
    QCOMPARE(item->getJid(), Jid("mymuc@chat.example.org"));
    QCOMPARE(item->getNick(), QString("myself"));
}

void KwakRosterTest::testMUCReceiveMessage()
{
    GroupChat muc(BareJid(Jid("chat@muc.example.org")), "");
    QCOMPARE(muc.getMessages().size(), 0);

    KwakMessage *msg = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));
    muc.addMessage(msg);
    QCOMPARE(muc.getMessages().size(), 1);

    QObject *obj = muc.getMessages().at(0);
    QVERIFY(obj != NULL);

    KwakMessage *msgIn = static_cast<KwakMessage *>(obj);
    QVERIFY(msgIn != NULL);

    QCOMPARE(msgIn->getText(), msg->getText());
}

void KwakRosterTest::testMUCReceiveMessageEmitsSignal()
{
    GroupChat muc(BareJid(Jid("chat@muc.example.org")), "");
    QCOMPARE(muc.getMessages().size(), 0);

    KwakMessage *msg = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));

    QObject::connect(&muc, SIGNAL(messagesChanged()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;
    muc.addMessage(msg);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testMUCSubject()
{
    GroupChat muc(BareJid(Jid("chat@muc.example.org")), "");

    QCOMPARE(muc.getSubject(), QString(""));
    muc.setSubject("Space time");
    QCOMPARE(muc.getSubject(), QString("Space time"));
}

void KwakRosterTest::testMUCSubjectChangeEmitsSignal()
{
    GroupChat muc(BareJid(Jid("chat@muc.example.org")), "");

    QObject::connect(&muc, SIGNAL(subjectChanged()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;
    muc.setSubject("Space time");
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testUpdateGroupChatFromChatMessage()
{
    GroupChat muc(BareJid(Jid("chat@muc.example.org")), "");
}

void KwakRosterTest::testPrivateChatConstructor()
{
    Jid jid("coven@chat.shakespeare.lit/secondwitch");
    PrivateChat c(jid);

    QCOMPARE(c.getMUC(), BareJid(Jid("coven@chat.shakespeare.lit")));
    QCOMPARE(QString(c.getName()), QString("secondwitch"));
}

void KwakRosterTest::testRosterAddPrivateChat()
{
    KwakRoster roster;
    Jid jid("mymuc@chat.example.org/user");

    PrivateChat *chat = new PrivateChat(jid);
    roster.add(chat);

    QList<RosterItem *> members = roster.getMembers();
    QCOMPARE(members.size(), 1);
}

void KwakRosterTest::testRosterFindPrivateChat()
{
    KwakRoster roster;
    RosterItem *item;
    PrivateChat *chat = new PrivateChat(Jid("chat@muc.example.org/user"));

    item = roster.findPrivateChat(Jid("chat@muc.example.org/user"));
    QVERIFY(item == NULL);

    roster.add(chat);
    item = roster.findPrivateChat(Jid("chat@muc.example.org/user"));
    QVERIFY(item != NULL);

    Contact *contact = qobject_cast<Contact *>(item);
    QVERIFY(contact == NULL);
    GroupChat *mymuc = qobject_cast<GroupChat *>(item);
    QVERIFY(mymuc == NULL);
    PrivateChat *mychat = qobject_cast<PrivateChat *>(item);
    QVERIFY(mychat != NULL);
    QCOMPARE(mychat->getJid(), Jid("chat@muc.example.org/user"));
    QCOMPARE(mychat->getName(), QString("user"));
}

void KwakRosterTest::testPrivateChatAddMessage()
{
    PrivateChat chat(Jid("chat@muc.example.org/user"));
    QCOMPARE(chat.getMessages().size(), 0);

    KwakMessage *msg = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));
    chat.addMessage(msg);
    QCOMPARE(chat.getMessages().size(), 1);

    QObject *obj = chat.getMessages().at(0);
    QVERIFY(obj != NULL);

    KwakMessage *msgIn = static_cast<KwakMessage *>(obj);
    QVERIFY(msgIn != NULL);

    QCOMPARE(msgIn->getText(), msg->getText());
}

void KwakRosterTest::testPrivateChatAddMessageEmitsSignal()
{
    PrivateChat chat(Jid("chat@muc.example.org/user"));
    QCOMPARE(chat.getMessages().size(), 0);

    KwakMessage *msg = new KwakMessage(QString("ktx72v49"),
            Jid("juliet@example.com/balcony"),
            Jid("romeo@example.com"),
            QString("Art thou not Romeo, and a Montague?"),
            QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));

    QObject::connect(&chat, SIGNAL(messagesChanged()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;
    chat.addMessage(msg);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testAddPrivateChatEmitsSignal()
{
    KwakRoster roster;

    QObject::connect(&roster, SIGNAL(rosterChanged()),
            this, SLOT(receiveSignal()));

    PrivateChat *chat = new PrivateChat(Jid("chat@muc.example.org/user"));

    this->signalReceived = false;
    roster.add(chat);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testMUCInviteConstructor()
{
    BareJid jid(Jid("chat1@muc.example.org"));
    Jid from("test@example.com");
    MUCInvite inv(jid, from, "s3kr1t");

    QCOMPARE(inv.getJid(), Jid("chat1@muc.example.org"));
    QCOMPARE(inv.getFrom(), Jid("test@example.com"));
    QCOMPARE(inv.getPassword(), QString("s3kr1t"));
}

void KwakRosterTest::testRosterAddMUCInvite()
{
    BareJid jid(Jid("chat1@muc.example.org"));
    Jid from("test@example.com");
    MUCInvite *inv = new MUCInvite(jid, from, "");

    KwakRoster roster;
    QCOMPARE(roster.getMembers().size(), 0);

    roster.add(inv);
    QList<RosterItem *> members = roster.getMembers();
    QCOMPARE(members.size(), 1);

    RosterItem *item = members.at(0);
    QVERIFY(item != NULL);

    MUCInvite *inv2 = qobject_cast<MUCInvite*>(item);
    QVERIFY(inv2 != NULL);

    QString muc = inv2->getJid();
    QCOMPARE(muc, QString("chat1@muc.example.org"));
}

void KwakRosterTest::testRosterAddMUCInviteIgnoreDupe()
{
    BareJid jid(Jid("chat1@muc.example.org"));
    Jid from("test@example.com");
    MUCInvite *inv = new MUCInvite(jid, from, "");
    MUCInvite *inv2 = new MUCInvite(jid, from, "");

    KwakRoster roster;
    QCOMPARE(roster.getMembers().size(), 0);

    roster.add(inv);
    roster.add(inv2);
    QList<RosterItem *> members = roster.getMembers();
    QCOMPARE(members.size(), 1);

    RosterItem *item = members.at(0);
    QVERIFY(item != NULL);

    MUCInvite *inv3 = qobject_cast<MUCInvite*>(item);
    QVERIFY(inv3 != NULL);

    QString muc = inv3->getJid();
    QCOMPARE(muc, QString("chat1@muc.example.org"));
}

void KwakRosterTest::testRosterAddMUCInviteEmitsSignal()
{
    KwakRoster roster;

    QObject::connect(&roster, SIGNAL(rosterChanged()),
            this, SLOT(receiveSignal()));

    BareJid jid(Jid("chat1@muc.example.org"));
    Jid from("test@example.com");
    MUCInvite *inv = new MUCInvite(jid, from, "");

    this->signalReceived = false;
    roster.add(inv);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testAcceptMUCInviteEmitsSignal()
{
    BareJid jid(Jid("chat1@muc.example.org"));
    Jid from("test@example.com");
    MUCInvite *inv = new MUCInvite(jid, from, "");

    QObject::connect(inv, SIGNAL(accepted(QString)),
            this, SLOT(receiveSignalWithString(QString)));

    this->signalReceived = false;

    MockSender sender;
    inv->accept(&sender, "Hecate");
    QVERIFY(this->signalReceived);
    QCOMPARE(this->stringReceived, QString("Hecate"));
}

void KwakRosterTest::testRosterAddsMUCForAcceptedMUCInvite()
{
    BareJid jid(Jid("chat1@muc.example.org"));
    Jid from("test@example.com");
    MUCInvite *inv = new MUCInvite(jid, from, "");

    KwakRoster roster;
    QCOMPARE(roster.getMembers().size(), 0);

    roster.add(inv);
    QList<RosterItem *> members = roster.getMembers();
    QCOMPARE(members.size(), 1);

    RosterItem *item = members.at(0);
    QVERIFY(item != NULL);
    GroupChat *c = dynamic_cast<GroupChat *>(item);
    QVERIFY(c == NULL);
    Invite *i = dynamic_cast<Invite *>(item);
    QVERIFY(i != NULL);

    MockSender sender;
    inv->accept(&sender, "Hecate");

    members = roster.getMembers();
    QCOMPARE(members.size(), 1);

    item = members.at(0);
    QVERIFY(item != NULL);
    c = dynamic_cast<GroupChat *>(item);
    QVERIFY(c != NULL);
    QCOMPARE(QString(c->getJid()), QString("chat1@muc.example.org"));
    QCOMPARE(c->getNick(), QString("Hecate"));
    i = dynamic_cast<Invite *>(item);
    QVERIFY(i == NULL);
}

void KwakRosterTest::testRejectMUCInviteEmitsSignal()
{
    BareJid jid(Jid("chat1@muc.example.org"));
    Jid from("test@example.com");
    MUCInvite *inv = new MUCInvite(jid, from, "");

    QObject::connect(inv, SIGNAL(rejected()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;

    inv->reject();
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testRosterRemovesRejectedMUCInvite()
{
    BareJid jid(Jid("chat1@muc.example.org"));
    Jid from("test@example.com");
    MUCInvite *inv = new MUCInvite(jid, from, "");

    KwakRoster roster;
    QCOMPARE(roster.getMembers().size(), 0);

    roster.add(inv);
    QList<RosterItem *> members = roster.getMembers();
    QCOMPARE(members.size(), 1);

    inv->reject();
    members = roster.getMembers();
    QCOMPARE(members.size(), 0);
}

void KwakRosterTest::testChatInviteConstructor()
{
    Jid jid("chat1@muc.example.org");
    ChatInvite inv(jid);

    QCOMPARE(inv.getJid(), jid);
}

void KwakRosterTest::testRosterAddChatInvite()
{
    Jid jid("me@example.com");
    ChatInvite *inv = new ChatInvite(jid);

    KwakRoster roster;
    QCOMPARE(roster.getMembers().size(), 0);

    roster.add(inv);
    QList<RosterItem *> members = roster.getMembers();
    QCOMPARE(members.size(), 1);

    RosterItem *item = members.at(0);
    QVERIFY(item != NULL);

    ChatInvite *inv2 = qobject_cast<ChatInvite*>(item);
    QVERIFY(inv2 != NULL);

    Jid jid2 = inv2->getJid();
    QCOMPARE(jid, jid2);
}

void KwakRosterTest::testRosterAddChatInviteEmitsSignal()
{
    KwakRoster roster;

    QObject::connect(&roster, SIGNAL(rosterChanged()),
            this, SLOT(receiveSignal()));

    Jid jid("me@example.com");
    ChatInvite *inv = new ChatInvite(jid);

    this->signalReceived = false;
    roster.add(inv);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testAcceptChatInviteEmitsSignal()
{
    Jid jid("me@example.com");
    ChatInvite *inv = new ChatInvite(jid);

    QObject::connect(inv, SIGNAL(accepted()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;

    MockSender sender;
    inv->accept(&sender);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testRosterAddsContactForAcceptedChatInvite()
{
    Jid jid("me@example.com");
    ChatInvite *inv = new ChatInvite(jid);

    KwakRoster roster;
    QCOMPARE(roster.getMembers().size(), 0);

    roster.add(inv);
    QList<RosterItem *> members = roster.getMembers();
    QCOMPARE(members.size(), 1);

    RosterItem *item = members.at(0);
    QVERIFY(item != NULL);
    Contact *c = dynamic_cast<Contact *>(item);
    QVERIFY(c == NULL);
    Invite *i = dynamic_cast<Invite *>(item);
    QVERIFY(i != NULL);

    MockSender sender;
    inv->accept(&sender);
    members = roster.getMembers();
    QCOMPARE(members.size(), 1);

    item = members.at(0);
    QVERIFY(item != NULL);
    c = dynamic_cast<Contact *>(item);
    QVERIFY(c != NULL);
    QCOMPARE(QString(c->getJid()), QString("me@example.com"));
    i = dynamic_cast<Invite *>(item);
    QVERIFY(i == NULL);

}

void KwakRosterTest::testRejectChatInviteEmitsSignal()
{
    Jid jid("me@example.com");
    ChatInvite *inv = new ChatInvite(jid);

    QObject::connect(inv, SIGNAL(rejected()),
            this, SLOT(receiveSignal()));

    this->signalReceived = false;

    MockSender sender;
    inv->reject(&sender);
    QVERIFY(this->signalReceived);
}

void KwakRosterTest::testRosterRemovesRejectedChatInvite()
{
    Jid jid("me@example.com");
    ChatInvite *inv = new ChatInvite(jid);

    KwakRoster roster;
    QCOMPARE(roster.getMembers().size(), 0);

    roster.add(inv);
    QList<RosterItem *> members = roster.getMembers();
    QCOMPARE(members.size(), 1);

    MockSender sender;
    inv->reject(&sender);
    members = roster.getMembers();
    QCOMPARE(members.size(), 0);
}

#ifdef BUILD_FOR_QT5
QTEST_GUILESS_MAIN(KwakRosterTest);
#else
QTEST_MAIN(KwakRosterTest);
#endif
