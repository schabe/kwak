#ifndef KWAK_TEST_ROSTER_H
#define KWAK_TEST_ROSTER_H

#include <QObject>

#include <strophe.h>


class KwakRosterTest: public QObject
{
    Q_OBJECT

public slots:
    void receiveSignal();
    void receiveSignalWithString(QString);
private slots:
    void testLoadFromQueryResultNoItems();
    void testLoadFromQueryResultSingleItem();
    void testLoadFromQueryResultMultipleItems();
    void testRepeatedLoadFromQueryResultMultipleItems();
    void testLoadFromQueryResultSingleItemWithoutName();
    void testLoadFromQueryResultSingleItemWithName();
    void testUpdateAddName();
    void testUpdateRemoveName();
    void testUpdateSetEmptyName();
    void testUpdateChangeName();
    void testUpdateKeepName();
    void testUpdateKeepNoName();
    void testUpdateAddMember();
    void testUpdateAddMemberWithName();
    void testUpdateRemoveMissingMember();
    void testUpdateLeaveMUCs();
    void testUpdateDeleteRosterItem();
    void testUpdateExistingItemFromPresence();
    void testReceivePresenceBeforeRoster();
    void testIgnoreUpdateFromUnknownPresence();
    void testSubscribe();
    void testFindContact();

    /* RFC 6121 Roster Item ('Contact') */
    void testContactLoadMessages();
    void testContactLoadMessagesEmitsSignal();
    void testContactConstructor();
    void testContactConstructorWithEmptyName();
    void testContactSetGetName();
    void testContactSetNameEmitsSignal();
    void testContactSetGetStatus();
    void testContactSetStatusEmitsSignal();
    void testContactReceiveMessage();
    void testContactReceiveMessageEmitsSignal();
    void testContactAddMessage();
    void testContactAddMessageEmitsSignal();

    /* RFC 6121 Section 5.1: Lock in on JID */
    void testContactInitialToAddress();
    void testLockContactOnInboundMessage();
    void testUnlockContactAfterMessageFromOtherResource();
    void testUnlockContactAfterPresenceFromOtherResource();

    /* XEP-0045: Multi-User Chat */
    void testGroupChatConstructor();
    void testGroupChatUpdateMessagesFromOtherChat();
    void testRosterAddFindMUC();
    void testRosterDoNotAddDuplicateMUCs();
    void testRosterReplaceMessagesWhenReAddingMUC();
    void testUpdateMUCWithRosterResult();
    void testUpdateMUCWithRosterPush();
    void testUpdateMUCWithPresenceBroadcast();
    void testMUCLoadMessages();
    void testMUCLoadMessagesEmitsSignal();
    void testAddMUCEmitsSignal();
    void testGetMUCs();
    void testMUCReceiveMessage();
    void testMUCReceiveMessageEmitsSignal();
    void testMUCSubject();
    void testMUCSubjectChangeEmitsSignal();
    void testRosterAddPrivateChat();
    void testRosterFindPrivateChat();
    void testUpdateGroupChatFromChatMessage();

    void testPrivateChatConstructor();
    void testPrivateChatAddMessage();
    void testPrivateChatAddMessageEmitsSignal();
    void testAddPrivateChatEmitsSignal();

    /* MUC invite */
    void testMUCInviteConstructor();
    void testRosterAddMUCInvite();
    void testRosterAddMUCInviteIgnoreDupe();
    void testRosterAddMUCInviteEmitsSignal();
    void testAcceptMUCInviteEmitsSignal();
    void testRosterAddsMUCForAcceptedMUCInvite();
    void testRejectMUCInviteEmitsSignal();
    void testRosterRemovesRejectedMUCInvite();

    /* Chat invite */
    void testChatInviteConstructor();
    void testRosterAddChatInvite();
    void testRosterAddChatInviteEmitsSignal();
    void testAcceptChatInviteEmitsSignal();
    void testRosterAddsContactForAcceptedChatInvite();
    void testRejectChatInviteEmitsSignal();
    void testRosterRemovesRejectedChatInvite();
private:
    xmpp_stanza_t *createRosterIQ(xmpp_ctx_t *, const char *type);
    void rosterAdd(xmpp_ctx_t *, xmpp_stanza_t *, const char *jid,
            const char *name);
    void rosterAddRemoved(xmpp_ctx_t *, xmpp_stanza_t *, const char *jid);
    bool signalReceived;
    QString stringReceived;
};

#endif
