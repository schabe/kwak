#ifndef KWAK_TEST_XMPP_H
#define KWAK_TEST_XMPP_H

#include <QObject>

#include <strophe.h>
#include "xmpp/xmpphandler.h"


class TestHandler : public XMPPHandler
{
public:
    TestHandler();
    ~TestHandler();
    void send(xmpp_stanza_t *);
    xmpp_ctx_t* getContext() const;
    xmpp_stanza_t *last;
private:
    xmpp_ctx_t* context;
};

class XMPPTest: public QObject
{
    Q_OBJECT

private slots:
    void testChatMessageFromStanza();
    void testChatMessageFromInvalidStanza();
    void testChatReplyMessageFromStanza();
    void testChatMessageWithDelay();
    void testChatMessageWithNonASCIICharacters();
    void testGroupChatMessageWithDelay();
    void testKwakMessageFromMessageStanza();
    void testDelayedKwakMessageFromMessageStanza();
    void testOutboundChatMessageStanza();

    void testRosterRequest();
    void testRosterResult();
    void testRosterResultCallback();
    void testRosterResultCallbackIgnoresInvalid();
    void testRosterResultFromInvalidStanza();
    void testRosterResultSubscriptionStatus();
    void testRosterSet();
    void testRosterSetFromEmptyStanza();
    void testRosterSetFromEmptyRosterQuery();
    void testRosterSetCallback();
    void testRosterSetCallbackIgnoresInvalid();
    void testRosterSetWithFrom();
    void testRosterSetRemove();

    void testPresenceSubscribeFromStanza();
    void testPresenceSubscribeFromEmptyStanza();
    void testPresenceSubscribeFromStanzaWithoutSender();
    void testPresenceSubscribeFromStanzaWithoutId();
    void testPresenceSubscribeFromToInvite();
    void testPresenceSubscribeToJid();
    void testPresenceSubscribeAccept();
    void testPresenceSubscribeApprove();
    void testPresenceSubscribeDenyStanza();
    void testPresenceSubscribeReject();
    void testPresenceSubscribeCallback();
    void testPresenceSubscribeCallbackIgnoresInvalid();

    void testPresenceOnline();
    void testPresenceOnlineHasCaps();
    void testPresenceAway();
    void testPresenceAwayHasCaps();
    void testPresenceChat();
    void testPresenceDND();
    void testPresenceXA();
    void testPresenceUnavailable();

    void testInboundPresenceOnline();
    void testInboundPresenceAway();
    void testInboundPresenceChat();
    void testInboundPresenceDND();
    void testInboundPresenceXA();
    void testInboundPresenceUnavailable();
    void testInboundPresenceWithId();
    void testEmptyInboundPresence();
    void testInboundPresenceWithoutSender();
    void testInboundPresenceCallback();
    void testInboundPresenceCallbackIgnoresUnknown();

    void testCapabilitiesString();
    void testCapabilitiesHash();
    void testCapabilitiesGetNode();

    void testDefaultMessageCallbackHandlesMessagesWithoutType();
    void testDefaultMessageCallbackHandlesMessagesWithUnknownType();
    void testDefaultMessageCallbackIgnoresUnknownMessageTypes();
    void testNormalMessageCallbackHandlesMessageAsChat();
    void testChatMessageCallbackReceivesMessage();
    void testChatMessageCallbackIgnoresInvalidChatMessage();

    /* XEP-0402: PEP Native Bookmarks */
    void testOutboundBookmarkRequestStanza();
    void testBookmarkResponseStanza();
    void testBookmarkResponseStanzaWithoutAutojoin();
    void testInvalidBookmarkResponseStanzaWithoutPubSubNode();
    void testInvalidBookmarkResponseStanzaWithEmptyPubSubNode();
    void testPubsubCallbackLoadsBookmarks();
    void testPubsubCallbackIgnoreEmptyStanza();
    void testPubsubCallbackIgnoreStanzaWithEmptyPubSubNode();
    void testPubsubCallbackIgnoreStanzaWithoutItemsNode();
    void testPubsubCallbackIgnoreStanzaWithWrongItemsNode();
    void testNewBookmarkNotification();
    void testInvalidNewBookmarkNotification();
    void testHeadlineMessageCallbackLoadsNewBookmarkNotification();
    void testBookmarkRetractionNotification();
    void testInvalidBookmarkRetractionNotification();
    void testHeadlineMessageCallbackLoadsBookmarkRetractionNotification();
    void testOutboundAddBookmarkStanza();

    /* XEP-0030: Service Discovery */
    void testDiscoInfoRequest();
    void testDiscoItemsRequest();
    void testCallbackLoadsDiscoInfoRequest();
    void testCallbackLoadsDiscoItemsRequest();
    void testCallbackIgnoresInvalidDiscoInfoRequest();
    void testCallbackIgnoresInvalidDiscoItemsRequest();
    void testEmptyDiscoInfoRequest();
    void testDiscoInfoRequestWithoutSender();
    void testDiscoInfoRequestWithoutRecipient();
    void testDiscoInfoRequestWithoutId();
    void testEmptyDiscoItemsRequest();
    void testDiscoItemsRequestWithoutSender();
    void testDiscoItemsRequestWithoutId();
    void testDiscoInfoResponse();
    void testDiscoItemsResponse();

    /* XEP-0115: Entity Capabilities */
    void testDiscoInfoNodeRequest();
    void testDiscoInfoNodeResponse();

    /* XEP-0045: Multi-User Chat */
    void testMUCJoinRequest();
    void testMUCMessage();
    void testMUCMessageWithSubject();
    void testMUCMessageWithDelay();
    void testMUCSubjectMessage();
    void testMUCSubjectMessageWithoutId();
    void testInvalidMUCMessage();
    void testInvalidMUCSubjectChange();
    void testGroupchatHandlerLoadsMessage();
    void testGroupchatHandlerLoadsSubjectChangeMessage();
    void testOutboundMUCMessage();
    void testInboundPrivateMessage();
    void testOutboundPrivateMessage();
    void testInvalidPrivateMessageWithoutID();
    void testInvalidPrivateMessageWithoutSender();
    void testMessageHandlerReceivesMUCPrivateMessage();
    void testMessageHandlerIgnoresInvalidMUCPrivateMessage();
    void testMUCPresenceXA();
    void testMUCPresenceOnline();

    /* MUC invite */
    void testLoadMUCInvite();
    void testLoadInvalidMUCInvite();
    void testMessageHandlerLoadsMUCInvite();
    void testLoadInviteFromMUCInvite();
    void testMUCInviteAddsBookmarkOnAccept();

    /* XEP-0249: Direct MUC Invitations */
    void testLoadDirectMUCInvite();
    void testLoadInvalidDirectMUCInvite();
    void testMessageHandlerLoadsDirectMUCInvite();
    void testLoadInviteFromDirectMUCInvite();
private:
    xmpp_stanza_t *stanzaFromFixture(xmpp_ctx_t *, const QString &fn);
};

#endif
