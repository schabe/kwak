HEADERS += \
    test_xmpp.h \
    ../mocksender.h \
    ../../client/account.h \
    ../../client/db/message.h \
    ../../client/db/messagedb.h \
    ../../client/roster/chatinvite.h \
    ../../client/roster/invite.h \
    ../../client/roster/muc.h \
    ../../client/roster/mucinvite.h \
    ../../client/roster/rosteritem.h \
    ../../client/stanza/base/inbound/inboundstanza.h \
    ../../client/stanza/base/inbound/invalidstanzaexception.h \
    ../../client/stanza/base/outbound/outboundstanza.h \
    ../../client/stanza/bookmarks/inbound/bookmarkresponsestanza.h \
    ../../client/stanza/bookmarks/inbound/bookmarkretractionnotificationstanza.h \
    ../../client/stanza/bookmarks/inbound/newbookmarknotificationstanza.h \
    ../../client/stanza/bookmarks/outbound/addbookmarkrequeststanza.h \
    ../../client/stanza/bookmarks/outbound/bookmarkrequeststanza.h \
    ../../client/stanza/disco/inbound/discoinforequeststanza.h \
    ../../client/stanza/disco/inbound/discoitemsrequeststanza.h \
    ../../client/stanza/disco/outbound/discoinforesponsestanza.h \
    ../../client/stanza/disco/outbound/discoitemsresponsestanza.h \
    ../../client/stanza/message/chatmessagestanza.h \
    ../../client/stanza/message/inboundmessagestanza.h \
    ../../client/stanza/message/messagestanza.h \
    ../../client/stanza/muc/invite/directmucinvitestanza.h \
    ../../client/stanza/muc/invite/mediatedmucinvitestanza.h \
    ../../client/stanza/muc/invite/mucinvitestanza.h \
    ../../client/stanza/muc/mucchatmessagestanza.h \
    ../../client/stanza/muc/mucjoinrequeststanza.h \
    ../../client/stanza/muc/mucmessagestanza.h \
    ../../client/stanza/muc/mucpresencestanza.h \
    ../../client/stanza/muc/privatemessagestanza.h \
    ../../client/stanza/muc/subjectchangestanza.h \
    ../../client/stanza/presence/inbound/inboundpresencestanza.h \
    ../../client/stanza/presence/outbound/broadcast/onlinepresencestanza.h \
    ../../client/stanza/presence/outbound/broadcast/presencebroadcaststanza.h \
    ../../client/stanza/presence/outbound/broadcast/presencewithstatusstanza.h \
    ../../client/stanza/presence/outbound/broadcast/unavailablepresencestanza.h \
    ../../client/stanza/presence/outbound/targeted/presencestanza.h \
    ../../client/stanza/roster/outbound/rostergetstanza.h \
    ../../client/stanza/roster/inbound/rosterresultstanza.h \
    ../../client/stanza/roster/inbound/rostersetstanza.h \
    ../../client/stanza/roster/inbound/rosterstanzaitem.h \
    ../../client/stanza/pubsub/inbound/pubsubresultstanza.h \
    ../../client/stanza/subscription/inbound/inboundsubscriptionrequeststanza.h \
    ../../client/stanza/subscription/outbound/subscriptionapprovalstanza.h \
    ../../client/stanza/subscription/outbound/subscriptiondeniedstanza.h \
    ../../client/stanza/subscription/outbound/subscriptionrequeststanza.h \
    ../../client/xmpp/bookmark.h \
    ../../client/xmpp/callbacks.h \
    ../../client/xmpp/capabilities.h \
    ../../client/xmpp/strophehelpers.h \
    ../../client/xmpp/xmpphandler.h \
    ../../client/xmpp/jid/barejid.h \
    ../../client/xmpp/jid/jid.h

SOURCES += \
    test_xmpp.cpp \
    ../mocksender.cpp \
    ../../client/account.cpp \
    ../../client/db/message.cpp \
    ../../client/db/messagedb.cpp \
    ../../client/roster/chatinvite.cpp \
    ../../client/roster/invite.cpp \
    ../../client/roster/muc.cpp \
    ../../client/roster/mucinvite.cpp \
    ../../client/roster/rosteritem.cpp \
    ../../client/stanza/base/inbound/inboundstanza.cpp \
    ../../client/stanza/base/inbound/invalidstanzaexception.cpp \
    ../../client/stanza/bookmarks/inbound/bookmarkresponsestanza.cpp \
    ../../client/stanza/bookmarks/inbound/bookmarkretractionnotificationstanza.cpp \
    ../../client/stanza/bookmarks/inbound/newbookmarknotificationstanza.cpp \
    ../../client/stanza/bookmarks/outbound/addbookmarkrequeststanza.cpp \
    ../../client/stanza/bookmarks/outbound/bookmarkrequeststanza.cpp \
    ../../client/stanza/disco/inbound/discoinforequeststanza.cpp \
    ../../client/stanza/disco/inbound/discoitemsrequeststanza.cpp \
    ../../client/stanza/disco/outbound/discoinforesponsestanza.cpp \
    ../../client/stanza/disco/outbound/discoitemsresponsestanza.cpp \
    ../../client/stanza/message/chatmessagestanza.cpp \
    ../../client/stanza/message/inboundmessagestanza.cpp \
    ../../client/stanza/message/messagestanza.cpp \
    ../../client/stanza/muc/invite/directmucinvitestanza.cpp \
    ../../client/stanza/muc/invite/mediatedmucinvitestanza.cpp \
    ../../client/stanza/muc/invite/mucinvitestanza.cpp \
    ../../client/stanza/muc/mucchatmessagestanza.cpp \
    ../../client/stanza/muc/mucjoinrequeststanza.cpp \
    ../../client/stanza/muc/mucmessagestanza.cpp \
    ../../client/stanza/muc/mucpresencestanza.cpp \
    ../../client/stanza/muc/privatemessagestanza.cpp \
    ../../client/stanza/muc/subjectchangestanza.cpp \
    ../../client/stanza/presence/inbound/inboundpresencestanza.cpp \
    ../../client/stanza/presence/outbound/broadcast/onlinepresencestanza.cpp \
    ../../client/stanza/presence/outbound/broadcast/presencebroadcaststanza.cpp \
    ../../client/stanza/presence/outbound/broadcast/presencewithstatusstanza.cpp \
    ../../client/stanza/presence/outbound/broadcast/unavailablepresencestanza.cpp \
    ../../client/stanza/presence/outbound/targeted/presencestanza.cpp \
    ../../client/stanza/roster/outbound/rostergetstanza.cpp \
    ../../client/stanza/roster/inbound/rosterresultstanza.cpp \
    ../../client/stanza/roster/inbound/rostersetstanza.cpp \
    ../../client/stanza/roster/inbound/rosterstanzaitem.cpp \
    ../../client/stanza/pubsub/inbound/pubsubresultstanza.cpp \
    ../../client/stanza/subscription/inbound/inboundsubscriptionrequeststanza.cpp \
    ../../client/stanza/subscription/outbound/subscriptionapprovalstanza.cpp \
    ../../client/stanza/subscription/outbound/subscriptiondeniedstanza.cpp \
    ../../client/stanza/subscription/outbound/subscriptionrequeststanza.cpp \
    ../../client/xmpp/bookmark.cpp \
    ../../client/xmpp/callbacks.cpp \
    ../../client/xmpp/capabilities.cpp \
    ../../client/xmpp/strophehelpers.cpp \
    ../../client/xmpp/xmpphandler.cpp \
    ../../client/xmpp/jid/barejid.cpp \
    ../../client/xmpp/jid/jid.cpp

LIBS += -lz

QT += sql network widgets

lessThan(QT_MAJOR_VERSION, 5) {
    QT += script
    CONFIG += qtestlib
} else {
    QT += network testlib
    DEFINES += BUILD_FOR_QT5
}

DEFINES += PROJECT_PATH=\"\\\"$${_PRO_FILE_PWD_}/\\\"\"

contains(MEEGO_EDITION,harmattan) {
    LIBS += -lexpat -lresolv \
        $$PWD/../../lib/harmattan/libstrophe.a \
        $$PWD/../../lib/harmattan/libssl.a \
        $$PWD/../../lib/harmattan/libcrypto.a
} else:simulator {
    LIBS += -lexpat -lresolv \
        $$PWD/../../lib/simulator/libstrophe.a \
        $$PWD/../../lib/simulator/libssl.a \
        $$PWD/../../lib/simulator/libcrypto.a
} else {
    LIBS += -lstrophe -lssl -lcrypto
}

INCLUDEPATH += $$PWD/../../include
INCLUDEPATH += $$PWD/../../client

TARGET = xmpp_test
QMAKE_CXXFLAGS += -g
