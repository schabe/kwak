#include "test_xmpp.h"

#include <QFileInfo>
#include <QtTest/QtTest>
#include <QFile>

#include <strophe.h>

#include "../mocksender.h"
#include "account.h"
#include "db/message.h"
#include "stanza/base/inbound/invalidstanzaexception.h"
#include "stanza/bookmarks/inbound/bookmarkresponsestanza.h"
#include "stanza/bookmarks/inbound/bookmarkretractionnotificationstanza.h"
#include "stanza/bookmarks/inbound/newbookmarknotificationstanza.h"
#include "stanza/bookmarks/outbound/addbookmarkrequeststanza.h"
#include "stanza/bookmarks/outbound/bookmarkrequeststanza.h"
#include "stanza/disco/inbound/discoinforequeststanza.h"
#include "stanza/disco/inbound/discoitemsrequeststanza.h"
#include "stanza/disco/outbound/discoinforesponsestanza.h"
#include "stanza/disco/outbound/discoitemsresponsestanza.h"
#include "stanza/message/chatmessagestanza.h"
#include "stanza/message/messagestanza.h"
#include "stanza/muc/invite/directmucinvitestanza.h"
#include "stanza/muc/invite/mediatedmucinvitestanza.h"
#include "stanza/muc/mucjoinrequeststanza.h"
#include "stanza/muc/mucmessagestanza.h"
#include "stanza/muc/mucpresencestanza.h"
#include "stanza/muc/privatemessagestanza.h"
#include "stanza/muc/subjectchangestanza.h"
#include "stanza/presence/inbound/inboundpresencestanza.h"
#include "stanza/presence/outbound/broadcast/presencebroadcaststanza.h"
#include "stanza/roster/outbound/rostergetstanza.h"
#include "stanza/roster/inbound/rosterresultstanza.h"
#include "stanza/roster/inbound/rostersetstanza.h"
#include "stanza/roster/inbound/rosterstanzaitem.h"
#include "stanza/subscription/inbound/inboundsubscriptionrequeststanza.h"
#include "xmpp/capabilities.h"
#include "xmpp/callbacks.h"
#include "xmpp/definitions.h"


class MockWorker : public StanzaReceiver
{
public:
    void handle(const InboundPresenceStanza *b) {
        this->events.append(QString("presence:" + b->getFrom()));
    };
    void handle(const ChatMessageStanza *m) {
        (void) m;
        this->events.append("chatmessage");
    };
    void handle(const InboundNewBookmarkNotificationStanza *msg) {
        Bookmark bookmark = msg->getBookmark();
        Jid jid = bookmark.getJid();
        QString nick = bookmark.getNick();

        if (bookmark.shouldAutojoin()) {
            QString event("bookmark:" + jid + ":" + nick + ":auto");
            this->events.append(event);
        } else {
            QString event("bookmark:" + jid + ":" + nick + ":noauto");
            this->events.append(event);
        }
    };
    void handle(const InboundRosterResultStanza *s) {
        this->events.append(QString("roster:result:") + s->getId());
    };
    void handle(const InboundRosterSetStanza *s) {
        this->events.append(QString("roster:set:") + s->getId());
    };
    void handle(const BookmarkResponseStanza *msg) {
        QList<Bookmark*> bookmarks = msg->getBookmarks();
        QList<Bookmark*>::iterator i;

        for (i = bookmarks.begin(); i != bookmarks.end(); ++i) {
            Bookmark *bookmark = *i;
            Jid jid = bookmark->getJid();
            if (bookmark->shouldAutojoin()) {
                this->events.append(QString("bookmark:" + jid + ":auto"));
            } else {
                this->events.append(QString("bookmark:" + jid + ":noauto"));
            }
        }
    };
    void handle(const InboundBookmarkRetractionNotificationStanza *notification)
    {
        Bookmark bookmark = notification->getBookmark();
        events.append(QString("retraction:" + bookmark.getJid()));
    };
    void handle(const InboundDiscoItemsRequestStanza *request)
    {
        events.append(QString("discoget:" + request->getId()));
    };
    void handle(const MUCChatMessageStanza *msg)
    {
        events.append(QString("groupchat:" + msg->getId()));
    };
    void handle(const MUCSubjectChangeStanza *msg)
    {
        events.append(QString("muc:subject:" + msg->getSubject()));
    };
    void handle(const PrivateMessageStanza *msg)
    {
        events.append(QString("privatemessage:" + msg->getId()));
    };
    void handle(const InboundSubscriptionRequestStanza *req)
    {
        events.append(QString("presence:subscribe:" + req->getFrom()));
    };
    void handle(const MUCInviteStanza *inv)
    {
        events.append(QString("mucinv:" + inv->getMUC()));
    };
    void handle(const InboundDiscoInfoRequestStanza *req)
    {
        events.append(QString("discoinfo:" + req->getId()));
    };
    QList<QString> events;
};

TestHandler::TestHandler()
{
    this->last = NULL;
    this->context = xmpp_ctx_new(NULL, NULL);
}

TestHandler::~TestHandler()
{
    if (this->last != NULL) {
        xmpp_stanza_release(this->last);
    }
    xmpp_ctx_free(this->context);
}

void TestHandler::send(xmpp_stanza_t *stanza)
{
    this->last = xmpp_stanza_clone(stanza);
}

xmpp_ctx_t* TestHandler::getContext() const
{
    return this->context;
}

xmpp_stanza_t * XMPPTest::stanzaFromFixture(xmpp_ctx_t *ctx, const QString &fn)
{
    QString data;
    QString fixture = QString(PROJECT_PATH) + "/fixtures/" + fn;

    QFile f(fixture);
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qWarning() << "Failed to open fixture: " << fixture;
        return NULL;
    }

    while (!f.atEnd()) {
        QByteArray line = f.readLine();
        data += line;
    }

    return xmpp_stanza_new_from_string(ctx, data.toStdString().c_str());
}

/* RFC 6121 Section 5.2.1 Example 1 */
void XMPPTest::testChatMessageFromStanza()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex1.xml");
    ChatMessageStanza msg(stanza);

    QCOMPARE(msg.getFrom(), Jid("juliet@example.com/balcony"));
    QCOMPARE(msg.getTo(), Jid("romeo@example.net"));
    QCOMPARE(msg.getId(), QString("ktx72v49"));
    QCOMPARE(msg.getBody(), QString("Art thou not Romeo, and a Montague?"));

    QDateTime sent = msg.getSent();
    QCOMPARE(sent.isValid(), true);
    QDateTime now = QDateTime::currentDateTime();
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY((now.toTime_t() - sent.toTime_t()) <= 2);
#else
    QVERIFY((now.toSecsSinceEpoch() - sent.toSecsSinceEpoch()) <= 2);
#endif

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testChatMessageFromInvalidStanza()
{

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        ChatMessageStanza msg(stanza);
    } catch (...) {
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* RFC 6121 Section 5.2.1 Example 2 */
void XMPPTest::testChatReplyMessageFromStanza()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex2.xml");
    ChatMessageStanza msg(stanza);

    QCOMPARE(msg.getFrom(), Jid("romeo@example.net/orchard"));
    QCOMPARE(msg.getTo(), Jid("juliet@example.com/balcony"));
    QCOMPARE(msg.getId(), QString("sl3nx51f"));
    QCOMPARE(msg.getBody(), QString("Neither, fair saint, if either thee dislike."));

    QDateTime sent = msg.getSent();
    QCOMPARE(sent.isValid(), true);
    QDateTime now = QDateTime::currentDateTime();
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY((now.toTime_t() - sent.toTime_t()) <= 2);
#else
    QVERIFY((now.toSecsSinceEpoch() - sent.toSecsSinceEpoch()) <= 2);
#endif

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP 0203 Example 1 */
void XMPPTest::testChatMessageWithDelay()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0203-ex1.xml");
    ChatMessageStanza msg(stanza);

    QCOMPARE(msg.getFrom(), Jid("romeo@montague.net/orchard"));
    QCOMPARE(msg.getTo(), Jid("juliet@capulet.com"));
    QCOMPARE(msg.getBody(), QString("O blessed, blessed night!"));

    QDateTime delay = msg.getSent();
    QCOMPARE(delay.isValid(), true);
    QCOMPARE(delay, QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP 0203 Example 1 with some non-ASCII character */
void XMPPTest::testChatMessageWithNonASCIICharacters()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0203-ex1-nonascii.xml");
    ChatMessageStanza msg(stanza);

    QCOMPARE(msg.getFrom(), Jid("romeo@montague.net/orchard"));
    QCOMPARE(msg.getTo(), Jid("juliet@capulet.com"));
    QCOMPARE(msg.getBody(), QString::fromUtf8("Ö blessed, blessed night!"));

    QDateTime delay = msg.getSent();
    QVERIFY(delay.isValid());
    QCOMPARE(delay, QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP 0203 Example 3 */
void XMPPTest::testGroupChatMessageWithDelay()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0203-ex3.xml");
    ChatMessageStanza msg(stanza);

    QCOMPARE(msg.getFrom(), Jid("coven@macbeth.shakespeare.lit/secondwitch"));
    QCOMPARE(msg.getTo(), Jid("macbeth@shakespeare.lit/laptop"));

    QDateTime delay = msg.getSent();
    QVERIFY(delay.isValid());
    QCOMPARE(delay, QDateTime(QDate(2002, 9, 10), QTime(23, 5, 37), Qt::UTC));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testKwakMessageFromMessageStanza()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex1.xml");
    ChatMessageStanza m(stanza);
    KwakMessage msg(m);

    QCOMPARE(msg.getFrom(), Jid("juliet@example.com/balcony"));
    QCOMPARE(msg.getText(), QString("Art thou not Romeo, and a Montague?"));

    QDateTime delay = m.getSent();
    QVERIFY(delay.isValid());

    delay = msg.getSent();
    QVERIFY(delay.isValid());

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testDelayedKwakMessageFromMessageStanza()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0203-ex1.xml");
    ChatMessageStanza m(stanza);
    KwakMessage msg(m);

    QDateTime delay = m.getSent();
    QVERIFY(delay.isValid());
    QCOMPARE(delay, QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));

    delay = msg.getSent();
    QVERIFY(delay.isValid());
    QCOMPARE(delay, QDateTime(QDate(2002, 9, 10), QTime(23, 8, 25), Qt::UTC));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* RFC 6121 Section 5.2.1 Example 1 */
void XMPPTest::testOutboundChatMessageStanza()
{
    TestHandler xmpp;

    Jid from("juliet@example.com/balcony");
    Jid to("romeo@example.net");
    ChatMessageStanza stanza(from, to,
            QString("Art thou not Romeo, and a Montague?"));

    QCOMPARE(stanza.getBody(), QString("Art thou not Romeo, and a Montague?"));
    QCOMPARE(stanza.getTo(), Jid("romeo@example.net"));
    QVERIFY(stanza.getId() != QString(""));
    QCOMPARE(stanza.getId().length(), 36);

    stanza.send(xmpp);

    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_from(xmpp.last), "juliet@example.com/balcony");
    QCOMPARE(xmpp_stanza_get_to(xmpp.last), "romeo@example.net");
    QCOMPARE(xmpp_stanza_get_type(xmpp.last), "chat");
    QCOMPARE(strlen(xmpp_stanza_get_id(xmpp.last)), (unsigned int) 36);
    QCOMPARE(QString(xmpp_stanza_get_id(xmpp.last)), stanza.getId());

    char *body = xmpp_message_get_body(xmpp.last);
    QCOMPARE(body, "Art thou not Romeo, and a Montague?");
    xmpp_free(xmpp_stanza_get_context(xmpp.last), body);
}

/* RFC 6121 Section 2.2 Example 1 */
void XMPPTest::testRosterRequest()
{
    TestHandler xmpp;

    Jid jid("juliet@example.com/balcony");
    OutboundRosterGetStanza req(jid);
    req.send(xmpp);

    QCOMPARE(xmpp_stanza_get_from(xmpp.last), "juliet@example.com/balcony");
    QCOMPARE(xmpp_stanza_get_type(xmpp.last), "get");

    const char *id = xmpp_stanza_get_id(xmpp.last);
    QVERIFY(id != NULL);
    QCOMPARE(strlen(id), (unsigned int) 36);
    QVERIFY(id[0] != '{');

    xmpp_stanza_t *query = xmpp_stanza_get_child_by_name(xmpp.last, "query");
    QVERIFY(query != NULL);
    QCOMPARE(xmpp_stanza_get_ns(query), "jabber:iq:roster");
}

/* RFC 6121 Section 2.2 Example 2 */
void XMPPTest::testRosterResult()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex3.xml");
    InboundRosterResultStanza res(stanza);

    QCOMPARE(res.getId(), QString("hu2bac18"));
    QCOMPARE(res.getTo(), Jid("juliet@example.com/balcony"));

    QList<RosterStanzaItem *> roster = res.getRoster();
    QCOMPARE(roster.length(), 3);

    RosterStanzaItem *row = roster[0];
    QCOMPARE(row->getJid(), BareJid(Jid("romeo@example.net")));
    QCOMPARE(row->getName(), QString("Romeo"));
    QCOMPARE(row->getSubscription(), RosterSubscription::Both);

    row = roster[1];
    QCOMPARE(row->getJid(), BareJid(Jid("mercutio@example.com")));
    QCOMPARE(row->getName(), QString("Mercutio"));
    QCOMPARE(row->getSubscription(), RosterSubscription::From);

    row = roster[2];
    QCOMPARE(row->getJid(), BareJid(Jid("benvolio@example.net")));
    QCOMPARE(row->getName(), QString("Benvolio"));
    QCOMPARE(row->getSubscription(), RosterSubscription::Both);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testRosterResultFromInvalidStanza()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_iq_new(ctx, "result", "roster");
    bool failed = false;

    try {
        InboundRosterResultStanza res(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Roster query not found");
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testRosterResultCallback()
{
    MockWorker worker;

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex3.xml");

    QCOMPARE(handle_iq_roster_result(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("roster:result:hu2bac18"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testRosterResultCallbackIgnoresInvalid()
{
    MockWorker worker;

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    QCOMPARE(handle_iq_roster_result(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testRosterResultSubscriptionStatus()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "roster-result-subscription-status.xml");
    InboundRosterResultStanza res(stanza);

    QCOMPARE(res.getId(), QString("hu2bac18"));
    QCOMPARE(res.getTo(), Jid("juliet@example.com/balcony"));

    QList<RosterStanzaItem *> roster = res.getRoster();
    QCOMPARE(roster.length(), 5);

    RosterStanzaItem *row = roster[0];
    QCOMPARE(row->getJid(), BareJid(Jid("001@example.net")));
    QCOMPARE(row->getSubscription(), RosterSubscription::Both);

    row = roster[1];
    QCOMPARE(row->getJid(), BareJid(Jid("002@example.net")));
    QCOMPARE(row->getSubscription(), RosterSubscription::From);

    row = roster[2];
    QCOMPARE(row->getJid(), BareJid(Jid("003@example.net")));
    QCOMPARE(row->getSubscription(), RosterSubscription::To);

    row = roster[3];
    QCOMPARE(row->getJid(), BareJid(Jid("004@example.net")));
    QCOMPARE(row->getSubscription(), RosterSubscription::None);

    row = roster[4];
    QCOMPARE(row->getJid(), BareJid(Jid("005@example.net")));
    QCOMPARE(row->getSubscription(), RosterSubscription::NA);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* RFC 6121 Section 2.3.2 Example 1 */
void XMPPTest::testRosterSet()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex4.xml");
    InboundRosterSetStanza res(stanza);

    QCOMPARE(res.getId(), QString("a78b4q6ha463"));
    QCOMPARE(res.getFrom(), Jid(""));

    RosterStanzaItem item = res.getItem();
    QCOMPARE(item.getJid(), BareJid(Jid("nurse@example.com")));
    QCOMPARE(item.getName(), QString("Nurse"));
    QCOMPARE(item.getSubscription(), RosterSubscription::None);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testRosterSetFromEmptyStanza()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        InboundRosterSetStanza res(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Roster query not found");
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testRosterSetFromEmptyRosterQuery()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    xmpp_stanza_t *query = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(query, "query");
    xmpp_stanza_set_ns(query, "jabber:iq:roster");
    xmpp_stanza_add_child(stanza, query);
    xmpp_stanza_release(query);

    bool failed = false;

    try {
        InboundRosterSetStanza res(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Empty roster");
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testRosterSetCallback()
{
    MockWorker worker;

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex4.xml");

    QCOMPARE(handle_iq_roster_set(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("roster:set:a78b4q6ha463"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testRosterSetCallbackIgnoresInvalid()
{
    MockWorker worker;

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    QCOMPARE(handle_iq_roster_set(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* RFC 6121 Section 2.3.2 Example 1 (from JID added) */
void XMPPTest::testRosterSetWithFrom()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex4-with-from.xml");
    InboundRosterSetStanza res(stanza);

    QCOMPARE(res.getId(), QString("a78b4q6ha463"));
    QCOMPARE(res.getFrom(), Jid("test@example.net"));

    RosterStanzaItem item = res.getItem();
    QCOMPARE(item.getJid(), BareJid(Jid("nurse@example.com")));
    QCOMPARE(item.getName(), QString("Nurse"));
    QCOMPARE(item.getSubscription(), RosterSubscription::None);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* RFC 6121 Section 2.5.1 Example 1 */
void XMPPTest::testRosterSetRemove()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex5.xml");
    InboundRosterSetStanza res(stanza);

    QCOMPARE(res.getId(), QString("hm4hs97y"));
    QCOMPARE(res.getFrom(), Jid("juliet@example.com/balcony"));

    RosterStanzaItem item = res.getItem();
    QCOMPARE(item.getJid(), BareJid(Jid("nurse@example.com")));
    QCOMPARE(item.getSubscription(), RosterSubscription::Remove);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP 0115 -- Example 3 */
void XMPPTest::testDiscoInfoNodeRequest()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0115-ex1.xml");
    InboundDiscoInfoRequestStanza req(stanza);

    QCOMPARE(req.getId(), QString("disco1"));
    QCOMPARE(req.getFrom(), Jid("juliet@capulet.lit/balcony"));
    QCOMPARE(req.getTo(), Jid("romeo@montague.lit/orchard"));
    QCOMPARE(req.getNode(), QString("http://code.google.com/p/exodus#QgayPKawpkPSDYmwT/WM94uAlu0="));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP 0115 -- Example 4 */
void XMPPTest::testDiscoInfoNodeResponse()
{
    TestHandler xmpp;

    Jid juliet("juliet@capulet.lit/balcony");
    Jid romeo("romeo@montague.lit/orchard");

    QApplication *app = qApp;
    app->setApplicationName("Kwak");
    app->setApplicationVersion("0.0.0");
    app->setProperty("URL", "https://git.ott.net/kwak/");

    Capabilities caps(app);
    OutboundDiscoInfoResponseStanza res("disco1", romeo, juliet, caps,
            caps.getNode());
    res.send(xmpp);

    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_id(xmpp.last), "disco1");
    QCOMPARE(xmpp_stanza_get_from(xmpp.last), "romeo@montague.lit/orchard");
    QCOMPARE(xmpp_stanza_get_to(xmpp.last), "juliet@capulet.lit/balcony");
    QCOMPARE(xmpp_stanza_get_type(xmpp.last), "result");

    xmpp_stanza_t *query = xmpp_stanza_get_child_by_name_and_ns(
            xmpp.last, "query", "http://jabber.org/protocol/disco#info");
    QVERIFY(query != NULL);
    QCOMPARE(xmpp_stanza_get_attribute(query, "node"),
            "https://git.ott.net/kwak/#eDMt7f9FXGWHwPnZYOowUkqBCLY=");

    xmpp_stanza_t *identity = xmpp_stanza_get_child_by_name(query, "identity");
    QVERIFY(identity != NULL);

    QCOMPARE(xmpp_stanza_get_attribute(identity, "category"), "client");
    QCOMPARE(xmpp_stanza_get_attribute(identity, "type"), "phone");
    QCOMPARE(xmpp_stanza_get_attribute(identity, "name"), "Kwak 0.0.0");

    QList<QString> features;

    xmpp_stanza_t *row = xmpp_stanza_get_children(query);
    for (; row != NULL; row = xmpp_stanza_get_next(row)) {
        if (xmpp_stanza_is_tag(row)) {
            if (!strcmp(xmpp_stanza_get_name(row), "feature")) {
                features.append(QString(xmpp_stanza_get_attribute(row, "var")));
            }
        }
    }

    QVERIFY(features.contains("jabber:client"));
    QVERIFY(features.contains("http://jabber.org/protocol/disco#items"));
    QVERIFY(features.contains("jabber:iq:roster"));
    QVERIFY(features.contains("http://jabber.org/protocol/caps"));
    QVERIFY(features.contains("fullunicode"));
    QVERIFY(features.contains("urn:xmpp:bookmarks:1+notify"));
    QVERIFY(features.contains("http://jabber.org/protocol/muc"));
    QVERIFY(features.contains("http://jabber.org/protocol/disco#info"));
    QVERIFY(features.contains("jabber:x:conference"));
    QCOMPARE(features.size(), 9);
}

/* RFC 6121 Section 3.1.2 Example 1 */
void XMPPTest::testPresenceSubscribeFromStanza()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex6.xml");
    InboundSubscriptionRequestStanza request(stanza);

    QCOMPARE(request.getFrom(), BareJid(Jid("romeo@example.net")));
    QCOMPARE(request.getId(), QString("xk3h1v69"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPresenceSubscribeFromEmptyStanza()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        InboundSubscriptionRequestStanza request(stanza);
    } catch (InvalidStanzaException &e) {
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPresenceSubscribeFromStanzaWithoutSender()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(stanza, "presence");
    xmpp_stanza_set_id(stanza, "info4");

    bool failed = false;

    try {
        InboundSubscriptionRequestStanza request(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Stanza sender not found");
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPresenceSubscribeFromStanzaWithoutId()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(stanza, "presence");
    xmpp_stanza_set_from(stanza, "juliet@capulet.com/balcony");

    InboundSubscriptionRequestStanza request(stanza);
    QCOMPARE(request.getFrom(), BareJid(Jid("juliet@capulet.com")));
    QCOMPARE(request.getId(), QString(""));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPresenceSubscribeFromToInvite()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex6.xml");
    InboundSubscriptionRequestStanza request(stanza);
    ChatInvite *invite = request.getInvite();

    QCOMPARE(invite->getJid(), Jid("romeo@example.net"));

    delete(invite);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPresenceSubscribeAccept()
{
    MockSender sender;
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex6.xml");
    InboundSubscriptionRequestStanza request(stanza);
    ChatInvite *invite = request.getInvite();

    QVERIFY(sender.lastSubApproved == NULL);
    QVERIFY(sender.lastSubRequest == NULL);

    invite->accept(&sender);

    QVERIFY(sender.lastSubApproved != NULL);
    Jid to(sender.lastSubApproved->getTo());
    QCOMPARE(QString(to), QString("romeo@example.net"));

    QVERIFY(sender.lastSubRequest != NULL);
    to = Jid(sender.lastSubRequest->getTo());
    QCOMPARE(QString(to), QString("romeo@example.net"));

    delete(invite);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPresenceSubscribeToJid()
{
    TestHandler xmpp;

    Jid romeo("romeo@example.net");
    SubscriptionRequestStanza request(romeo);
    request.send(xmpp);

    QVERIFY(xmpp.last != NULL);

    const char *id = xmpp_stanza_get_id(xmpp.last);
    QVERIFY(id != NULL);
    QCOMPARE(strlen(id), (unsigned int) 36);

    const char *type = xmpp_stanza_get_type(xmpp.last);
    QCOMPARE(type, "subscribe");

    const char *to = xmpp_stanza_get_to(xmpp.last);
    QCOMPARE(to, "romeo@example.net");
}

/* RFC 6121 Section 3.1.4 */
void XMPPTest::testPresenceSubscribeApprove()
{
    TestHandler xmpp;

    Jid romeo("romeo@example.com");
    SubscriptionApprovalStanza res(romeo);
    res.send(xmpp);

    QVERIFY(xmpp.last != NULL);

    const char *id = xmpp_stanza_get_id(xmpp.last);
    QVERIFY(id != NULL);
    QCOMPARE(strlen(id), (unsigned int) 36);

    const char *type = xmpp_stanza_get_type(xmpp.last);
    QCOMPARE(type, "subscribed");

    const char *to = xmpp_stanza_get_to(xmpp.last);
    QCOMPARE(to, "romeo@example.com");
}

/* RFC 6121 Section 3.1.4 */
void XMPPTest::testPresenceSubscribeReject()
{
    MockSender sender;
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex6.xml");
    InboundSubscriptionRequestStanza request(stanza);
    ChatInvite *invite = request.getInvite();

    QVERIFY(sender.lastSubApproved == NULL);
    QVERIFY(sender.lastSubRequest == NULL);

    invite->reject(&sender);

    QVERIFY(sender.lastSubApproved == NULL);
    QVERIFY(sender.lastSubRequest == NULL);
    QVERIFY(sender.lastSubRejected != NULL);

    Jid to(sender.lastSubRejected->getTo());
    QCOMPARE(QString(to), QString("romeo@example.net"));

    delete(invite);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* RFC 6121 Section 3.1.4 */
void XMPPTest::testPresenceSubscribeDenyStanza()
{
    TestHandler xmpp;

    Jid romeo("romeo@example.org");
    SubscriptionDeniedStanza res(romeo);
    res.send(xmpp);

    QVERIFY(xmpp.last != NULL);

    const char *id = xmpp_stanza_get_id(xmpp.last);
    QVERIFY(id != NULL);
    QCOMPARE(strlen(id), (unsigned int) 36);

    const char *type = xmpp_stanza_get_type(xmpp.last);
    QCOMPARE(type, "unsubscribed");

    const char *to = xmpp_stanza_get_to(xmpp.last);
    QCOMPARE(to, "romeo@example.org");
}

void XMPPTest::testPresenceSubscribeCallback()
{
    MockWorker worker;

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex6.xml");

    QCOMPARE(handle_presence(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0),
            QString("presence:subscribe:romeo@example.net"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPresenceSubscribeCallbackIgnoresInvalid()
{
    MockWorker worker;

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(stanza, "presence");
    xmpp_stanza_set_type(stanza, "subscribe");

    QCOMPARE(handle_presence(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* RFC 6121 Section 4.2.1 */
void XMPPTest::testPresenceOnline()
{
    TestHandler xmpp;
    OutboundPresenceBroadcastStanza *stanza =
        OutboundPresenceBroadcastStanza::fromStatus("online");

    stanza->send(xmpp);
    QVERIFY(xmpp.last != NULL);

    const char *id = xmpp_stanza_get_id(xmpp.last);
    QVERIFY(id != NULL);
    QCOMPARE(strlen(id), (unsigned int) 36);

    const char *type = xmpp_stanza_get_type(xmpp.last);
    QVERIFY(type == NULL);

    xmpp_stanza_t *show = xmpp_stanza_get_child_by_name(xmpp.last, "show");
    QVERIFY(show == NULL);

    delete stanza;
}

/* XEP 0115 Section 1.2 */
void XMPPTest::testPresenceOnlineHasCaps()
{
    TestHandler xmpp;

    QApplication *app = qApp;
    app->setProperty("URL", "https://git.ott.net/kwak/");
    app->setApplicationName("Kwak");
    app->setApplicationVersion("0.0.0");

    OutboundPresenceBroadcastStanza *stanza =
        OutboundPresenceBroadcastStanza::fromStatus("online");
    stanza->send(xmpp);

    QVERIFY(xmpp.last != NULL);

    xmpp_stanza_t *caps = xmpp_stanza_get_child_by_name(xmpp.last, "c");
    QVERIFY(caps != NULL);

    const char *xmlns = xmpp_stanza_get_attribute(caps, "xmlns");
    QCOMPARE(xmlns, "http://jabber.org/protocol/caps");

    const char *node = xmpp_stanza_get_attribute(caps, "node");
    QCOMPARE(node, "https://git.ott.net/kwak/");

    const char *hash = xmpp_stanza_get_attribute(caps, "hash");
    QCOMPARE(hash, "sha-1");

    const char *ver = xmpp_stanza_get_attribute(caps, "ver");
    QCOMPARE(ver, "eDMt7f9FXGWHwPnZYOowUkqBCLY=");

    delete stanza;
}

/* RFC 6121 Section 4.5.1 */
void XMPPTest::testPresenceUnavailable()
{
    TestHandler xmpp;
    OutboundPresenceBroadcastStanza *stanza =
        OutboundPresenceBroadcastStanza::fromStatus("offline");
    stanza->send(xmpp);

    QVERIFY(xmpp.last != NULL);

    const char *id = xmpp_stanza_get_id(xmpp.last);
    QVERIFY(id != NULL);
    QCOMPARE(strlen(id), (unsigned int) 36);

    const char *type = xmpp_stanza_get_type(xmpp.last);
    QCOMPARE(type, "unavailable");

    xmpp_stanza_t *show = xmpp_stanza_get_child_by_name(xmpp.last, "show");
    QVERIFY(show == NULL);

    delete stanza;
}

/* RFC 6121 Section 4.7.2.1 -- "away" */
void XMPPTest::testPresenceAway()
{
    TestHandler xmpp;
    OutboundPresenceBroadcastStanza *stanza =
        OutboundPresenceBroadcastStanza::fromStatus("away");
    stanza->send(xmpp);

    QVERIFY(xmpp.last != NULL);

    const char *id = xmpp_stanza_get_id(xmpp.last);
    QVERIFY(id != NULL);
    QCOMPARE(strlen(id), (unsigned int) 36);

    const char *type = xmpp_stanza_get_type(xmpp.last);
    QVERIFY(type == NULL);

    xmpp_stanza_t *show = xmpp_stanza_get_child_by_name(xmpp.last, "show");
    QVERIFY(show != NULL);

    char *text = xmpp_stanza_get_text(show);
    QCOMPARE(text, "away");
    xmpp_free(xmpp_stanza_get_context(show), text);

    delete stanza;
}

/* XEP 0115 Section 1.2 */
void XMPPTest::testPresenceAwayHasCaps()
{
    TestHandler xmpp;
    OutboundPresenceBroadcastStanza *stanza =
        OutboundPresenceBroadcastStanza::fromStatus("away");
    stanza->send(xmpp);

    QVERIFY(xmpp.last != NULL);

    xmpp_stanza_t *caps = xmpp_stanza_get_child_by_name(xmpp.last, "c");
    QVERIFY(caps != NULL);

    const char *xmlns = xmpp_stanza_get_attribute(caps, "xmlns");
    QCOMPARE(xmlns, "http://jabber.org/protocol/caps");

    const char *node = xmpp_stanza_get_attribute(caps, "node");
    QCOMPARE(node, "https://git.ott.net/kwak/");

    const char *hash = xmpp_stanza_get_attribute(caps, "hash");
    QCOMPARE(hash, "sha-1");

    const char *ver = xmpp_stanza_get_attribute(caps, "ver");
    QCOMPARE(ver, "eDMt7f9FXGWHwPnZYOowUkqBCLY=");

    delete stanza;
}

/* RFC 6121 Section 4.7.2.1 -- "chat" */
void XMPPTest::testPresenceChat()
{
    TestHandler xmpp;
    OutboundPresenceBroadcastStanza *stanza =
        OutboundPresenceBroadcastStanza::fromStatus("chat");
    stanza->send(xmpp);

    QVERIFY(xmpp.last != NULL);

    const char *id = xmpp_stanza_get_id(xmpp.last);
    QVERIFY(id != NULL);
    QCOMPARE(strlen(id), (unsigned int) 36);

    const char *type = xmpp_stanza_get_type(xmpp.last);
    QVERIFY(type == NULL);

    xmpp_stanza_t *show = xmpp_stanza_get_child_by_name(xmpp.last, "show");
    QVERIFY(show != NULL);

    char *text = xmpp_stanza_get_text(show);
    QCOMPARE(text, "chat");
    xmpp_free(xmpp_stanza_get_context(show), text);

    delete stanza;
}

/* RFC 6121 Section 4.7.2.1 -- "dnd" */
void XMPPTest::testPresenceDND()
{
    TestHandler xmpp;
    OutboundPresenceBroadcastStanza *stanza =
        OutboundPresenceBroadcastStanza::fromStatus("dnd");
    stanza->send(xmpp);

    QVERIFY(xmpp.last != NULL);

    const char *id = xmpp_stanza_get_id(xmpp.last);
    QVERIFY(id != NULL);
    QCOMPARE(strlen(id), (unsigned int) 36);

    const char *type = xmpp_stanza_get_type(xmpp.last);
    QVERIFY(type == NULL);

    xmpp_stanza_t *show = xmpp_stanza_get_child_by_name(xmpp.last, "show");
    QVERIFY(show != NULL);

    char *text = xmpp_stanza_get_text(show);
    QCOMPARE(text, "dnd");
    xmpp_free(xmpp_stanza_get_context(show), text);

    delete stanza;
}

/* RFC 6121 Section 4.7.2.1 -- "xa" */
void XMPPTest::testPresenceXA()
{
    TestHandler xmpp;
    OutboundPresenceBroadcastStanza *stanza =
        OutboundPresenceBroadcastStanza::fromStatus("xa");
    stanza->send(xmpp);

    QVERIFY(xmpp.last != NULL);

    const char *id = xmpp_stanza_get_id(xmpp.last);
    QVERIFY(id != NULL);
    QCOMPARE(strlen(id), (unsigned int) 36);

    const char *type = xmpp_stanza_get_type(xmpp.last);
    QVERIFY(type == NULL);

    xmpp_stanza_t *show = xmpp_stanza_get_child_by_name(xmpp.last, "show");
    QVERIFY(show != NULL);

    char *text = xmpp_stanza_get_text(show);
    QCOMPARE(text, "xa");
    xmpp_free(xmpp_stanza_get_context(show), text);

    delete stanza;
}

/* RFC 6121 Section 4.2.3 */
void XMPPTest::testInboundPresenceOnline()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex7.xml");
    InboundPresenceStanza broadcast(stanza);

    QCOMPARE(broadcast.getFrom(), Jid("juliet@example.com/balcony"));
    QCOMPARE(broadcast.getTo(), Jid("romeo@example.net"));
    QCOMPARE(broadcast.getStatus(), QString("online"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInboundPresenceAway()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex8.xml");
    InboundPresenceStanza broadcast(stanza);

    QCOMPARE(broadcast.getFrom(), Jid("juliet@example.com/chamber"));
    QCOMPARE(broadcast.getStatus(), QString("away"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInboundPresenceChat()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex8-chat.xml");
    InboundPresenceStanza broadcast(stanza);

    QCOMPARE(broadcast.getFrom(), Jid("juliet@example.com/chamber"));
    QCOMPARE(broadcast.getStatus(), QString("chat"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInboundPresenceDND()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex8-dnd.xml");
    InboundPresenceStanza broadcast(stanza);

    QCOMPARE(broadcast.getFrom(), Jid("juliet@example.com/chamber"));
    QCOMPARE(broadcast.getStatus(), QString("dnd"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInboundPresenceXA()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex8-xa.xml");
    InboundPresenceStanza broadcast(stanza);

    QCOMPARE(broadcast.getFrom(), Jid("juliet@example.com/chamber"));
    QCOMPARE(broadcast.getStatus(), QString("xa"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInboundPresenceUnavailable()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex9.xml");
    InboundPresenceStanza broadcast(stanza);

    QCOMPARE(broadcast.getFrom(), Jid("romeo@example.net/orchard"));
    QCOMPARE(broadcast.getStatus(), QString("offline"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInboundPresenceWithId()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex8.xml");
    InboundPresenceStanza broadcast(stanza);

    QCOMPARE(broadcast.getFrom(), Jid("juliet@example.com/chamber"));
    QCOMPARE(broadcast.getId(), QString("pres1"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testEmptyInboundPresence()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    bool failed = false;

    try {
        InboundPresenceStanza broadcast(stanza);
    } catch (InvalidStanzaException &e) {
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInboundPresenceWithoutSender()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    QCOMPARE(xmpp_stanza_set_name(stanza, "presence"), 0);

    bool failed = false;

    try {
        InboundPresenceStanza broadcast(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Stanza sender not found");
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInboundPresenceCallback()
{
    MockWorker worker;

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex8.xml");

    QCOMPARE(handle_presence(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("presence:juliet@example.com/chamber"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInboundPresenceCallbackIgnoresUnknown()
{
    MockWorker worker;

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    QCOMPARE(handle_presence(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testCapabilitiesString()
{
    QApplication *app = qApp;

    app->setApplicationName("Kwak");
    app->setApplicationVersion("0.0.0");

    Capabilities cap(app);
    QCOMPARE(cap.capString(), QString("client/phone//Kwak 0.0.0<") +
            QString("fullunicode") +
            QString("<http://jabber.org/protocol/caps") +
            QString("<http://jabber.org/protocol/disco#info") +
            QString("<http://jabber.org/protocol/disco#items<") +
            QString("http://jabber.org/protocol/muc<") +
            QString("jabber:client<") +
            QString("jabber:iq:roster<") +
            QString("jabber:x:conference<") +
            QString("urn:xmpp:bookmarks:1+notify<"));
}

void XMPPTest::testCapabilitiesHash()
{
    QApplication *app = qApp;

    app->setApplicationName("Kwak");
    app->setApplicationVersion("0.0.0");

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    Capabilities cap(app);
    QCOMPARE(cap.hash(), QString("eDMt7f9FXGWHwPnZYOowUkqBCLY="));

    xmpp_ctx_free(ctx);
}

void XMPPTest::testCapabilitiesGetNode()
{
    QApplication *app = qApp;

    app->setProperty("URL", "https://git.ott.net/kwak/");

    Capabilities cap(app);
    QCOMPARE(cap.getNode(),
            QString("https://git.ott.net/kwak/#eDMt7f9FXGWHwPnZYOowUkqBCLY="));
}

void XMPPTest::testDefaultMessageCallbackHandlesMessagesWithoutType()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "msg_type_missing.xml");
    QCOMPARE(worker.events.size(), 0);

    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("chatmessage"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testDefaultMessageCallbackHandlesMessagesWithUnknownType()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "msg_type_missing.xml");
    xmpp_stanza_set_type(stanza, "something");
    QCOMPARE(worker.events.size(), 0);

    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("chatmessage"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testDefaultMessageCallbackIgnoresUnknownMessageTypes()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "msg_type_missing.xml");
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_set_type(stanza, STANZA_TYPE_ERROR);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_set_type(stanza, "foo");
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("chatmessage"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testNormalMessageCallbackHandlesMessageAsChat()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "msg_type_missing.xml");
    xmpp_stanza_set_type(stanza, STANZA_TYPE_NORMAL);

    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("chatmessage"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testChatMessageCallbackReceivesMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "rfc-6121-ex1.xml");

    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("chatmessage"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testChatMessageCallbackIgnoresInvalidChatMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0402, Example 4 */
void XMPPTest::testOutboundBookmarkRequestStanza()
{
    TestHandler xmpp;

    OutboundBookmarkRequestStanza req;
    req.send(xmpp);

    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_name(xmpp.last), "iq");
    QCOMPARE(xmpp_stanza_get_type(xmpp.last), "get");
    QCOMPARE(strlen(xmpp_stanza_get_id(xmpp.last)), (unsigned int) 36);

    xmpp_stanza_t *pubsub = xmpp_stanza_get_child_by_name_and_ns(xmpp.last,
        "pubsub", "http://jabber.org/protocol/pubsub");
    QVERIFY(pubsub != NULL);

    xmpp_stanza_t *items = xmpp_stanza_get_child_by_name(pubsub, "items");
    QVERIFY(items != NULL);
    QCOMPARE(xmpp_stanza_get_attribute(items, "node"), "urn:xmpp:bookmarks:1");
}

/* XEP-0402, Example 5 */
void XMPPTest::testBookmarkResponseStanza()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0402-ex5.xml");

    BookmarkResponseStanza response(stanza);
    QCOMPARE(response.getBookmarks().size(), 2);

    Bookmark *bookmark = response.getBookmarks().at(0);
    QCOMPARE(bookmark->getJid(), Jid("theplay@conference.shakespeare.lit"));
    QCOMPARE(bookmark->getNick(), QString("JC"));
    QCOMPARE(bookmark->shouldAutojoin(), true);

    bookmark = response.getBookmarks().at(1);
    QCOMPARE(bookmark->getJid(), Jid("orchard@conference.shakespeare.lit"));
    QCOMPARE(bookmark->getNick(), QString("JC"));
    QCOMPARE(bookmark->shouldAutojoin(), true);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testBookmarkResponseStanzaWithoutAutojoin()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0402-ex5b.xml");

    BookmarkResponseStanza response(stanza);
    QCOMPARE(response.getBookmarks().size(), 2);

    Bookmark *bookmark = response.getBookmarks().at(0);
    QCOMPARE(bookmark->getJid(), Jid("theplay@conference.shakespeare.lit"));
    QCOMPARE(bookmark->shouldAutojoin(), false);

    bookmark = response.getBookmarks().at(1);
    QCOMPARE(bookmark->getJid(), Jid("orchard@conference.shakespeare.lit"));
    QCOMPARE(bookmark->shouldAutojoin(), false);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInvalidBookmarkResponseStanzaWithoutPubSubNode()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        BookmarkResponseStanza response(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Pubsub node not found");
        failed = true;
    }

    QCOMPARE(failed, true);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInvalidBookmarkResponseStanzaWithEmptyPubSubNode()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    xmpp_stanza_t *pubsub = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(pubsub, "pubsub");
    xmpp_stanza_set_ns(pubsub, "http://jabber.org/protocol/pubsub");
    xmpp_stanza_add_child(stanza, pubsub);
    xmpp_stanza_release(pubsub);

    bool failed = false;

    try {
        BookmarkResponseStanza response(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "No items found in pubsub node");
        failed = true;
    }

    QCOMPARE(failed, true);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPubsubCallbackLoadsBookmarks()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0402-ex5.xml");

    QCOMPARE(worker.events.size(), 0);
    handle_iq_pubsub_result(NULL, stanza, &worker);
    QCOMPARE(worker.events.size(), 2);

    QCOMPARE(worker.events.at(0),
            QString("bookmark:theplay@conference.shakespeare.lit:auto"));
    QCOMPARE(worker.events.at(1),
            QString("bookmark:orchard@conference.shakespeare.lit:auto"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPubsubCallbackIgnoreEmptyStanza()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    QCOMPARE(worker.events.size(), 0);
    handle_iq_pubsub_result(NULL, stanza, &worker);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPubsubCallbackIgnoreStanzaWithEmptyPubSubNode()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    xmpp_stanza_t *pubsub = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(pubsub, "pubsub");
    xmpp_stanza_set_ns(pubsub, "http://jabber.org/protocol/pubsub");
    xmpp_stanza_add_child(stanza, pubsub);
    xmpp_stanza_release(pubsub);

    QCOMPARE(worker.events.size(), 0);
    handle_iq_pubsub_result(NULL, stanza, &worker);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPubsubCallbackIgnoreStanzaWithoutItemsNode()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    xmpp_stanza_t *pubsub = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(pubsub, "pubsub");
    xmpp_stanza_set_ns(pubsub, "http://jabber.org/protocol/pubsub");
    xmpp_stanza_add_child(stanza, pubsub);
    xmpp_stanza_release(pubsub);

    xmpp_stanza_t *items = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(items, "items");
    xmpp_stanza_add_child(pubsub, items);
    xmpp_stanza_release(items);

    QCOMPARE(worker.events.size(), 0);
    handle_iq_pubsub_result(NULL, stanza, &worker);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testPubsubCallbackIgnoreStanzaWithWrongItemsNode()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    xmpp_stanza_t *pubsub = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(pubsub, "pubsub");
    xmpp_stanza_set_ns(pubsub, "http://jabber.org/protocol/pubsub");
    xmpp_stanza_add_child(stanza, pubsub);
    xmpp_stanza_release(pubsub);

    xmpp_stanza_t *items = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(items, "items");
    xmpp_stanza_set_attribute(items, "node", "test");
    xmpp_stanza_add_child(pubsub, items);
    xmpp_stanza_release(items);

    QCOMPARE(worker.events.size(), 0);
    handle_iq_pubsub_result(NULL, stanza, &worker);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0402, Example 12 */
void XMPPTest::testNewBookmarkNotification()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0402-ex12.xml");

    InboundNewBookmarkNotificationStanza msg(stanza);
    Bookmark bookmark = msg.getBookmark();

    QCOMPARE(bookmark.getJid(), Jid("theplay@conference.shakespeare.lit"));
    QCOMPARE(bookmark.shouldAutojoin(), true);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInvalidNewBookmarkNotification()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        InboundNewBookmarkNotificationStanza msg(stanza);
    } catch (...) {
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testHeadlineMessageCallbackLoadsNewBookmarkNotification()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0402-ex12.xml");

    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);

    QCOMPARE(worker.events.at(0),
            QString("bookmark:theplay@conference.shakespeare.lit:JC:auto"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0402, Example 13 */
void XMPPTest::testBookmarkRetractionNotification()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0402-ex13.xml");

    InboundBookmarkRetractionNotificationStanza msg(stanza);
    Bookmark bookmark = msg.getBookmark();

    QCOMPARE(bookmark.getJid(), Jid("theplay@conference.shakespeare.lit"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInvalidBookmarkRetractionNotification()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        InboundBookmarkRetractionNotificationStanza msg(stanza);
    } catch (...) {
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testHeadlineMessageCallbackLoadsBookmarkRetractionNotification()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0402-ex13.xml");

    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);

    const QString event = worker.events.at(0);
    QCOMPARE(event, QString("retraction:theplay@conference.shakespeare.lit"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0402 Example 6 */
void XMPPTest::testOutboundAddBookmarkStanza()
{
    TestHandler xmpp;

    Jid jid("orchard@conference.shakespeare.lit");
    GroupChat muc(BareJid(jid), "JC");
    OutboundAddBookmarkRequestStanza req(muc);
    req.send(xmpp);

    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_name(xmpp.last), "iq");
    QCOMPARE(xmpp_stanza_get_type(xmpp.last), "set");
    QCOMPARE(strlen(xmpp_stanza_get_id(xmpp.last)), (unsigned int) 36);

    xmpp_stanza_t *pubsub = xmpp_stanza_get_child_by_name_and_ns(xmpp.last,
        "pubsub", "http://jabber.org/protocol/pubsub");
    QVERIFY(pubsub != NULL);

    xmpp_stanza_t *publish = xmpp_stanza_get_child_by_name(pubsub, "publish");
    QVERIFY(publish != NULL);
    QCOMPARE(xmpp_stanza_get_attribute(publish, "node"),
            "urn:xmpp:bookmarks:1");

    xmpp_stanza_t *item = xmpp_stanza_get_child_by_name(publish, "item");
    QVERIFY(item != NULL);
    QCOMPARE(xmpp_stanza_get_attribute(item, "id"),
            "orchard@conference.shakespeare.lit");

    xmpp_stanza_t *conference = xmpp_stanza_get_child_by_name_and_ns(item,
            "conference", "urn:xmpp:bookmarks:1");
    QVERIFY(conference != NULL);
    QCOMPARE(xmpp_stanza_get_attribute(conference, "autojoin"), "true");

    xmpp_stanza_t *nick = xmpp_stanza_get_child_by_name(conference, "nick");
    QVERIFY(nick != NULL);
    char *text = xmpp_stanza_get_text(nick);
    QVERIFY(text != NULL);
    QCOMPARE(text, "JC");
    free(text);

    xmpp_stanza_t *options = xmpp_stanza_get_child_by_name(pubsub,
            "publish-options");
    QVERIFY(options != NULL);

    xmpp_stanza_t *x = xmpp_stanza_get_child_by_name_and_ns(options, "x",
            "jabber:x:data");
    QCOMPARE(xmpp_stanza_get_attribute(x, "type"), "submit");
    QVERIFY(x != NULL);

    xmpp_stanza_t *field = xmpp_stanza_get_child_by_name(x, "field");
    QVERIFY(field != NULL);
    QCOMPARE(xmpp_stanza_get_attribute(field, "var"), "FORM_TYPE");
    QCOMPARE(xmpp_stanza_get_attribute(field, "type"), "hidden");

    xmpp_stanza_t *value = xmpp_stanza_get_child_by_name(field, "value");
    QVERIFY(value != NULL);
    text = xmpp_stanza_get_text(value);
    QCOMPARE(text, "http://jabber.org/protocol/pubsub#publish-options");
    free(text);

    field = xmpp_stanza_get_next(field);
    QVERIFY(field != NULL);
    QCOMPARE(xmpp_stanza_get_name(field), "field");
    QCOMPARE(xmpp_stanza_get_attribute(field, "var"), "pubsub#persist_items");

    value = xmpp_stanza_get_child_by_name(field, "value");
    QVERIFY(value != NULL);
    text = xmpp_stanza_get_text(value);
    QCOMPARE(text, "true");

    field = xmpp_stanza_get_next(field);
    QVERIFY(field != NULL);
    QCOMPARE(xmpp_stanza_get_name(field), "field");
    QCOMPARE(xmpp_stanza_get_attribute(field, "var"), "pubsub#max_items");

    value = xmpp_stanza_get_child_by_name(field, "value");
    QVERIFY(value != NULL);
    text = xmpp_stanza_get_text(value);
    QCOMPARE(text, "max");

    field = xmpp_stanza_get_next(field);
    QVERIFY(field != NULL);
    QCOMPARE(xmpp_stanza_get_name(field), "field");
    QCOMPARE(xmpp_stanza_get_attribute(field, "var"),
            "pubsub#send_last_published_item");

    value = xmpp_stanza_get_child_by_name(field, "value");
    QVERIFY(value != NULL);
    text = xmpp_stanza_get_text(value);
    QCOMPARE(text, "never");

    field = xmpp_stanza_get_next(field);
    QVERIFY(field != NULL);
    QCOMPARE(xmpp_stanza_get_name(field), "field");
    QCOMPARE(xmpp_stanza_get_attribute(field, "var"), "pubsub#access_model");

    value = xmpp_stanza_get_child_by_name(field, "value");
    QVERIFY(value != NULL);
    text = xmpp_stanza_get_text(value);
    QCOMPARE(text, "whitelist");
}

/* XEP-0030 Example 8 */
void XMPPTest::testDiscoInfoRequest()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0030-ex8.xml");

    InboundDiscoInfoRequestStanza req(stanza);
    QCOMPARE(QString(req.getFrom()), QString("juliet@capulet.com/balcony"));
    QCOMPARE(req.getId(), QString("info4"));
    QCOMPARE(req.getNode(), QString(""));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0030 Example 16 */
void XMPPTest::testDiscoItemsRequest()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0030-ex16.xml");

    InboundDiscoItemsRequestStanza req(stanza);
    QCOMPARE(QString(req.getFrom()), QString("romeo@montague.net/orchard"));
    QCOMPARE(req.getId(), QString("items1"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testCallbackLoadsDiscoInfoRequest()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    MockWorker worker;

    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0030-ex8.xml");

    QCOMPARE(worker.events.size(), 0);
    handle_iq_disco_info(NULL, stanza, &worker);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("discoinfo:info4"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testCallbackLoadsDiscoItemsRequest()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    MockWorker worker;

    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0030-ex16.xml");

    QCOMPARE(worker.events.size(), 0);
    handle_iq_disco_get(NULL, stanza, &worker);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("discoget:items1"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testCallbackIgnoresInvalidDiscoInfoRequest()
{
    MockWorker worker;

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    QCOMPARE(handle_iq_disco_info(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testCallbackIgnoresInvalidDiscoItemsRequest()
{
    MockWorker worker;

    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    QCOMPARE(handle_iq_disco_get(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testEmptyDiscoInfoRequest()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        InboundDiscoInfoRequestStanza req(stanza);
    } catch (InvalidStanzaException &e) {
        failed = true;
    }

    QVERIFY(failed);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testDiscoInfoRequestWithoutSender()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(stanza, "iq");
    QCOMPARE(xmpp_stanza_set_id(stanza, "info4"), 0);
    QCOMPARE(xmpp_stanza_set_to(stanza, "romeo@montague.net/orchard"), 0);

    bool failed = false;

    try {
        InboundDiscoInfoRequestStanza req(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Stanza sender not found");
        failed = true;
    }

    QVERIFY(failed);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testDiscoInfoRequestWithoutRecipient()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(stanza, "iq");
    QCOMPARE(xmpp_stanza_set_id(stanza, "info4"), 0);
    QCOMPARE(xmpp_stanza_set_from(stanza, "juliet@capulet.com/balcony"), 0);

    bool failed = false;

    try {
        InboundDiscoInfoRequestStanza req(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Stanza recipient not found");
        failed = true;
    }

    QVERIFY(failed);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testDiscoInfoRequestWithoutId()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(stanza, "iq");
    QCOMPARE(xmpp_stanza_set_from(stanza, "juliet@capulet.com/balcony"), 0);
    QCOMPARE(xmpp_stanza_set_to(stanza, "romeo@montague.net/orchard"), 0);

    bool failed = false;

    try {
        InboundDiscoInfoRequestStanza req(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Stanza id not found");
        failed = true;
    }

    QVERIFY(failed);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testEmptyDiscoItemsRequest()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        InboundDiscoItemsRequestStanza req(stanza);
    } catch (InvalidStanzaException &e) {
        failed = true;
    }

    QVERIFY(failed);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testDiscoItemsRequestWithoutSender()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(stanza, "iq");
    QCOMPARE(xmpp_stanza_set_id(stanza, "id-1933"), 0);

    bool failed = false;

    try {
        InboundDiscoItemsRequestStanza req(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Stanza sender not found");
        failed = true;
    }

    QVERIFY(failed);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testDiscoItemsRequestWithoutId()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(stanza, "iq");
    QCOMPARE(xmpp_stanza_set_from(stanza, "me@example.org"), 0);

    bool failed = false;

    try {
        InboundDiscoItemsRequestStanza req(stanza);
    } catch (InvalidStanzaException &e) {
        QCOMPARE(e.what(), "Stanza id not found");
        failed = true;
    }

    QVERIFY(failed);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testDiscoInfoResponse()
{
    TestHandler xmpp;

    Jid juliet("juliet@capulet.lit/balcony");
    Jid romeo("romeo@montague.lit/orchard");

    QApplication *app = qApp;
    app->setApplicationName("Kwak");
    app->setApplicationVersion("0.0.0");
    app->setProperty("URL", "https://git.ott.net/kwak/");

    Capabilities caps(app);
    OutboundDiscoInfoResponseStanza res("disco1", romeo, juliet, caps);
    res.send(xmpp);

    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_id(xmpp.last), "disco1");
    QCOMPARE(xmpp_stanza_get_from(xmpp.last), "romeo@montague.lit/orchard");
    QCOMPARE(xmpp_stanza_get_to(xmpp.last), "juliet@capulet.lit/balcony");
    QCOMPARE(xmpp_stanza_get_type(xmpp.last), "result");

    xmpp_stanza_t *query = xmpp_stanza_get_child_by_name_and_ns(
            xmpp.last, "query", "http://jabber.org/protocol/disco#info");
    QVERIFY(query != NULL);
    QVERIFY(xmpp_stanza_get_attribute(query, "node") == NULL);

    xmpp_stanza_t *identity = xmpp_stanza_get_child_by_name(query, "identity");
    QVERIFY(identity != NULL);

    QCOMPARE(xmpp_stanza_get_attribute(identity, "category"), "client");
    QCOMPARE(xmpp_stanza_get_attribute(identity, "type"), "phone");
    QCOMPARE(xmpp_stanza_get_attribute(identity, "name"), "Kwak 0.0.0");
}

/* XEP-0030 Example 17 */
void XMPPTest::testDiscoItemsResponse()
{
    TestHandler xmpp;

    Jid to("romeo@montague.net/orchard");
    QString id("items2");
    OutboundDiscoItemsResponseStanza response(to, id);
    response.send(xmpp);

    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_name(xmpp.last), "iq");
    QCOMPARE(xmpp_stanza_get_type(xmpp.last), "result");
    QCOMPARE(xmpp_stanza_get_to(xmpp.last), "romeo@montague.net/orchard");
    QCOMPARE(xmpp_stanza_get_id(xmpp.last), "items2");

    xmpp_stanza_t *query = xmpp_stanza_get_child_by_name_and_ns(xmpp.last,
            "query", "http://jabber.org/protocol/disco#items");
    QVERIFY(query != NULL);
}

/* XEP-0045 Example 18 */
void XMPPTest::testMUCJoinRequest()
{
    TestHandler xmpp;

    Jid room("coven@chat.shakespeare.lit");
    QString nick("thirdwitch");
    MUCJoinRequestStanza req(room, nick);
    req.send(xmpp);

    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_name(xmpp.last), "presence");
    QCOMPARE(strlen(xmpp_stanza_get_id(xmpp.last)), (unsigned int) 36);
    QCOMPARE(xmpp_stanza_get_to(xmpp.last),
            "coven@chat.shakespeare.lit/thirdwitch");

    xmpp_stanza_t *x = xmpp_stanza_get_child_by_name_and_ns(xmpp.last, "x",
            "http://jabber.org/protocol/muc");
    QVERIFY(x != NULL);

}

/* XEP-0045 Example 45 */
void XMPPTest::testMUCMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex45.xml");

    MUCChatMessageStanza msg(stanza);
    Jid from = msg.getFrom();
    QCOMPARE(QString(from), QString("coven@chat.shakespeare.lit/thirdwitch"));
    QCOMPARE(msg.getTo(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(msg.getId(), QString("hysf1v37"));
    QCOMPARE(msg.getBody(), QString("Harpier cries: 'tis time, 'tis time."));

    QDateTime sent = msg.getSent();
    QVERIFY(sent.isValid());

    QDateTime now = QDateTime::currentDateTime();
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY((now.toTime_t() - sent.toTime_t()) <= 2);
#else
    QVERIFY((now.toSecsSinceEpoch() - sent.toSecsSinceEpoch()) <= 2);
#endif

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0045, Section 7.2.15: only a message that contains a <subject/> but no
 * <body/> element shall be considered a subject change for MUC purposes */
/* XEP-0045 Example 33 */
void XMPPTest::testMUCMessageWithSubject()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex45-modified.xml");

    MUCMessageStanza *mm = MUCMessageStanza::load(stanza);
    QVERIFY(mm != NULL);

    MUCChatMessageStanza *msg = dynamic_cast<MUCChatMessageStanza*>(mm);
    QVERIFY(msg != NULL);

    Jid from = msg->getFrom();
    QCOMPARE(QString(from), QString("coven@chat.shakespeare.lit/thirdwitch"));
    QCOMPARE(msg->getTo(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(msg->getId(), QString("hysf1v37"));
    QCOMPARE(msg->getBody(), QString("Harpier cries: 'tis time, 'tis time."));

    QDateTime sent = msg->getSent();
    QVERIFY(sent.isValid());

    QDateTime now = QDateTime::currentDateTime();
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY((now.toTime_t() - sent.toTime_t()) <= 2);
#else
    QVERIFY((now.toSecsSinceEpoch() - sent.toSecsSinceEpoch()) <= 2);
#endif

    delete(mm);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testMUCMessageWithDelay()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex33.xml");

    MUCChatMessageStanza msg(stanza);
    Jid from = msg.getFrom();
    QCOMPARE(QString(from), QString("coven@chat.shakespeare.lit/firstwitch"));
    QCOMPARE(msg.getTo(), Jid("hecate@shakespeare.lit/broom"));
    QCOMPARE(msg.getId(), QString("162BEBB1-F6DB-4D9A-9BD8-CFDCC801A0B2"));
    QCOMPARE(msg.getBody(), QString("Thrice the brinded cat hath mew'd."));

    QDateTime sent = msg.getSent();
    QVERIFY(sent.isValid());
    QCOMPARE(sent, QDateTime(QDate(2002, 10, 13), QTime(23, 58, 37), Qt::UTC));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testMUCSubjectMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex40.xml");

    MUCSubjectChangeStanza msg(stanza);
    Jid from = msg.getFrom();
    QCOMPARE(QString(from), QString("coven@chat.shakespeare.lit/secondwitch"));
    QCOMPARE(msg.getTo(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(msg.getId(), QString("F437C672-D438-4BD3-9BFF-091050D32EE2"));
    QCOMPARE(msg.getSubject(), QString("Fire Burn and Cauldron Bubble!"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testMUCSubjectMessageWithoutId()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex40-noid.xml");

    MUCSubjectChangeStanza msg(stanza);
    Jid from = msg.getFrom();
    QCOMPARE(QString(from), QString("coven@chat.shakespeare.lit/secondwitch"));
    QCOMPARE(msg.getTo(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(msg.getId(), QString(""));
    QCOMPARE(msg.getSubject(), QString("Fire Burn and Cauldron Bubble!"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInvalidMUCMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        MUCChatMessageStanza msg(stanza);
    } catch (...) {
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInvalidMUCSubjectChange()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        MUCChatMessageStanza msg(stanza);
    } catch (...) {
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testGroupchatHandlerLoadsMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    MockWorker worker;

    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex45.xml");
    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("groupchat:hysf1v37"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testGroupchatHandlerLoadsSubjectChangeMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    MockWorker worker;

    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex40.xml");
    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0),
            QString("muc:subject:Fire Burn and Cauldron Bubble!"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0045 Example 44 */
void XMPPTest::testOutboundMUCMessage()
{
    TestHandler xmpp;
    BareJid jid(Jid("coven@chat.shakespeare.lit"));
    MUCChatMessageStanza msg(jid, "Harpier cries: 'tis time, 'tis time.");
    QCOMPARE(msg.getId().length(), 36);

    msg.send(xmpp);
    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_name(xmpp.last), "message");
    QCOMPARE(strlen(xmpp_stanza_get_id(xmpp.last)), (unsigned int) 36);
    QCOMPARE(QString(xmpp_stanza_get_id(xmpp.last)), msg.getId());
    QCOMPARE(xmpp_stanza_get_to(xmpp.last),
            "coven@chat.shakespeare.lit");
    QCOMPARE(xmpp_stanza_get_type(xmpp.last), "groupchat");

    xmpp_stanza_t *body = xmpp_stanza_get_child_by_name(xmpp.last, "body");
    QVERIFY(body != NULL);
    char *text = xmpp_stanza_get_text(body);
    QVERIFY(text != NULL);
    QCOMPARE(text, "Harpier cries: 'tis time, 'tis time.");
    xmpp_free(xmpp_stanza_get_context(xmpp.last), text);

    QDateTime sent = msg.getSent();
    QVERIFY(sent.isValid());

    QDateTime now = QDateTime::currentDateTime();
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY((now.toTime_t() - sent.toTime_t()) <= 2);
#else
    QVERIFY((now.toSecsSinceEpoch() - sent.toSecsSinceEpoch()) <= 2);
#endif
}

void XMPPTest::testInboundPrivateMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex47.xml");

    PrivateMessageStanza msg(stanza);
    QCOMPARE(msg.getFrom(), Jid("coven@chat.shakespeare.lit/secondwitch"));
    QCOMPARE(msg.getTo(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(msg.getId(), QString("hgn27af1"));
    QCOMPARE(msg.getBody(), QString("I'll give thee a wind."));

    QDateTime sent = msg.getSent();
    QVERIFY(sent.isValid());

    QDateTime now = QDateTime::currentDateTime();
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY((now.toTime_t() - sent.toTime_t()) <= 2);
#else
    QVERIFY((now.toSecsSinceEpoch() - sent.toSecsSinceEpoch()) <= 2);
#endif

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0045 Example 46 */
void XMPPTest::testOutboundPrivateMessage()
{
    TestHandler xmpp;
    Jid jid("coven@chat.shakespeare.lit/firstwitch");
    PrivateMessageStanza msg(jid, "I'll give thee a wind.");
    QCOMPARE(msg.getId().length(), 36);

    msg.send(xmpp);
    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_name(xmpp.last), "message");
    QCOMPARE(strlen(xmpp_stanza_get_id(xmpp.last)), (unsigned int) 36);
    QCOMPARE(xmpp_stanza_get_to(xmpp.last),
            "coven@chat.shakespeare.lit/firstwitch");
    QCOMPARE(xmpp_stanza_get_type(xmpp.last), "chat");
    QCOMPARE(strlen(xmpp_stanza_get_id(xmpp.last)), (unsigned int) 36);
    QCOMPARE(QString(xmpp_stanza_get_id(xmpp.last)), msg.getId());

    xmpp_stanza_t *body = xmpp_stanza_get_child_by_name(xmpp.last, "body");
    QVERIFY(body != NULL);
    char *text = xmpp_stanza_get_text(body);
    QVERIFY(text != NULL);
    QCOMPARE(text, "I'll give thee a wind.");
    xmpp_free(xmpp_stanza_get_context(xmpp.last), text);

    QDateTime sent = msg.getSent();
    QVERIFY(sent.isValid());

    QDateTime now = QDateTime::currentDateTime();
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    QVERIFY((now.toTime_t() - sent.toTime_t()) <= 2);
#else
    QVERIFY((now.toSecsSinceEpoch() - sent.toSecsSinceEpoch()) <= 2);
#endif

    xmpp_stanza_t *x = xmpp_stanza_get_child_by_name_and_ns(xmpp.last,
            "x", "http://jabber.org/protocol/muc#user");
    QVERIFY(x != NULL);
}

void XMPPTest::testInvalidPrivateMessageWithoutID()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex47.xml");

    xmpp_stanza_del_attribute(stanza, "id");
    bool failed = false;
    try {
        PrivateMessageStanza msg(stanza);
    } catch (...) {
        failed = true;
    }
    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testInvalidPrivateMessageWithoutSender()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex47.xml");

    xmpp_stanza_del_attribute(stanza, "from");
    bool failed = false;
    try {
        PrivateMessageStanza msg(stanza);
    } catch (...) {
        failed = true;
    }
    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0045 Section 7.5: Sending a Private Message */
void XMPPTest::testMessageHandlerReceivesMUCPrivateMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex47.xml");

    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("privatemessage:hgn27af1"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0045 Section 7.5: Sending a Private Message */
void XMPPTest::testMessageHandlerIgnoresInvalidMUCPrivateMessage()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);

    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 0);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

/* XEP-0045 Example 54 */
void XMPPTest::testMUCPresenceXA()
{
    TestHandler xmpp;
    Jid to("coven@chat.shakespeare.lit/oldhag");

    MUCPresenceStanza update("xa", GroupChat(BareJid(to), "oldhag"));
    update.send(xmpp);

    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_name(xmpp.last), "presence");
    QCOMPARE(strlen(xmpp_stanza_get_id(xmpp.last)), (unsigned int) 36);
    QCOMPARE(xmpp_stanza_get_to(xmpp.last),
            "coven@chat.shakespeare.lit/oldhag");

    xmpp_stanza_t *show = xmpp_stanza_get_child_by_name(xmpp.last, "show");
    QVERIFY(show != NULL);
    char *text = xmpp_stanza_get_text(show);
    QCOMPARE(text, "xa");
    free(text);
}

void XMPPTest::testMUCPresenceOnline()
{
    TestHandler xmpp;
    Jid to("coven@chat.shakespeare.lit/oldhag");

    MUCPresenceStanza update("online", GroupChat(BareJid(to), "oldhag"));
    update.send(xmpp);

    QVERIFY(xmpp.last != NULL);
    QCOMPARE(xmpp_stanza_get_name(xmpp.last), "presence");
    QCOMPARE(strlen(xmpp_stanza_get_id(xmpp.last)), (unsigned int) 36);
    QCOMPARE(xmpp_stanza_get_to(xmpp.last),
            "coven@chat.shakespeare.lit/oldhag");

    xmpp_stanza_t *show = xmpp_stanza_get_child_by_name(xmpp.last, "show");
    QVERIFY(show == NULL);
}

/* XEP-0045 Example 57 */
void XMPPTest::testLoadMUCInvite()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex57.xml");
    MediatedMUCInviteStanza msg(stanza);

    QCOMPARE(msg.getFrom(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(msg.getTo(), BareJid(Jid("hecate@shakespeare.lit")));
    QCOMPARE(msg.getMUC(), BareJid(Jid("coven@chat.shakespeare.lit")));
    QCOMPARE(msg.getReason(),
            QString("Hey Hecate, this is the place for all good witches!"));
    QCOMPARE(msg.getPassword(), QString("cauldronburn"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testLoadInvalidMUCInvite()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        MediatedMUCInviteStanza msg(stanza);
    } catch (...) {
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testMessageHandlerLoadsMUCInvite()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex57.xml");

    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0), QString("mucinv:coven@chat.shakespeare.lit"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testLoadInviteFromMUCInvite()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0045-ex57.xml");
    MediatedMUCInviteStanza msg(stanza);
    MUCInvite *invite = msg.getInvite();

    QCOMPARE(invite->getFrom(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(invite->getJid(), Jid("coven@chat.shakespeare.lit"));
    QCOMPARE(invite->getPassword(), QString("cauldronburn"));

    delete(invite);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testMUCInviteAddsBookmarkOnAccept()
{
    Jid from("crone1@shakespeare.lit/desktop");
    BareJid muc(Jid("darkcave@macbeth.shakespeare.lit"));
    MUCInvite inv(muc, from, "");

    MockSender sender;
    QVERIFY(sender.lastBookmarkAdded == NULL);

    inv.accept(&sender, QString("Hecate"));
    QVERIFY(sender.lastBookmarkAdded != NULL);
}

/* XEP-0249 Example 1 */
void XMPPTest::testLoadDirectMUCInvite()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0249-ex1.xml");
    DirectMUCInviteStanza msg(stanza);

    QCOMPARE(msg.getFrom(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(msg.getTo(), BareJid(Jid("hecate@shakespeare.lit")));
    QCOMPARE(msg.getMUC(), BareJid(Jid("darkcave@macbeth.shakespeare.lit")));
    QCOMPARE(msg.getReason(),
            QString("Hey Hecate, this is the place for all good witches!"));
    QCOMPARE(msg.getPassword(), QString("cauldronburn"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testLoadInvalidDirectMUCInvite()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = xmpp_stanza_new(ctx);
    bool failed = false;

    try {
        MediatedMUCInviteStanza msg(stanza);
    } catch (...) {
        failed = true;
    }

    QVERIFY(failed);

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testMessageHandlerLoadsDirectMUCInvite()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);

    MockWorker worker;
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0249-ex1.xml");

    QCOMPARE(worker.events.size(), 0);
    QCOMPARE(handle_message(NULL, stanza, &worker), 1);
    QCOMPARE(worker.events.size(), 1);
    QCOMPARE(worker.events.at(0),
            QString("mucinv:darkcave@macbeth.shakespeare.lit"));

    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

void XMPPTest::testLoadInviteFromDirectMUCInvite()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    xmpp_stanza_t *stanza = stanzaFromFixture(ctx, "xep-0249-ex1.xml");
    DirectMUCInviteStanza msg(stanza);
    MUCInvite *invite = msg.getInvite();

    QCOMPARE(invite->getFrom(), Jid("crone1@shakespeare.lit/desktop"));
    QCOMPARE(invite->getJid(), Jid("darkcave@macbeth.shakespeare.lit"));
    QCOMPARE(invite->getPassword(), QString("cauldronburn"));

    delete(invite);
    xmpp_stanza_release(stanza);
    xmpp_ctx_free(ctx);
}

#ifdef BUILD_FOR_QT5
QTEST_GUILESS_MAIN(XMPPTest);
#else
QTEST_MAIN(XMPPTest);
#endif
