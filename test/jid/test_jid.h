#ifndef KWAK_TEST_JID_H
#define KWAK_TEST_JID_H

#include <QObject>


class KwakJidTest: public QObject
{
    Q_OBJECT
private slots:
    void testConstruct();
    void testConstructWithoutResource();
    void testConstructWithBareAndResource();
    void testClone();
    void testConstructEmpty();
    void testConstructNullPointer();
    void testInequality();
    void testEquality();
    void testEmptyBareJid();
    void testIsBare();
    void testGetResourcePart();
    void testBareJidFromFullJid();
    void testBareJidFromBareJid();
};

#endif
