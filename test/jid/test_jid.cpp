#include <QtTest/QtTest>

#include "xmpp/jid/barejid.h"
#include "xmpp/jid/jid.h"

#include "test_jid.h"


void KwakJidTest::testConstruct() {
    Jid jid("me@example.com/foo");
    QCOMPARE(QString(jid), QString("me@example.com/foo"));
}

void KwakJidTest::testConstructWithoutResource() {
    Jid jid("me@example.org");
    QCOMPARE(QString(jid), QString("me@example.org"));
}

void KwakJidTest::testConstructWithBareAndResource() {
    Jid jid(BareJid(Jid("romeo@example.net")), "orchard");
    QCOMPARE(QString(jid), QString("romeo@example.net/orchard"));
}

void KwakJidTest::testClone() {
    Jid jid("me@example.com/foo");
    Jid clone(jid);
    QCOMPARE(QString(clone), QString("me@example.com/foo"));
}

void KwakJidTest::testConstructEmpty() {
    Jid jid("");
    QCOMPARE(QString(jid), QString(""));
}

void KwakJidTest::testConstructNullPointer() {
    Jid jid(NULL);
    QCOMPARE(QString(jid), QString(""));
}

void KwakJidTest::testEquality() {
    Jid jidA("me@example.com/here");
    Jid jidB("me@example.com/here");
    Jid jidC("me@example.com/there");
    QVERIFY(jidA == jidA);
    QVERIFY(jidA == jidB);

    QVERIFY(!(jidA == jidC));
    QVERIFY(!(jidB == jidC));
}

void KwakJidTest::testInequality() {
    Jid jidA("me@example.com/here");
    Jid jidB("me@example.com/here");
    Jid jidC("me@example.com/there");
    QVERIFY(!(jidA != jidA));
    QVERIFY(!(jidA != jidB));

    QVERIFY(jidA != jidC);
    QVERIFY(jidB != jidC);
}

void KwakJidTest::testIsBare() {
    Jid jidA("me@example.com/here");
    Jid jidB("me@example.com");
    Jid jidC("");

    QVERIFY(!jidA.isBare());
    QVERIFY(jidB.isBare());
    QVERIFY(jidC.isBare());
}

void KwakJidTest::testEmptyBareJid()
{
    BareJid empty;
    QCOMPARE(QString(empty), QString(""));
}

void KwakJidTest::testGetResourcePart()
{
    Jid jidA("me@example.com/here");
    Jid jidB("me@example.com");
    Jid jidC("");

    QCOMPARE(jidA.getResourcePart(), QString("here"));
    QCOMPARE(jidB.getResourcePart(), QString(""));
    QCOMPARE(jidC.getResourcePart(), QString(""));
}

void KwakJidTest::testBareJidFromFullJid() {
    Jid full("me@example.com/here");
    BareJid bare(full);
    QCOMPARE(QString(bare), QString("me@example.com"));
}

void KwakJidTest::testBareJidFromBareJid() {
    Jid full("me@example.com/here");
    BareJid bare(full);
    QCOMPARE(QString(bare), QString("me@example.com"));
}

#ifdef BUILD_FOR_QT5
QTEST_GUILESS_MAIN(KwakJidTest);
#else
QTEST_MAIN(KwakJidTest);
#endif
