HEADERS += \
    test_jid.h \
    ../../client/xmpp/jid/barejid.h \
    ../../client/xmpp/jid/jid.h

SOURCES += \
    test_jid.cpp \
    ../../client/xmpp/jid/barejid.cpp \
    ../../client/xmpp/jid/jid.cpp

LIBS += -lz

lessThan(QT_MAJOR_VERSION, 5) {
    CONFIG += qtestlib
} else {
    QT += testlib
}

INCLUDEPATH += $$PWD/../../include
INCLUDEPATH += $$PWD/../../client

TARGET = jid_test
QMAKE_CXXFLAGS += -g
