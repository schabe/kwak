#!/bin/bash

clear

set -e
set -u

if [ -e /usr/bin/qmake6 ] ; then
    QMAKE=/usr/bin/qmake6
elif [ -e /usr/bin/qmake ] ; then
    QMAKE=/usr/bin/qmake
else
    QMAKE=/home/stefan/opt/QtSDK/Desktop/Qt/4.8.1/gcc/bin/qmake
fi

MAKEOPTS="-j3"

${QMAKE} test.pro
make

export DISPLAY=:0
test/account/account_test \
&& test/jid/jid_test \
&& test/messagedb/messagedb_test \
&& test/roster/roster_test \
&& test/view/view_test \
&& test/xmpp/xmpp_test \
&& echo -e "\e[1;32mSUCCESS\e[0;37m"
