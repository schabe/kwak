Kwak
====

Kwak is a simple XMPP client for Meego Harmattan using [libstrophe].


Downloads
---------

Currently there are no pre-built packages available. You can download the source
code via git and build it yourself.


Packaged libraries
------------------

You will find pre-compiled versions of [OpenSSL] and [libstrophe] in the
repository. Since Meego Harmattan does not ship any recent libraries, this
allows Kwak to support modern day TLS encryption and XMPP features.

The repository also includes scripts and documentation to compile these
libraries yourself.


Fonts
-----

Install the [harmoji] package to enable support for displaying emoji.


Project status
--------------

Kwak is still very much incomplete: it can do unencrypted 1:1 messages and group
chats (MUCs) and it uses modern-day encryption (TLS 1.3) to connect to your
server. That's pretty much it.

Most notably, you can *not* modify your roster (add or remove contacts) or join
new MUCs with Kwak at the moment, unless someone else sends you an invite.
End-to-end encryption schemes such as OMEMO as well as support for media-realted
features (audio, video, profile pictures) are also missing; Kwak can only deal
with plain text messages.


XMPP compliance
---------------

The [compliance] document offers a detailed overview of Kwak's compliance with
the XMPP Compliance Suites as published in [XEP-0479]. Briefly put, we currently
support:

* [RFC 6120]: XMPP core features
* [RFC 6121]: XMPP IM core features
* [RFC 7590]: TLS encryption

As well as a bunch of XEPs:

* [XEP-0030]: Feature discovery
* [XEP-0115]: Feature broadcasts
* [XEP-0045]: Multi-User Chat


[libstrophe]: http://strophe.im/libstrophe/
[compliance]: Compliance.txt
[OpenSSL]: https://www.openssl.org/
[harmoji]: https://openrepos.net/content/hamedrepublic/harmoji

[RFC 6120]: https://datatracker.ietf.org/doc/html/rfc6120/
[RFC 6121]: https://datatracker.ietf.org/doc/html/rfc6121/
[RFC 7590]: https://datatracker.ietf.org/doc/html/rfc7590/

[XEP-0030]: https://xmpp.org/extensions/xep-0030.html
[XEP-0045]: https://xmpp.org/extensions/xep-0045.html
[XEP-0115]: https://xmpp.org/extensions/xep-0115.html
[XEP-0479]: https://xmpp.org/extensions/xep-0479.html
