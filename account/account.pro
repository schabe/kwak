qml.source = qml/account
qml.target = qml
DEPLOYMENTFOLDERS += qml

TEMPLATE = app

QT += declarative dbus core
CONFIG += meegotouch link_pkgconfig
CONFIG += qdeclarative-boostable

#PKGCONFIG += accounts-qt \
#             AccountSetup

TARGET = kwakplugin
SOURCES += main.cpp
#HEADERS += qmlapplicationviewer/qmlapplicationviewer.h

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

invoker.files = invoker/*
invoker.path = /usr/lib/AccountSetup
INSTALLS += invoker

acocuntprovider.files = kwak.provider
acocuntprovider.path = /usr/share/accounts/providers/
INSTALLS += acocuntprovider

accounticon.files = icon-m-service-kwak.png
accounticon.path = /usr/share/themes/blanco/meegotouch/icons/
INSTALLS += accounticon

serviceprovider.files = kwak.service
serviceprovider.path = /usr/share/accounts/services/
INSTALLS += serviceprovider

contains(MEEGO_EDITION,harmattan) {
    target.path = /usr/lib/AccountSetup/bin
    INSTALLS += target
}
