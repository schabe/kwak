/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "directmucinvitestanza.h"

#include <strophe.h>

#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"
#include "xmpp/jid/barejid.h"
#include "xmpp/jid/jid.h"


DirectMUCInviteStanza::DirectMUCInviteStanza(xmpp_stanza_t *stanza)
{
    /* from: required */
    const char *from = xmpp_stanza_get_from(stanza);
    if (!from) {
        throw 1;
    }
    this->from = Jid(from);

    /* to: optional */
    const char *to = xmpp_stanza_get_to(stanza);
    if (to) {
        this->to = BareJid(Jid(to));
    }

    /* x element contains the MUC details -> required */
    xmpp_stanza_t *x = xmpp_stanza_get_x_conference(stanza);
    if (!x) {
        throw 2;
    }

    /* MUC: required */
    const char *muc = xmpp_stanza_get_attribute(x, STANZA_ATTR_JID);
    if (!muc) {
        throw 3;
    }
    this->muc = BareJid(Jid(muc));

    /* reason: optional */
    const char *reason = xmpp_stanza_get_attribute(x, STANZA_ATTR_REASON);
    if (reason) {
        this->reason = QString(reason);
    }

    /* password: optional */
    const char *password = xmpp_stanza_get_attribute(x, STANZA_ATTR_PASSWORD);
    if (password) {
        this->password = QString(password);
    }
}
