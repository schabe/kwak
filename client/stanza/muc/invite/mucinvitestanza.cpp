/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mucinvitestanza.h"

#include <QString>

#include "xmpp/jid/barejid.h"
#include "xmpp/jid/jid.h"


Jid MUCInviteStanza::getFrom() const
{
    return Jid(this->from);
}

BareJid MUCInviteStanza::getTo() const
{
    return BareJid(this->to);
}

BareJid MUCInviteStanza::getMUC() const
{
    return BareJid(this->muc);
}

QString MUCInviteStanza::getReason() const
{
    return QString(this->reason);
}

QString MUCInviteStanza::getPassword() const
{
    return QString(this->password);
}

MUCInvite* MUCInviteStanza::getInvite() const
{
    return new MUCInvite(this->muc, this->from, this->password);
}

void MUCInviteStanza::receive(StanzaReceiver *handler)
{
    handler->handle(this);
}
