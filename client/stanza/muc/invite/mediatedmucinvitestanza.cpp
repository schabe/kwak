/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mediatedmucinvitestanza.h"

#include <stdlib.h> // for 'free'

#include "xmpp/definitions.h"
#include "xmpp/jid/barejid.h"
#include "xmpp/jid/jid.h"
#include "xmpp/strophehelpers.h"


namespace
{
xmpp_stanza_t *xmpp_stanza_get_reason(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name(stanza, STANZA_NAME_REASON);
}

xmpp_stanza_t *xmpp_stanza_get_password(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name(stanza, STANZA_NAME_PASSWORD);
}
}


MediatedMUCInviteStanza::MediatedMUCInviteStanza(xmpp_stanza_t *stanza)
{
    /* to: optional */
    const char *to = xmpp_stanza_get_to(stanza);
    this->to = BareJid(Jid(to));

    /* MUC: required */
    const char *muc = xmpp_stanza_get_from(stanza);
    this->muc = BareJid(Jid(muc));

    /* x element contains the invite */
    xmpp_stanza_t *x = xmpp_stanza_get_x_muc_user(stanza);
    if (!x) {
        throw 1;
    }

    /* invite element contains sender and messages */
    xmpp_stanza_t *inv = xmpp_stanza_get_invite(x);
    if (!inv) {
        throw 2;
    }

    /* from: required */
    const char *from = xmpp_stanza_get_from(inv);
    this->from = Jid(from);

    /* reason: optional */
    xmpp_stanza_t *rsn = xmpp_stanza_get_reason(inv);
    if (rsn) {
        char *reason = xmpp_stanza_get_text(rsn);
        this->reason = QString(reason);
        free(reason);
    }

    /* password: optional */
    xmpp_stanza_t *pw = xmpp_stanza_get_password(x);
    if (pw) {
        char *password = xmpp_stanza_get_text(pw);
        this->password = QString(password);
        free(password);
    }
}
