/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MUCINVITESTANZA_H
#define MUCINVITESTANZA_H

#include <strophe.h>

#include "roster/mucinvite.h"
#include "stanza/message/inboundmessagestanza.h"
#include "xmpp/jid/barejid.h"
#include "xmpp/jid/jid.h"


class MUCInviteStanza : public InboundMessageStanza
{
public:
    Jid getFrom() const;
    BareJid getTo() const;
    BareJid getMUC() const;
    QString getReason() const;
    QString getPassword() const;
    MUCInvite* getInvite() const;
    void receive(StanzaReceiver *);
protected:
    Jid from;
    BareJid to;
    BareJid muc;
    QString reason;
    QString password;
};

#endif // MUCINVITESTANZA_H
