/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PRIVATEMESSAGESTANZA_H
#define PRIVATEMESSAGESTANZA_H

#include <QDateTime>
#include <QString>
#include <strophe.h>

#include "stanza/base/outbound/outboundstanza.h"
#include "stanza/message/inboundmessagestanza.h"
#include "xmpp/jid/jid.h"


class PrivateMessageStanza : public OutboundStanza, public InboundMessageStanza
{
public:
    explicit PrivateMessageStanza(xmpp_stanza_t *);
    PrivateMessageStanza(const Jid &to, const QString &body);
    virtual ~PrivateMessageStanza();
    virtual void send(XMPPHandler &);
    void receive(StanzaReceiver *);
    QDateTime getSent() const;
    QString getBody() const;
    QString getId() const;
    Jid getFrom() const;
    Jid getTo() const;
private:
    QDateTime sent;
    QString body;
    QString id;
    Jid from;
    Jid to;
};

#endif // PRIVATEMESSAGESTANZA_H
