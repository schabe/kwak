/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mucchatmessagestanza.h"

#include <QDebug>

#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"


namespace {
xmpp_stanza_t *xmpp_groupchat_new(xmpp_ctx_t *ctx, QString to, QString id)
{
    return xmpp_message_new_q(ctx, STANZA_TYPE_GROUPCHAT, to, id);
}
}


MUCChatMessageStanza::MUCChatMessageStanza(xmpp_stanza_t *stanza)
    : MUCMessageStanza(stanza)
{
    char *body = xmpp_message_get_body(stanza);
    if (!body) {
        qWarning() << "Message body not found";
        throw 1;
    }

    this->body = QString::fromUtf8(body);
    xmpp_free(xmpp_stanza_get_context(stanza), body);
}

MUCChatMessageStanza::MUCChatMessageStanza(const BareJid &to,
                                           const QString &body)
    : MUCMessageStanza(to)
{
    this->body = QString(body);
    this->sent = QDateTime::currentDateTime();
}

void MUCChatMessageStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}

void MUCChatMessageStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();

    xmpp_stanza_t *msg = xmpp_groupchat_new(ctx, this->to, this->id);
    xmpp_stanza_t *body = xmpp_body_new(ctx);
    xmpp_stanza_add_text(body, this->body);
    xmpp_stanza_adopt_child(msg, body);

    xmpp.send(msg);
    xmpp_stanza_release(msg);
}
