/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mucpresencestanza.h"

#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"


namespace
{
xmpp_stanza_t *xmpp_show_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *show = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(show, STANZA_NAME_SHOW);
    return show;
}
}


MUCPresenceStanza::MUCPresenceStanza(const QString &status, const GroupChat &muc)
{
    this->status = QString(status);
    this->to = Jid(muc.getJid(), muc.getNick());
}

void MUCPresenceStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();

    xmpp_stanza_t *response = xmpp_presence_new_with_random_id(ctx);
    xmpp_stanza_set_to_q(response, this->to);

    if (this->status != STATUS_ONLINE) {
        xmpp_stanza_t *show = xmpp_show_new(ctx);
        xmpp_stanza_add_text(show, this->status);
        xmpp_stanza_adopt_child(response, show);
    }

    xmpp.send(response);
    xmpp_stanza_release(response);
}
