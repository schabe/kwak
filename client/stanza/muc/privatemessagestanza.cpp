/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "privatemessagestanza.h"

#include <stdlib.h>

#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"
#include "xmpp/xmpphandler.h"


namespace
{
xmpp_stanza_t *xmpp_x_user_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *x = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(x, STANZA_NAME_X);
    xmpp_stanza_set_ns(x, STANZA_NS_MUC_USER);
    return x;
}
}


PrivateMessageStanza::~PrivateMessageStanza()
{
}

PrivateMessageStanza::PrivateMessageStanza(xmpp_stanza_t *msg)
{
    /* id: required */
    const char *id = xmpp_stanza_get_id(msg);
    if (!id) {
        throw 1;
    }
    this->id = QString(id);

    /* from: required */
    const char *from = xmpp_stanza_get_from(msg);
    if (!from) {
        throw 2;
    }
    this->from = Jid(from);

    /* to: optional */
    const char *to = xmpp_stanza_get_to(msg);
    if (to) {
        this->to = Jid(to);
    }

    /* body: may be empty */
    char *body = xmpp_message_get_body(msg);
    this->body = QString::fromUtf8(body);
    xmpp_free(xmpp_stanza_get_context(msg), body);

    /* sent: now */
    this->sent = QDateTime::currentDateTime();
}

PrivateMessageStanza::PrivateMessageStanza(const Jid &to, const QString &body)
{
    this->to = Jid(to);
    this->body = QString(body);
    this->sent = QDateTime::currentDateTime();

    /* generate id */
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    char *id = xmpp_uuid_gen(ctx);
    this->id = QString(id);
    xmpp_free(ctx, id);
    xmpp_ctx_free(ctx);
}

void PrivateMessageStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();
    xmpp_stanza_t *msg = xmpp_message_new_q(ctx, STANZA_TYPE_CHAT, to,
                                            this->id);

    xmpp_stanza_t *body = xmpp_body_new(ctx);
    xmpp_stanza_add_text(body, this->body);
    xmpp_stanza_adopt_child(msg, body);

    xmpp_stanza_t *x = xmpp_x_user_new(ctx);
    xmpp_stanza_adopt_child(msg, x);

    xmpp.send(msg);
    xmpp_stanza_release(msg);

    /* sent: now */
    this->sent = QDateTime::currentDateTime();
}

void PrivateMessageStanza::receive(StanzaReceiver *handler)
{
    handler->handle(this);
}

QDateTime PrivateMessageStanza::getSent() const
{
    return this->sent;
}

QString PrivateMessageStanza::getBody() const
{
    return this->body;
}

QString PrivateMessageStanza::getId() const
{
    return this->id;
}

Jid PrivateMessageStanza::getFrom() const
{
    return this->from;
}

Jid PrivateMessageStanza::getTo() const
{
    return this->to;
}
