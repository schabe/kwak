/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mucmessagestanza.h"

#include <QDebug>
#include <stdlib.h>

#include "stanza/muc/mucchatmessagestanza.h"
#include "stanza/muc/subjectchangestanza.h"
#include "xmpp/strophehelpers.h"


MUCMessageStanza::MUCMessageStanza(xmpp_stanza_t *stanza)
    : MessageStanza(stanza)
{
}

MUCMessageStanza::MUCMessageStanza(const BareJid &to)
{
    this->to = Jid(to);

    /* generate id */
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    char *id = xmpp_uuid_gen(ctx);
    this->id = QString(id);
    xmpp_free(ctx, id);
    xmpp_ctx_free(ctx);
}

MUCMessageStanza *MUCMessageStanza::load(xmpp_stanza_t *s)
{
    /* A message with a <body> is either a room message or a private message */
    char *body = xmpp_message_get_body(s);
    if (body) {
        MUCChatMessageStanza *msg = new MUCChatMessageStanza(s);
        xmpp_free(xmpp_stanza_get_context(s), body);
        return msg;
    }

    /* XEP-0045 Section 7.2.15: Only a message that contains a <subject/> but
     * no <body/> element shall be considered a subject change */
    xmpp_stanza_t *subj = xmpp_stanza_get_subject(s);
    if (subj) {
        return new MUCSubjectChangeStanza(s);
    }

    qWarning() << "Unknown MUC message received";
    throw 1;
}

