/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MUCCHATMESSAGESTANZA_H
#define MUCCHATMESSAGESTANZA_H

#include <strophe.h>

#include "stanza/receiver.h"
#include "stanza/base/outbound/outboundstanza.h"
#include "stanza/muc/mucmessagestanza.h"
#include "xmpp/jid/barejid.h"


class MUCChatMessageStanza : public MUCMessageStanza, public OutboundStanza
{
public:
    MUCChatMessageStanza(xmpp_stanza_t *);
    MUCChatMessageStanza(const BareJid &to, const QString &body);
    virtual void send(XMPPHandler &);
    void receive(StanzaReceiver *);
};

#endif // MUCCHATMESSAGESTANZA_H
