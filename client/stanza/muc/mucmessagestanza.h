/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MUCMESSAGESTANZA_H
#define MUCMESSAGESTANZA_H

#include <strophe.h>

#include "stanza/message/inboundmessagestanza.h"
#include "stanza/message/messagestanza.h"
#include "xmpp/jid/barejid.h"


/* forward declaration */
class StanzaReceiver;


/* XEP-0045: Multi-User Chat */
class MUCMessageStanza : public InboundMessageStanza, public MessageStanza
{
public:
    explicit MUCMessageStanza(xmpp_stanza_t *);
    explicit MUCMessageStanza(const BareJid &to);
    virtual ~MUCMessageStanza() {}
    static MUCMessageStanza *load(xmpp_stanza_t *);
    virtual void receive(StanzaReceiver *) = 0;
};

#endif // MUCMESSAGESTANZA_H
