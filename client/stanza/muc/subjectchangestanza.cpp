/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "subjectchangestanza.h"

#include <QDebug>

#include <strophe.h>

#include "xmpp/strophehelpers.h"


MUCSubjectChangeStanza::MUCSubjectChangeStanza(xmpp_stanza_t *s)
    : MUCMessageStanza(s)
{
    xmpp_stanza_t *subj = xmpp_stanza_get_subject(s);
    if (!subj) {
        qWarning() << "Message subject not found";
        throw 1;
    }

    char *body = xmpp_stanza_get_text(subj);
    if (body) {
        this->subject = QString::fromUtf8(body);
        xmpp_free(xmpp_stanza_get_context(s), body);
    }
}

void MUCSubjectChangeStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}

QString MUCSubjectChangeStanza::getSubject() const
{
    return this->subject;
}
