/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mucjoinrequeststanza.h"

#include "xmpp/definitions.h"
#include "xmpp/jid/barejid.h"
#include "xmpp/strophehelpers.h"


namespace
{
xmpp_stanza_t *xmpp_x_muc_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *x = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(x, STANZA_NAME_X);
    xmpp_stanza_set_ns(x, STANZA_NS_MUC);
    return x;
}
}


MUCJoinRequestStanza::MUCJoinRequestStanza(Jid &room, QString &nick)
{
    this->muc = Jid(BareJid(room) + "/" + nick);
}

void MUCJoinRequestStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();

    xmpp_stanza_t *presence = xmpp_presence_new_with_random_id(ctx);
    xmpp_stanza_set_to_q(presence, this->muc);

    xmpp_stanza_t *x = xmpp_x_muc_new(ctx);
    xmpp_stanza_adopt_child(presence, x);

    xmpp.send(presence);
    xmpp_stanza_release(presence);
}
