/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pubsubresultstanza.h"

#include <QString>

#include <strophe.h>

#include "stanza/base/inbound/inboundstanza.h"
#include "stanza/bookmarks/inbound/bookmarkresponsestanza.h"
#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"


InboundStanza* InboundPubSubResultStanza::load(xmpp_stanza_t *stanza)
{
    xmpp_stanza_t *pubsub = xmpp_stanza_get_pubsub(stanza);
    if (!pubsub) {
        return NULL;
    }

    xmpp_stanza_t *items = xmpp_stanza_get_items(pubsub);
    if (!items) {
        return NULL;
    }

    const char *node = xmpp_stanza_get_node(items);
    if (!node) {
        return NULL;
    }

    if (QString(node) == STANZA_NS_BOOKMARKS) {
        return new BookmarkResponseStanza(stanza);
    }

    return NULL;
}
