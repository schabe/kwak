/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rostergetstanza.h"

#include <strophe.h>

#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"
#include "xmpp/jid/jid.h"


namespace
{
xmpp_stanza_t *xmpp_roster_query_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *query = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(query, STANZA_NAME_QUERY);
    xmpp_stanza_set_ns(query, XMPP_NS_ROSTER);
    return query;
}
}


OutboundRosterGetStanza::OutboundRosterGetStanza(Jid &from)
{
    this->from = Jid(from);
}

void OutboundRosterGetStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();

    xmpp_stanza_t *iq = xmpp_iq_new_with_random_id(ctx, STANZA_TYPE_GET);
    xmpp_stanza_set_from_q(iq, this->from);

    xmpp_stanza_t *query = xmpp_roster_query_new(ctx);
    xmpp_stanza_adopt_child(iq, query);

    xmpp.send(iq);
    xmpp_stanza_release(iq);
}
