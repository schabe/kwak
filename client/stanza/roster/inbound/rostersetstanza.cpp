/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rostersetstanza.h"

#include "stanza/base/inbound/invalidstanzaexception.h"
#include "xmpp/strophehelpers.h"
#include "xmpp/jid/jid.h"


InboundRosterSetStanza::InboundRosterSetStanza(xmpp_stanza_t* const stanza)
{
    this->id = xmpp_stanza_get_id(stanza);
    this->from = Jid(xmpp_stanza_get_from(stanza));

    xmpp_stanza_t *roster = xmpp_stanza_get_roster_query(stanza);
    if (!roster) {
        throw InvalidStanzaException("Roster query not found");
    }

    xmpp_stanza_t *item = xmpp_stanza_get_children(roster);
    if (!item) {
        throw InvalidStanzaException("Empty roster");
    }

    this->rosterItem = RosterStanzaItem(item);
}

RosterStanzaItem InboundRosterSetStanza::getItem() const
{
    return rosterItem;
}

QString InboundRosterSetStanza::getId() const
{
    return this->id;
}

Jid InboundRosterSetStanza::getFrom() const
{
    return this->from;
}

void InboundRosterSetStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}
