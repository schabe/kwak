/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rosterstanzaitem.h"

#include "xmpp/definitions.h"
#include "xmpp/jid/barejid.h"


enum RosterSubscription::type RosterStanzaItem::getSubscription() const {
    return subscription;
}

RosterStanzaItem::RosterStanzaItem()
{
}

RosterStanzaItem::RosterStanzaItem(xmpp_stanza_t* const stanza)
{
    Jid full(xmpp_stanza_get_attribute(stanza, STANZA_ATTR_JID));
    this->jid = BareJid(full);
    this->name = xmpp_stanza_get_attribute(stanza, STANZA_ATTR_NAME);
    QString sub(xmpp_stanza_get_attribute(stanza, STANZA_ATTR_SUBSCRIPTION));

    if (sub == SUB_BOTH) {
        subscription = RosterSubscription::Both;
    } else if (sub == SUB_FROM) {
        subscription = RosterSubscription::From;
    } else if (sub == SUB_TO) {
        subscription = RosterSubscription::To;
    } else if (sub == SUB_NONE) {
        subscription = RosterSubscription::None;
    } else if (sub == SUB_REMOVE) {
        subscription = RosterSubscription::Remove;
    } else {
        subscription = RosterSubscription::NA;
    }
}

BareJid RosterStanzaItem::getJid() const
{
    return this->jid;
}

QString RosterStanzaItem::getName() const
{
    return this->name;
}
