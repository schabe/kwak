/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rosterresultstanza.h"

#include <strophe.h>

#include "stanza/base/inbound/invalidstanzaexception.h"
#include "xmpp/strophehelpers.h"
#include "xmpp/jid/jid.h"


InboundRosterResultStanza::InboundRosterResultStanza(xmpp_stanza_t* const stanza)
{
    this->to = Jid(xmpp_stanza_get_to(stanza));
    this->id = xmpp_stanza_get_id(stanza);

    xmpp_stanza_t *roster = xmpp_stanza_get_roster_query(stanza);
    if (!roster) {
        throw InvalidStanzaException("Roster query not found");
    }

    xmpp_stanza_t *item = xmpp_stanza_get_children(roster);
    for (; item; item = xmpp_stanza_get_next(item))
    {
        RosterStanzaItem *row = new RosterStanzaItem(item);
        this->roster.append(row);
    }
}

InboundRosterResultStanza::~InboundRosterResultStanza()
{
    while (roster.count() > 0) {
        delete roster.takeAt(0);
    }
}

QList<RosterStanzaItem *> InboundRosterResultStanza::getRoster() const
{
    return roster;
}

Jid InboundRosterResultStanza::getTo() const
{
    return this->to;
}

QString InboundRosterResultStanza::getId() const
{
    return this->id;
}

void InboundRosterResultStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}
