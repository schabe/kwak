/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INBOUND_ROSTER_STANZA_ITEM_H
#define INBOUND_ROSTER_STANZA_ITEM_H

#include <QString>

#include <strophe.h>

#include "xmpp/jid/barejid.h"


/* An enum for subscription types */
class RosterSubscription {
public:
    enum type {
        NA,
        None,
        To,
        From,
        Both,
        Remove
    };
};

/* The <item/> element as defined in RFC 6121 Section 2.1.5 */
class RosterStanzaItem {
public:
    RosterStanzaItem();
    explicit RosterStanzaItem(xmpp_stanza_t * const);
    BareJid getJid() const;
    QString getName() const;
    enum RosterSubscription::type getSubscription() const;
private:
    BareJid jid;
    QString name;
    enum RosterSubscription::type subscription;
};

#endif // INBOUND_ROSTER_STANZA_ITEM_H
