/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STANZA_RECEIVER_H
#define STANZA_RECEIVER_H

/* forward declarations */
class BookmarkResponseStanza;
class ChatMessageStanza;
class InboundBookmarkRetractionNotificationStanza;
class InboundDiscoInfoRequestStanza;
class InboundDiscoItemsRequestStanza;
class InboundNewBookmarkNotificationStanza;
class InboundPresenceStanza;
class InboundRosterResultStanza;
class InboundRosterSetStanza;
class InboundSubscriptionRequestStanza;
class KwakAccount;
class MUCChatMessageStanza;
class MUCInviteStanza;
class MUCSubjectChangeStanza;
class PrivateMessageStanza;


class StanzaReceiver {
public:
    virtual void handle(const BookmarkResponseStanza *) = 0;
    virtual void handle(const ChatMessageStanza *) = 0;
    virtual void handle(const InboundBookmarkRetractionNotificationStanza *) = 0;
    virtual void handle(const InboundDiscoInfoRequestStanza *) = 0;
    virtual void handle(const InboundDiscoItemsRequestStanza *) = 0;
    virtual void handle(const InboundNewBookmarkNotificationStanza *) = 0;
    virtual void handle(const InboundPresenceStanza *) = 0;
    virtual void handle(const InboundRosterResultStanza *) = 0;
    virtual void handle(const InboundRosterSetStanza *) = 0;
    virtual void handle(const InboundSubscriptionRequestStanza *) = 0;
    virtual void handle(const MUCChatMessageStanza *) = 0;
    virtual void handle(const MUCInviteStanza *) = 0;
    virtual void handle(const MUCSubjectChangeStanza *) = 0;
    virtual void handle(const PrivateMessageStanza *) = 0;
};

#endif // STANZA_RECEIVER_H
