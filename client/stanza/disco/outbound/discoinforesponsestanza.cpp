/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "discoinforesponsestanza.h"

#include <stdlib.h> // for 'free'

#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"


namespace
{
xmpp_stanza_t *xmpp_disco_info_query_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *query = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(query, STANZA_NAME_QUERY);
    xmpp_stanza_set_ns(query, XMPP_NS_DISCO_INFO);

    return query;
}

xmpp_stanza_t *xmpp_disco_info_result_new(xmpp_ctx_t *ctx, QString id,
                                          QString from, QString to)
{
    char *text = xmpp_str_from_qstr(id);
    xmpp_stanza_t *response = xmpp_iq_new(ctx, STANZA_TYPE_RESULT, text);
    free(text);

    xmpp_stanza_set_from_q(response, from);
    xmpp_stanza_set_to_q(response, to);
    return response;
}

void xmpp_stanza_add_node(xmpp_stanza_t *stanza, QString node)
{
    char *text = xmpp_str_from_qstr(node);
    xmpp_stanza_set_attribute(stanza, STANZA_ATTR_NODE, text);
    free(text);
}

void xmpp_stanza_add_identity(xmpp_stanza_t *stanza, Capabilities &caps)
{
    xmpp_ctx_t *ctx = xmpp_stanza_get_context(stanza);
    xmpp_stanza_t *identity = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(identity, STANZA_NAME_IDENTITY);

    char *text = xmpp_str_from_qstr(caps.getCategory());
    xmpp_stanza_set_attribute(identity, STANZA_ATTR_CATEGORY, text);
    free(text);

    text = xmpp_str_from_qstr(caps.getType());
    xmpp_stanza_set_type(identity, text);
    free(text);

    text = xmpp_str_from_qstr(caps.getName());
    xmpp_stanza_set_attribute(identity, STANZA_ATTR_NAME, text);
    free(text);

    xmpp_stanza_adopt_child(stanza, identity);
}

void xmpp_stanza_add_features(xmpp_stanza_t *stanza, Capabilities &cap)
{
    xmpp_ctx_t *ctx = xmpp_stanza_get_context(stanza);
    char *text;
    QList<QString>::iterator i;
    QList<QString> caps = cap.getCaps();

    for (i = caps.begin(); i != caps.end(); ++i) {
        xmpp_stanza_t *row = xmpp_stanza_new(ctx);
        xmpp_stanza_set_name(row, STANZA_NAME_FEATURE);
        text = xmpp_str_from_qstr(*i);
        xmpp_stanza_set_attribute(row, STANZA_ATTR_VAR, text);
        free(text);
        xmpp_stanza_add_child(stanza, row);
        xmpp_stanza_release(row);
    }
}
}


OutboundDiscoInfoResponseStanza::OutboundDiscoInfoResponseStanza(QString id,
                    Jid from, Jid to, Capabilities &caps)
    : caps(caps)
{
    this->id = id;
    this->to = to;
    this->from = from;
}

OutboundDiscoInfoResponseStanza::OutboundDiscoInfoResponseStanza(QString id,
                    Jid from, Jid to, Capabilities &caps, const QString &node)
    : caps(caps)
{
    this->id = id;
    this->to = to;
    this->from = from;
    this->node = QString(node);
}

void OutboundDiscoInfoResponseStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();
    xmpp_stanza_t *response = xmpp_disco_info_result_new(ctx, this->id,
                                                         this->from, this->to);

    xmpp_stanza_t *query = xmpp_disco_info_query_new(ctx);
    xmpp_stanza_adopt_child(response, query);

    if (this->node != "")
    {
        xmpp_stanza_add_node(query, this->node);
    }

    xmpp_stanza_add_identity(query, this->caps);
    xmpp_stanza_add_features(query, this->caps);

    xmpp.send(response);
    xmpp_stanza_release(response);
}
