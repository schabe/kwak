/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "discoitemsrequeststanza.h"

#include <strophe.h>

#include "stanza/base/inbound/invalidstanzaexception.h"
#include "xmpp/jid/jid.h"


InboundDiscoItemsRequestStanza::InboundDiscoItemsRequestStanza(
        xmpp_stanza_t *stanza)
{
    const char *from = xmpp_stanza_get_from(stanza);
    if (!from) {
        throw InvalidStanzaException("Stanza sender not found");
    }

    const char *id = xmpp_stanza_get_id(stanza);
    if (!id) {
        throw InvalidStanzaException("Stanza id not found");
    }

    this->from = Jid(from);
    this->id = QString(id);
}

Jid InboundDiscoItemsRequestStanza::getFrom() const
{
    return this->from;
}

QString InboundDiscoItemsRequestStanza::getId() const
{
    return this->id;
}

void InboundDiscoItemsRequestStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}
