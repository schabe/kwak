/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "discoinforequeststanza.h"

#include "stanza/base/inbound/invalidstanzaexception.h"
#include "xmpp/jid/jid.h"
#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"


namespace
{
xmpp_stanza_t *xmpp_stanza_get_disco_info_query(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name_and_ns(stanza, STANZA_NAME_QUERY,
                                                XMPP_NS_DISCO_INFO);
}
}


InboundDiscoInfoRequestStanza::InboundDiscoInfoRequestStanza(
        xmpp_stanza_t *const stanza)
{
    const char *from = xmpp_stanza_get_from(stanza);
    if (!from) {
        throw InvalidStanzaException("Stanza sender not found");
    }

    const char *to = xmpp_stanza_get_to(stanza);
    if (!to) {
        throw InvalidStanzaException("Stanza recipient not found");
    }

    const char *id = xmpp_stanza_get_id(stanza);
    if (!id) {
        throw InvalidStanzaException("Stanza id not found");
    }

    this->id = id;
    this->from = Jid(from);
    this->to = Jid(to);

    xmpp_stanza_t *query = xmpp_stanza_get_disco_info_query(stanza);

    if (query) {
        this->node = xmpp_stanza_get_node(query);
    }
}

void InboundDiscoInfoRequestStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}

QString InboundDiscoInfoRequestStanza::getId() const
{
    return this->id;
}

Jid InboundDiscoInfoRequestStanza::getFrom() const
{
    return this->from;
}

Jid InboundDiscoInfoRequestStanza::getTo() const
{
    return this->to;
}

QString InboundDiscoInfoRequestStanza::getNode() const
{
    return this->node;
}
