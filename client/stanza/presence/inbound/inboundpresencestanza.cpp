/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "inboundpresencestanza.h"

#include <QString>

#include <strophe.h>

#include "stanza/base/inbound/invalidstanzaexception.h"
#include "xmpp/definitions.h"
#include "xmpp/jid/jid.h"


namespace
{
xmpp_stanza_t *xmpp_stanza_get_show(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name(stanza, STANZA_NAME_SHOW);
}

QString xmpp_stanza_get_status(xmpp_stanza_t *const stanza)
{
    const char *type = xmpp_stanza_get_type(stanza);
    if ((type) && (!strcmp(type, STANZA_TYPE_UNAVAILABLE)))
    {
        return STATUS_OFFLINE;
    }

    xmpp_stanza_t *show = xmpp_stanza_get_show(stanza);
    if (show) {
        char *status = xmpp_stanza_get_text(show);
        QString res;

        if (status) {
            QString st(status);
            if (st == STATUS_AWAY) {
                res = STATUS_AWAY;
            } else if (st == STATUS_CHAT) {
                res = STATUS_CHAT;
            } else if (st == STATUS_DND) {
                res = STATUS_DND;
            } else if (st == STATUS_XA) {
                res = STATUS_XA;
            } else {
                res = STATUS_UNKNOWN;
            }
        }

        xmpp_free(xmpp_stanza_get_context(stanza), status);
        return res;
    }

    return STATUS_ONLINE;
}
}


InboundPresenceStanza::InboundPresenceStanza(
        xmpp_stanza_t * const stanza)
{
    this->id = xmpp_stanza_get_id(stanza);

    const char *from = xmpp_stanza_get_from(stanza);
    if (!from) {
        throw InvalidStanzaException("Stanza sender not found");
    }
    this->from = Jid(from);

    const char *to = xmpp_stanza_get_to(stanza);
    if (to) {
        this->to = Jid(to);
    }

    this->status = xmpp_stanza_get_status(stanza);
}

Jid InboundPresenceStanza::getFrom() const
{
    return this->from;
}

Jid InboundPresenceStanza::getTo() const
{
    return this->to;
}

QString InboundPresenceStanza::getStatus() const
{
    return this->status;
}

QString InboundPresenceStanza::getId() const
{
    return this->id;
}

void InboundPresenceStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}
