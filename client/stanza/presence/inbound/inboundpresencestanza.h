/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INBOUND_PRESENCE_STANZA_H
#define INBOUND_PRESENCE_STANZA_H

#include <strophe.h>
#include "stanza/base/inbound/inboundstanza.h"
#include "xmpp/jid/jid.h"


/* RFC 6121 Section 4.2.4 */
class InboundPresenceStanza : public InboundStanza
{
public:
    explicit InboundPresenceStanza(xmpp_stanza_t * const);
    Jid getFrom() const;
    Jid getTo() const;
    QString getId() const;
    QString getStatus() const;
    void receive(StanzaReceiver *);
private:
    Jid from;
    Jid to;
    QString id;
    QString status;
};

#endif // INBOUND_PRESENCE_STANZA_H
