/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "presencestanza.h"

#include <stdlib.h> // for 'free'

#include <QString>

#include <strophe.h>

#include "xmpp/strophehelpers.h"
#include "xmpp/jid/barejid.h"
#include "xmpp/jid/jid.h"


namespace
{
void xmpp_stanza_set_type_q(xmpp_stanza_t *stanza, QString type)
{
    char *text = xmpp_str_from_qstr(type);
    xmpp_stanza_set_type(stanza, text);
    free(text);
}
}


OutboundTargetedPresenceStanza::OutboundTargetedPresenceStanza(const Jid &to)
{
    this->to = to;
}

Jid OutboundTargetedPresenceStanza::getTo() const
{
    return this->to;
}

void OutboundTargetedPresenceStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();
    xmpp_stanza_t *presence = xmpp_presence_new_with_random_id(ctx);

    QString type = this->getType();
    xmpp_stanza_set_type_q(presence, type);

    QString jid = BareJid(this->getTo());
    xmpp_stanza_set_to_q(presence, jid);

    xmpp.send(presence);
    xmpp_stanza_release(presence);
}
