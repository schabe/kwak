/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "presencebroadcaststanza.h"

#include <QApplication>
#include <QString>
#include <QVariant>

#include <strophe.h>

#include "stanza/presence/outbound/broadcast/onlinepresencestanza.h"
#include "stanza/presence/outbound/broadcast/presencewithstatusstanza.h"
#include "stanza/presence/outbound/broadcast/unavailablepresencestanza.h"
#include "xmpp/capabilities.h"
#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"

#define APP_PROPERTY_URL "URL"


OutboundPresenceBroadcastStanza* OutboundPresenceBroadcastStanza::fromStatus(
        const QString &s)
{
    if (s == STATUS_ONLINE) {
        return new OutboundOnlinePresenceStanza();
    } else if (s == STATUS_OFFLINE) {
        return new OutboundUnavailablePresenceStanza();
    } else {
        return new OutboundPresenceWithStatusStanza(s);
    }
}

OutboundPresenceBroadcastStanza::~OutboundPresenceBroadcastStanza() {
}

void OutboundPresenceBroadcastStanza::addCapabilities(
        xmpp_stanza_t *stanza) const
{
    xmpp_ctx_t *ctx = xmpp_stanza_get_context(stanza);

    xmpp_stanza_t *c = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(c, STANZA_NAME_C);
    xmpp_stanza_set_ns(c, STANZA_NS_CAPS);
    xmpp_stanza_set_attribute(c, STANZA_ATTR_HASH, STANZA_VALUE_SHA1);

    QApplication *app = qApp;
    QString url = app->property(APP_PROPERTY_URL).toString();
    xmpp_stanza_set_attribute_q(c, STANZA_ATTR_NODE, url);

    Capabilities caps(app);
    xmpp_stanza_set_attribute_q(c, STANZA_ATTR_VER, caps.hash());

    xmpp_stanza_adopt_child(stanza, c);
}
