/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "presencewithstatusstanza.h"

#include <QString>

#include <strophe.h>

#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"
#include "xmpp/xmpphandler.h"


OutboundPresenceWithStatusStanza::OutboundPresenceWithStatusStanza(
        const QString &status)
{
    this->status = status;
}

void OutboundPresenceWithStatusStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();
    xmpp_stanza_t *presence = xmpp_presence_new_with_random_id(ctx);

    this->addCapabilities(presence);

    xmpp_stanza_t* show = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(show, STANZA_NAME_SHOW);
    xmpp_stanza_adopt_child(presence, show);

    xmpp_stanza_add_text(show, this->status);

    xmpp.send(presence);
    xmpp_stanza_release(presence);
}
