/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MESSAGESTANZA_H
#define MESSAGESTANZA_H

#include <QDateTime>

#include <strophe.h>

#include "xmpp/jid/jid.h"


/* RFC 6120 Section 8.2.1 (Message stanza) */
class MessageStanza
{
public:
    MessageStanza();
    explicit MessageStanza(xmpp_stanza_t *);
    virtual ~MessageStanza();
    Jid getTo() const;
    Jid getFrom() const;
    QString getId() const;
    QString getBody() const;
    QDateTime getSent() const;
protected:
    Jid to;
    Jid from;
    QString id;
    QString body;
    QDateTime sent; /* XEP 0203 */
};

#endif // MESSAGESTANZA_H
