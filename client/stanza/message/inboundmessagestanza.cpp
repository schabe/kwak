/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "inboundmessagestanza.h"

#include <QDebug>

#include <strophe.h>

#include "stanza/bookmarks/inbound/bookmarkretractionnotificationstanza.h"
#include "stanza/bookmarks/inbound/newbookmarknotificationstanza.h"
#include "stanza/message/chatmessagestanza.h"
#include "stanza/muc/invite/directmucinvitestanza.h"
#include "stanza/muc/invite/mediatedmucinvitestanza.h"
#include "stanza/muc/mucmessagestanza.h"
#include "stanza/muc/privatemessagestanza.h"
#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"


namespace
{
InboundMessageStanza* loadNormalMessage(xmpp_stanza_t *stanza)
{
    xmpp_stanza_t *x = xmpp_stanza_get_x_muc_user(stanza);

    if (x) {
        if (xmpp_stanza_get_invite(x)) {
            return new MediatedMUCInviteStanza(stanza);
        } else {
            return new PrivateMessageStanza(stanza);
        }
    } else if (xmpp_stanza_get_x_conference(stanza)) {
        return new DirectMUCInviteStanza(stanza);
    } else {
        return new ChatMessageStanza(stanza);
    }
}

InboundStanza* loadHeadlineMessage(xmpp_stanza_t *stanza)
{
    xmpp_stanza_t *event = xmpp_stanza_get_pubsub_event(stanza);
    if (!event) {
        throw 1;
    }

    xmpp_stanza_t *it = xmpp_stanza_get_items(event);
    if (!it) {
        throw 2;
    }

    if (xmpp_stanza_get_item(it)) {
        return new InboundNewBookmarkNotificationStanza(stanza);
    } else if (xmpp_stanza_get_bookmark_retraction(it)) {
        return new InboundBookmarkRetractionNotificationStanza(stanza);
    } else {
        throw 3;
    }
}
}


InboundStanza* InboundMessageStanza::load(xmpp_stanza_t *stanza)
{
    /* RFC 6121: If an application receives a message with no 'type' attribute
     * or the application does not understand the value of the 'type' attribute
     * provided, it MUST consider the message to be of type "normal" */

    const char *type = xmpp_stanza_get_type(stanza);

    if ((!type)
        || (!strcmp(type, STANZA_TYPE_NORMAL))
        || (!strcmp(type, STANZA_TYPE_CHAT)))
    {
        return loadNormalMessage(stanza);
    } else if (!strcmp(type, STANZA_TYPE_HEADLINE)) {
        return loadHeadlineMessage(stanza);
    } else if (!strcmp(type, STANZA_TYPE_GROUPCHAT)) {
        return MUCMessageStanza::load(stanza);
    } else if (!strcmp(type, STANZA_TYPE_ERROR)) {
        qDebug() << "(ignoring" << type << "message)";
        throw 1;
    } else {
        return loadNormalMessage(stanza);
    }
}
