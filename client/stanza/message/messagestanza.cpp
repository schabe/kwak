/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "messagestanza.h"

#include <QDateTime>

#include <strophe.h>

#include "xmpp/strophehelpers.h"
#include "xmpp/jid/jid.h"


MessageStanza::MessageStanza() {
}

MessageStanza::MessageStanza(xmpp_stanza_t *stanza)
{
    /* from: required */
    const char *from = xmpp_stanza_get_from(stanza);
    if (!from) {
        throw 1;
    }
    this->from = Jid(from);

    /* to: optional */
    const char *to = xmpp_stanza_get_to(stanza);
    if (to) {
        this->to = Jid(xmpp_stanza_get_to(stanza));
    }

    /* message id: optional */
    const char *id = xmpp_stanza_get_id(stanza);
    if (id) {
        this->id = QString(id);
    }

    /* delay: optional (use current timestamp if missing */
    this->sent = QDateTime::currentDateTime();

    xmpp_stanza_t *delay = xmpp_stanza_get_delay(stanza);
    if (delay) {
        const char *ts = xmpp_stanza_get_stamp(delay);
        if (ts) {
            this->sent = QDateTime::fromString(ts, Qt::ISODate);
        }
    }
}

MessageStanza::~MessageStanza()
{
}

Jid MessageStanza::getTo() const
{
    return this->to;
}

Jid MessageStanza::getFrom() const
{
    return this->from;
}

QString MessageStanza::getId() const
{
    return this->id;
}

QString MessageStanza::getBody() const
{
    return this->body;
}

QDateTime MessageStanza::getSent() const
{
    return this->sent;
}
