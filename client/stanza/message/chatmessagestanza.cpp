/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "messagestanza.h"

#include <QDebug>

#include "stanza/message/chatmessagestanza.h"
#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"


ChatMessageStanza::ChatMessageStanza(xmpp_stanza_t *stanza)
    : MessageStanza(stanza)
{
    char *body = xmpp_message_get_body(stanza);
    if (!body) {
        qWarning() << "Message body not found";
        throw 1;
    }

    this->body = QString::fromUtf8(body);
    xmpp_free(xmpp_stanza_get_context(stanza), body);
}

ChatMessageStanza::ChatMessageStanza(const Jid &from, const Jid &to,
                                     const QString &msg)
{
    this->from = Jid(from);
    this->to = Jid(to);
    this->body = QString(msg);

    // TODO: this should be set after the message has been successfully sent
    this->sent = QDateTime::currentDateTime();

    /* generate id */
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    char *id = xmpp_uuid_gen(ctx);
    this->id = QString(id);
    xmpp_free(ctx, id);
    xmpp_ctx_free(ctx);
}

void ChatMessageStanza::receive(StanzaReceiver *handler)
{
    handler->handle(this);
}

void ChatMessageStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();

    xmpp_stanza_t *msg = xmpp_message_new_q(ctx, STANZA_TYPE_CHAT, this->to,
                                            this->id);
    xmpp_stanza_set_from_q(msg, this->from);
    xmpp_message_set_body_q(msg, this->body);

    xmpp.send(msg);
    xmpp_stanza_release(msg);
}
