/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "addbookmarkrequeststanza.h"

#include <stdlib.h> // for 'free'

#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"


#define VAR_FORM_TYPE           "FORM_TYPE"
#define PUBSUB_ACCESS_MODEL     "pubsub#access_model"
#define PUBSUB_MAX_ITEMS        "pubsub#max_items"
#define PUBSUB_PERSIST_ITEMS    "pubsub#persist_items"
#define PUBLISH_OPTIONS         "http://jabber.org/protocol/pubsub#publish-options"
#define PUBSUB_SEND_LAST        "pubsub#send_last_published_item"

#define STANZA_VALUE_MAX        "max"
#define STANZA_VALUE_NEVER      "never"
#define STANZA_VALUE_WHITELIST  "whitelist"


namespace
{
xmpp_stanza_t *xmpp_publish_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *publish = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(publish, STANZA_NAME_PUBLISH);
    xmpp_stanza_set_attribute(publish, STANZA_ATTR_NODE, STANZA_NS_BOOKMARKS);

    return publish;
}

xmpp_stanza_t *xmpp_item_new_with_id(xmpp_ctx_t *ctx, QString id)
{
    xmpp_stanza_t *item = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(item, STANZA_NAME_ITEM);
    char *id_ = xmpp_str_from_qstr(id);
    xmpp_stanza_set_id(item, id_);
    free(id_);

    return item;
}

xmpp_stanza_t *xmpp_conference_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *conference = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(conference, STANZA_NAME_CONFERENCE);
    xmpp_stanza_set_ns(conference, STANZA_NS_BOOKMARKS);
    xmpp_stanza_set_attribute(conference, STANZA_ATTR_AUTOJOIN,
                              STANZA_VALUE_TRUE);
    return conference;
}

xmpp_stanza_t *xmpp_nick_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *nick = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(nick, STANZA_NAME_NICK);
    return nick;
}

xmpp_stanza_t *xmpp_options_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *options = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(options, STANZA_NAME_PUBLISH_OPTIONS);
    return options;
}

xmpp_stanza_t *xmpp_data_submit_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *x = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(x, STANZA_NAME_X);
    xmpp_stanza_set_ns(x, STANZA_NS_DATA);
    xmpp_stanza_set_type(x, STANZA_TYPE_SUBMIT);
    return x;
}

xmpp_stanza_t *xmpp_field_new(xmpp_ctx_t *ctx, const char *key,
                              const char *val)
{
    xmpp_stanza_t *field = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(field, STANZA_NAME_FIELD);
    xmpp_stanza_set_attribute(field, STANZA_ATTR_VAR, key);

    xmpp_stanza_t *value = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(value, STANZA_NAME_VALUE);
    xmpp_stanza_adopt_child(field, value);

    xmpp_stanza_t *innerValue = xmpp_stanza_new(ctx);
    xmpp_stanza_set_text(innerValue, val);
    xmpp_stanza_adopt_child(value, innerValue);

    return field;
}
}


OutboundAddBookmarkRequestStanza::OutboundAddBookmarkRequestStanza(
        GroupChat &muc)
    : muc(muc)
{
}

void OutboundAddBookmarkRequestStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();
    xmpp_stanza_t *iq = xmpp_iq_new_with_random_id(ctx, STANZA_TYPE_SET);

    xmpp_stanza_t *pubsub = xmpp_pubsub_new(ctx);
    xmpp_stanza_adopt_child(iq, pubsub);

    xmpp_stanza_t *publish = xmpp_publish_new(ctx);
    xmpp_stanza_adopt_child(pubsub, publish);

    xmpp_stanza_t *item = xmpp_item_new_with_id(ctx, muc.getJid());
    xmpp_stanza_adopt_child(publish, item);

    xmpp_stanza_t *conference = xmpp_conference_new(ctx);
    xmpp_stanza_adopt_child(item, conference);

    xmpp_stanza_t *nick = xmpp_nick_new(ctx);
    xmpp_stanza_add_text(nick, muc.getNick());
    xmpp_stanza_adopt_child(conference, nick);

    xmpp_stanza_t *options = xmpp_options_new(ctx);
    xmpp_stanza_adopt_child(pubsub, options);

    xmpp_stanza_t *x = xmpp_data_submit_new(ctx);
    xmpp_stanza_adopt_child(options, x);

    xmpp_stanza_t *field = xmpp_field_new(ctx, VAR_FORM_TYPE, PUBLISH_OPTIONS);
    xmpp_stanza_set_type(field, STANZA_TYPE_HIDDEN);
    xmpp_stanza_adopt_child(x, field);

    field = xmpp_field_new(ctx, PUBSUB_PERSIST_ITEMS, STANZA_VALUE_TRUE);
    xmpp_stanza_adopt_child(x, field);

    field = xmpp_field_new(ctx, PUBSUB_MAX_ITEMS, STANZA_VALUE_MAX);
    xmpp_stanza_adopt_child(x, field);

    field = xmpp_field_new(ctx, PUBSUB_SEND_LAST, STANZA_VALUE_NEVER);
    xmpp_stanza_adopt_child(x, field);

    field = xmpp_field_new(ctx, PUBSUB_ACCESS_MODEL, STANZA_VALUE_WHITELIST);
    xmpp_stanza_adopt_child(x, field);

    xmpp.send(iq);
    xmpp_stanza_release(iq);
}
