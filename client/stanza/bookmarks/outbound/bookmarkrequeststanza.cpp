/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bookmarkrequeststanza.h"

#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"


namespace
{
    xmpp_stanza_t *xmpp_bookmarks_new(xmpp_ctx_t *ctx)
    {
        xmpp_stanza_t *items = xmpp_stanza_new(ctx);
        xmpp_stanza_set_name(items, STANZA_NAME_ITEMS);
        xmpp_stanza_set_attribute(items, STANZA_ATTR_NODE, STANZA_NS_BOOKMARKS);

        return items;
    }
}


void OutboundBookmarkRequestStanza::send(XMPPHandler &xmpp)
{
    xmpp_ctx_t *ctx = xmpp.getContext();
    xmpp_stanza_t *iq = xmpp_iq_new_with_random_id(ctx, STANZA_TYPE_GET);

    xmpp_stanza_t *pubsub = xmpp_pubsub_new(ctx);
    xmpp_stanza_adopt_child(iq, pubsub);

    xmpp_stanza_t *items = xmpp_bookmarks_new(ctx);
    xmpp_stanza_adopt_child(pubsub, items);

    xmpp.send(iq);
    xmpp_stanza_release(iq);
}
