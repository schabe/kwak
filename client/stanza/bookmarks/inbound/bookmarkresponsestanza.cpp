/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bookmarkresponsestanza.h"

#include "stanza/base/inbound/invalidstanzaexception.h"
#include "stanza/receiver.h"
#include "xmpp/bookmark.h"
#include "xmpp/definitions.h"
#include "xmpp/strophehelpers.h"


BookmarkResponseStanza::BookmarkResponseStanza(xmpp_stanza_t *stanza)
{
    xmpp_stanza_t *pubsub = xmpp_stanza_get_pubsub(stanza);
    if (!pubsub) {
        throw InvalidStanzaException("Pubsub node not found");
    }

    xmpp_stanza_t *items = xmpp_stanza_get_items(pubsub);
    if (!items) {
        throw InvalidStanzaException("No items found in pubsub node");
    }

    xmpp_stanza_t *item = xmpp_stanza_get_children(items);
    for (; item; item = xmpp_stanza_get_next(item)) {
        const char *name = xmpp_stanza_get_name(item);
        if (!name || strcmp(xmpp_stanza_get_name(item), STANZA_NAME_ITEM)) {
            continue;
        }

        const char *id = xmpp_stanza_get_id(item);
        xmpp_stanza_t *conference = xmpp_stanza_get_conference(item);

        if (conference) {
            bool autojoin = xmpp_stanza_do_autojoin(conference);

            xmpp_stanza_t *nick = xmpp_stanza_get_nick(conference);
            if (nick) {
                char *text = xmpp_stanza_get_text(nick);
                this->bookmarks.append(new Bookmark(Jid(id), text, autojoin));
                xmpp_free(xmpp_stanza_get_context(stanza), text);
            }
        }
    }
}

BookmarkResponseStanza::~BookmarkResponseStanza()
{
    while (this->bookmarks.count() > 0) {
        delete this->bookmarks.takeAt(0);
    }
}

QList<Bookmark*> BookmarkResponseStanza::getBookmarks() const
{
    return this->bookmarks;
}

void BookmarkResponseStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}
