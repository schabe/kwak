/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "newbookmarknotificationstanza.h"

#include <strophe.h>

#include "xmpp/strophehelpers.h"
#include "xmpp/jid/jid.h"


InboundNewBookmarkNotificationStanza::InboundNewBookmarkNotificationStanza(
        xmpp_stanza_t *st)
{
    xmpp_stanza_t *event = xmpp_stanza_get_pubsub_event(st);
    if (!event) {
        throw 1;
    }

    xmpp_stanza_t *items = xmpp_stanza_get_items(event);
    if (!items) {
        throw 2;
    }

    xmpp_stanza_t *item = xmpp_stanza_get_item(items);
    if (!item) {
        throw 3;
    }

    xmpp_stanza_t *conference = xmpp_stanza_get_conference(item);
    if (!conference) {
        throw 4;
    }

    bool join = xmpp_stanza_do_autojoin(conference);

    const char *id = xmpp_stanza_get_id(item);
    if (!id) {
        throw 5;
    }

    xmpp_stanza_t *nick = xmpp_stanza_get_nick(conference);
    if (!nick) {
        throw 6;
    }

    Jid jid(id);
    char *text = xmpp_stanza_get_text(nick);
    this->bookmark = Bookmark(jid, QString(text), join);
    xmpp_free(xmpp_stanza_get_context(nick), text);
}

Bookmark InboundNewBookmarkNotificationStanza::getBookmark() const
{
    return this->bookmark;
}

void InboundNewBookmarkNotificationStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}
