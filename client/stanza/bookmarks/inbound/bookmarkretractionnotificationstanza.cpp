/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bookmarkretractionnotificationstanza.h"

#include "xmpp/strophehelpers.h"


InboundBookmarkRetractionNotificationStanza::InboundBookmarkRetractionNotificationStanza(
                                                        xmpp_stanza_t *stanza)
{
    xmpp_stanza_t *event = xmpp_stanza_get_pubsub_event(stanza);
    if (!event) {
        throw 1;
    }

    xmpp_stanza_t *items = xmpp_stanza_get_items(event);
    if (!items) {
        throw 2;
    }

    xmpp_stanza_t *retract = xmpp_stanza_get_bookmark_retraction(items);
    if (!retract) {
        throw 3;
    }

    const char *id = xmpp_stanza_get_id(retract);
    if (!id) {
        throw 4;
    }

    Jid jid(id);
    this->bookmark = Bookmark(jid, "", false);
}

Bookmark InboundBookmarkRetractionNotificationStanza::getBookmark() const
{
    return this->bookmark;
}

void InboundBookmarkRetractionNotificationStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}
