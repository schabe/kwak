/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INBOUNDSUBSCRIPTIONREQUESTSTANZA_H
#define INBOUNDSUBSCRIPTIONREQUESTSTANZA_H

#include "roster/chatinvite.h"
#include "stanza/base/inbound/inboundstanza.h"
#include "xmpp/jid/jid.h"


/* RFC 6121 Section 3.1.4 */
class InboundSubscriptionRequestStanza : public InboundStanza
{
public:
    explicit InboundSubscriptionRequestStanza(xmpp_stanza_t * const);
    BareJid getFrom() const;
    QString getId() const;
    ChatInvite* getInvite() const;
    void receive(StanzaReceiver *);
private:
    BareJid from;
    QString id;
};

#endif // INBOUNDSUBSCRIPTIONREQUESTSTANZA_H
