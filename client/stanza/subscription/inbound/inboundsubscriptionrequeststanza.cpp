/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "inboundsubscriptionrequeststanza.h"

#include <strophe.h>

#include "stanza/base/inbound/invalidstanzaexception.h"
#include "xmpp/jid/jid.h"


InboundSubscriptionRequestStanza::InboundSubscriptionRequestStanza(
        xmpp_stanza_t * const stanza)
{
    // RFC 6121, Section 3: When a server processes or generates an outbound
    // presence stanza of type "subscribe" [..] the server MUST stamp the
    // outgoing presence stanza with the bare JID <localpart@domainpart>
    // of the sending entity
    const char *from = xmpp_stanza_get_from(stanza);
    if (!from) {
        throw InvalidStanzaException("Stanza sender not found");
    }
    this->from = BareJid(Jid(from));

    // RFC 6121, Section 3.1.1: For tracking purposes, a client SHOULD include
    // an 'id' attribute in a presence subscription request
    const char *id = xmpp_stanza_get_id(stanza);
    if (id) {
        this->id = QString(id);
    }
}

BareJid InboundSubscriptionRequestStanza::getFrom() const
{
    return this->from;
}

QString InboundSubscriptionRequestStanza::getId() const
{
    return this->id;
}

ChatInvite* InboundSubscriptionRequestStanza::getInvite() const
{
    return new ChatInvite(this->from);
}

void InboundSubscriptionRequestStanza::receive(StanzaReceiver *receiver)
{
    receiver->handle(this);
}
