/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STANZA_SENDER_H
#define STANZA_SENDER_H

#include <QString>


/* forward declarations */
class OutboundAddBookmarkRequestStanza;
class Contact;
class MUCChatMessageStanza;
class PrivateChat;
class PrivateMessageStanza;
class SubscriptionApprovalStanza;
class SubscriptionDeniedStanza;
class SubscriptionRequestStanza;


class StanzaSender
{
public:
    virtual void send(OutboundAddBookmarkRequestStanza &) = 0;
    virtual void send(Contact *, QString msg) = 0;
    virtual void send(MUCChatMessageStanza &) = 0;
    virtual void send(PrivateChat &, PrivateMessageStanza &) = 0;
    virtual void send(SubscriptionApprovalStanza &) = 0;
    virtual void send(SubscriptionDeniedStanza &) = 0;
    virtual void send(SubscriptionRequestStanza &) = 0;
};

#endif // STANZA_SENDER_H
