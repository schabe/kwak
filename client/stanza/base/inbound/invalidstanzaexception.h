#ifndef INVALIDSTANZAEXCEPTION_H
#define INVALIDSTANZAEXCEPTION_H

#include <exception>
#include <QString>


class InvalidStanzaException : public std::exception
{
public:
    explicit InvalidStanzaException(QString reason);
    const char * what();
private:
    QString reason;
};

#endif // INVALIDSTANZAEXCEPTION_H
