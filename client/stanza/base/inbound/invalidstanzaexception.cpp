#include "invalidstanzaexception.h"

InvalidStanzaException::InvalidStanzaException(QString reason)
{
    this->reason = reason;
}

const char * InvalidStanzaException::what()
{
    return qPrintable(this->reason);
}
