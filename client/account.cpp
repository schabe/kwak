/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QSettings>
#include <stdlib.h>
#include <qplatformdefs.h> // MEEGO_EDITION_HARMATTAN

#include "account.h"
#include "xmpp/definitions.h"

#define CONFIG_GROUP_ACCOUNT "account"
#define CONFIG_KEY_JID "jid"
#define CONFIG_KEY_PASSWORD "password"
#define CONFIG_KEY_RESOURCE "resource"


KwakAccount::KwakAccount()
    : QObject(), status(STATUS_UNKNOWN)
{
}

QString KwakAccount::getJid() const
{
    return this->jid;
}

void KwakAccount::setJid(const QString &value)
{
    this->jid = value;
    emit jidChanged();
}

void KwakAccount::update(const Jid &jid)
{
    QString resource = jid.getResourcePart();
    this->resourcepart = QString(resource);
}

QString KwakAccount::getResourcePart() const
{
    return resourcepart;
}

QString KwakAccount::getPassword() const
{
    return password;
}

void KwakAccount::setPassword(const QString &value)
{
    password = value;
    emit passwordChanged();
}

QString KwakAccount::getStatus() const
{
    return status;
}

void KwakAccount::setStatus(const QString &presence)
{
    status = presence;
    emit statusChanged();
}

void KwakAccount::save()
{
#if defined(MEEGO_EDITION_HARMATTAN)
    QString path = QString(getenv("HOME")) + "/private/kwak.ini";
    QSettings cfg(path, QSettings::NativeFormat);
#else
    QCoreApplication *app = QApplication::instance();
    QSettings cfg(app->organizationName(), app->applicationName());
#endif

    cfg.beginGroup(CONFIG_GROUP_ACCOUNT);
    cfg.setValue(CONFIG_KEY_JID, this->jid);
    cfg.setValue(CONFIG_KEY_PASSWORD, this->password);
    cfg.setValue(CONFIG_KEY_RESOURCE, this->resourcepart);
    cfg.endGroup();
}

void KwakAccount::load()
{
#if defined(MEEGO_EDITION_HARMATTAN)
    QString path = QString(getenv("HOME")) + "/private/kwak.ini";
    QSettings cfg(path, QSettings::NativeFormat);
#else
    QCoreApplication *app = QApplication::instance();
    QSettings cfg(app->organizationName(), app->applicationName());
#endif

    cfg.beginGroup(CONFIG_GROUP_ACCOUNT);
    this->jid = cfg.value(CONFIG_KEY_JID).toString();
    this->password = cfg.value(CONFIG_KEY_PASSWORD).toString();
    this->resourcepart = cfg.value(CONFIG_KEY_RESOURCE).toString();
    cfg.endGroup();
}
