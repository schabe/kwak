/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KWAK_CLIENT_H
#define KWAK_CLIENT_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include <strophe.h>

#include "account.h"
#include "roster.h"
#include "connection/state.h"
#include "db/messagedb.h"
#include "notifications/notifier.h"
#include "stanza/message/messagestanza.h"
#include "stanza/sender.h"
#include "roster/muc.h"


class Kwak : public QObject, public StanzaSender
{
    Q_OBJECT

    Q_PROPERTY(KwakAccount* account READ getAccount NOTIFY accountChanged)
    Q_PROPERTY(QString status READ getStatus NOTIFY statusChanged())

    signals:
        void errorOccured(QString msg);
        void accountChanged();
        void rosterChanged();
        void statusChanged();
        void errorOccurred(QString message);
        void chatMessageReceived(KwakMessage*,Contact*);
        void privateMessageReceived(KwakMessage*,PrivateChat*);
        void mucMessageReceived(KwakMessage*,GroupChat*);
        void chatInviteReceived(ChatInvite*);
        void mucInviteReceived(MUCInvite*);
        void focusReceived();
    public:
        explicit Kwak(KwakRoster &roster, QObject *parent = 0);
        ~Kwak();
        Q_INVOKABLE void connect();
        Q_INVOKABLE void setPresence(QString);
        void changeState(ConnectionState *);
        KwakAccount* getAccount();
        void receive(ChatInvite *);
        void receive(const MessageStanza &, xmpp_conn_t *);
        void receive(const PrivateMessageStanza *);
        void receive(const MUCChatMessageStanza *);
        void receive(MUCInvite *);
        void receive(const MUCSubjectChangeStanza *);
        QString getStatus();
        Contact *subscribe(const BareJid &, xmpp_conn_t *);
        void addMUC(GroupChat *);
        QList<GroupChat*> getMUCs() const;
        void updateRoster(const InboundPresenceStanza *);
        void updateRoster(const InboundRosterResultStanza *);
        void updateRoster(const InboundRosterSetStanza *);
        bool isMUC(const BareJid &) const;

        /* MessageSender interface */
        void send(OutboundAddBookmarkRequestStanza &);
        void send(Contact *, QString);
        void send(MUCChatMessageStanza &);
        void send(PrivateChat &, PrivateMessageStanza &);
        void send(SubscriptionApprovalStanza &);
        void send(SubscriptionDeniedStanza &);
        void send(SubscriptionRequestStanza &);
    public slots:
        void connected(ConnectionHandler*, const Jid &);
        void disconnected(ConnectionHandler*);
        void exitHandler();
        void onFocusChanged(QWidget*,QWidget*);
    private:
        ConnectionState *state;
        KwakAccount *account;
        MessageDB db;
        KwakRoster &roster;
        Notifier notifier;

        void loadAccount();
        void initDB();
};

#endif
