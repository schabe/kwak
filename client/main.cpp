/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QDir>
#include <QApplication>
#include <QFontDatabase>

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QtDeclarative>
#include "qmlapplicationviewer4.h"
#else
#include <QQmlContext>
#include <QQmlEngine>
#include "qmlapplicationviewer5.h"
#endif

#include "view/rosterview.h"
#include "client.h"

void loadFont(const QString &name)
{
    /* This is where the harmoji package keeps its fonts */
    QString path = QString("/opt/Harmoji/fonts/" + name);
    if (QFontDatabase::addApplicationFont(path) != -1)
    {
        return;
    }

    /* If the harmoji font is not available, try ~/.fonts/ */
    path = QString(QDir::homePath() + "/.fonts/" + name);
    if (QFontDatabase::addApplicationFont(path) != -1)
    {
        return;
    }

    qWarning() << "Failed to load" << name;
}

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    qmlRegisterType<KwakAccount,1>("ch.kwak", 1, 0, "KwakAccount");

    QScopedPointer<QApplication> app(createApplication(argc,argv));

    QApplication *data = app.data();
    data->setApplicationName("Kwak");
    data->setOrganizationName("cockroach");
    data->setApplicationVersion("0.0.0");
    data->setProperty("URL", "https://git.ott.net/kwak/");

//    char buf[1024];

//    FILE *pipe = popen("accli -qI", "r");
//    if (pipe) {
//        while (fgets(buf, sizeof(buf), pipe) != NULL) {
//            qDebug() << buf;
//        }
//    } else {
//        qDebug() << "Command failed";
//    }

    KwakRoster roster;
    Kwak k(roster);
    RosterView view(roster);

    qRegisterMetaType<Jid>("Jid");
//    qmlRegisterType<RosterItem,1>("Kwak", 1, 1, "RosterItem");

    loadFont("AndroidEmoji.ttf");
    loadFont("SoftbankOSXMLEmoji.ttf");

    QmlApplicationViewer *viewer = new QmlApplicationViewer();
    viewer->rootContext()->setContextProperty("kwakClient", &k);
    viewer->rootContext()->setContextProperty("roster", &view);
    viewer->setMainQmlFile(QLatin1String("qml/kwak/main.qml"));
    viewer->showExpanded();

    QObject::connect(data, SIGNAL(focusChanged(QWidget*,QWidget*)),
                     &k, SLOT(onFocusChanged(QWidget*,QWidget*)));
    QObject::connect(data, SIGNAL(aboutToQuit()), &k, SLOT(exitHandler()));

    return app->exec();
}
