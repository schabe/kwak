TEMPLATE = app

QT += sql
QT += network

folder_01.source = qml/kwak
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

symbian:TARGET.UID3 = 0xE20C98F4

# Smart Installer package's UID
# This UID is from the protected range and therefore the package will
# fail to install if self-signed. By default qmake uses the unprotected
# range value if unprotected UID is defined for the application and
# 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += NetworkServices

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
CONFIG += mobility
MOBILITY += feedback multimedia

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
CONFIG += qdeclarative-boostable
CONFIG += meegotouch
CONFIG += meegotouchevents

# Add dependency to Symbian components
# CONFIG += qt-components

TARGET = kwak
SOURCES += main.cpp \
    account.cpp \
    client.cpp \
    roster.cpp \
    connection/handler.cpp \
    connection/state/connected.cpp \
    connection/state/connecting.cpp \
    connection/state/connectionlost.cpp \
    connection/state/disconnected.cpp \
    connection/state/disconnecting.cpp \
    db/message.cpp \
    db/messagedb.cpp \
    notifications/debugnotifier.cpp \
    notifications/notifier.cpp \
    roster/chatinvite.cpp \
    roster/contact.cpp \
    roster/invite.cpp \
    roster/muc.cpp \
    roster/mucinvite.cpp \
    roster/privatechat.cpp \
    roster/rosteritem.cpp \
    stanza/base/inbound/inboundstanza.cpp \
    stanza/base/inbound/invalidstanzaexception.cpp \
    stanza/bookmarks/inbound/bookmarkresponsestanza.cpp \
    stanza/bookmarks/inbound/bookmarkretractionnotificationstanza.cpp \
    stanza/bookmarks/inbound/newbookmarknotificationstanza.cpp \
    stanza/bookmarks/outbound/addbookmarkrequeststanza.cpp \
    stanza/bookmarks/outbound/bookmarkrequeststanza.cpp \
    stanza/disco/inbound/discoinforequeststanza.cpp \
    stanza/disco/inbound/discoitemsrequeststanza.cpp \
    stanza/disco/outbound/discoinforesponsestanza.cpp \
    stanza/disco/outbound/discoitemsresponsestanza.cpp \
    stanza/message/chatmessagestanza.cpp \
    stanza/message/errormessagestanza.cpp \
    stanza/message/inboundmessagestanza.cpp \
    stanza/message/messagestanza.cpp \
    stanza/muc/invite/directmucinvitestanza.cpp \
    stanza/muc/invite/mediatedmucinvitestanza.cpp \
    stanza/muc/invite/mucinvitestanza.cpp \
    stanza/muc/mucchatmessagestanza.cpp \
    stanza/muc/mucjoinrequeststanza.cpp \
    stanza/muc/mucmessagestanza.cpp \
    stanza/muc/mucpresencestanza.cpp \
    stanza/muc/privatemessagestanza.cpp \
    stanza/muc/subjectchangestanza.cpp \
    stanza/presence/inbound/inboundpresencestanza.cpp \
    stanza/presence/outbound/broadcast/onlinepresencestanza.cpp \
    stanza/presence/outbound/broadcast/presencebroadcaststanza.cpp \
    stanza/presence/outbound/broadcast/presencewithstatusstanza.cpp \
    stanza/presence/outbound/broadcast/unavailablepresencestanza.cpp \
    stanza/presence/outbound/targeted/presencestanza.cpp \
    stanza/pubsub/inbound/pubsubresultstanza.cpp \
    stanza/roster/inbound/rosterresultstanza.cpp \
    stanza/roster/inbound/rostersetstanza.cpp \
    stanza/roster/inbound/rosterstanzaitem.cpp \
    stanza/roster/outbound/rostergetstanza.cpp \
    stanza/subscription/inbound/inboundsubscriptionrequeststanza.cpp \
    stanza/subscription/outbound/subscriptionapprovalstanza.cpp \
    stanza/subscription/outbound/subscriptiondeniedstanza.cpp \
    stanza/subscription/outbound/subscriptionrequeststanza.cpp \
    view/chatinviteview.cpp \
    view/contactview.cpp \
    view/mucinviteview.cpp \
    view/mucview.cpp \
    view/privatechatview.cpp \
    view/rosteritemview.cpp \
    view/rosterview.cpp \
    xmpp/bookmark.cpp \
    xmpp/callbacks.cpp \
    xmpp/capabilities.cpp \
    xmpp/jid/barejid.cpp \
    xmpp/jid/jid.cpp \
    xmpp/strophehelpers.cpp \
    xmpp/xmpphandler.cpp

HEADERS += client.h \
    account.h \
    roster.h \
    connection/handler.h \
    connection/state.h \
    connection/state/connected.h \
    connection/state/connecting.h \
    connection/state/connectionlost.h \
    connection/state/disconnected.h \
    connection/state/disconnecting.h \
    db/message.h \
    db/messagedb.h \
    notifications/debugnotifier.h \
    notifications/notifier.h \
    notifications/notifierbackend.h \
    roster/chatinvite.h \
    roster/contact.h \
    roster/invite.h \
    roster/muc.h \
    roster/mucinvite.h \
    roster/privatechat.h \
    roster/rosteritem.h \
    roster/rostervisitor.h \
    stanza/base/inbound/inboundstanza.h \
    stanza/base/inbound/invalidstanzaexception.h \
    stanza/base/outbound/outboundstanza.h \
    stanza/bookmarks/inbound/bookmarkresponsestanza.h \
    stanza/bookmarks/inbound/bookmarkretractionnotificationstanza.h \
    stanza/bookmarks/inbound/newbookmarknotificationstanza.h \
    stanza/bookmarks/outbound/addbookmarkrequeststanza.h \
    stanza/bookmarks/outbound/bookmarkrequeststanza.h \
    stanza/disco/inbound/discoinforequeststanza.h \
    stanza/disco/inbound/discoitemsrequeststanza.h \
    stanza/disco/outbound/discoinforesponsestanza.h \
    stanza/disco/outbound/discoitemsresponsestanza.h \
    stanza/message/chatmessagestanza.h \
    stanza/message/errormessagestanza.h \
    stanza/message/inboundmessagestanza.h \
    stanza/message/messagestanza.h \
    stanza/muc/invite/directmucinvitestanza.h \
    stanza/muc/invite/mediatedmucinvitestanza.h \
    stanza/muc/invite/mucinvitestanza.h \
    stanza/muc/mucchatmessagestanza.h \
    stanza/muc/mucjoinrequeststanza.h \
    stanza/muc/mucmessagestanza.h \
    stanza/muc/mucpresencestanza.h \
    stanza/muc/privatemessagestanza.h \
    stanza/muc/subjectchangestanza.h \
    stanza/presence/inbound/inboundpresencestanza.h \
    stanza/presence/outbound/broadcast/onlinepresencestanza.h \
    stanza/presence/outbound/broadcast/presencebroadcaststanza.h \
    stanza/presence/outbound/broadcast/presencewithstatusstanza.h \
    stanza/presence/outbound/broadcast/unavailablepresencestanza.h \
    stanza/presence/outbound/targeted/presencestanza.h \
    stanza/pubsub/inbound/pubsubresultstanza.h \
    stanza/receiver.h \
    stanza/roster/inbound/rosterresultstanza.h \
    stanza/roster/inbound/rostersetstanza.h \
    stanza/roster/inbound/rosterstanzaitem.h \
    stanza/roster/outbound/rostergetstanza.h \
    stanza/sender.h \
    stanza/subscription/inbound/inboundsubscriptionrequeststanza.h \
    stanza/subscription/outbound/subscriptionapprovalstanza.h \
    stanza/subscription/outbound/subscriptiondeniedstanza.h \
    stanza/subscription/outbound/subscriptionrequeststanza.h \
    view/chatinviteview.h \
    view/contactview.h \
    view/mucinviteview.h \
    view/mucview.h \
    view/privatechatview.h \
    view/rosteritemview.h \
    view/rosterview.h \
    xmpp/bookmark.h \
    xmpp/callbacks.h \
    xmpp/capabilities.h \
    xmpp/definitions.h \
    xmpp/jid/barejid.h \
    xmpp/jid/jid.h \
    xmpp/strophehelpers.h \
    xmpp/xmpphandler.h

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

icons.path = /usr/share/icons/hicolor/80x80/apps/
icons.files = icons/*
INSTALLS += icons

sounds.files = sounds/*
INSTALLS += sounds

INCLUDEPATH += $$PWD/../include

contains(MEEGO_EDITION,harmattan) {
    SOURCES += notifications/harmattannotifier.cpp
    HEADERS += notifications/harmattannotifier.h
    LIBS += $$PWD/../lib/harmattan/libstrophe.a \
        $$PWD/../lib/harmattan/libssl.a \
        $$PWD/../lib/harmattan/libcrypto.a -lexpat -lresolv -lcares -lz
    QT += declarative
} else:simulator {
    LIBS += $$PWD/../lib/simulator/libstrophe.a \
        $$PWD/../lib/simulator/libssl.a \
        $$PWD/../lib/simulator/libcrypto.a -lexpat -lresolv -lz
} else {
    LIBS += -lstrophe -lssl -lcrypto
}

# https://doc.qt.io/qt-5/qtquick-porting-qt5.html
greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets qml quick
}

contains(MEEGO_EDITION,harmattan) {
    target.path = /opt/kwak/bin
    sounds.path = /opt/kwak/sounds
    INSTALLS += target
}

CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT
