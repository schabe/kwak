/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include "db/message.h"
#include "stanza/muc/mucchatmessagestanza.h"
#include "stanza/sender.h"
#include "view/mucview.h"


MUCView::MUCView(GroupChat *chat, QObject *parent)
    : RosterItemView(parent)
{
    this->chat = chat;

    QObject::connect(chat, SIGNAL(messagesChanged()),
                this, SIGNAL(messagesChanged()));
    QObject::connect(chat, SIGNAL(subjectChanged()),
                this, SIGNAL(nameChanged()));
}

QString MUCView::getDelegate()
{
    return "MUC.qml";
}

QList<QObject *> MUCView::getMessages() const
{
    QList<QObject *> msg;
    QList<KwakMessage *> messages = this->chat->getMessages();
    QList<KwakMessage *>::iterator it;

    for (it = messages.begin(); it < messages.end(); ++it) {
        msg.append((*it));
    }

    return msg;
}

void MUCView::sendMessage(QObject *obj, QString msg) const
{
    StanzaSender* kwak = dynamic_cast<StanzaSender *>(obj);
    if (kwak) {
        MUCChatMessageStanza stanza(this->chat->getJid(), msg);
        kwak->send(stanza);
    } else {
        qWarning() << "Invalid object received, expected Kwak";
    }
}

QString MUCView::getRoomName() const
{
    QString subject(this->chat->getSubject());

    if (subject != "") {
        return subject;
    } else {
        return QString(this->chat->getJid());
    }
}

QObject* MUCView::getLastMessage() const
{
    QList<KwakMessage *> messages = this->chat->getMessages();

    if (messages.size() > 0) {
        return messages.last();
    }

    return NULL;
}

bool MUCView::hasMessages() const
{
    QList<KwakMessage *> messages = this->chat->getMessages();
    return messages.size() > 0;
}
