/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QObject>

#include "stanza/sender.h"
#include "view/chatinviteview.h"


ChatInviteView::ChatInviteView(ChatInvite *inv, QObject *parent)
    : RosterItemView(parent)
{
    this->invite = inv;
}

QString ChatInviteView::getDelegate()
{
    return "ChatInvite.qml";
}

QString ChatInviteView::getFromJid() const
{
    return this->invite->getJid();
}

void ChatInviteView::accept(QObject *obj)
{
    StanzaSender* kwak = dynamic_cast<StanzaSender *>(obj);
    if (kwak) {
        this->invite->accept(kwak);
    } else {
        qWarning() << "Invalid object received, expected StanzaSender";
    }
}

void ChatInviteView::reject(QObject *obj)
{
    StanzaSender* kwak = dynamic_cast<StanzaSender *>(obj);
    if (kwak) {
        this->invite->reject(kwak);
    } else {
        qWarning() << "Invalid object received, expected StanzaSender";
    }
}
