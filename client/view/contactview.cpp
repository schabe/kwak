/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QObject>

#include "stanza/sender.h"
#include "view/contactview.h"


ContactView::ContactView(Contact *c, QObject *parent)
    : RosterItemView(parent)
{
    this->contact = c;

    QObject::connect(c, SIGNAL(nameChanged()),
                this, SIGNAL(nameChanged()));
    QObject::connect(c, SIGNAL(statusChanged()),
                this, SIGNAL(statusChanged()));
    QObject::connect(c, SIGNAL(messagesChanged()),
                this, SIGNAL(messagesChanged()));
}

QString ContactView::getDelegate()
{
    return "Contact.qml";
}

QString ContactView::getName() const
{
    return this->contact->getName();
}

QString ContactView::getJid() const
{
    return this->contact->getJid();
}

QString ContactView::getStatus() const
{
    return this->contact->getStatus();
}

bool ContactView::hasMessages() const
{
    QList<KwakMessage *> messages = this->contact->getMessages();
    return messages.count() > 0;
}

QList<QObject *> ContactView::getMessages() const
{
    QList<QObject *> msg;
    QList<KwakMessage *> messages = this->contact->getMessages();
    QList<KwakMessage *>::iterator it;

    for (it = messages.begin(); it < messages.end(); ++it) {
        msg.append((*it));
    }

    return msg;
}

QObject* ContactView::getLastMessage() const
{
    QList<KwakMessage *> messages = this->contact->getMessages();

    if (messages.size() > 0) {
        return messages.last();
    }

    return NULL;
}

void ContactView::sendMessage(QObject *obj, QString msg) const
{
    StanzaSender* kwak = dynamic_cast<StanzaSender *>(obj);
    if (kwak) {
        kwak->send(this->contact, msg);
    } else {
        qWarning() << "Invalid object received, expected Kwak";
    }
}
