/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHATVIEW_H
#define CHATVIEW_H

#include <QObject>

#include "roster/muc.h"
#include "rosteritemview.h"


class MUCView : public RosterItemView
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject*> messages READ getMessages NOTIFY messagesChanged)
    Q_PROPERTY(QString name READ getRoomName NOTIFY nameChanged)
    Q_PROPERTY(QObject* lastMessage READ getLastMessage NOTIFY messagesChanged)
    Q_PROPERTY(bool hasMessages READ hasMessages NOTIFY messagesChanged())
public:
    explicit MUCView(GroupChat *, QObject *parent = 0);
    QString getDelegate();
    QList<QObject *> getMessages() const;
    Q_INVOKABLE void sendMessage(QObject *kwak, QString) const;
    QString getRoomName() const;
protected:
    GroupChat *chat;
private:
    bool hasMessages() const;
    QObject* getLastMessage() const;
signals:
    void messagesChanged();
    void nameChanged();
};

#endif // CHATVIEW_H
