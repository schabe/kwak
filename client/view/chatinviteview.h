/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHATINVITEVIEW_H
#define CHATINVITEVIEW_H

#include "roster/chatinvite.h"
#include "view/rosteritemview.h"


class ChatInviteView : public RosterItemView
{
    Q_OBJECT
    Q_PROPERTY(QString from READ getFromJid CONSTANT)
public:
    explicit ChatInviteView(ChatInvite *, QObject *parent = 0);
    QString getDelegate();
    Q_INVOKABLE void accept(QObject *kwak);
    Q_INVOKABLE void reject(QObject *kwak);
private:
    QString getFromJid() const;
    ChatInvite *invite;
};

#endif // CHATINVITEVIEW_H
