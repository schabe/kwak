/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ROSTERVIEW_H
#define ROSTERVIEW_H

#include <QObject>

#include "roster.h"
#include "roster/rostervisitor.h"


class RosterView : public QObject, public RosterVisitor
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject*> contacts READ getContacts NOTIFY contactsChanged)
public slots:
    void updateContacts();
public:
    RosterView(KwakRoster &roster, QObject *parent = 0);
    ~RosterView();
    QList<QObject *> getContacts();
    void visit(ChatInvite *);
    void visit(Contact *);
    void visit(GroupChat *);
    void visit(MUCInvite *);
    void visit(PrivateChat *);
signals:
    void contactsChanged();
private:
    void clearContacts();
    QList<QObject *> contacts;
    KwakRoster &roster;
};

#endif // ROSTERVIEW_H
