/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTACTVIEW_H
#define CONTACTVIEW_H

#include "roster/contact.h"
#include "rosteritemview.h"


class ContactView : public RosterItemView
{
    Q_OBJECT
    Q_PROPERTY(QString name READ getName NOTIFY nameChanged)
    Q_PROPERTY(QString jid READ getJid CONSTANT)
    Q_PROPERTY(QString status READ getStatus NOTIFY statusChanged)
    Q_PROPERTY(QList<QObject*> messages READ getMessages NOTIFY messagesChanged)
    Q_PROPERTY(QObject* lastMessage READ getLastMessage NOTIFY messagesChanged)
    Q_PROPERTY(bool hasMessages READ hasMessages NOTIFY messagesChanged())
public:
    explicit ContactView(Contact *, QObject *parent = 0);
    QString getDelegate();
    QString getName() const;
    QString getJid() const;
    QString getStatus() const;
    QList<QObject *> getMessages() const;
    RosterItem *getItem() const;
    Q_INVOKABLE void sendMessage(QObject *kwak, QString msg) const;
private:
    bool hasMessages() const;
    QObject* getLastMessage() const;
    Contact *contact;
signals:
    void nameChanged();
    void statusChanged();
    void messagesChanged();
};

#endif // CONTACTVIEW_H
