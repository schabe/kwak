/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>

#include "roster/rostervisitor.h"

#include "view/chatinviteview.h"
#include "view/contactview.h"
#include "view/mucinviteview.h"
#include "view/mucview.h"
#include "view/privatechatview.h"
#include "view/rosterview.h"


RosterView::RosterView(KwakRoster &roster, QObject *parent)
    : QObject(parent), roster(roster)
{
    QObject::connect(&roster, SIGNAL(rosterChanged()),
                     this, SLOT(updateContacts()));
}

RosterView::~RosterView()
{
    this->clearContacts();
}

void RosterView::clearContacts()
{
    while (!contacts.isEmpty()) {
        QObject *obj = contacts.takeFirst();
        delete(obj);
    }
}

void RosterView::updateContacts()
{
    this->clearContacts();

    QList<RosterItem *>::iterator i;
    QList<RosterItem *>members = roster.getMembers();

    for (i=members.begin(); i != members.end(); ++i) {
        RosterItem *item = *i;
        item->accept(this);
    }

    emit contactsChanged();
}

QList<QObject *>RosterView::getContacts()
{
    return contacts;
}

void RosterView::visit(ChatInvite *invite)
{
    contacts.append(new ChatInviteView(invite));
}

void RosterView::visit(Contact *contact)
{
    contacts.append(new ContactView(contact));
}

void RosterView::visit(GroupChat *chat)
{
    contacts.append(new MUCView(chat));
}

void RosterView::visit(MUCInvite *invite)
{
    contacts.append(new MUCInviteView(invite));
}

void RosterView::visit(PrivateChat *chat)
{
    contacts.append(new PrivateChatView(chat));
}
