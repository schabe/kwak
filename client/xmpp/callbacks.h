/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef XMPP_CALLBACKS_H
#define XMPP_CALLBACKS_H

#include <strophe.h>

int handle_error(xmpp_conn_t* const,
                 xmpp_stanza_t* const, void* const);
int handle_iq_roster_set(xmpp_conn_t* const,
                         xmpp_stanza_t* const, void* const);
int handle_iq_roster_result(xmpp_conn_t* const,
                            xmpp_stanza_t* const, void* const);
int handle_iq_pubsub_result(xmpp_conn_t* const,
                            xmpp_stanza_t* const, void* const);
int handle_iq_disco_get(xmpp_conn_t* const,
                    xmpp_stanza_t* const, void* const);
int handle_iq_disco_info(xmpp_conn_t* const,
                    xmpp_stanza_t* const, void* const);
int handle_message(xmpp_conn_t* const,
                   xmpp_stanza_t* const, void* const);
int handle_presence(xmpp_conn_t* const,
                    xmpp_stanza_t* const, void* const);

#endif // XMPP_CALLBACKS_H
