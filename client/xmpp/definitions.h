/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define STANZA_NAME_BIND            "bind"
#define STANZA_NAME_BODY            "body"
#define STANZA_NAME_C               "c"
#define STANZA_NAME_CONFERENCE      "conference"
#define STANZA_NAME_DELAY           "delay"
#define STANZA_NAME_ERROR           "error"
#define STANZA_NAME_EVENT           "event"
#define STANZA_NAME_FEATURE         "feature"
#define STANZA_NAME_FIELD           "field"
#define STANZA_NAME_IDENTITY        "identity"
#define STANZA_NAME_IQ              "iq"
#define STANZA_NAME_INVITE          "invite"
#define STANZA_NAME_ITEM            "item"
#define STANZA_NAME_ITEMS           "items"
#define STANZA_NAME_JID             "jid"
#define STANZA_NAME_MESSAGE         "message"
#define STANZA_NAME_NICK            "nick"
#define STANZA_NAME_PASSWORD        "password"
#define STANZA_NAME_PRESENCE        "presence"
#define STANZA_NAME_PUBSUB          "pubsub"
#define STANZA_NAME_PUBLISH         "publish"
#define STANZA_NAME_PUBLISH_OPTIONS "publish-options"
#define STANZA_NAME_QUERY           "query"
#define STANZA_NAME_REASON          "reason"
#define STANZA_NAME_RETRACT         "retract"
#define STANZA_NAME_SHOW            "show"
#define STANZA_NAME_SUBJECT         "subject"
#define STANZA_NAME_VALUE           "value"
#define STANZA_NAME_X               "x"

/* iq types */
#define STANZA_TYPE_RESULT          "result"
#define STANZA_TYPE_SET             "set"
#define STANZA_TYPE_GET             "get"

/* message types */
#define STANZA_TYPE_CHAT            "chat"
#define STANZA_TYPE_ERROR           "error"
#define STANZA_TYPE_GROUPCHAT       "groupchat"
#define STANZA_TYPE_HEADLINE        "headline"
#define STANZA_TYPE_HIDDEN          "hidden"
#define STANZA_TYPE_NORMAL          "normal"
#define STANZA_TYPE_SUBMIT          "submit"

/* presence types */
#define STANZA_TYPE_SUBSCRIBE       "subscribe"
#define STANZA_TYPE_SUBSCRIBED      "subscribed"
#define STANZA_TYPE_UNAVAILABLE     "unavailable"
#define STANZA_TYPE_UNSUBSCRIBED    "unsubscribed"

/* attributes */
#define STANZA_ATTR_AUTOJOIN        "autojoin"
#define STANZA_ATTR_CATEGORY        "category"
#define STANZA_ATTR_HASH            "hash"
#define STANZA_ATTR_JID             "jid"
#define STANZA_ATTR_NAME            "name"
#define STANZA_ATTR_NODE            "node"
#define STANZA_ATTR_PASSWORD        "password"
#define STANZA_ATTR_REASON          "reason"
#define STANZA_ATTR_STAMP           "stamp"
#define STANZA_ATTR_SUBSCRIPTION    "subscription"
#define STANZA_ATTR_VAR             "var"
#define STANZA_ATTR_VER             "ver"

#define STANZA_VALUE_SHA1           "sha-1"
#define STANZA_VALUE_TRUE           "true"

/* subscriptions */
#define SUB_BOTH                    "both"
#define SUB_FROM                    "from"
#define SUB_TO                      "to"
#define SUB_NONE                    "none"
#define SUB_REMOVE                  "remove"

/* namespaces */
#define STANZA_NS_BOOKMARKS         "urn:xmpp:bookmarks:1"
#define STANZA_NS_CONFERENCE        "jabber:x:conference"
#define STANZA_NS_CAPS              "http://jabber.org/protocol/caps"
#define STANZA_NS_DATA              "jabber:x:data"
#define STANZA_NS_DELAY             "urn:xmpp:delay"
#define STANZA_NS_MUC               "http://jabber.org/protocol/muc"
#define STANZA_NS_MUC_USER          "http://jabber.org/protocol/muc#user"
#define STANZA_NS_PUBSUB            "http://jabber.org/protocol/pubsub"
#define STANZA_NS_PUBSUB_EVENT      "http://jabber.org/protocol/pubsub#event"


#define STATUS_UNKNOWN              "unknown"
#define STATUS_OFFLINE              "offline"
#define STATUS_ONLINE               "online"
#define STATUS_AWAY                 "away"
#define STATUS_CHAT                 "chat"
#define STATUS_DND                  "dnd"
#define STATUS_XA                   "xa"

#endif // DEFINITIONS_H
