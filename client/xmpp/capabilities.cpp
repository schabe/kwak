/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QString>
#include <QVariant>

#include <stdlib.h>
#include <strophe.h>

#include "capabilities.h"

Capabilities::Capabilities(const QApplication *app)
{
    this->name = app->applicationName() + " " + app->applicationVersion();;
    this->url = app->property("URL").toString();

    /* Category as defined on
     * https://xmpp.org/registrar/disco-categories.html */
    this->type = "phone";
    this->category = "client";

    /* Features as defined on
     * https://xmpp.org/registrar/disco-features.html */
    this->capabilities.append("fullunicode");

    /* XEP-0115 */
    this->capabilities.append("http://jabber.org/protocol/caps");

    /* XEP-0030 */
    this->capabilities.append("http://jabber.org/protocol/disco#info");
    this->capabilities.append("http://jabber.org/protocol/disco#items");

    /* XEP-0045 */
    this->capabilities.append("http://jabber.org/protocol/muc");

    /* RFC 6121 */
    this->capabilities.append("jabber:client");
    this->capabilities.append("jabber:iq:roster");

    /* XEP-0249 */
    this->capabilities.append("jabber:x:conference");

    /* XEP-0402 */
    this->capabilities.append("urn:xmpp:bookmarks:1+notify");
}

QString Capabilities::capString()
{
    QString caps = this->category + "/" + this->type + "//" + this->name + "<";
    QList<QString>::iterator i;

    for (i = this->capabilities.begin(); i != this->capabilities.end(); ++i) {
        caps += *i + "<";
    }

    return caps;
}

QString Capabilities::hash()
{
    xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, NULL);
    unsigned char buf[XMPP_SHA1_DIGEST_SIZE];
    memset(buf, 0, sizeof(buf));

    char *caps = strdup(qPrintable(this->capString()));
    xmpp_sha1_digest((unsigned char *)caps, strlen(caps), buf);
    free(caps);

    char *b64 = xmpp_base64_encode(ctx, (const unsigned char*)buf, sizeof(buf));
    QString digest(b64);
    xmpp_free(ctx, b64);
    xmpp_ctx_free(ctx);

    return digest;
}

QString Capabilities::getNode()
{
    return this->url + "#" + this->hash();
}

QList<QString> Capabilities::getCaps()
{
    return this->capabilities;
}

QString Capabilities::getName()
{
    return this->name;
}

QString Capabilities::getType()
{
    return this->type;
}

QString Capabilities::getCategory()
{
    return this->category;
}
