/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STROPHEHELPERS_H
#define STROPHEHELPERS_H

#include <QString>

#include <strophe.h>


char *xmpp_str_from_qstr(const QString &);

void xmpp_stanza_add_text(xmpp_stanza_t *, QString);
void xmpp_stanza_add_capabilities(xmpp_stanza_t *);
void xmpp_stanza_adopt_child(xmpp_stanza_t *parent, xmpp_stanza_t *child);
void xmpp_stanza_set_attribute_q(xmpp_stanza_t *, const char *k, QString v);
void xmpp_stanza_set_from_q(xmpp_stanza_t *, QString from);
void xmpp_stanza_set_to_q(xmpp_stanza_t *, QString to);
xmpp_stanza_t *xmpp_body_new(xmpp_ctx_t *);
xmpp_stanza_t *xmpp_iq_new_q(xmpp_ctx_t *, const char *type, QString id);
xmpp_stanza_t *xmpp_iq_new_with_random_id(xmpp_ctx_t *, const char *type);
xmpp_stanza_t *xmpp_message_new_q(xmpp_ctx_t *, const char *type, QString to,
                                  QString id);
int xmpp_message_set_body_q(xmpp_stanza_t *, QString body);
xmpp_stanza_t *xmpp_presence_new_with_random_id(xmpp_ctx_t *);
xmpp_stanza_t *xmpp_pubsub_new(xmpp_ctx_t *);

bool xmpp_stanza_do_autojoin(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_bookmark_retraction(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_conference(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_delay(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_invite(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_item(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_items(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_nick(xmpp_stanza_t *);
const char *xmpp_stanza_get_node(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_pubsub(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_pubsub_event(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_roster_query(xmpp_stanza_t *);
const char *xmpp_stanza_get_stamp(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_subject(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_x_conference(xmpp_stanza_t *);
xmpp_stanza_t *xmpp_stanza_get_x_muc_user(xmpp_stanza_t *);
#endif // STROPHEHELPERS_H
