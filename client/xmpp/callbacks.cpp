/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include "account.h"
#include "stanza/base/inbound/invalidstanzaexception.h"
#include "stanza/disco/inbound/discoinforequeststanza.h"
#include "stanza/disco/inbound/discoitemsrequeststanza.h"
#include "stanza/disco/outbound/discoinforesponsestanza.h"
#include "stanza/message/inboundmessagestanza.h"
#include "stanza/presence/inbound/inboundpresencestanza.h"
#include "stanza/pubsub/inbound/pubsubresultstanza.h"
#include "stanza/roster/inbound/rosterresultstanza.h"
#include "stanza/roster/inbound/rostersetstanza.h"
#include "stanza/subscription/inbound/inboundsubscriptionrequeststanza.h"
#include "xmpp/definitions.h"


int handle_error(xmpp_conn_t *const conn, xmpp_stanza_t *, void *const)
{
    qWarning() << "An error occurred";

    /* RFC 6120 Section 4.9.1.1: Disconnect on error */
    xmpp_disconnect(conn);

    return 1;
}

int handle_iq_roster_result(xmpp_conn_t* const, xmpp_stanza_t* const stanza,
                            void* const data)
{
    qDebug() << "handle_iq_roster_result";
    StanzaReceiver *worker = (StanzaReceiver *) data;

    try {
        InboundRosterResultStanza(stanza).receive(worker);
    } catch (InvalidStanzaException &e) {
        qWarning() << "Failed to load roster result:" << e.what();
    }

    return 1;
}

int handle_iq_pubsub_result(xmpp_conn_t* const, xmpp_stanza_t* const stanza,
                            void* const data)
{
    qDebug() << "handle_iq_pubsub_result";
    StanzaReceiver *worker = (StanzaReceiver *) data;
    InboundStanza *result = NULL;

    try {
        result = InboundPubSubResultStanza::load(stanza);
    } catch (InvalidStanzaException &e) {
        qWarning() << "Failed to load pubsub result:" << e.what();
        return 1;
    }

    if (result) {
        result->receive(worker);
        delete result;
    } else {
        qWarning("Failed to load pubsub message");
    }

    return 1;
}

/* A roster push as defined in RFC 6121 Section 2.1.6 */
int handle_iq_roster_set(xmpp_conn_t* const, xmpp_stanza_t* const stanza,
                         void* const data)
{
    qDebug() << "handle_iq_roster_set";
    StanzaReceiver *worker = (StanzaReceiver *) data;

    try {
        InboundRosterSetStanza(stanza).receive(worker);
    } catch (InvalidStanzaException &e) {
        qWarning() << "Failed to load roster set stanza:" << e.what();
    }

    return 1;
}

/* XEP 0030 - Service discovery */
int handle_iq_disco_get(xmpp_conn_t* const, xmpp_stanza_t* const stanza,
                        void* const data)
{
    qDebug() << "handle_iq_disco_get";
    StanzaReceiver *worker = (StanzaReceiver *) data;

    try {
        InboundDiscoItemsRequestStanza(stanza).receive(worker);
    } catch (InvalidStanzaException &e) {
        qWarning() << "Failed to load disco item request:" << e.what();
    }

    return 1;
}

/* XEP 0030 - Discover capabilities */
int handle_iq_disco_info(xmpp_conn_t* const, xmpp_stanza_t* const stanza,
                         void* const data)
{
    qDebug() << "handle_iq_disco_info";
    StanzaReceiver *worker = (StanzaReceiver *) data;

    try {
        InboundDiscoInfoRequestStanza(stanza).receive(worker);
    } catch (InvalidStanzaException &e) {
        qWarning() << "Failed to load disco info request:" << e.what();
    }

    return 1;
}

int handle_message(xmpp_conn_t* const, xmpp_stanza_t* const stanza,
                   void* const data)
{
    qDebug() << "handle_message";
    StanzaReceiver *worker = static_cast<StanzaReceiver *>(data);

    try {
        InboundStanza *msg = InboundMessageStanza::load(stanza);
        msg->receive(worker);
    } catch (...) {
        qWarning("Failed to handle messsage"); // TODO: should be 'load'
    }

    return 1;
}

int handle_presence(xmpp_conn_t *const, xmpp_stanza_t *const stanza,
                    void *const data)
{
    qDebug() << "handle_presence";
    StanzaReceiver *worker = (StanzaReceiver *) data;

    try {
        const char *type = xmpp_stanza_get_type(stanza);

        // RFC 6121, Section 3.1: A "subscription request" [..] is a presence
        // stanza whose 'type' attribute has a value of "subscribe"
        if ((type != NULL) && (!strcmp(type, STANZA_TYPE_SUBSCRIBE))) {
            InboundSubscriptionRequestStanza(stanza).receive(worker);
        } else {
            InboundPresenceStanza(stanza).receive(worker);
        }
    } catch (InvalidStanzaException &e) {
        qWarning() << "Failed to load presence message:" << e.what();
    }

    return 1;
}
