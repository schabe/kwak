/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOOKMARK_H
#define BOOKMARK_H

#include "xmpp/jid/jid.h"


class Bookmark
{
public:
    Bookmark();
    Bookmark(const Jid &, const QString &, bool autojoin);
    Jid getJid() const;
    QString getNick() const;
    bool shouldAutojoin() const;
private:
    Jid jid;
    QString nick;
    bool autojoin;
};

#endif // BOOKMARK_H
