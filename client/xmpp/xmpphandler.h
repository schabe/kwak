/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef XMPPHANDLER_H
#define XMPPHANDLER_H

#include <strophe.h>


/* Interface for classes that send libstrophe stanzas */

class XMPPHandler
{
public:
    virtual void send(xmpp_stanza_t *) = 0;
    virtual xmpp_ctx_t *getContext() const = 0 ;
};

class StropheHandler : public XMPPHandler
{
public:
    StropheHandler(xmpp_conn_t *);
    void send(xmpp_stanza_t *);
    xmpp_ctx_t *getContext() const;
private:
    xmpp_conn_t *connection;
};

#endif // XMPPHANDLER_H
