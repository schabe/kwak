/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h> // for 'free'
#include <string.h>

#include "definitions.h"
#include "strophehelpers.h"


char *xmpp_str_from_qstr(const QString &str)
{
    QByteArray qb = str.toLocal8Bit();
    return strdup(qb.data());
}

void xmpp_stanza_add_text(xmpp_stanza_t *stanza, QString text)
{
    xmpp_ctx_t *ctx = xmpp_stanza_get_context(stanza);
    xmpp_stanza_t *inner = xmpp_stanza_new(ctx);
    char *text_ = xmpp_str_from_qstr(text);
    xmpp_stanza_set_text(inner, text_);
    xmpp_stanza_add_child(stanza, inner);
    free(text_);
}

void xmpp_stanza_adopt_child(xmpp_stanza_t *parent, xmpp_stanza_t *child)
{
    xmpp_stanza_add_child(parent, child);
    xmpp_stanza_release(child);
}

void xmpp_stanza_set_attribute_q(xmpp_stanza_t *s, const char *k, QString v)
{
    char *str = xmpp_str_from_qstr(v);
    xmpp_stanza_set_attribute(s, k, str);
    free(str);
}

void xmpp_stanza_set_from_q(xmpp_stanza_t *stanza, QString from)
{
    char *str = xmpp_str_from_qstr(from);
    xmpp_stanza_set_from(stanza, str);
    free(str);
}

void xmpp_stanza_set_to_q(xmpp_stanza_t *stanza, QString to)
{
    char *str = xmpp_str_from_qstr(to);
    xmpp_stanza_set_to(stanza, str);
    free(str);
}

xmpp_stanza_t *xmpp_message_new_q(xmpp_ctx_t *ctx, const char *type,
                                  QString to, QString id)
{
    char *id_ = xmpp_str_from_qstr(id);
    char *to_ = xmpp_str_from_qstr(to);
    xmpp_stanza_t *msg = xmpp_message_new(ctx, type, to_, id_);
    free(to_);
    free(id_);

    return msg;
}

int xmpp_message_set_body_q(xmpp_stanza_t *msg, QString body)
{
    char *str = xmpp_str_from_qstr(body);
    int res = xmpp_message_set_body(msg, str);
    free(str);

    return res;
}

xmpp_stanza_t *xmpp_presence_new_with_random_id(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *stanza = xmpp_presence_new(ctx);

    char *id = xmpp_uuid_gen(ctx);
    xmpp_stanza_set_id(stanza, id);
    xmpp_free(ctx, id);

    return stanza;
}

xmpp_stanza_t *xmpp_body_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *body = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(body, STANZA_NAME_BODY);
    return body;
}

xmpp_stanza_t *xmpp_iq_new_q(xmpp_ctx_t *ctx, const char *type, QString id)
{
    char *str = xmpp_str_from_qstr(id);
    xmpp_stanza_t *iq = xmpp_iq_new(ctx, type, str);
    free(str);
    return iq;
}

xmpp_stanza_t *xmpp_iq_new_with_random_id(xmpp_ctx_t *ctx, const char *type)
{
    char *id = xmpp_uuid_gen(ctx);
    xmpp_stanza_t *iq = xmpp_iq_new(ctx, type, id);
    xmpp_free(ctx, id);

    return iq;
}

xmpp_stanza_t *xmpp_pubsub_new(xmpp_ctx_t *ctx)
{
    xmpp_stanza_t *pubsub = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(pubsub, STANZA_NAME_PUBSUB);
    xmpp_stanza_set_ns(pubsub, STANZA_NS_PUBSUB);

    return pubsub;
}

bool xmpp_stanza_do_autojoin(xmpp_stanza_t *stanza)
{
    const char *ajoin = xmpp_stanza_get_attribute(stanza, STANZA_ATTR_AUTOJOIN);

    if (!ajoin) {
        return false;
    }

    return ((strcmp(ajoin, "1") == 0) || (strcmp(ajoin, "true") == 0));
}

xmpp_stanza_t *xmpp_stanza_get_bookmark_retraction(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name(stanza, STANZA_NAME_RETRACT);
}

xmpp_stanza_t *xmpp_stanza_get_conference(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name_and_ns(
                stanza, STANZA_NAME_CONFERENCE, STANZA_NS_BOOKMARKS);
}

xmpp_stanza_t *xmpp_stanza_get_delay(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name_and_ns(stanza, STANZA_NAME_DELAY,
                                                STANZA_NS_DELAY);
}

xmpp_stanza_t *xmpp_stanza_get_invite(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name(stanza, STANZA_NAME_INVITE);
}

xmpp_stanza_t *xmpp_stanza_get_item(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name(stanza, STANZA_NAME_ITEM);
}

xmpp_stanza_t *xmpp_stanza_get_items(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name(stanza, STANZA_NAME_ITEMS);
}

xmpp_stanza_t *xmpp_stanza_get_nick(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name(stanza, STANZA_NAME_NICK);
}

const char *xmpp_stanza_get_node(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_attribute(stanza, STANZA_ATTR_NODE);
}

xmpp_stanza_t *xmpp_stanza_get_pubsub(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name_and_ns(stanza,
                                        STANZA_NAME_PUBSUB, STANZA_NS_PUBSUB);
}

xmpp_stanza_t *xmpp_stanza_get_pubsub_event(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name_and_ns(stanza,
                                    STANZA_NAME_EVENT, STANZA_NS_PUBSUB_EVENT);
}

xmpp_stanza_t *xmpp_stanza_get_roster_query(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name_and_ns(stanza,
                                    STANZA_NAME_QUERY, XMPP_NS_ROSTER);
}

const char *xmpp_stanza_get_stamp(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_attribute(stanza, STANZA_ATTR_STAMP);
}

xmpp_stanza_t *xmpp_stanza_get_subject(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name(stanza, STANZA_NAME_SUBJECT);
}

xmpp_stanza_t *xmpp_stanza_get_x_conference(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name_and_ns(stanza, STANZA_NAME_X,
                                                STANZA_NS_CONFERENCE);
}

xmpp_stanza_t *xmpp_stanza_get_x_muc_user(xmpp_stanza_t *stanza)
{
    return xmpp_stanza_get_child_by_name_and_ns(stanza, STANZA_NAME_X,
                                                STANZA_NS_MUC_USER);
}
