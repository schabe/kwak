/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BAREJID_H
#define BAREJID_H

#include "jid.h"


/* This class represents a 'bare' Jid, i.e. one without a resource part */
class BareJid
{
public:
    BareJid();
    BareJid(const Jid &);
    operator QString() const;
private:
    QString jid;

    friend bool operator== (const BareJid &self, const BareJid &other)
    {
        return (self.jid == other.jid);
    }
};

#endif // BAREJID_H
