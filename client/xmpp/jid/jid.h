/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KWAK_JID_H
#define KWAK_JID_H

#include <QString>

/* forward declaration */
class BareJid;


class Jid
{
public:
    Jid();
    explicit Jid(const QString &);
    Jid(const BareJid &, const QString &resource);
    operator QString() const;
    QString getResourcePart() const;
    bool isBare() const;
private:
    QString bare; /* Bare JID as per RFC 6120 */
    QString resourcepart; /* Resource part as per RFC 6120 */

    friend class BareJid;
    friend bool operator== (const Jid &self, const Jid &other)
    {
        return (self.bare == other.bare) && (self.resourcepart == other.resourcepart);
    }
    friend bool operator!= (const Jid &self, const Jid &other)
    {
        return !(self == other);
    }
};

#endif // KWAK_JID_H
