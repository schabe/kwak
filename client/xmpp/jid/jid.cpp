/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QString>
#include <QStringList>

#include "jid.h"
#include "barejid.h"


Jid::Jid()
{
}

Jid::Jid(const QString &jid)
{
    QStringList parts = jid.split("/");

    this->bare = parts[0];
    if (parts.length() > 1) {
        this->resourcepart = parts[1];
    } else {
        this->resourcepart = "";
    }
}

Jid::Jid(const BareJid &bare, const QString &resource)
{
    this->bare = QString(bare);
    this->resourcepart = QString(resource);
}

QString Jid::getResourcePart() const
{
    return this->resourcepart;
}

bool Jid::isBare() const
{
    return this->resourcepart == "";
}

Jid::operator QString() const
{
    if (this->resourcepart == "") {
        return bare;
    } else {
        return bare + "/" + this->resourcepart;
    }
}
