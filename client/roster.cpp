/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "roster.h"

#include <QDebug>

#include <strophe.h>

#include "db/messagedb.h"
#include "roster/chatinvite.h"
#include "roster/contact.h"
#include "roster/mucinvite.h"
#include "roster/rosteritem.h"
#include "stanza/roster/inbound/rosterresultstanza.h"
#include "stanza/roster/inbound/rostersetstanza.h"


KwakRoster::~KwakRoster()
{
    this->itemsMutex.lock();
    while (members.count() > 0) {
        delete members.takeAt(0);
    }
    while (chats.count() > 0) {
        delete chats.takeAt(0);
    }
    while (privateChats.count() > 0) {
        delete privateChats.takeAt(0);
    }
    while (invites.count() > 0) {
        delete invites.takeAt(0);
    }
    this->itemsMutex.unlock();
}

Contact *KwakRoster::addMember(const BareJid &jid, const QString &name)
{
    Contact *member = new Contact(jid, name);

    this->itemsMutex.lock();
    members.append(member);
    this->itemsMutex.unlock();

    statusCacheMutex.lock();
    if (statusCache.contains(jid)) {
        QString status = statusCache.take(jid);
        statusCacheMutex.unlock(); /* unlock early */
        member->setStatus(status, Jid(jid));
    } else {
        statusCacheMutex.unlock();
    }

    return member;
}

void KwakRoster::updateMember(Contact *m, const RosterStanzaItem &stanza)
{
    if (stanza.getSubscription() == RosterSubscription::Remove) {

        this->itemsMutex.lock();
        members.removeAll(m);
        this->itemsMutex.unlock();

        delete m;
    } else {
        m->setName(stanza.getName());
    }
}

QList<RosterItem*> KwakRoster::getMembers()
{
    QList<RosterItem*> items;

    this->itemsMutex.lock();
    items.append(members);
    items.append(chats);
    items.append(privateChats);
    items.append(invites);
    this->itemsMutex.unlock();

    return items;
}

QList<GroupChat*> KwakRoster::getMUCs()
{
    QList<GroupChat*> mucs;
    QList<RosterItem*>::iterator i;
    GroupChat *muc;

    this->itemsMutex.lock();
    for (i = chats.begin(); i != chats.end(); ++i) {
        muc = qobject_cast<GroupChat *>(*i);
        if (muc) {
            mucs.append(muc);
        }
    }
    this->itemsMutex.unlock();

    return mucs;
}

Contact *KwakRoster::findContact(const BareJid &jid)
{
    Contact *member;
    QList<RosterItem*>::iterator i;

    this->itemsMutex.lock();
    for (i = members.begin(); i != members.end(); ++i) {
        member = qobject_cast<Contact *>(*i);
        if (member->getJid() == jid) {
            this->itemsMutex.unlock();
            return member;
        }
    }
    this->itemsMutex.unlock();

    return NULL;
}

GroupChat *KwakRoster::findMUC(const BareJid &jid)
{
    GroupChat *member;
    QList<RosterItem*>::iterator i;

    this->itemsMutex.lock();
    for (i = chats.begin(); i != chats.end(); ++i) {
        member = qobject_cast<GroupChat *>(*i);
        if (member->getJid() == jid) {
            this->itemsMutex.unlock();
            return member;
        }
    }
    this->itemsMutex.unlock();

    return NULL;
}

MUCInvite *KwakRoster::findMUCInvite(const BareJid &jid)
{
    MUCInvite *invite;
    QList<RosterItem*>::iterator i;

    this->itemsMutex.lock();
    for (i = invites.begin(); i != invites.end(); ++i) {
        invite = qobject_cast<MUCInvite *>(*i);
        if ((invite != NULL) && (invite->getJid() == jid)) {
            this->itemsMutex.unlock();
            return invite;
        }
    }
    this->itemsMutex.unlock();

    return NULL;
}

PrivateChat *KwakRoster::findPrivateChat(const Jid &jid)
{
    PrivateChat *member;
    QList<RosterItem*>::iterator i;

    this->itemsMutex.lock();
    for (i = privateChats.begin(); i != privateChats.end(); ++i) {
        member = qobject_cast<PrivateChat *>(*i);
        if (member->getJid() == jid) {
            this->itemsMutex.unlock();
            return member;
        }
    }
    this->itemsMutex.unlock();

    return NULL;
}

/* Roster Result */
void KwakRoster::update(const InboundRosterResultStanza *stanza, MessageDB &db)
{
    Contact *contact;
    RosterItem *item;
    RosterStanzaItem *row;
    QList<RosterStanzaItem *> roster = stanza->getRoster();
    QList<RosterStanzaItem *>::iterator i;
    QList<RosterItem *>::iterator j;

    this->itemsMutex.lock();
    QList<RosterItem *> missingMembers(this->members);
    this->itemsMutex.unlock();

    /* update or add members */
    for (i = roster.begin(); i != roster.end(); ++i)
    {
        row = *i;

        if ((item = this->findMUC(row->getJid()))) {
            /* contact is a MUC -> ignore */
            qWarning() << item->getJid() << "not a contact, cannot update";
        } else if ((item = this->findContact(row->getJid()))) {
            missingMembers.removeAll(item);

            contact = qobject_cast<Contact *>(item);
            if (contact) {
                /* contact found -> update it */
                updateMember(contact, *row);
            } else {
                /* roster item found but it's not a contact -> ignore */
                qWarning() << item->getJid() << "not a contact, cannot update";
            }
        } else {
            /* contact not found -> create it */
            contact = addMember(row->getJid(), row->getName());
            contact->loadMessages(db);
        }
    }

    /* remove members missing from the new stanza */
    for (j = missingMembers.begin(); j != missingMembers.end(); ++j)
    {
        contact = (Contact *) *j;

        this->itemsMutex.lock();
        this->members.removeAll(contact);
        this->itemsMutex.unlock();

        delete contact;
    }

    emit rosterChanged();
}

/* Roster Push */
void KwakRoster::update(const InboundRosterSetStanza *stanza, MessageDB &db)
{
    Contact *contact;
    RosterItem *member;
    RosterStanzaItem item = stanza->getItem();
    BareJid jid = item.getJid();

    if ((member = this->findMUC(jid))) {
        /* roster item is a MUC -> ignore */
        qWarning() << member->getJid() << "is not a contact, cannot update";
        return;
    } else if ((contact = this->findContact(jid))) {
        /* contact found -> update it */
        updateMember(contact, item);
    } else {
        /* contact not found -> create it */
        contact = addMember(jid, item.getName());
        contact->loadMessages(db);
    }

    emit rosterChanged();
}

/* Presence broadcast */
void KwakRoster::update(const InboundPresenceStanza *p)
{
    BareJid jid = BareJid(p->getFrom());
    RosterItem *member = findContact(jid);

    if (!member) {
        /* roster item not found -> remember for later */
        statusCacheMutex.lock();
        statusCache[jid] = p->getStatus();
        statusCacheMutex.unlock();

        return;
    }

    Contact* contact = qobject_cast<Contact *>(member);
    if (contact) {
        /* contact found -> update */
        contact->setStatus(p->getStatus(), p->getFrom());
        emit rosterChanged();
    } else {
        /* roster item found but it's not a contact -> ignore */
        qWarning() << member->getJid() << "is not a contact, cannot update";
    }
}

Contact *KwakRoster::subscribe(const BareJid &jid)
{
    Contact *member = addMember(jid, "");
    emit rosterChanged();
    return member;
}

void KwakRoster::add(MUCInvite *inv)
{
    if (this->findMUCInvite(inv->getJid())) {
        qDebug() << "Ignoring duplicate invite to" << inv->getJid();
        return;
    }
    this->itemsMutex.lock();
    this->invites.append(inv);
    this->itemsMutex.unlock();

    QObject::connect(inv, SIGNAL(accepted(QString)),
                     this, SLOT(mucInviteAccepted(QString)));
    QObject::connect(inv, SIGNAL(rejected()),
                     this, SLOT(inviteRejected()));

    emit rosterChanged();
}

void KwakRoster::add(ChatInvite *inv)
{
    this->itemsMutex.lock();
    this->invites.append(inv);
    this->itemsMutex.unlock();

    QObject::connect(inv, SIGNAL(accepted()),
                     this, SLOT(chatInviteAccepted()));
    QObject::connect(inv, SIGNAL(rejected()),
                     this, SLOT(inviteRejected()));

    emit rosterChanged();
}

void KwakRoster::add(GroupChat *chat)
{
    GroupChat *muc = this->findMUC(chat->getJid());

    if (muc) {
        muc->updateMessages(chat);
    } else {
        this->itemsMutex.lock();
        this->chats.append(chat);
        this->itemsMutex.unlock();
    }

    emit rosterChanged();
}

void KwakRoster::add(PrivateChat *chat)
{
    this->itemsMutex.lock();
    this->privateChats.append(chat);
    this->itemsMutex.unlock();

    emit rosterChanged();
}

void KwakRoster::remove(Invite *invite)
{
    this->itemsMutex.lock();
    this->invites.removeAll(invite);
    this->itemsMutex.unlock();
}

void KwakRoster::inviteRejected()
{
    QObject *sender = QObject::sender();
    Invite *inv = dynamic_cast<Invite*>(sender);

    if (inv) {
        this->remove(inv);
        delete(inv);
        emit rosterChanged();
    }
}

void KwakRoster::chatInviteAccepted()
{
    QObject *sender = QObject::sender();
    ChatInvite *inv = dynamic_cast<ChatInvite*>(sender);

    if (inv) {
        BareJid jid(inv->getJid());
        this->addMember(jid, "");

        this->remove(inv);
        delete(inv);
        emit rosterChanged();
    }
}

void KwakRoster::mucInviteAccepted(QString nick)
{
    QObject *sender = QObject::sender();
    MUCInvite *inv = dynamic_cast<MUCInvite*>(sender);

    if (inv) {
        BareJid jid(inv->getJid());
        GroupChat *muc = new GroupChat(jid, nick);
        this->add(muc);

        this->remove(inv);
        delete(inv);
        emit rosterChanged();
    }
}
