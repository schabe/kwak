import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.1

import "Dialogs"
import "Messages/MUC"
import "Messages/PrivateChat"


PageStackWindow {
    id: appWindow

    Component.onCompleted: {
        theme.inverted = true
    }

    initialPage: mainPage

    LoginScreen {
        visible: (kwakClient.status === "Offline" ||
                  kwakClient.status === "Connecting")
    }

    BusyIndicator {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        platformStyle: BusyIndicatorStyle { size: 'large' }
        id: busy
        running: visible
        z: 100
        visible: (kwakClient.status === "Connecting" ||
                  kwakClient.status === "Disconnecting")
    }

    MainPage {
        id: mainPage
    }

    InfoBanner {
        id: banner

        anchors {
            top: parent.top
            topMargin: 42
        }
    }

    Connections {
        target: kwakClient

        onErrorOccurred: {
            banner.text = message
            banner.show()
        }
    }

    Component {
        id: chat
        ChatPage {}
    }

    Component {
        id: privateChat
        PrivateChat {}
    }

    Component {
        id: muc
        MUC {}
    }

    Component {
        id: chatInvite
        ChatInviteConfirmation {}
    }

    Component {
        id: mucInvite
        MUCInviteConfirmation {}
    }
}
