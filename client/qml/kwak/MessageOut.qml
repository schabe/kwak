import QtQuick 1.1
import com.nokia.meego 1.0

Rectangle {
    color: '#f6f6f6'
    width: parent.width
    height: inner.height + 20

    Rectangle
    {
        id: inner
        height: text.height + time.height + 30
        width: parent.width - 70
        anchors.left: parent.left
        anchors.leftMargin: 10
        color: '#111111'
        border.color: '#000000'
        radius: 15
        y: 15

        Rectangle {
            color: inner.border.color
            border.color: inner.border.color
            height: 25
            width: 25
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            z: -1
        }

        Column {
            z: 1
            height: text.height + time.height
            width: parent.width - 30

            anchors {
                leftMargin: 50
                verticalCenter: parent.verticalCenter
                top: inner.top
                topMargin: 10
            }

            x: 15
            spacing: 10

            Text {
                text: model.modelData.text
                color: '#ffffff';
                id: text
                font.pixelSize: 26
                wrapMode: Text.WordWrap
                width: parent.width
                horizontalAlignment: Text.AlignRight
            }

            Text {
                text: Qt.formatDateTime(model.modelData.sent,"MMMM d, hh:mm:ss")
                id: time
                font.pixelSize: 18
                wrapMode: Text.WordWrap
                width: parent.width
                color: '#dddddd'
                horizontalAlignment: Text.AlignRight
            }
        }
    }
}
