import com.nokia.meego 1.1

QueryDialog {
    property variant invite

    titleText: invite.jid
    message: invite.from + ' invited you to this multi-user chat room. '
             + 'Would you like to join?'
    acceptButtonText : 'Yes'
    rejectButtonText: 'No'

    onAccepted: {
        var component = Qt.createComponent("MUCNickNamePrompt.qml")
        var dialog = component.createObject(appWindow,
                        { invite: invite })
    }
    onRejected: invite.reject()
}
