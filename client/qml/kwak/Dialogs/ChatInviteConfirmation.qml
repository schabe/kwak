import com.nokia.meego 1.1

QueryDialog {
    property variant invite

    titleText: invite.from
    message: 'Allow this person to see your online presence'
              + ' and add them to your roster?'
    acceptButtonText : 'Yes'
    rejectButtonText: 'No'

    onAccepted: invite.accept(kwakClient)
    onRejected: invite.reject(kwakClient)
}
