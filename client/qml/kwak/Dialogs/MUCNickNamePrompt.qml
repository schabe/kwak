import QtQuick 1.1
import com.nokia.meego 1.1

Sheet {
    id: root

    property variant invite

    acceptButtonText: "Join"
    rejectButtonText: "Cancel"

    content: Flickable {
        anchors {
            fill: parent
            leftMargin: 10
            rightMargin: 10
            topMargin: 10
        }

        contentWidth: contentColumn.width
        contentHeight: contentColumn.height

        Column {
            anchors.topMargin: 20
            id: contentColumn
            spacing: 10

            Label {
                width: root.width
                platformStyle: LabelStyle { fontPixelSize: 32 }
                text: invite.jid
            }

            Label {
                width: root.width
                anchors.topMargin: 10
                text: "Please enter your nick name to use in this chat room:"
            }

            TextField {
                width: parent.width - 20
                anchors.topMargin: 10
                id: input
                text: kwakClient.account.jid.split('@')[0]
                placeholderText: "Your nick name for this chat"
            }
        }
    }

    Component.onCompleted: {
        root.open()
    }

    onAccepted: {
        invite.accept(kwakClient, input.text)
    }
}
