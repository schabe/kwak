import QtQuick 1.1
import com.nokia.meego 1.0
import "Roster/"


Page {
    function setPresence(status)
    {
        kwakClient.setPresence(status);
        selectStatus.close();
    }

    Item {
        id: titleBar

        visible: (kwakClient.status === "Online" ||
                  kwakClient.status === "Connection lost")
        width: parent.width
        height: visible ? 72 : 0

        Rectangle {
            anchors.fill: parent
            color: "#0b5795"
            property string status: kwakClient.account.status

            Loader {
                id: image1
                source: 'MyStatus/' + parent.status + '-icon.qml'
                anchors {
                    left: parent.left
                    leftMargin: 20
                    verticalCenter: parent.verticalCenter
                }
            }

            Image {
                id: image2
                source: "image://theme/meegotouch-combobox-indicator-inverted"
                anchors {
                    right: parent.right
                    rightMargin: 20
                    verticalCenter: parent.verticalCenter
                }
            }

            Loader {
                id: label
                source: 'MyStatus/' + parent.status + '-label.qml'
                anchors {
                    left: image1.right
                    leftMargin: 10
                    verticalCenter: parent.verticalCenter
                }
            }

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                enabled: parent.enabled
                onClicked: {
                    selectStatus.open();
                }
            }
        }
    }

    SelectionDialog {
        id: selectStatus
        titleText: qsTr("Change status")
        model: statusModel

        delegate: Item {
            id: listItem
            width: parent.width
            height: mainText.height + 20

            Image {
                id: statusIcon
                source: model.icon
                x: 20
                anchors.verticalCenter: parent.verticalCenter
            }

            Label {
                id: mainText
                text: model.name
                font.pixelSize: 32
                color: "white"
                anchors.left: statusIcon.right
                anchors.leftMargin: 10
                anchors.verticalCenter: parent.verticalCenter
            }

            MouseArea {
                id: statusMouseArea
                anchors.fill: parent
                onClicked: {
                    setPresence(model.status)
                }
            }
        }
    }

    ListModel {
        id: statusModel
        ListElement {
            status: "chat"
            name: "Available for chat"
            icon: "image://theme/icon-m-presence-online"
        }
        ListElement {
            status: "online"
            name: "Online"
            icon: "image://theme/icon-m-presence-online"
        }
        ListElement {
            status: "away"
            name: "Away"
            icon: "image://theme/icon-m-presence-away"
        }
        ListElement {
            status: "xa"
            name: "Extended Away"
            icon: "image://theme/icon-m-presence-busy"
        }
        ListElement {
            status: "dnd"
            name: "Do not disturb"
            icon: "image://theme/icon-m-presence-busy"
        }
        ListElement {
            status: "offline"
            name: "Offline"
            icon: "image://theme/icon-m-presence-offline"
        }
    }

    Roster
    {
        id: contacts
        height: parent.height - titleBar.height
    }
}
