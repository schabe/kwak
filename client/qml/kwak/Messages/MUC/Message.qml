import QtQuick 1.1
import com.nokia.meego 1.0

Rectangle {
    color: 'white'
    width: parent.width
    height: inner.height + 20

    Rectangle {
        width: 64
        height: 64
        id: contactImage
        radius: 1

        anchors {
            top: parent.top
            left: parent.left
            topMargin: 10
            leftMargin: 10
        }

        Image {
            width: 64
            height: 64
            source: "image://theme/icon-m-content-avatar-placeholder"
            anchors {
                top: parent.top
                left: parent.left
            }
        }
    }

    Rectangle {
        id: inner
        height: from.height + text.height + time.height + 20
        width: parent.width - contactImage.width - 22

        anchors {
            left: contactImage.right
            top: contactImage.top
        }

        Column {
            width: parent.width
            x: 10
            spacing: 2

            anchors {
                top: inner.top
            }

            Text {
                text: model.modelData.from
                id: from
                font.pixelSize: 18
                wrapMode: Text.WordWrap
                width: parent.width - 10
            }

            Text {
                text: model.modelData.text
                id: text
                font.pixelSize: 26
                wrapMode: Text.WordWrap
                width: parent.width - 10
            }

            Text {
                text: Qt.formatDateTime(model.modelData.sent, "MMMM d, hh:mm:ss")
                id: time
                font.pixelSize: 18
                wrapMode: Text.WordWrap
                width: parent.width - 10
                color: 'lightgray'
            }
        }
    }
}
