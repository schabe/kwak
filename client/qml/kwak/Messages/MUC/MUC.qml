import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.1

Page {
    property variant muc

    id: page

    Item {
        id: titleBar

        width: parent.width
        height: 72

        Rectangle {
            id: statusBar
            anchors.fill: parent
            color: "#eeeeee"

            /* Only works in Simulator if using 'Harmattan' extras */
            ToolIcon {
                id: back
                platformIconId: "toolbar-back"
                onClicked: pageStack.pop()
                platformStyle: ToolItemStyle {
                    inverted: false
                }
            }
            Label {
                id: label
                width: parent.width - back.width - 25
                horizontalAlignment: Text.AlignRight
                font.pixelSize: 32

                color: "#000000"
                anchors {
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                    rightMargin: 20
                }

                platformStyle: LabelStyle {
                    inverted: true
                }

                elide: Text.ElideRight
                text: muc.name
            }
        }
    }

    Rectangle {
        width: parent.width
        height: parent.height - titleBar.height - footer.height
        color: '#f6f6f6'
        anchors.top: titleBar.bottom
        id: messageListContainer

        ListView {
            id: messages
            width: parent.width
            height: parent.height
            clip: true
            orientation: ListView.Vertical
            anchors.top: parent.top
            model: muc.messages
            currentIndex: muc.messages.length - 1;

            delegate: Loader {
                width: parent.width
                source: "Message.qml"
            }

            Component.onCompleted: {
                positionViewAtEnd()
            }
        }
    }

    Rectangle {
        id: footer
        anchors.top: messageListContainer.bottom
        width: parent.width
        color: 'white'
        height: send.visible ? input.height + send.height + 25 : input.height + 20

        TextArea {
            id: input
            width: parent.width - 20
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            placeholderText: "Write your message here"
        }

        Button {
            text: "Send"
            id: send
            width: 100
            visible: input.activeFocus || input.text != ""
            platformStyle: ButtonStyle {
                fontPixelSize: 24
            }
            anchors.top: input.bottom
            anchors.topMargin: 5
            anchors.right: input.right
            onClicked: {
                muc.sendMessage(kwakClient, input.text);
                input.text = "";
            }
        }
    }
}
