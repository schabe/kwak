import QtQuick 1.1
import com.nokia.meego 1.1


Rectangle {
    width: parent.width
    height: parent.height
    color: 'black'

    Column {
        width: 400
        spacing: 25
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }

        Image {
            width: 200
            height: 200
            source: "Images/kwak200.png"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Row {
            width: parent.width
            spacing: 10
            anchors {
                left: parent.left
            }
            Image {
                width: 64
                height: 64
                id: jidIcon
                source: "image://theme/icon-m-content-avatar-placeholder-inverse"
                anchors.verticalCenter: parent.verticalCenter
            }
            TextField {
                width: parent.width - jidIcon.width - 10
                id: jid
                text: kwakClient.account.jid
                placeholderText: "Jid"
                font.pixelSize: 32
                anchors.verticalCenter: parent.verticalCenter
                enabled: !busy.visible
            }
        }

        Row {
            width: parent.width
            spacing: 10
            anchors {
                left: parent.left
            }
            Image {
                width: 64
                height: 64
                id: passwordIcon
                source: "image://theme/icon-m-common-passcode"
                anchors.verticalCenter: parent.verticalCenter
            }
            TextField {
                width: parent.width - passwordIcon.width - 10
                id: password
                text: kwakClient.account.password
                echoMode: TextInput.Password
                placeholderText: "Password"
                font.pixelSize: 32
                anchors.verticalCenter: parent.verticalCenter
                enabled: !busy.visible
            }
        }

        Button {
            id: connectButton
            anchors {
                horizontalCenter: parent.horizontalCenter
            }

            text: 'Connect'
            enabled: !busy.visible
            onClicked: {
                kwakClient.account.password = password.text
                kwakClient.account.jid = jid.text
                kwakClient.connect();
            }
        }
    }
}
