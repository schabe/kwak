import QtQuick 1.1
import com.nokia.meego 1.0

Rectangle {
    width: parent.width
    visible: (kwakClient.status === "Online" ||
              kwakClient.status === "Connection lost")

    anchors.top: titleBar.bottom
    property variant chatPage

    ListView {
        width: parent.width
        height: parent.height - anchors.bottomMargin - anchors.topMargin
        clip: true
        orientation: ListView.Vertical
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.bottomMargin: 10
        spacing: 10

        model: roster.contacts

        delegate: Loader {
            width: parent.width
            source: model.modelData.delegate
        }
    }

    Text {
        visible: roster.contacts.length < 1
        text: "Your roster is empty"
        font.pixelSize: 50
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenterOffset: -50
        color: 'darkgray'
    }
}
