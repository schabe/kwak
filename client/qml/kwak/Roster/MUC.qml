import com.nokia.meego 1.0
import QtQuick 1.1

Rectangle
{
    height: 80
    width: parent.width

    MouseArea {
        id: scrollContacts
        height: parent.height
        width: parent.width
        hoverEnabled: true
        onPressed: {
            parent.color = '#eeeeee'
        }
        onReleased: {
            parent.color = 'white'

            if (chatPage) {
                chatPage.destroy()
            }

            chatPage = muc.createObject(appWindow, { muc: model.modelData })
            pageStack.push(chatPage)
        }
        onCanceled: {
            parent.color = 'white'
        }
    }

    Row {
        width: parent.width - 20
        anchors.verticalCenter: parent.verticalCenter
        x: 10
        spacing: 10

        Rectangle {
            width: 6
            height: contactImage.height
            color: model.modelData.unread
                    ? '#0b5795'
                    : 'white'
            anchors.verticalCenter: parent.verticalCenter
        }

        Column {
            width: parent.width - contactImage.width - 6 - 20
            anchors {
                leftMargin: 10
                rightMargin: 10
                verticalCenter: parent.verticalCenter
            }
            Label {
                color: "black"
                width: parent.width
                text: model.modelData.name
                font.weight: Font.Bold
                font.pixelSize: 26
                wrapMode: Text.NoWrap
                elide: Text.ElideRight
            }
            Label {
                color: model.modelData.hasMessages
                        ? "black"
                        : "darkgray"
                width: parent.width
                text: model.modelData.hasMessages
                        ? model.modelData.lastMessage.from + ": "
                          + model.modelData.lastMessage.text
                        : "Idle chat"
                font.pixelSize: 26
                font.weight: model.modelData.unread
                        ? Font.Bold
                        : Font.Normal
                wrapMode: Text.NoWrap
                elide: Text.ElideRight
            }
            Label {
                color: "darkgray"
                text: model.modelData.hasMessages
                        ? Qt.formatDateTime(model.modelData.lastMessage.sent,
                                            "MMMM d, hh:mm")
                        : ""
                font.pixelSize: 16
                font.weight: model.unread ? Font.Bold : Font.Normal
            }
        }

        Rectangle {
            width: contactImage.width
            height: contactImage.height

            anchors {
                verticalCenter: parent.verticalCenter
            }

            Image {
                id: contactImage
                width: 64
                height: 64
                source: "image://theme/icon-m-contacts-group"
                anchors {
                    verticalCenter: parent.verticalCenter
                }
            }
        }
    }
}
