import com.nokia.meego 1.0
import QtQuick 1.1

Rectangle
{
    height: 80
    width: parent.width

    MouseArea {
        id: scrollContacts
        height: parent.height
        width: parent.width
        hoverEnabled: true
        onPressed: {
            parent.color = '#eeeeee'
        }
        onReleased: {
            parent.color = 'white'

            var dialog = chatInvite.createObject(appWindow,
                         { invite: model.modelData })
            dialog.open()
        }
        onCanceled: {
            parent.color = 'white'
        }
    }

    Row {
        width: parent.width - 20
        anchors.verticalCenter: parent.verticalCenter
        x: 10
        spacing: 10

        Rectangle {
            width: 6
            height: contactImage.height
            color: '#0b5795'
            anchors.verticalCenter: parent.verticalCenter
        }

        Column {
            width: parent.width - contactImage.width - 6 - 20
            anchors {
                leftMargin: 10
                rightMargin: 10
                verticalCenter: parent.verticalCenter
            }
            Label {
                color: "black"
                width: parent.width
                text: model.modelData.from
                font.weight: Font.Bold
                font.pixelSize: 26
                wrapMode: Text.NoWrap
                elide: Text.ElideRight
            }
            Label {
                color: "black"
                width: parent.width
                text: "Chat invite"
                font.pixelSize: 26
                wrapMode: Text.NoWrap
                elide: Text.ElideRight
            }
        }

        Rectangle {
            width: contactImage.width
            height: contactImage.height

            anchors {
                verticalCenter: parent.verticalCenter
            }

            Image {
                id: contactImage
                width: 64
                height: 64
                source: "image://theme/icon-m-content-avatar-placeholder"
                anchors {
                    verticalCenter: parent.verticalCenter
                }
            }
        }
    }
}
