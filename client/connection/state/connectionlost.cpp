/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h> // rand

#include "client.h"

#include "connection/state/connected.h"
#include "connection/state/connectionlost.h"
#include "connection/state/disconnecting.h"


ConnectionLost::ConnectionLost(Kwak *app, ConnectionHandler *con)
{
    this->app = app;
    this->con = con;

    this->timer = new QTimer(this);
    QObject::connect(this->timer, SIGNAL(timeout()), this, SLOT(timeout()));

    int delay = 10 + rand() % 10; // 10 - 20 seconds random delay
    this->timer->start(delay * 1000);
}

void ConnectionLost::timeout()
{
    con->connect();
}

void ConnectionLost::logout()
{
    this->timer->stop();

    ConnectionState *state = new Disconnecting(app, con);
    this->app->changeState(state);
}

void ConnectionLost::connected(ConnectionHandler *con, const Jid &jid)
{
    this->timer->stop();

    ConnectionState *state = new Connected(app, con, jid);
    app->changeState(state);
}
