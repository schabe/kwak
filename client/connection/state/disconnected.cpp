/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h> // snprintf

#include "connection/state/connecting.h"
#include "connection/state/disconnected.h"
#include "client.h"


Disconnected::Disconnected(Kwak *app)
{
    this->app = app;
}

void Disconnected::login(KwakAccount *account)
{
    xmpp_ctx_t *ctx;
    xmpp_conn_t *conn;
    xmpp_log_t *log;

    char jid[128];
    char pass[128];

    snprintf(pass, sizeof(pass), "%s", qPrintable(account->getPassword()));

    QString resource = account->getResourcePart();
    if (resource != "") {
        snprintf(jid, sizeof(jid), "%s/%s", qPrintable(account->getJid()),
                 qPrintable(resource));
    } else {
        snprintf(jid, sizeof(jid), "%s", qPrintable(account->getJid()));
    }

    long flags = 0;
    flags |= XMPP_CONN_FLAG_TRUST_TLS;                      /* TODO: optional */

//    log = xmpp_get_default_logger(XMPP_LEVEL_DEBUG);
    log = xmpp_get_default_logger(XMPP_LEVEL_INFO);
    ctx = xmpp_ctx_new(NULL, log);
    conn = xmpp_conn_new(ctx);                              /* TODO: delete */
    xmpp_conn_set_flags(conn, flags);

    xmpp_conn_set_jid(conn, jid);
    xmpp_conn_set_pass(conn, pass);

    ConnectionHandler *con = new ConnectionHandler(conn, app);
    int res = con->connect();

    if (res == XMPP_EOK) {
        ConnectionState *state = new Connecting(this->app);
        app->changeState(state);
        con->start();
    } else {
        emit errorOccurred("Cannot connect to server");
    }
}
