/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONNECTING_H
#define CONNECTING_H

#include "connection/handler.h"
#include "connection/state.h"
#include "xmpp/jid/jid.h"


class Connecting : public ConnectionState
{
    Q_OBJECT
public:
    QString getName() { return QString("Connecting"); }
    explicit Connecting(Kwak*);
    void login(KwakAccount*) {}
    void logout() {}
    void connected(ConnectionHandler*, const Jid &);
    void disconnected(ConnectionHandler*);
    void setPresence(QString) {}
    void send(OutboundStanza&) {}
};

#endif // CONNECTING_H
