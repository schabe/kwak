/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "client.h"

#include "connection/state/connected.h"
#include "connection/state/disconnecting.h"
#include "connection/state/connectionlost.h"
#include "xmpp/definitions.h"
#include "xmpp/jid/jid.h"


Connected::Connected(Kwak *app, ConnectionHandler *con, const Jid &jid)
{
    this->app = app;
    this->connection = con;

    KwakAccount *account = app->getAccount();
    account->update(jid);

    con->setPresence(STATUS_ONLINE);
    con->requestRoster(account);
    con->requestBookmarks();

    /* for now, assume our status is 'online' */
    account->setStatus(STATUS_ONLINE);
}

void Connected::logout()
{
    this->connection->setPresence(STATUS_OFFLINE);

    ConnectionState *state = new Disconnecting(app, connection);
    this->app->changeState(state);
}

void Connected::disconnected(ConnectionHandler *worker)
{
    ConnectionState *state = new ConnectionLost(app, worker);
    this->app->changeState(state);
}

void Connected::send(OutboundStanza &stanza)
{
    this->connection->send(stanza);
}

void Connected::setPresence(QString a)
{
    this->connection->setPresence(a);
}
