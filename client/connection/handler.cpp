/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QDebug>
#include <QObject>

#include "connection/handler.h"

#include "account.h"
#include "client.h"
#include "roster/chatinvite.h"
#include "roster/mucinvite.h"
#include "stanza/bookmarks/inbound/bookmarkresponsestanza.h"
#include "stanza/bookmarks/inbound/newbookmarknotificationstanza.h"
#include "stanza/bookmarks/outbound/bookmarkrequeststanza.h"
#include "stanza/disco/inbound/discoinforequeststanza.h"
#include "stanza/disco/outbound/discoinforesponsestanza.h"
#include "stanza/disco/outbound/discoitemsresponsestanza.h"
#include "stanza/message/chatmessagestanza.h"
#include "stanza/muc/invite/mucinvitestanza.h"
#include "stanza/muc/mucjoinrequeststanza.h"
#include "stanza/muc/mucpresencestanza.h"
#include "stanza/presence/outbound/broadcast/presencebroadcaststanza.h"
#include "stanza/roster/outbound/rostergetstanza.h"
#include "stanza/subscription/inbound/inboundsubscriptionrequeststanza.h"
#include "xmpp/callbacks.h"
#include "xmpp/capabilities.h"
#include "xmpp/definitions.h"


ConnectionHandler::ConnectionHandler(xmpp_conn_t *conn, Kwak *app)
    : xmpp(conn)
{
    this->conn = conn;
    this->app = app;

    QObject::connect(this, SIGNAL(connected(ConnectionHandler*, const Jid &)),
                app, SLOT(connected(ConnectionHandler*, const Jid &)));
    QObject::connect(this, SIGNAL(disconnected(ConnectionHandler*)),
                app, SLOT(disconnected(ConnectionHandler*)));

    this->registerHandlers();
}

void ConnectionHandler::setPresence(const QString &status)
{
    this->setAccountPresence(status);
    this->setMUCPresence(status);
}

void ConnectionHandler::setAccountPresence(const QString &status)
{
    OutboundStanza *stanza = OutboundPresenceBroadcastStanza::fromStatus(status);
    stanza->send(this->xmpp);
    delete stanza;
}

void ConnectionHandler::setMUCPresence(const QString &status)
{
    QList<GroupChat*> mucs = this->app->getMUCs();
    QList<GroupChat*>::iterator i;

    for (i = mucs.begin(); i != mucs.end(); ++i) {
        GroupChat *muc = *i;
        MUCPresenceStanza presence(status, *muc);
        presence.send(xmpp);
    }
}

void ConnectionHandler::requestRoster(KwakAccount *account)
{
    Jid jid(account->getJid());
    OutboundRosterGetStanza req(jid);
    req.send(this->xmpp);
}

void ConnectionHandler::requestBookmarks()
{
    OutboundBookmarkRequestStanza req;
    req.send(this->xmpp);
}

void ConnectionHandler::registerHandlers()
{
    /* error stanzas */
    xmpp_handler_add(conn, handle_error, XMPP_NS_STREAMS,
                     STANZA_NAME_ERROR, NULL, NULL);

    /* IQ stanzas */
    xmpp_handler_add(conn, handle_iq_roster_result, XMPP_NS_ROSTER,
                     STANZA_NAME_IQ, STANZA_TYPE_RESULT,
                     dynamic_cast<StanzaReceiver *>(this));
    xmpp_handler_add(conn, handle_iq_pubsub_result, STANZA_NS_PUBSUB,
                     STANZA_NAME_IQ, STANZA_TYPE_RESULT,
                     dynamic_cast<StanzaReceiver *>(this));
    xmpp_handler_add(conn, handle_iq_roster_set, XMPP_NS_ROSTER,
                     STANZA_NAME_IQ, STANZA_TYPE_SET,
                     dynamic_cast<StanzaReceiver *>(this));
    xmpp_handler_add(conn, handle_iq_disco_get, XMPP_NS_DISCO_ITEMS,
                     STANZA_NAME_IQ, STANZA_TYPE_GET,
                     dynamic_cast<StanzaReceiver *>(this));
    xmpp_handler_add(conn, handle_iq_disco_info, XMPP_NS_DISCO_INFO,
                     STANZA_NAME_IQ, STANZA_TYPE_GET,
                     dynamic_cast<StanzaReceiver *>(this));

    /* message stanzas */
    xmpp_handler_add(conn, handle_message, NULL,
                     STANZA_NAME_MESSAGE, NULL,
                     dynamic_cast<StanzaReceiver *>(this));

    /* presence stanzas */
    xmpp_handler_add(conn, handle_presence, NULL,
                     STANZA_NAME_PRESENCE, NULL,
                     dynamic_cast<StanzaReceiver *>(this));
}

void ConnectionHandler::deregisterHandlers()
{
    xmpp_handler_delete(conn, handle_error);
    xmpp_handler_delete(conn, handle_iq_roster_result);
    xmpp_handler_delete(conn, handle_iq_pubsub_result);
    xmpp_handler_delete(conn, handle_iq_roster_set);
    xmpp_handler_delete(conn, handle_iq_disco_get);
    xmpp_handler_delete(conn, handle_iq_disco_info);
    xmpp_handler_delete(conn, handle_message);
    xmpp_handler_delete(conn, handle_presence);
}

void ConnectionHandler::run()
{
    xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
    xmpp_run(ctx);

    qDebug("ConnectionHandler::run (OVER)");

    xmpp_conn_release(conn);
    xmpp_ctx_free(ctx);
}

void ConnectionHandler::stop()
{
    qDebug("ConnectionHandler::stop");
    this->deregisterHandlers();
    xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
    xmpp_stop(ctx);
}

void ConnectionHandler::close()
{
    qDebug("ConnectionHandler::close");
    xmpp_disconnect(conn);
}

void ConnectionHandler::handle(const ChatMessageStanza *msg)
{
    Jid jid(msg->getFrom());

    if (this->app->isMUC(BareJid(jid))) {
        /* if 'from' address is a MUC, treat as private message
         * (XEP-0045 Section 7.5) */
        PrivateMessageStanza pm(msg->getTo(), msg->getBody());
        this->app->receive(&pm);
    } else if (msg->getBody() == "") {
        qDebug() << "Ignoring empty message";
    } else {
        this->app->receive(*msg, conn);
    }
}

void ConnectionHandler::handle(const PrivateMessageStanza *msg)
{
    this->app->receive(msg);
}

void ConnectionHandler::handle(const InboundPresenceStanza *presence)
{
    KwakAccount *account = this->app->getAccount();

    /* TODO: the account should use a Jid object, that would make this a bit
     * simpler */
    Jid accountJid(account->getJid());
    Jid accountFull(BareJid(accountJid), account->getResourcePart());

    if (accountFull == presence->getFrom()) {
        /* presence from myself */
        account->setStatus(presence->getStatus());
    } else {
        this->app->updateRoster(presence);
    }
}

void ConnectionHandler::handle(const InboundRosterResultStanza *stanza)
{
    this->app->updateRoster(stanza);
}

void ConnectionHandler::handle(const InboundRosterSetStanza *stanza)
{
    this->app->updateRoster(stanza);
}

void ConnectionHandler::join(const Bookmark &bookmark)
{
    Jid muc = bookmark.getJid();
    QString nick = bookmark.getNick();
    MUCJoinRequestStanza join(muc, nick);
    join.send(this->xmpp);
}

void ConnectionHandler::handle(const BookmarkResponseStanza *stanza)
{
    QList<Bookmark*> bookmarks = stanza->getBookmarks();
    QList<Bookmark*>::iterator i;

    for (i = bookmarks.begin(); i != bookmarks.end(); ++i) {
        Bookmark *bookmark = (*i);

        if (bookmark->shouldAutojoin()) {
            this->join(*bookmark);
        }

        BareJid jid(bookmark->getJid());
        GroupChat *chat = new GroupChat(jid, bookmark->getNick());
        this->app->addMUC(chat);
    }
}

void ConnectionHandler::handle(
        const InboundNewBookmarkNotificationStanza *stanza)
{
    Bookmark bookmark = stanza->getBookmark();

    if (bookmark.shouldAutojoin()) {
        this->join(bookmark);
    }
}

void ConnectionHandler::handle(
        const InboundBookmarkRetractionNotificationStanza *stanza)
{
    Bookmark bookmark = stanza->getBookmark();
    qWarning() << "LEAVE" << bookmark.getJid(); /* TODO */
}

void ConnectionHandler::handle(const InboundDiscoItemsRequestStanza *stanza)
{
    OutboundDiscoItemsResponseStanza res(stanza->getFrom(), stanza->getId());
    res.send(this->xmpp);
}

void ConnectionHandler::handle(const MUCChatMessageStanza *msg)
{
    this->app->receive(msg);
}

void ConnectionHandler::handle(const MUCSubjectChangeStanza *msg)
{
    this->app->receive(msg);
}

void ConnectionHandler::handle(const InboundSubscriptionRequestStanza *req)
{
    ChatInvite *invite = req->getInvite();
    this->app->receive(invite);
}

void ConnectionHandler::handle(const MUCInviteStanza *inv)
{
    MUCInvite *invite = inv->getInvite();
    this->app->receive(invite);
}

void ConnectionHandler::handle(const InboundDiscoInfoRequestStanza *req)
{
    QApplication *app = qApp;
    Capabilities caps(app);
    QString node = req->getNode();

    if (node == caps.getNode()) {
        /* XEP-0115 */
        OutboundDiscoInfoResponseStanza res(req->getId(), req->getTo(),
                                            req->getFrom(), caps, node);
        res.send(this->xmpp);
    } else if (node == "") {
        /* XEP-0030, no node */
        OutboundDiscoInfoResponseStanza res(req->getId(), req->getTo(),
                                            req->getFrom(), caps);
        res.send(this->xmpp);
    } else {
        /* XEP-0030 for specific node */
        qWarning() << "Unsupported disco info for node" << node;
    }
}

void ConnectionHandler::send(OutboundStanza &stanza)
{
    stanza.send(this->xmpp);
}

void ConnectionHandler::handler(xmpp_conn_t *c, xmpp_conn_event_t status,
        int, xmpp_stream_error_t *, void *userdata)
{
    ConnectionHandler *worker = (ConnectionHandler *) userdata;
    const char *bound_jid = xmpp_conn_get_bound_jid(c);
    Jid jid(bound_jid);

    switch (status)
    {
        case XMPP_CONN_RAW_CONNECT:
            qDebug() << "XMPP_CONN_RAW_CONNECT";
            break;
        case XMPP_CONN_CONNECT:
            qDebug() << "XMPP_CONN_CONNECT";
            emit worker->connected(worker, jid);
            break;
        case XMPP_CONN_DISCONNECT:
            qDebug() << "XMPP_CONN_DISCONNECT";
            emit worker->disconnected(worker);
            break;
        case XMPP_CONN_FAIL:
            qDebug() << "XMPP_CONN_FAIL";
            break;
        default:
            qDebug() << "Unknown status";
            break;
    }
}

int ConnectionHandler::connect()
{
    return xmpp_connect_client(conn, NULL, 0, this->handler, this);
}
