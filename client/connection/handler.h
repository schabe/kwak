/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HANDLER_H
#define HANDLER_H

#include <QThread>

#include "stanza/bookmarks/inbound/bookmarkretractionnotificationstanza.h"
#include "stanza/disco/inbound/discoitemsrequeststanza.h"
#include "stanza/muc/privatemessagestanza.h"
#include "stanza/muc/subjectchangestanza.h"

/* forward declarations */
class Kwak;


class ConnectionHandler : public QThread, public StanzaReceiver
{
    Q_OBJECT
public:
    ConnectionHandler(xmpp_conn_t*, Kwak*);
    void run();
    void stop();

    void close();

    void setPresence(const QString &);
    void requestRoster(KwakAccount *);
    void requestBookmarks();

    static void handler(xmpp_conn_t *,
                       xmpp_conn_event_t status,
                       int error,
                       xmpp_stream_error_t *stream_error,
                       void *userdata);
    int connect();

    void handle(const BookmarkResponseStanza *);
    void handle(const ChatMessageStanza *);
    void handle(const InboundBookmarkRetractionNotificationStanza *);
    void handle(const InboundDiscoInfoRequestStanza *);
    void handle(const InboundDiscoItemsRequestStanza *);
    void handle(const InboundNewBookmarkNotificationStanza *);
    void handle(const InboundPresenceStanza *);
    void handle(const InboundRosterResultStanza *);
    void handle(const InboundRosterSetStanza *);
    void handle(const InboundSubscriptionRequestStanza *);
    void handle(const MUCChatMessageStanza *);
    void handle(const MUCInviteStanza *);
    void handle(const MUCSubjectChangeStanza *);
    void handle(const PrivateMessageStanza *);

    void send(OutboundStanza &);
signals:
    void connected(ConnectionHandler*, const Jid &);
    void disconnected(ConnectionHandler*);
private:
    void join(const Bookmark &);
    void registerHandlers();
    void deregisterHandlers();
    void setAccountPresence(const QString &);
    void setMUCPresence(const QString &);
    StropheHandler xmpp;
    xmpp_conn_t *conn;
    Kwak *app;
};

#endif // HANDLER_H
