/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONNECTIONSTATE_H
#define CONNECTIONSTATE_H

#include <QObject>
#include "account.h"
#include "xmpp/jid/jid.h"

/* forward declarations */
class ConnectionHandler;
class Kwak;
class OutboundStanza;


class ConnectionState : public QObject
{
    Q_OBJECT
public:
    virtual QString getName() = 0;
    virtual void login(KwakAccount*) = 0;
    virtual void logout() = 0;
    virtual void connected(ConnectionHandler*, const Jid &) = 0;
    virtual void disconnected(ConnectionHandler*) = 0;
    virtual void setPresence(QString) = 0;
    virtual void send(OutboundStanza&) = 0;
protected:
    Kwak* app;
signals:
    void errorOccurred(QString message);
};

#endif // CONNECTIONSTATE_H
