/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KWAK_MESSAGEDB_H
#define KWAK_MESSAGEDB_H

#include "stanza/message/messagestanza.h"
#include "stanza/muc/mucchatmessagestanza.h"
#include "stanza/muc/privatemessagestanza.h"
#include "xmpp/jid/barejid.h"
#include "db/message.h"


class MessageDB
{
public:
    explicit MessageDB();
    ~MessageDB();
    void open(const QString &fileName);
    void addMessage(const ChatMessageStanza &);
    void addMessage(const MessageStanza &);
    void addMessage(const MUCChatMessageStanza &);
    void addMessage(const PrivateMessageStanza &);
    QList<KwakMessage*> getMessagesFrom(const Jid &) const;
    QList<KwakMessage*> getMessagesTo(const Jid &) const;
    QList<KwakMessage*> getMUCMessages(const BareJid &) const;
protected:
    int getVersion();
private:
    void insert(const QString &id, const QString &from, const QString &to,
                const QString &body, const QDateTime &);
    void initialize();
    void upgrade(int from);
};

#endif // KWAK_MESSAGEDB_H
