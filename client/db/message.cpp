/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDateTime>
#include <QStringList>

#include "db/message.h"
#include "stanza/message/chatmessagestanza.h"
#include "xmpp/jid/jid.h"


KwakMessage::KwakMessage(const QString &id, const Jid &from, const Jid &to,
                         const QString &body, const QDateTime &dt)
{
    this->id = QString(id);
    this->from = Jid(from);
    this->to = Jid(to);
    this->text = QString(body);
    this->sent = dt;
}

KwakMessage::KwakMessage(const MessageStanza &msg)
{
    this->from = msg.getFrom();
    this->to = msg.getTo();
    this->text = msg.getBody();
    this->sent = msg.getSent();
    this->id = msg.getId();

    if (!this->sent.isValid()) {
        this->sent = QDateTime::currentDateTime();
    }
}

KwakMessage::KwakMessage(const ChatMessageStanza &msg)
{
    this->from = msg.getFrom();
    this->to = msg.getTo();
    this->text = msg.getBody();
    this->sent = msg.getSent();
    this->id = msg.getId();
}

KwakMessage::KwakMessage(const MUCChatMessageStanza &msg)
{
    /* In MUC meesages the sending jid is room@server/nick but all we really
     * care about is the nick */
    QString from(msg.getFrom());
    QStringList parts = from.split("/");
    if (parts.size() > 1) {
        this->from = Jid(parts[1]);
    }

    this->text = msg.getBody();
    this->to = msg.getTo();
    this->sent = msg.getSent();
    this->id = msg.getId();
}

KwakMessage::KwakMessage(const KwakMessage &msg)
    : QObject()
{
    this->from = msg.from;
    this->to = msg.to;
    this->text = msg.text;
    this->sent = msg.sent;
    this->id = msg.id;
}

KwakMessage::KwakMessage(const PrivateMessageStanza &msg)
    : QObject()
{
    this->from = msg.getFrom();
    this->to = msg.getTo();
    this->text = msg.getBody();
    this->sent = msg.getSent();
    this->id = msg.getId();
}

KwakMessage::~KwakMessage()
{
}

QString KwakMessage::getId() const
{
    return this->id;
}

QString KwakMessage::getText() const
{
    return this->text;
}

Jid KwakMessage::getFrom() const
{
    return Jid(this->from);
}

Jid KwakMessage::getTo() const
{
    return Jid(this->to);
}

QDateTime KwakMessage::getSent() const
{
    return this->sent;
}
