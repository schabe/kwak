/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KWAK_MESSAGE_H
#define KWAK_MESSAGE_H

#include <QObject>

#include "stanza/message/messagestanza.h"
#include "stanza/muc/mucchatmessagestanza.h"
#include "stanza/muc/privatemessagestanza.h"
#include "xmpp/jid/jid.h"


class KwakMessage : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString text READ getText NOTIFY textChanged)
    Q_PROPERTY(QString from READ getFrom NOTIFY fromChanged)
    Q_PROPERTY(QDateTime sent READ getSent NOTIFY sentChanged)

public:
    KwakMessage(const QString &id, const Jid &from, const Jid &to,
                const QString &body, const QDateTime &);
    explicit KwakMessage(const ChatMessageStanza &);
    explicit KwakMessage(const MessageStanza &);
    explicit KwakMessage(const MUCChatMessageStanza &);
    explicit KwakMessage(const KwakMessage &);
    explicit KwakMessage(const PrivateMessageStanza &);
    QString getId() const;
    QString getText() const;
    Jid getFrom() const;
    Jid getTo() const;
    QDateTime getSent() const;
    virtual ~KwakMessage();
private:
    QString id;
    QString text;
    Jid from;
    Jid to;
    QDateTime sent;
signals:
    void textChanged();
    void fromChanged();
    void sentChanged();
};

#endif // KWAK_MESSAGE_H
