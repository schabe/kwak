/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "messagedb.h"

#include <QDateTime>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QStringList>
#include <QVariant>

#include "db/message.h"

#include "stanza/message/chatmessagestanza.h"
#include "stanza/muc/mucchatmessagestanza.h"
#include "xmpp/jid/barejid.h"


MessageDB::MessageDB()
{
}

MessageDB::~MessageDB()
{
    QString name;

    {
        QSqlDatabase db = QSqlDatabase::database();
        name = db.connectionName();

        if (db.isValid() && db.isOpen()) {
            db.close();
        }
    }

    QSqlDatabase::removeDatabase(name);
}

void MessageDB::open(const QString &fileName)
{
    QSqlDatabase db = QSqlDatabase::database();

    if (!db.isValid()) {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        if (!db.isValid()) {
            qWarning() << "Error:" << db.lastError().driverText();
        }
        db.setDatabaseName(fileName);
        if (!db.open()) {
            qWarning() << "Error: Failed to open database " << fileName;
        }
    }

    initialize();
}

void MessageDB::upgrade(int version)
{
    QSqlQuery query;

    switch (version) {
    case -1:
        qDebug() << "Initializing database";

        if (!query.exec("DROP TABLE IF EXISTS messages")) {
            qWarning() << "Failed to drop old database: "
                       << query.lastError().driverText();
        }
        break;
    case 0:
        qDebug() << "Upgrading database from version" << version;

        if (!query.exec("DROP TABLE IF EXISTS messages")) {
            qWarning() << "Failed to drop old database: "
                       << query.lastError().driverText();
        }
    }
}

void MessageDB::initialize()
{
    int version = this->getVersion();

    if (version != 1) {
        this->upgrade(version);
    }

    QSqlQuery query;

    if (!query.exec("CREATE TABLE IF NOT EXISTS messages ("
                     "id TEXT PRIMARY KEY, "
                     "from_jid TEXT, "
                     "to_jid TEXT, "
                     "message TEXT, "
                     "sent INTEGER DEFAULT CURRENT_TIMESTAMP)"
    )) {
        qWarning() << "Failed to create messages table: "
                   << query.lastError().driverText();
    }
    query.finish();

    if (!query.exec("CREATE TABLE IF NOT EXISTS meta ("
                    "key TEXT PRIMARY KEY ON CONFLICT REPLACE, value TEXT)"
    )) {
        qWarning() << "Failed to create meta table: "
                   << query.lastError().driverText();
    }
    query.finish();

    if (!query.exec("INSERT INTO meta (key, value) "
                    "VALUES ('version', '0') "

    )) {
        qWarning() << "Failed to add to meta table: "
                   << query.lastError().driverText();
    }
    query.finish();
}

int MessageDB::getVersion()
{
    QSqlQuery query("SELECT value FROM meta WHERE key='version'");
    int version = -1;

    while (query.next()) {
        version = query.value(0).toInt();
    }

    return version;
}

void MessageDB::insert(const QString &id, const QString &from,
                       const QString &to, const QString &body,
                       const QDateTime &tm)
{
    QSqlQuery query;
    query.prepare("INSERT INTO messages (id, from_jid, to_jid, message, sent) "
                  "VALUES(:id, :from, :to, :message, :tm)");
    query.bindValue(":id", id);
    query.bindValue(":from", from);
    query.bindValue(":to", to);
    query.bindValue(":message", body);
    query.bindValue(":tm", QVariant::fromValue(tm.toMSecsSinceEpoch() / 1000));

    if (!query.exec()) {
        qWarning() << "Failed to save message: "
                   << query.lastError().driverText();
    }
}

void MessageDB::addMessage(const MessageStanza &msg)
{
    QDateTime tm = msg.getSent();
    if (!tm.isValid()) {
        tm = QDateTime::currentDateTime();
    }

    Jid from(msg.getFrom());
    Jid to(msg.getTo());

    this->insert(msg.getId(), BareJid(from), BareJid(to), msg.getBody(), tm);
}

void MessageDB::addMessage(const ChatMessageStanza &msg)
{
    Jid from(msg.getFrom());
    Jid to(msg.getTo());
    QDateTime tm = QDateTime::currentDateTime();

    this->insert(msg.getId(), BareJid(from), BareJid(to), msg.getBody(), tm);
}

void MessageDB::addMessage(const MUCChatMessageStanza &msg)
{
    Jid from(msg.getFrom());
    Jid to(msg.getTo());
    QDateTime tm = msg.getSent();

    this->insert(msg.getId(), from, BareJid(to), msg.getBody(), tm);
}

void MessageDB::addMessage(const PrivateMessageStanza &msg)
{
    Jid from(msg.getFrom());
    Jid to(msg.getTo());
    QDateTime tm = msg.getSent();

    this->insert(msg.getId(), from, to, msg.getBody(), tm);
}

QList<KwakMessage*> MessageDB::getMessagesFrom(const Jid &from) const
{
    QSqlQuery query;
    query.prepare("SELECT id,message,sent,to_jid "
                  "FROM messages WHERE from_jid=:jid");
    query.bindValue(":jid", QString(from));
    QList<KwakMessage*> res;

    if (!query.exec()) {
        qWarning() << "Failed to load messages: "
                   << query.lastError().driverText();
    }

    while (query.next()) {
        QString id = query.value(0).toString();
        QString text = query.value(1).toString();
        quint64 ts = query.value(2).toLongLong();
        Jid to = Jid(query.value(3).toString());
        QDateTime sent;
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
        sent.setTime_t(ts);
#else
        sent.setSecsSinceEpoch(ts);
#endif
        KwakMessage *msg = new KwakMessage(id, Jid(from), to, text, sent);
        res.append(msg);
    }
    return res;
}

QList<KwakMessage*> MessageDB::getMessagesTo(const Jid &to) const
{
    QSqlQuery query;
    query.prepare("SELECT id,message,sent,from_jid "
                  "FROM messages WHERE to_jid=:jid");
    query.bindValue(":jid", QString(to));
    QList<KwakMessage*> res;

    if (!query.exec()) {
        qWarning() << "Failed to load messages: "
                   << query.lastError().driverText();
    }

    while (query.next()) {
        QString id = query.value(0).toString();
        QString text = query.value(1).toString();
        quint64 ts = query.value(2).toLongLong();
        Jid from = Jid(query.value(3).toString());
        QDateTime sent;
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
        sent.setTime_t(ts);
#else
        sent.setSecsSinceEpoch(ts);
#endif
        KwakMessage *msg = new KwakMessage(id, from, Jid(to), text, sent);
        res.append(msg);
    }
    return res;
}

QList<KwakMessage*> MessageDB::getMUCMessages(const BareJid &muc) const
{
    QSqlQuery query;
    query.prepare("SELECT id,message,sent,from_jid,to_jid "
                  "FROM messages "
                  "WHERE from_jid LIKE :jid "
                  "OR to_jid = :bare "
                  "ORDER BY sent ASC");

    query.bindValue(":jid", QString("%1/%%").arg(muc));
    query.bindValue(":bare", QString(muc));

    QList<KwakMessage*> res;

    if (!query.exec()) {
        qWarning() << "Failed to load messages: "
                   << query.lastError().driverText();
    }

    while (query.next()) {
        QString id = query.value(0).toString();
        QString body = query.value(1).toString();
        quint64 ts = query.value(2).toLongLong();
        Jid from = Jid(query.value(3).toString());
        Jid to = Jid(query.value(4).toString());

        QDateTime sent;
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
        sent.setTime_t(ts);
#else
        sent.setSecsSinceEpoch(ts);
#endif
        KwakMessage *msg = new KwakMessage(id, from, to, body, sent);
        res.append(msg);
    }
    return res;
}
