/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>

#include "db/message.h"
#include "db/messagedb.h"
#include "xmpp/definitions.h"
#include "contact.h"


/* RFC 6121: Roster Item ("Contact") */
Contact::Contact(const BareJid &jid, const QString &name, QObject *parent)
    : RosterItem(jid, parent)
{
    this->currentBestJid = Jid(this->getJid());
    this->status = STATUS_UNKNOWN;
    this->setName(name);
}

Contact::~Contact()
{
}

void Contact::setName(const QString &value)
{
    if (value.isEmpty()) {
        BareJid jid(this->getJid());
        name = QString(jid);
    } else {
        name = QString(value);
    }

    emit nameChanged();
}

QString Contact::getName() const
{
    return this->name;
}

void Contact::setStatus(const QString &status, const Jid &from)
{
    this->status = QString(status);

    if (from != this->currentBestJid) {
        /* Unlock on presence from other resource */
        this->unlockJid();
    }

    emit statusChanged();
}

QString Contact::getStatus()
{
    return status;
}

Jid Contact::getToAddress()
{
    return this->currentBestJid;
}

void Contact::lockJid(const Jid &from)
{
    this->currentBestJid = Jid(from);
}

void Contact::unlockJid()
{
    Jid jid(this->getJid());
    this->currentBestJid = jid;
}

bool Contact::sortMessages(const KwakMessage *left, const KwakMessage *right)
{
    return left->getSent() < right->getSent();
}

void Contact::loadMessages(const MessageDB &db)
{
    BareJid bare(this->getJid());
    QList<KwakMessage*> msgs = db.getMessagesFrom(Jid(bare));

    msgs.append(db.getMessagesTo(Jid(bare)));
    std::sort(msgs.begin(), msgs.end(), Contact::sortMessages);

    this->messageLock.lock();
    this->messages.append(msgs);
    this->messageLock.unlock();

    emit messagesChanged();
}

void Contact::receiveMessage(KwakMessage* msg)
{
    /* RFC 6121 Section 5.1: Once the user's client receives a reply from the
     * contact's full JID, it SHOULD address its subsequent messages to the
     * contact's full JID as provided in the 'from' address of the contact's
     * replies, thus "locking in" on that full JID. */
    Jid from = msg->getFrom();

    if (from != this->currentBestJid)
    {
        if (this->currentBestJid.isBare()) {
            /* Lock if currently unlocked */
            this->lockJid(from);
        } else {
            /* Unlock if currently locked */
            this->unlockJid();
        }
    }

    this->messageLock.lock();
    this->messages.append(msg);
    this->messageLock.unlock();

    emit messagesChanged();
}

void Contact::addMessage(KwakMessage* msg)
{
    this->messageLock.lock();
    this->messages.append(msg);
    this->messageLock.unlock();

    emit messagesChanged();
}

void Contact::accept(RosterVisitor *visitor)
{
    visitor->visit(this);
}
