/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>
#include "rosteritem.h"


RosterItem::RosterItem(const BareJid &jid, QObject *parent)
    : QObject(parent)
{
    this->jid = Jid(jid);
}

RosterItem::RosterItem(const Jid &jid, QObject *parent)
    : QObject(parent)
{
    this->jid = Jid(jid);
}

RosterItem::~RosterItem()
{
    this->deleteMessages();
}

Jid RosterItem::getJid() const
{
    return this->jid;
}

QList<KwakMessage*> RosterItem::getMessages()
{
    QList<KwakMessage*> messages;

    this->messageLock.lock();
    messages.append(this->messages);
    this->messageLock.unlock();

    return messages;
}

void RosterItem::deleteMessages()
{
    QList<KwakMessage*>::iterator i;

    this->messageLock.lock();
    while (this->messages.size() > 0) {
        KwakMessage *msg = this->messages.takeFirst();
        delete(msg);
    }
    this->messageLock.unlock();
}
