/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTACT_H
#define CONTACT_H

#include <QObject>

#include "db/message.h"
#include "db/messagedb.h"
#include "roster/rosteritem.h"
#include "roster/rostervisitor.h"
#include "xmpp/jid/barejid.h"


class Contact : public RosterItem
{
    Q_OBJECT
public:
    Contact(const BareJid &, const QString &name, QObject *parent = 0);
    virtual ~Contact();
    void accept(RosterVisitor *);

    /* jid */
    Jid getToAddress();

    /* name */
    QString getName() const;
    void setName(const QString &);

    /* status */
    QString getStatus();
    void setStatus(const QString &status, const Jid &);

    /* messages */
    void loadMessages(const MessageDB &);
    void receiveMessage(KwakMessage *);
    void addMessage(KwakMessage *);
protected:
    Jid currentBestJid;
    QString name;
    QString status;
    void lockJid(const Jid &);
    void unlockJid();
private:
    static bool sortMessages(const KwakMessage *, const KwakMessage *);
signals:
    void nameChanged();
    void statusChanged();
    void messagesChanged();
};

#endif // CONTACT_H
