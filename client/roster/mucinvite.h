/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MUCINVITE_H
#define MUCINVITE_H

#include "roster/invite.h"
#include "stanza/sender.h"


class MUCInvite : public Invite
{
    Q_OBJECT
public:
    explicit MUCInvite(const BareJid &muc, const Jid &from,
                       const QString &password, QObject *parent = 0);
    virtual ~MUCInvite();
    void accept(RosterVisitor *);
    Jid getFrom() const;
    QString getPassword() const;
    virtual void accept(StanzaSender *, QString nick);
    virtual void reject();
private:
    Jid from;
    QString password;
signals:
    void accepted(QString nick);
    void rejected();
};

#endif // MUCINVITE_H
