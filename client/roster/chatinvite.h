/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHATINVITE_H
#define CHATINVITE_H

#include "roster/invite.h"
#include "stanza/sender.h"


class ChatInvite : public Invite
{
    Q_OBJECT
public:
    explicit ChatInvite(const BareJid &, QObject *parent = 0);
    virtual ~ChatInvite();
    void accept(RosterVisitor *);
    virtual void accept(StanzaSender *);
    virtual void reject(StanzaSender *);
signals:
    void accepted();
    void rejected();
};

#endif // CHATINVITE_H
