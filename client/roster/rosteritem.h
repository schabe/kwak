/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ROSTERITEM_H
#define ROSTERITEM_H

#include <QMutex>
#include <QObject>

#include "db/message.h"
#include "xmpp/jid/barejid.h"
#include "rostervisitor.h"


class RosterItem : public QObject
{
public:
    RosterItem(const BareJid &, QObject *parent = 0);
    RosterItem(const Jid &, QObject *parent = 0);
    virtual ~RosterItem();
    virtual void accept(RosterVisitor *) = 0;
    Jid getJid() const;
    QList<KwakMessage*> getMessages();
protected:
    QList<KwakMessage*> messages;
    QMutex messageLock;
    void deleteMessages();
private:
    Jid jid;
};

#endif // ROSTERITEM_H
