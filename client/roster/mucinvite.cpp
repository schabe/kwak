/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stanza/bookmarks/outbound/addbookmarkrequeststanza.h"

#include "mucinvite.h"


MUCInvite::MUCInvite(const BareJid &muc, const Jid &from,
                     const QString &password, QObject *parent)
    : Invite(Jid(muc), parent)
{
    this->from = Jid(from);
    this->password = QString(password);
}

MUCInvite::~MUCInvite()
{
}

void MUCInvite::accept(RosterVisitor *visitor)
{
    visitor->visit(this);
}

Jid MUCInvite::getFrom() const
{
    return this->from;
}

QString MUCInvite::getPassword() const
{
    return this->password;
}

void MUCInvite::accept(StanzaSender *xmpp, QString nick)
{
    GroupChat muc(this->getJid(), nick);
    OutboundAddBookmarkRequestStanza req(muc);
    xmpp->send(req);

    emit accepted(nick);
}

void MUCInvite::reject()
{
    emit rejected();
}
