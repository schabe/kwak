/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ROSTERVISITOR_H
#define ROSTERVISITOR_H

/* forward declarations */
class ChatInvite;
class Contact;
class GroupChat;
class MUCInvite;
class PrivateChat;


class RosterVisitor
{
public:
    virtual void visit(ChatInvite *) = 0;
    virtual void visit(Contact *) = 0;
    virtual void visit(GroupChat *) = 0;
    virtual void visit(MUCInvite *) = 0;
    virtual void visit(PrivateChat *) = 0;
};

#endif // ROSTERVISITOR_H
