/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stanza/subscription/outbound/subscriptionapprovalstanza.h"
#include "stanza/subscription/outbound/subscriptiondeniedstanza.h"
#include "stanza/subscription/outbound/subscriptionrequeststanza.h"
#include "roster/chatinvite.h"


ChatInvite::ChatInvite(const BareJid &jid, QObject *parent)
    : Invite(Jid(jid), parent)
{
}

ChatInvite::~ChatInvite()
{
}

void ChatInvite::accept(RosterVisitor *visitor)
{
    visitor->visit(this);
}

void ChatInvite::accept(StanzaSender *xmpp)
{
    Jid contact = this->getJid();
    SubscriptionApprovalStanza approval(contact);
    SubscriptionRequestStanza sub(contact);

    xmpp->send(approval);
    xmpp->send(sub);

    emit accepted();
}

void ChatInvite::reject(StanzaSender *xmpp)
{
    Jid contact = this->getJid();
    SubscriptionDeniedStanza stanza(contact);
    xmpp->send(stanza);

    emit rejected();
}
