/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "db/messagedb.h"
#include "muc.h"


GroupChat::GroupChat(const BareJid &jid, const QString &nick, QObject *parent)
    : RosterItem(jid, parent)
{
    this->nick = QString(nick);
}

GroupChat::~GroupChat()
{
}

QString GroupChat::getNick() const
{
    return this->nick;
}

void GroupChat::addMessage(KwakMessage* msg)
{
    this->messageLock.lock();
    this->messages.append(msg);
    this->messageLock.unlock();

    emit messagesChanged();
}

void GroupChat::updateMessages(GroupChat *other)
{
    this->deleteMessages();

    QList<KwakMessage*> incoming = other->getMessages();
    QList<KwakMessage*>::iterator i;

    this->messageLock.lock();
    for (i = incoming.begin(); i != incoming.end(); ++i) {
        KwakMessage *msg = *i;
        this->messages.append(new KwakMessage(*msg));
    }
    this->messageLock.unlock();
}

void GroupChat::loadMessages(const MessageDB &db)
{
    BareJid jid(this->getJid());
    QList<KwakMessage*> msgs = db.getMUCMessages(jid);

    this->messageLock.lock();
    this->messages.append(msgs);
    this->messageLock.unlock();

    emit messagesChanged();
}

void GroupChat::accept(RosterVisitor *visitor)
{
    visitor->visit(this);
}

QString GroupChat::getSubject() const
{
    return this->subject;
}

void GroupChat::setSubject(const QString &s)
{
    this->subject = QString(s);
    emit subjectChanged();
}
