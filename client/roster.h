/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KWAK_ROSTER_H
#define KWAK_ROSTER_H

#include <QHash>
#include <QList>
#include <QMutex>
#include <QObject>

#include <strophe.h>

#include "db/messagedb.h"
#include "roster/contact.h"
#include "roster/invite.h"
#include "roster/muc.h"
#include "roster/privatechat.h"
#include "roster/rosteritem.h"
#include "stanza/presence/inbound/inboundpresencestanza.h"
#include "stanza/roster/inbound/rosterstanzaitem.h"
#include "xmpp/jid/barejid.h"


class KwakRoster: public QObject
{
    Q_OBJECT
public:
    ~KwakRoster();

    void update(const InboundRosterResultStanza *, MessageDB &);
    void update(const InboundRosterSetStanza *, MessageDB &);
    void update(const InboundPresenceStanza *);
    Contact *subscribe(const BareJid &);
    Contact *findContact(const BareJid &);
    GroupChat *findMUC(const BareJid &);
    PrivateChat *findPrivateChat(const Jid &);
    QList<RosterItem*> getMembers();
    QList<GroupChat*> getMUCs();
    void add(ChatInvite *);
    void add(GroupChat *);
    void add(MUCInvite *);
    void add(PrivateChat *);
private:
    QList<RosterItem*> members;
    QList<RosterItem*> chats;
    QList<RosterItem*> privateChats;
    QList<RosterItem*> invites;
    QMutex itemsMutex;

    friend class MockRoster;
    Contact *addMember(const BareJid &, const QString &name);
    void updateMember(Contact *, const RosterStanzaItem &);
    MUCInvite *findMUCInvite(const BareJid &);
    QHash<BareJid, QString> statusCache;
    QMutex statusCacheMutex;
    void remove(Invite *);
signals:
    void rosterChanged();
public slots:
    void chatInviteAccepted();
    void mucInviteAccepted(QString nick);
    void inviteRejected();
};

#endif // KWAK_ROSTER_H
