/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "debugnotifier.h"

#include <QDebug>

#include "db/message.h"
#include "roster/chatinvite.h"
#include "roster/contact.h"
#include "roster/muc.h"
#include "roster/mucinvite.h"
#include "roster/privatechat.h"


void DebugNotifier::notify(KwakMessage *, Contact *contact)
{
    qDebug() << "Notification for message from" << contact->getName();
}

void DebugNotifier::notify(KwakMessage *, GroupChat *chat)
{
    qDebug() << "Notification for chat message in" << chat->getJid();
}

void DebugNotifier::notify(KwakMessage *, PrivateChat *chat)
{
    qDebug() << "Notification for private message from" << chat->getJid();
}

void DebugNotifier::notify(ChatInvite *inv)
{
    qDebug() << "Notification for chat invite from" << inv->getJid();
}

void DebugNotifier::notify(MUCInvite *inv)
{
    qDebug() << "Notification for MUC invite to" << inv->getJid();
}

void DebugNotifier::clear()
{
    qDebug() << "Clear all notifications";
}
