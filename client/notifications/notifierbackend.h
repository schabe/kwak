/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOTIFIERBACKEND_H
#define NOTIFIERBACKEND_H

#include "roster/chatinvite.h"
#include "roster/contact.h"
#include "roster/muc.h"
#include "roster/mucinvite.h"
#include "roster/privatechat.h"


class NotifierBackend
{
public:
    virtual ~NotifierBackend() {}
    virtual void notify(KwakMessage *, Contact *) = 0;
    virtual void notify(KwakMessage *, GroupChat *) = 0;
    virtual void notify(KwakMessage *, PrivateChat *) = 0;
    virtual void notify(ChatInvite *) = 0;
    virtual void notify(MUCInvite *) = 0;
    virtual void clear() = 0;
};

#endif // NOTIFIERBACKEND_H
