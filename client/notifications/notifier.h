#ifndef NOTIFIER_H
#define NOTIFIER_H

#include <QObject>

#include <qplatformdefs.h>

#include "db/message.h"
#include "notifications/notifierbackend.h"
#include "roster/contact.h"
#include "roster/privatechat.h"


class Notifier : public QObject
{
    Q_OBJECT
public:
    Notifier();
    virtual ~Notifier();
private:
    NotifierBackend *backend;
public slots:
    void notify(KwakMessage *, Contact *);
    void notify(KwakMessage *, GroupChat *);
    void notify(KwakMessage *, PrivateChat *);
    void notify(ChatInvite *);
    void notify(MUCInvite *);
    void clear();
};

#endif // NOTIFIER_H
