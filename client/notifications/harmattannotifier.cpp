/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QWidget>

#include <MNotification>
#include <MRemoteAction>

#include "harmattannotifier.h"

#include "roster/muc.h"
#include "roster/privatechat.h"


namespace
{
MNotification *createNotification(const QString &identifier,
                                  const QString &summary)
{
    MNotification *n = new MNotification(MNotification::ImReceivedEvent,
                                         summary);
    n->setIdentifier(identifier);
    n->setImage("/usr/share/icons/hicolor/80x80/apps/kwak200.png");
    n->setCount(0);

    return n;
}

MNotification *findNotification(const QString &identifier)
{
    QList<MNotification *> notifications = MNotification::notifications();
    MNotification *notification = NULL;

    QList<MNotification *>::iterator i;
    for (i = notifications.begin(); i != notifications.end(); ++i) {
        if ((*i)->identifier() == identifier) {
            notification = *i;
        } else {
            delete(*i);
        }
    }

    return notification;
}

MNotification *getNotification(Jid jid, QString name)
{
    BareJid bare(jid);
    MNotification *notification = findNotification(bare);

    if (!notification) {
        notification = createNotification(jid, name);
    }

    return notification;
}

MNotification *getNotification(Jid jid)
{
    return getNotification(jid, jid);
}

void bump(MNotification *notification)
{
    int count = notification->count() + 1;
    notification->setCount(count);

    if (count > 1) {
        notification->setBody(QString::number(count) + " messages");
    }

    notification->publish();
}
}


HarmattanNotifier::HarmattanNotifier()
{
    audio.setMedia(QUrl::fromLocalFile("/opt/kwak/sounds/kwak.mp3"));

    vibrate.setIntensity(1.0);
    vibrate.setDuration(250);
}

void HarmattanNotifier::notify(KwakMessage *msg, Contact *contact)
{
    vibrate.start();

    QWidget *w = QApplication::activeWindow();
    if (!w) {
        audio.play();

        MNotification *notification = getNotification(contact->getJid(),
                                                      contact->getName());
        notification->setBody(msg->getText());
        bump(notification);
        delete(notification);
    }
}

void HarmattanNotifier::notify(KwakMessage *msg, GroupChat *chat)
{
    vibrate.start();

    QWidget *w = QApplication::activeWindow();
    if (!w) {
        audio.play();

        MNotification *notification = getNotification(chat->getJid());
        notification->setBody(msg->getText());
        bump(notification);
        delete(notification);
    }
}

void HarmattanNotifier::notify(KwakMessage *msg, PrivateChat *chat)
{
    vibrate.start();

    QWidget *w = QApplication::activeWindow();
    if (!w) {
        audio.play();

        MNotification *notification = getNotification(chat->getJid());
        notification->setBody(msg->getText());
        bump(notification);
        delete(notification);
    }
}

void HarmattanNotifier::notify(ChatInvite *invite)
{
    vibrate.start();

    QWidget *w = QApplication::activeWindow();
    if (!w) {
        audio.play();

        MNotification *notification = getNotification(invite->getJid());
        notification->setBody("Invite to chat");
        bump(notification);
        delete(notification);
    }
}

void HarmattanNotifier::notify(MUCInvite *invite)
{
    vibrate.start();

    QWidget *w = QApplication::activeWindow();
    if (!w) {
        audio.play();

        MNotification *notification = getNotification(invite->getJid());
        notification->setBody("Invite to group chat");
        bump(notification);
        delete(notification);
    }
}

void HarmattanNotifier::clear()
{
    QList<MNotification *> notifications = MNotification::notifications();
    MNotification *notification = NULL;

    QList<MNotification *>::iterator i;
    for (i = notifications.begin(); i != notifications.end(); ++i) {
        notification = *i;
        notification->remove();
    }
}
