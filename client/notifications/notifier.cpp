/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "notifier.h"

#include <QApplication>
#include <QObject>

#include <qplatformdefs.h>

#if defined MEEGO_EDITION_HARMATTAN
#include <meventfeed.h>
#include <MNotification>
#include <MNotificationGroup>
#endif

#include "db/message.h"

#if defined MEEGO_EDITION_HARMATTAN
#include "notifications/harmattannotifier.h"
#else
#include "notifications/debugnotifier.h"
#endif

#include "roster/contact.h"
#include "roster/muc.h"


Notifier::Notifier()
{
#if defined MEEGO_EDITION_HARMATTAN
    this->backend = new HarmattanNotifier();
#else
    this->backend = new DebugNotifier();
#endif
}

Notifier::~Notifier()
{
    delete this->backend;
}

void Notifier::notify(KwakMessage *msg, Contact *contact)
{
    this->backend->notify(msg, contact);
}

void Notifier::notify(KwakMessage *msg, GroupChat *chat)
{
    this->backend->notify(msg, chat);
}

void Notifier::notify(KwakMessage *msg, PrivateChat *chat)
{
    this->backend->notify(msg, chat);
}

void Notifier::notify(ChatInvite *invite)
{
    this->backend->notify(invite);
}

void Notifier::notify(MUCInvite *invite)
{
    this->backend->notify(invite);
}

void Notifier::clear()
{
    this->backend->clear();
}
