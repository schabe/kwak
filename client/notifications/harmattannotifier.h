/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HARMATTANNOTIFIER_H
#define HARMATTANNOTIFIER_H

#include <QMediaPlayer>
#include <QFeedbackHapticsEffect>

#include "notifications/notifierbackend.h"
#include "roster/chatinvite.h"
#include "roster/contact.h"
#include "roster/muc.h"
#include "roster/mucinvite.h"
#include "roster/privatechat.h"


class HarmattanNotifier : public NotifierBackend
{
public:
    HarmattanNotifier();
    void notify(KwakMessage *, Contact *);
    void notify(KwakMessage *, GroupChat *);
    void notify(KwakMessage *, PrivateChat *);
    void notify(ChatInvite *);
    void notify(MUCInvite *);
    void clear();
private:
    QtMobility::QFeedbackHapticsEffect vibrate;
    QMediaPlayer audio;
};

#endif // HARMATTANNOTIFIER_H
