/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "client.h"

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QWidget>

#include "connection/state/disconnected.h"
#include "stanza/bookmarks/outbound/addbookmarkrequeststanza.h"
#include "stanza/message/chatmessagestanza.h"
#include "stanza/muc/mucchatmessagestanza.h"
#include "stanza/roster/inbound/rostersetstanza.h"
#include "stanza/subscription/outbound/subscriptionapprovalstanza.h"
#include "stanza/subscription/outbound/subscriptiondeniedstanza.h"
#include "stanza/subscription/outbound/subscriptionrequeststanza.h"


Kwak::Kwak(KwakRoster &roster, QObject *parent)
    : QObject(parent), roster(roster)
{
    state = NULL;

    this->initDB();
    this->loadAccount();
    this->changeState(new Disconnected(this));
    xmpp_initialize();

    QObject::connect(&roster, SIGNAL(rosterChanged()),
                     this, SIGNAL(rosterChanged()));
    QObject::connect(this, SIGNAL(chatMessageReceived(KwakMessage*,Contact*)),
                     &notifier, SLOT(notify(KwakMessage*,Contact*)));
    QObject::connect(this,
                     SIGNAL(privateMessageReceived(KwakMessage*,PrivateChat*)),
                     &notifier, SLOT(notify(KwakMessage*,PrivateChat*)));
    QObject::connect(this, SIGNAL(mucMessageReceived(KwakMessage*,GroupChat*)),
                     &notifier, SLOT(notify(KwakMessage*,GroupChat*)));
    QObject::connect(this, SIGNAL(chatInviteReceived(ChatInvite*)),
                     &notifier, SLOT(notify(ChatInvite*)));
    QObject::connect(this, SIGNAL(mucInviteReceived(MUCInvite*)),
                     &notifier, SLOT(notify(MUCInvite*)));
    QObject::connect(this, SIGNAL(focusReceived()), &notifier, SLOT(clear()));
}

Kwak::~Kwak()
{
    xmpp_shutdown();
    delete account;
}

void Kwak::initDB()
{
    QCoreApplication *app = QApplication::instance();

    QString path = QString(".config/%1/%2").arg(
        app->organizationName(),
        app->applicationName()
    );

    QDir dir = QDir::home();
    if (!dir.mkpath(path))
    {
        qDebug() << "ERROR: Failed to create folder for database";
    }
    dir.cd(path);

    db.open(dir.filePath("messages.db"));
}

void Kwak::loadAccount()
{
    this->account = new KwakAccount();
    account->load();
    emit accountChanged();
}

void Kwak::connected(ConnectionHandler *worker, const Jid &jid)
{
    this->state->connected(worker, jid);
    this->account->save();
}

void Kwak::connect()
{
    this->state->login(this->account);
}

void Kwak::setPresence(QString a)
{
    if (a == "offline") {
        this->state->logout();
    } else {
        this->state->setPresence(a);
    }
}

void Kwak::disconnected(ConnectionHandler *worker)
{
    qDebug("Kwak::disconnected");
    this->state->disconnected(worker);
}

void Kwak::changeState(ConnectionState *newState)
{
    ConnectionState *oldState = this->state;
    this->state = newState;
    emit statusChanged();

    if (oldState) {
        delete oldState;
    }

    QObject::connect(newState, SIGNAL(errorOccurred(QString)),
                this, SIGNAL(errorOccurred(QString)));
}

QString Kwak::getStatus()
{
    return state->getName();
}

KwakAccount* Kwak::getAccount()
{
    return this->account;
}

void Kwak::receive(const MessageStanza &msg, xmpp_conn_t *c)
{
    db.addMessage(msg);

    BareJid jid(msg.getFrom());
    Contact *contact = roster.findContact(jid);

    if (!contact) {
        /* contact not found -> create it */
        qDebug() << "Message from unknown jid" << jid;
        contact = subscribe(jid, c);
    }

    KwakMessage *message = new KwakMessage(msg);
    contact->receiveMessage(message);
    emit chatMessageReceived(message, contact);
}

void Kwak::receive(const PrivateMessageStanza *msg)
{
    db.addMessage(*msg);

    Jid jid(msg->getFrom());
    PrivateChat *contact = roster.findPrivateChat(jid);

    if (!contact) {
        /* contact not found -> create it */
        qDebug() << "Private message from unknown jid" << jid;
        contact = new PrivateChat(jid);
        roster.add(contact);
    }

    KwakMessage *message = new KwakMessage(*msg);
    contact->addMessage(message);
    emit privateMessageReceived(message, contact);
}

void Kwak::receive(const MUCChatMessageStanza *msg)
{
    db.addMessage(*msg);

    BareJid jid(msg->getFrom());
    GroupChat *muc = roster.findMUC(jid);

    if (muc) {
        /* MUC found -> deliver message */
        KwakMessage *message = new KwakMessage(*msg);
        muc->addMessage(message);
        emit mucMessageReceived(message, muc);
    } else {
        /* MUC not found -> ignore message */
        qWarning() << "Message belongs to unknown chat" << jid;
    }
}

void Kwak::receive(const MUCSubjectChangeStanza *msg)
{
    /* find roster item */
    BareJid jid(msg->getFrom());
    RosterItem *sender = roster.findMUC(jid);

    if (sender) {
        GroupChat *muc = qobject_cast<GroupChat *>(sender);
        if (muc) {
            /* MUC found -> deliver message */
            muc->setSubject(msg->getSubject());
        } else {
            /* roster item found but is no MUC -> ignore message */
            qWarning() << sender->getJid() << "is not a group chat, "
                          << "cannot receive";
        }
    } else {
        /* roster item not found -> ignore message */
        qWarning() << "Subject change for unknown chat" << jid;
    }
}

void Kwak::receive(ChatInvite *invite)
{
    roster.add(invite);
    emit chatInviteReceived(invite);
}

void Kwak::receive(MUCInvite *invite)
{
    roster.add(invite);
    emit mucInviteReceived(invite);
}

Contact* Kwak::subscribe(const BareJid &jid, xmpp_conn_t *c)
{
    Contact *contact = roster.subscribe(jid);
    SubscriptionRequestStanza sub(jid);

    StropheHandler xmpp(c);
    sub.send(xmpp);

    return contact;
}

void Kwak::addMUC(GroupChat *chat)
{
    chat->loadMessages(db);
    roster.add(chat);
}

QList<GroupChat*> Kwak::getMUCs() const
{
    return roster.getMUCs();
}

void Kwak::updateRoster(const InboundPresenceStanza *stanza)
{
    roster.update(stanza);
}

/* Roster Result */
void Kwak::updateRoster(const InboundRosterResultStanza *stanza)
{
    roster.update(stanza, db);
}

/* Roster Push */
void Kwak::updateRoster(const InboundRosterSetStanza *stanza)
{
    Jid jid = stanza->getFrom();

    /* RFC 6121 Section 2.1.6: A receiving client MUST ignore the stanza
     * unless it has no 'from' attribute (..) or it has a 'from' attribute
     * whose value matches the user's bare JID */
    Jid accountJid = Jid(account->getJid());
    if ((jid == Jid("")) || (BareJid(jid) == BareJid(accountJid)))
    {
        roster.update(stanza, db);
    }
}

void Kwak::send(OutboundAddBookmarkRequestStanza &stanza)
{
    state->send(stanza);
}

void Kwak::send(Contact *contact, QString msg)
{
    Jid from(this->account->getJid());
    Jid to(contact->getToAddress());
    ChatMessageStanza stanza(from, to, msg);
    state->send(stanza);
    db.addMessage(stanza);
    contact->addMessage(new KwakMessage(stanza));
}

void Kwak::send(MUCChatMessageStanza &msg)
{
    state->send(msg);
    db.addMessage(msg);
}

void Kwak::send(PrivateChat &chat, PrivateMessageStanza &msg)
{
    state->send(msg);
    chat.addMessage(new KwakMessage(msg));
    db.addMessage(msg);
}

void Kwak::send(SubscriptionApprovalStanza &stanza)
{
    state->send(stanza);
}

void Kwak::send(SubscriptionDeniedStanza &stanza)
{
    state->send(stanza);
}

void Kwak::send(SubscriptionRequestStanza &stanza)
{
    state->send(stanza);
}

void Kwak::exitHandler()
{
    qDebug() << "Shutting down";
    this->state->logout();
}

void Kwak::onFocusChanged(QWidget *, QWidget *focused)
{
    if (focused) { /* can be NULL */
        emit focusReceived();
    }
}

bool Kwak::isMUC(const BareJid &jid) const
{
    GroupChat *chat = this->roster.findMUC(jid);
    return chat != NULL;
}
