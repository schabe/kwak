/*
 * This file is part of the Kwak XMPP client
 * Copyright (C) 2024  Stefan Ott
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KWAK_ACCOUNT_H
#define KWAK_ACCOUNT_H

#include <QObject>
#include <QSettings>

#include "xmpp/jid/jid.h"


class KwakAccount : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString jid READ getJid
            WRITE setJid NOTIFY jidChanged)
    Q_PROPERTY(QString password READ getPassword
            WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString status READ getStatus
            NOTIFY statusChanged)
public:
    KwakAccount();
    QString getJid() const;
    QString getPassword() const;
    QString getStatus() const;
    QString getResourcePart() const;
    void setJid(const QString&);
    void setPassword(const QString&);
    void setStatus(const QString&);
    void update(const Jid&);
    void load();
    void save();
private:
    QString jid; // TODO: this should be a Jid object
    QString resourcepart; // TODO: this should be part of the jid object
    QString password;
    QString status;
signals:
    void jidChanged();
    void passwordChanged();
    void statusChanged();
};

#endif // KWAK_ACCOUNT_H
