#!/bin/sh
#
# Build static libstrophe for Meego Harmattan

# Note: You will need to have libexpat1-dev installed on your system

set -e
set -u

if [ $# -ne 3 ]
then
    echo "Syntax: %0 <libstrophe_dir> <openssl_dir> <harmattan_sdk_dir>"
    exit 1
fi

set -x

LIBSTROPHE_DIR=${1}
OPENSSL_DIR=${2}
HARMATTAN_SDK=${3}

JOBS=$( grep -c ^processor /proc/cpuinfo )

build_src()
{
    local njobs=${1}

    #./configure --build='' --host=arm-none-linux-gnueabi
    ./configure
    make -j ${njobs}
}

install_src()
{
    local target=${1}

    cp .libs/libstrophe.a ${target}/lib/simulator/
    mkdir -p ${target}/include/
    cp strophe.h ${target}/include/
}

parent_dir()
{
    local self=${1}
    local mydir=$( dirname ${self} )
    readlink -f ${mydir}/..
}

export CFLAGS="-static -I${OPENSSL_DIR}/include -I/lib/i386-linux-gnu/"
export PATH=${HARMATTAN_SDK}/Madde/toolchains/arm-2009q3-67-arm-none-linux-gnueabi-i686-pc-linux-gnu/arm-2009q3-67/bin:$PATH
export LDFLAGS="-L${OPENSSL_DIR} -l:libcrypto.a -l:libssl.a"

self=$( readlink -f ${0} )
MYDIR=$( parent_dir ${self} )

(
    cd ${LIBSTROPHE_DIR}
    build_src ${JOBS}
    install_src ${MYDIR}
)
