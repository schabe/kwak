#!/bin/sh
#
# Build static openssl library for Meego Harmattan

set -e
set -u

if [ $# -ne 2 ]
then
    echo "Syntax: %0 <openssl_dir> <harmattan_sdk_dir>"
    exit 1
fi

set -x

OPENSSL_DIR=${1}
HARMATTAN_SDK=${2}

JOBS=$( grep -c ^processor /proc/cpuinfo )

inherits_from()
{
    local arch=${1}

    grep -B 1 asm_arch.*${arch} ${OPENSSL_DIR}/Configurations/10-main.conf \
        | head -n 1 \
        | cut -d '"' -f 2
}

patch_src()
{
    local dir=${1}
    local arch=$( inherits_from armv4 )

    case ${arch} in
        linux-generic32)
            # patch has been applied
            ;;
        linux-latomic)
            patch --forward -p1 < ${dir}/patches/openssl-no-atomic-for-armv4.patch
            ;;
        *)
            echo "Wrong value in 10-main.conf: ${arch}"
            echo "Aborting"
            exit 2
    esac

    if ! grep -q X509_NAME_hash_old ${OPENSSL_DIR}/crypto/x509/by_dir.c
    then
        patch --forward -p1 < ${dir}/patches/openssl-use-old-hash.patch
    fi
}

build_src()
{
    local njobs=${1}

    ./Configure linux-armv4 -march=armv7-a -static no-dso --openssldir=/etc/ssl
    make -j ${njobs}
}

install_src()
{
    local target=${1}

    cp libcrypto.a ${target}
    cp libssl.a ${target}
}

parent_dir()
{
    local self=${1}
    local mydir=$( dirname ${self} )
    readlink -f ${mydir}/..
}

export ARCH=armel
export CROSS_COMPILE=${HARMATTAN_SDK}/Madde/toolchains/arm-2009q3-67-arm-none-linux-gnueabi-i686-pc-linux-gnu/arm-2009q3-67/bin/arm-none-linux-gnueabi-
export CFLAGS="--sysroot=${HARMATTAN_SDK}/Madde/sysroots/harmattan_sysroot_10.2011.34-1_slim -fPIC"
export PATH=${HARMATTAN_SDK}/Madde/toolchains/arm-2009q3-67-arm-none-linux-gnueabi-i686-pc-linux-gnu/arm-2009q3-67/bin:$PATH

self=$( readlink -f ${0} )
MYDIR=$( parent_dir ${self} )

(
    cd ${OPENSSL_DIR}
    patch_src $( dirname ${self} )
    build_src ${JOBS}
    install_src ${MYDIR}/lib/harmattan/
)
