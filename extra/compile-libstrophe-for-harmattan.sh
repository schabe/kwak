#!/bin/sh
#
# Build static libstrophe for Meego Harmattan

set -e
set -u

if [ $# -ne 3 ]
then
    echo "Syntax: %0 <libstrophe_dir> <openssl_dir> <harmattan_sdk_dir>"
    exit 1
fi

if [ -z "$( which pkg-config )" ] ; then
    echo "You need to have pkg-config installed"
    exit 1
fi

set -x

LIBSTROPHE_DIR=${1}
OPENSSL_DIR=${2}
HARMATTAN_SDK=${3}

JOBS=$( grep -c ^processor /proc/cpuinfo )

build_src()
{
    local njobs=${1}

    export PKG_CONFIG="pkg-config"
    export PKG_CONFIG_PATH=${HARMATTAN_SDK}/Madde/sysroots/harmattan_sysroot_10.2011.34-1_slim/usr/lib/pkgconfig
    ./configure --build='' --host=arm-none-linux-gnueabi --enable-cares
    make -j ${njobs}
}

install_src()
{
    local target=${1}

    cp .libs/libstrophe.a ${target}/lib/harmattan/
    mkdir -p ${target}/include/
    cp strophe.h ${target}/include/
}

parent_dir()
{
    local self=${1}
    local mydir=$( dirname ${self} )
    readlink -f ${mydir}/..
}

export ARCH=armel
export CROSS_COMPILE=${HARMATTAN_SDK}/Madde/toolchains/arm-2009q3-67-arm-none-linux-gnueabi-i686-pc-linux-gnu/arm-2009q3-67/bin/arm-none-linux-gnueabi-
export CFLAGS="--sysroot=${HARMATTAN_SDK}/Madde/sysroots/harmattan_sysroot_10.2011.34-1_slim -D__LITTLE_ENDIAN__ -fPIC -static -I${OPENSSL_DIR}/include"
export PATH=${HARMATTAN_SDK}/Madde/toolchains/arm-2009q3-67-arm-none-linux-gnueabi-i686-pc-linux-gnu/arm-2009q3-67/bin:$PATH
export LDFLAGS="-L${OPENSSL_DIR} -L${HARMATTAN_SDK}/Madde/toolchains/arm-2009q3-67-arm-none-linux-gnueabi-i686-pc-linux-gnu/arm-2009q3-67/usr/lib -l:libcrypto.a -l:libssl.a"

self=$( readlink -f ${0} )
MYDIR=$( parent_dir ${self} )

(
    cd ${LIBSTROPHE_DIR}
    build_src ${JOBS}
    install_src ${MYDIR}
)
